import networkx as nx
import gzip
import random
from collections import defaultdict
import numpy as np
import sys
import os

if len(sys.argv) != 3:
    print 'Incorrect number of arguments. Usage:'
    print '\t{} <dataset name> <output path>'.format(sys.argv[0])
    sys.exit(1)

dataset = str(sys.argv[1])
outpath = str(sys.argv[2])
resultsdir = '{}/{}/'.format(outpath,dataset)

if dataset == 'lj':
    attrib_file='data/datasets/lj/com-lj.top5000.cmty.txt.gz'
    graph_file='data/datasets/lj/com-lj.ungraph.txt.gz'
    accept_trait=4915
    budget=1250

#elif dataset == 'amazon':
#    attrib_file='com-amazon.top5000.cmty.txt.gz'
#    graph_file='com-amazon.ungraph.txt.gz'
#    accept_trait=4832
#    budget=300

elif dataset == 'dblp':
    attrib_file='data/datasets/dblp/com-dblp.top5000.cmty.txt.gz'
    graph_file='data/datasets/dblp/com-dblp.ungraph.txt.gz'
    accept_trait=4972
    budget=1250

elif dataset == 'citeseer':
    attrib_file='data/datasets/gccs/citeseer/labeled/citeseer.att.txt.gz'
    graph_file='data/datasets/gccs/citeseer/labeled/citeseer.gcc.txt.gz'
    accept_trait=2
    budget=1500


elif dataset == 'wikipedia':
    attrib_file='data/datasets/wang2013/wikipedia/done/wikipedia.attribs.txt.gz'
    graph_file='data/datasets/wang2013/wikipedia/done/wikipedia.ungraph.txt.gz'
    accept_trait=48
    budget=1000

elif dataset == 'dbpedia':
    attrib_file='data/datasets/wang2013/dbpedia/done/dbpedia.attribs.txt.gz'
    graph_file='data/datasets/wang2013/dbpedia/done/dbpedia.ungraph.txt.gz'
    accept_trait=0
    budget=1000

elif dataset == 'kickstarter':
    attrib_file='data/datasets/kickstarter/kickstarter.attribs.txt.gz'
    graph_file='data/datasets/kickstarter/kickstarter.ungraph.txt.gz'
    accept_trait=180
    budget=1500

elif dataset == 'donors':
    attrib_file='data/datasets/gccs/donors/donors.att.txt.gz'
    graph_file='data/datasets/gccs/donors/donors.gcc.txt.gz'
    accept_trait=284
    budget=300

else:
    print 'Dataset',dataset,'not found.'
    sys.exit(2)

###################
RUNS=80
Cp = np.sqrt(2)
M = 1

class Inc(object):
    def __init__(self):
        self.i = 0
        self.t = 0.0
    def __iadd__(self, f):
        self.i += 1
        self.t += f
        return self
    def get(self):
        return self.t/max(1,self.i)

def evalTuple(t, n):
    if t[2] == 0:
        return np.inf
    else:
        return t[0]+Cp*np.sqrt(np.log(n-t[1])/t[2])

def VUCB1(d, n):
    return max(d.iteritems(), key=lambda x: evalTuple(x[1],n))[0]

def getSeed(G, seeds, visible, equiv_classes, h, bandit_data, t, recruited):
    #node = random.choice(list(seeds))
    node = seeds[0]
    recruited.append(node)
    visible[node]=True
    for neigh in G[node]:
        G.node[neigh]['sees'] = set([node])

    key = frozenset([node])
    equiv_classes[key] = set(G.neighbors(node))
    h[key] = 0.0
    bandit_data[key] = (h[key],t,M)



# read graph
G = nx.read_edgelist(graph_file)

# read all targets
with gzip.open(attrib_file,'r') as f:
    for idx,line in enumerate(f):
        if idx == accept_trait:
            targets = frozenset([x for x in line.split()])

# read pre-defined list of seeds from file
with open('seeds/{}.txt'.format(dataset) ) as f:
    seeds = f.read().splitlines()

if RUNS > len(seeds):
    RUNS = len(seeds)
    print 'Number of runs > number of seeds. RUNS was set to {}'.format(RUNS)


for rdx in xrange(RUNS):
    # initialize variables
    visible = defaultdict(bool)
    equiv_classes = defaultdict(set)  # dict[common neighbors] = set of nodes
    bandit_data = dict()  # dict[equivalence class] = (performance, creation time, recruited nodes)
    pf = defaultdict(Inc) # promising factor
    h = dict()

    # keep track of equivalence sets and MABs
    rewards = []
    recruited = []
    for t in xrange(1,budget+1):
        #print '\n\n---> (t={})'.format(t), bandit_data
        #find seed
        while not bandit_data:
            if seeds[rdx] not in visible.keys():
                getSeed(G, [seeds[rdx]], visible, equiv_classes, h, bandit_data, t-1, recruited)
            else:
                print 'ERROR: exhausted border set, no more seeds available.'
                sys.exit(1)
            #getSeed(G, seeds-set(visible.keys()), visible, equiv_classes, h, bandit_data, t-1, recruited)

        #find best equivalence class
        key = VUCB1(bandit_data, t)

        #choose random node, mark as visible, get reward, update pf
        node = random.choice(list(equiv_classes[key]))
        visible[node] = True
        rwd = 1.0*(node in targets)
        recruited.append(node)
        rewards.append(rwd)
        for neigh in G.node[node]['sees']:
            pf[neigh] += rwd
        #print '---> (t={}) Key:{}\n\tNode:{} Neighbors:{}'.format(t,key,node,G.neighbors(node))
        #print '\tReward:', rwd

        #node is no longer part of the equivalence class
        equiv_classes[key].remove(node)
        if len(equiv_classes[key]) == 0:
            del equiv_classes[key]
            del bandit_data[key]

        #update all neighbors of node
        to_be_updated = [neigh for neigh in G[node] if not visible[neigh]]
        for neigh in to_be_updated:
            if 'sees' in G.node[neigh]:
                old_key = frozenset(G.node[neigh]['sees'])
                equiv_classes[old_key].remove(neigh)
                old_perf = bandit_data[old_key][0]
                G.node[neigh]['sees'].add(node)
                new_key = frozenset(G.node[neigh]['sees'])
                if not equiv_classes[new_key]:
                    equiv_classes[new_key] = set([neigh])
                    h[new_key] = 1.0-np.prod([1-pf[idx].get() for idx in new_key])
                    #P = float(len(old_key))/len(new_key)                       # proportion of frontier nodes
                    P = float(len(old_key & targets))/max(1,len(new_key & targets)) # proportion of frontier leads
                    bandit_data[new_key] = ( P*(bandit_data[old_key][0]-h[new_key])+h[new_key],t,M)
                else:
                    equiv_classes[new_key].add(neigh)

                if not equiv_classes[old_key]:
                    del equiv_classes[old_key]
                    del bandit_data[old_key]
            else:
                G.node[neigh]['sees'] = set([node])
                new_key = frozenset([node])
                if not equiv_classes[new_key]:
                    equiv_classes[new_key] = set([neigh])
                    h[new_key] = pf[node].get()
                    bandit_data[new_key] = (h[new_key],t,M)
                else:
                    equiv_classes[new_key].add(neigh)

        #update MAB observations
        if equiv_classes[key]:
            n = bandit_data[key][2]+1
            bandit_data[key] = (bandit_data[key][0]+(rwd-bandit_data[key][0])/n, bandit_data[key][1], n)

    #save output
    if not os.path.exists(resultsdir):
        os.makedirs(resultsdir)

    basename='{}/SN_UCB1_r{:03d}'.format(resultsdir,rdx)
    for suf,vec in [('.lab',recruited),('.txt',rewards)]:
        with open(basename+suf,'w') as f:
            f.write(' '.join([str(i) for i in vec]))

    #clear attributes
    for n,d in G.node.items():
        if 'sees' in d:
            del G.node[n]['sees']
