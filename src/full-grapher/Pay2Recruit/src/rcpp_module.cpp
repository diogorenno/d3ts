// -*- mode: C++; c-indent-level: 4; c-basic-offset: 4; indent-tabs-mode: nil; -*-
//
// rcpp_module.cpp: Rcpp R/C++ interface class library -- Rcpp Module examples
//
// Copyright (C) 2010 - 2012  Dirk Eddelbuettel and Romain Francois
//
// This file is part of Rcpp.
//
// Rcpp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// Rcpp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rcpp.  If not, see <http://www.gnu.org/licenses/>.

#include <Rcpp.h>
#include <RcppEigen.h>
#include <stdio.h>
#include <unordered_set>
#include <fstream>
#include <queue>
#include <algorithm>
#include <map>
#define MAX( a, b ) ( (a) > (b) ? (a) : (b) )
#define MIN( a, b ) ( (a) < (b) ? (a) : (b) )

#define DEBUG 0
#define INCLUDE_RW 1
using namespace Rcpp;

typedef Eigen::SparseMatrix<double> SpMat; 


class Graph {
public:
    Graph( bool undirected_ ) : undirected(undirected_), totalE(0), label_max(1), target_label(1) { srand48(0); }
    ~Graph() {}
    void addEdge( int u, int v );
    int getTargetSize() { return this->target_nodes.size(); }
    int getN() { return this->out.size(); } // FIXME
    int getE() { return this->totalE; }
    int                 getRandomNode();

    std::vector<int>    getNeighbors( int i ) { std::vector<int> ret(out[i].begin(),out[i].end()); return ret; }
    std::vector<int>    getInNeighbors( int i ) { std::vector<int> ret(in[i].begin(),in[i].end()); return ret; }
    int                 getLabel( int i ) { return label[i]; }
    IntegerVector       getAttributes( int i ) { return wrap(attribs[i]); }

    List                getInfo( int i ) {
        if( share_neighs[i] )
            return List::create(Named("accept")     = (int)accept[i],
                                Named("neighbors")  = wrap(out[i]),
                                Named("label")      = (int)label[i],
                                Named("attributes") = wrap(attribs[i]) );
        else
            return List::create(Named("accept")     = (int)accept[i],
                                Named("label")      = (int)label[i],
                                Named("attributes") = wrap(attribs[i]) );
    }

    //std::vector<double> getAttributes( int i ) { return attribs[i]; }
    std::vector<std::string> getFeatureNames() { return this->feat_names; }
    std::vector<bool> isFeatureFraction() { return this->feat_isFraction; }

    //void initFeatures( int ntypes, int target, std::vector<int> att_max );
    void initFeatures( int ntypes, int target, int nattribs );

    void readGraph( std::string filename );
    void readCommunities3( std::string filename, int accept_trait, int target_trait,
            double p_accept_one=1.0, double p_accept_zero=0.0,
            double p_neighs_accept=1.0, double p_neighs_reject=0.0,
            std::string amount_file = std::string () );
    void readCommunities2( std::string filename, int accept_trait,
            double p_accept_one=1.0, double p_accept_zero=0.0, double p_neighs_accept=1.0, double p_neighs_reject=0.0 ) {
        readCommunities3( filename, accept_trait, accept_trait, p_accept_one, p_accept_zero, p_neighs_accept, p_neighs_reject );
    }
    std::vector<int>getTargetNodes( int nsamples );
    void writeAttributesToFile( std::string filename );
    
    // (renno)
    SpMat computeFeaturesForAll(int verbose);

private:
    
    // (renno)
    bool computeFeaturesVerbose;
  
    // TODO: make vertex a class
    std::vector<std::unordered_set<int> > out;
    std::vector<std::unordered_set<int> > in;
    std::vector<char> valid;
    std::vector<int> label;
    std::vector<bool> accept;
    std::vector<bool> share_neighs;
    std::vector<std::vector<int> > attribs; // TODO: make this of type double

    int label_max, target_label;
    std::vector<std::string> feat_names;
    std::vector<bool> feat_isFraction;
    // TODO: map ids to save space
    //std::map<int,int> old2new;
    //std::map<int,int> new2old;
    int totalE;
    int nattribs;
    int id_offset, max_id;
    bool undirected;
    std::vector<int> target_nodes;

    void computeFeatures( std::unordered_set<int> v, NumericMatrix &feats );
    void computeFeaturesSparse( std::unordered_set<int> v, SpMat &feats );
};
RCPP_EXPOSED_CLASS(Graph);

std::vector<int> Graph::getTargetNodes( int nsamples ) {
    if( nsamples > target_nodes.size() ) {
        char err_msg[100];
        sprintf( err_msg, "Graph::getTargetNodes() -- nsamples > target_nodes.size() (%d > %lu)\n", nsamples, target_nodes.size()  );
        throw std::runtime_error( err_msg );
    }
    std::vector<int> ret( target_nodes.begin(), target_nodes.end() );
    std::random_shuffle(ret.begin(),ret.end());
    ret.resize(nsamples);

    return ret;
}


int Graph::getRandomNode() {
    int id = (int)(drand48()*out.size());
    while( !valid[id] )
        id = (int)(drand48()*out.size());
    return id;
}

class FileHandler {
    public:
        bool use_gzip;
        FILE *f;

        FILE * open( std::string filename );
        void close();
};

FILE * FileHandler::open( std::string filename ) {
    use_gzip = filename.substr(filename.size()-3,3) == ".gz";
    if( use_gzip ) {
        char st[1000];
        sprintf( st, "gunzip -c %s", filename.c_str() ); //FIXME: use zcat instead
        f = popen(st, "r");
    } else {
        f = fopen(filename.c_str(), "r");
    }

    return f;
}

void FileHandler::close( void ) {
    if( use_gzip ) pclose(f);
    else fclose(f);
}

//TODO: modify the code to skip comment lines
void Graph::readGraph( std::string filename ) {
    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t min_id = 1000;
    max_id = -1;
    char line[1000];
    while( fgets(line, sizeof(line), f) != NULL ) {
        if( line[0] != '#' ) {
            int32_t i, j;
            if( sscanf(line, "%d %d", &i, &j) ==  2 ) {
                if(i != j) {
                    min_id = MIN(min_id,MIN(i,j));
                    max_id = MAX(max_id,MAX(i,j));
                }
            }
        }
    }
    fh->close();

    id_offset = min_id-1;
    max_id -= id_offset;
    out.resize(max_id+1);
    in.resize(max_id+1);
    //attribs.resize(max_id+1);
    attribs.resize(max_id+1);
    label.resize(max_id+1,0);
    valid.resize(max_id+1,false);
    accept.resize(max_id+1,false);
    share_neighs.resize(max_id+1,false);

    std::cout << "id_offset " << id_offset << ", max_id " << max_id << std::endl;

    totalE = 0;
    f = fh->open( filename );
    fprintf( stdout, "Processing edges...\n" );
    while( fgets(line, sizeof(line), f) != NULL ) {
        if( line[0] != '#' ) {
            int32_t i, j;
            if( sscanf(line, "%d %d", &i, &j) ==  2 ) {
                if(i != j) {
                    i -= id_offset;
                    j -= id_offset;
                    out[i].insert(j);
                    in[j].insert(i);
                    totalE++;
                    if( undirected ) {
                        out[j].insert(i);
                        in[i].insert(j);
                    }
                    valid[i] = 1;
                    valid[j] = 1;

                    if( totalE%1000000 == 0 )
                        fprintf( stdout, "Processed %d edges.\n", totalE );
                }
            }
        }
    }
    fprintf( stdout, "Processed %d edges.\n", totalE );
    fh->close();
    delete fh;
}

void Graph::readCommunities3( std::string filename, int accept_trait, int target_trait,
        double p_accept_one, double p_accept_zero, double p_neighs_accept, double p_neighs_reject,
        std::string amount_file ) {

    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t community = 0, id;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, f)) != -1) {
    
        const char *sep = " \t,";
        char *id_str, *last;

        if( line[0] == '#' ) // skip headers
            continue;

        for( id_str = strtok_r(line, sep, &last);
             id_str;
             id_str = strtok_r(NULL, sep, &last) ) {
            id = atoi(id_str);
            if(community == accept_trait) {
                accept[id-id_offset] = true; // att corresponding to label is not included, on purpose
            }
            if( community == target_trait ) {
                label[id-id_offset] = 1;
                target_nodes.push_back(id-id_offset);
            }
            if ((community != accept_trait) and (community != target_trait)) {  // (renno)
                attribs[id-id_offset].push_back(community);
            }
        }
        community++;
    }
    free(line);
    fh->close();

    // if probabilistic, flip coins to determine acceptance
    if( p_accept_one < 1 || p_accept_zero > 0 ) {
        double prob[] = { p_accept_zero, p_accept_one };
        for( int i = 0; i < label.size(); i++ )
            if( valid[i] ) {
                accept[i] = ( drand48() <= prob[accept[i]] );
                if( accept_trait == target_trait ) { // objective function is max accepts, so accept <==> label
                    label[i] = 1*accept[i];
                }
            }
    }

    // if probabilistic, flip coins to determine whether node shares neighbors
    if( p_neighs_accept < 1 || p_neighs_reject < 1 ) {
        double prob[] = { p_neighs_reject, p_neighs_accept };
        for( int i = 0; i < label.size(); i++ )
            if( valid[i] )
                share_neighs[i] = ( drand48() < prob[accept[i]] );
    } else {
        for( int i = 0; i < label.size(); i++ )
            if( valid[i] )
                share_neighs[i] = true;
    }

    if( amount_file.empty() ) {
      fprintf( stderr, "No amount file provided.\n");
    } else {
      fprintf( stderr, "Amount file %s\n", amount_file.c_str() );
      FileHandler *fh = new FileHandler();
      FILE *f = fh->open( amount_file );
      int id;
      float amount;
      while( fscanf( f, " %d %f", &id, &amount ) == 2 ) {
        label[id-id_offset] = (int)amount;
      }

      fh->close();
      delete fh;
    }

    //fprintf( stdout, "Largest community: %d (%d members)\n", largest_community, max_k );

    //while(!feof(f)) {
    //    fprintf( stdout, "Community %d:", community );
    //    while( fscanf( f, "%d[^\n]", &id ) == 1 ) {
    //        fprintf( stdout, " %d", id);
    //        attribs[id-id_offset][community] = 1;

    //        if(community == target)
    //            label[id-id_offset]++;
    //    }
    //    fprintf( stdout, "\n");
    //    community++;
    //}
    delete fh;

}

void Graph::writeAttributesToFile( std::string filename ) {
    std::ofstream outfile;
    outfile.open(filename );
    for( int i = 0; i < out.size(); i++ ) {
        outfile << attribs[i][0];
        for( int j = 1; j < nattribs; j++ ) {
            outfile << "\t" << attribs[i][j];
        }
        if( i < out.size()-1 )
            outfile << "\n";
    }
    outfile.close();
}



void Graph::computeFeaturesSparse( std::unordered_set<int> toBeComputed, SpMat &M ) { //colMajor version
    std::vector<int> entries;
    for( std::unordered_set<int>::iterator v=toBeComputed.begin(); v != toBeComputed.end(); v++ ) {
            int count = 12;
            for( std::unordered_set<int>::iterator u = in[*v].begin(); u != in[*v].end(); u++) {
                count += attribs[*u].size();
            }
            entries.push_back(count);
    }
    if(entries.empty()) return;
    else M.reserve(entries);

    unsigned int counter = 0;
    int v_ind=0;
    for( std::unordered_set<int>::iterator it=toBeComputed.begin();
            it != toBeComputed.end(); it++ ) {
        int v = *it;
      
        // (renno)
        counter++;
        if (this->computeFeaturesVerbose and (counter % 10 == 0)) {
          fprintf(stdout, "Computing features: node %d\r", counter);
        }
      
        //double *x = new double[feat_names.size()]();
        double eps = 1e-10, epsilon;
        std::unordered_set<int> observed_neighs = in[v]; //construct set
        for( std::unordered_set<int>::iterator u = out[v].begin(); u != out[v].end(); u++)
            if( valid[*u] )
                observed_neighs.insert(*u);

        if( DEBUG>1 )
            fprintf( stderr, "Computing features for node %d\n", v);


        int feat_ind = 0;
        //printf("M[%d,%d] = %d\n", feat_ind,v_ind,v);
        
        M.insert(feat_ind++, v_ind) = v;
        M.insert(feat_ind++, v_ind) = label[v];
        
        for( std::vector<std::string>::iterator it = feat_names.begin();
                it != feat_names.end(); it++ ) {

            // read and parse feat_name
            std::istringstream f(*it);
            std::string x1, x2, x3, x4;
            std::getline( f, x1, ',' );
            std::getline( f, x2, ',' );
            if( x2.compare(0,3,"att") != 0 ) // not for attributes
                std::getline( f, x3, ',' );
            if( x2.compare(0,3,"tri") == 0 || x2.compare("rw") == 0 ) // only for triangles, tri-fraction and rw
                std::getline( f, x4, ',' );
                
            if( x2.compare("counts") == 0 && x3.compare("0") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "counts\n");
                // counts
                int counts[] = {0,0};
                for( std::unordered_set<int>::iterator u = observed_neighs.begin(); u != observed_neighs.end(); u++ ) {
                    counts[label[*u]>0]++;
                    //fprintf( stderr, "feat_ind %d label %d\n", feat_ind, label[*u]);
                }
                for( int val = 0; val <= label_max; val++ )
                    M.insert(feat_ind+val,v_ind) = counts[val];
                
                // counts-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ ) {
                    //fprintf( stderr, "feat_ind %d val %d\n", feat_ind, val);
                    M.insert(feat_ind+(label_max+1)+val,v_ind ) = (counts[val]+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
                }
            } else if( x2.compare("triangles") == 0 && x3[0] == '~' && x4[0] == '~' && x1.compare("y") == 0 ) {
                //fprintf( stderr, "triangles\n");
                // triangles
                int tmp = 0;
                int counts[] = {0,0,0};
                if( observed_neighs.size() > 0 )
                    for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                        for( std::unordered_set<int>::iterator b = a; b != observed_neighs.end(); b++ ) {
                            if( out[*a].find(*b) != out[*a].end() ||
                                out[*b].find(*a) != out[*b].end() ) {
                                int pos = ((label[*a]>0) == target_label ) + ((label[*b]>0) == target_label );
                                counts[pos]++;
                                tmp++;
                            }
                        }
                    }
                for( int val = 0; val <= 2; val++ )
                    M.insert(feat_ind+val,v_ind) = counts[val];
                // tri-fraction
                epsilon = tmp > 0 ? 0.0 : eps;
                for( int val = 0; val < 2; val++ ) {
                    //fprintf( stderr, "Setting pos %d to %lf/%lf = %lf\n", feat_ind+3+val,
                    //        (M(v_ind,feat_ind+val)+epsilon),(tmp+3*epsilon),
                    //        (M(v_ind,feat_ind+val)+epsilon)/(tmp+3*epsilon) );
                    M.insert(feat_ind+3+val,v_ind) = (counts[val]+epsilon)/(tmp+3*epsilon);
                }
            } else if( x1.compare("yi") == 0 && x2.compare("most") == 0 && x3.compare("0") == 0 ) {
                int counts = 0;
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    int balance = 0;

                    std::unordered_set<int> observed_neighs_a = in[*a]; //construct set
                    for( std::unordered_set<int>::iterator it = out[*a].begin(); it != out[*a].end(); it++)
                        if( valid[*it] )
                            observed_neighs_a.insert(*it);

                    for( std::unordered_set<int>::iterator b = observed_neighs_a.begin(); b != observed_neighs_a.end(); b++ ) {
                        balance += label[*b]==0 ? 1 : -1;
                    }

                    if (balance >= 0) counts++;
                }
                M.insert(feat_ind,v_ind) = counts;
                // most-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ )
                    M.insert(feat_ind+1+val,v_ind) = (counts+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);

            } else if( x1.compare(0,4,"att0") == 0 && x2.compare("average") == 0 ) {
                double inc = 1.0/MAX(1,observed_neighs.size());
                int *counts = new int [nattribs]();
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    for( int j = 0; j < attribs[*a].size(); j++ )
                        if( attribs[*a][j] < nattribs )
                            counts[attribs[*a][j]]++;
                }
                for( int j = 0; j < nattribs; j++ )
                    if( counts[j]>0 )
                        M.insert(feat_ind+j,v_ind) = counts[j]*inc;
                delete[] counts;

            } else if( x2.compare("rw") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "rw\n");
                int steps = atoi(x3.c_str());
                int times = atoi(x4.c_str());

                double sum = 0.0;
                for( int t = 0; t < times; t++ ) {
                    int rw_pos = v;
                    for( int s = 0; s < steps; s++ ) {
                        if( in[rw_pos].size() == 0 )
                            break;
                        double p_in;
                        p_in = in[rw_pos].size()/(in[rw_pos].size()+out[rw_pos].size());
                        p_in = 1.0;
                        if( drand48() <= p_in ) {
                            std::unordered_set<int>::iterator it = in[rw_pos].begin();
                            std::advance(it,(int)(drand48()*in[rw_pos].size()));
                            rw_pos = *it;
                        } else {
                            std::unordered_set<int>::iterator it = out[rw_pos].begin();
                            std::advance(it,(int)(drand48()*out[rw_pos].size()));
                            rw_pos = *it;
                        }
                    }
                    sum += label[rw_pos];
                }
                M.insert(feat_ind,v_ind) = sum/times;
            }
            feat_ind++;
        }
        //for( int i = 0; i < feat_names.size(); i++ )
        //    feats[i] = x[i];
        //delete x;
        v_ind++;
    }
    M.makeCompressed();

    //IntegerVector coef(wrap(Eigen::MatrixXd(M.block(0,0,1,11))));
    
    // (renno)
    if (this->computeFeaturesVerbose) {
      fprintf(stdout, "\n");
    }
}


void Graph::computeFeatures( std::unordered_set<int> toBeComputed, NumericMatrix &M ) { //colMajor version

    int v_ind=0;
    for( std::unordered_set<int>::iterator it=toBeComputed.begin();
            it != toBeComputed.end(); it++ ) {
        int v = *it;
        //double *x = new double[feat_names.size()]();
        double eps = 1e-10, epsilon;
        std::unordered_set<int> observed_neighs = in[v]; //construct set
        for( std::unordered_set<int>::iterator u = out[v].begin(); u != out[v].end(); u++)
            if( valid[*u] )
                observed_neighs.insert(*u);

        if( DEBUG>1 )
            fprintf( stderr, "Computing features for node %d\n", v);


        int feat_ind = 0;
        M(feat_ind++,v_ind) = v;
        for( std::vector<std::string>::iterator it = feat_names.begin();
                it != feat_names.end(); it++ ) {

            // read and parse feat_name
            std::istringstream f(*it);
            std::string x1, x2, x3, x4;
            std::getline( f, x1, ',' );
            std::getline( f, x2, ',' );
            if( x2.compare(0,3,"att") != 0 ) // not for attributes
                std::getline( f, x3, ',' );
            if( x2.compare(0,3,"tri") == 0 || x2.compare("rw") == 0 ) // only for triangles, tri-fraction and rw
                std::getline( f, x4, ',' );
                
            if( x2.compare("counts") == 0 && x3.compare("0") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "counts\n");
                // counts
                for( std::unordered_set<int>::iterator u = observed_neighs.begin(); u != observed_neighs.end(); u++ ) {
                    //fprintf( stderr, "feat_ind %d label %d\n", feat_ind, label[*u]);
                    M( feat_ind+(label[*u]>0) ,v_ind) += 1.0;

                }
                // counts-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ ) {
                    //fprintf( stderr, "feat_ind %d val %d\n", feat_ind, val);
                    M(feat_ind+(label_max+1)+val,v_ind ) = (M(feat_ind+val,v_ind  )+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
                }
            } else if( x2.compare("triangles") == 0 && x3[0] == '~' && x4[0] == '~' && x1.compare("y") == 0 ) {
                //fprintf( stderr, "triangles\n");
                // triangles
                int tmp = 0;
                if( observed_neighs.size() > 0 )
                    for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                        for( std::unordered_set<int>::iterator b = a; b != observed_neighs.end(); b++ ) {
                            if( out[*a].find(*b) != out[*a].end() ||
                                out[*b].find(*a) != out[*b].end() ) {
                                int pos = ((label[*a]>0) == target_label ) + ((label[*b]>0) == target_label );
                                M(feat_ind+pos,v_ind ) += 1.0;
                                tmp++;
                            }
                        }
                    }
                // tri-fraction
                epsilon = tmp > 0 ? 0.0 : eps;
                for( int val = 0; val < 2; val++ ) {
                    //fprintf( stderr, "Setting pos %d to %lf/%lf = %lf\n", feat_ind+3+val,
                    //        (M(v_ind,feat_ind+val)+epsilon),(tmp+3*epsilon),
                    //        (M(v_ind,feat_ind+val)+epsilon)/(tmp+3*epsilon) );
                    M(feat_ind+3+val,v_ind) = (M(feat_ind+val,v_ind)+epsilon)/(tmp+3*epsilon);
                }
            } else if( x1.compare("yi") == 0 && x2.compare("most") == 0 && x3.compare("0") == 0 ) {
                //fprintf( stderr, "most\n");
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    int count[label_max+1];

                    std::unordered_set<int> observed_neighs_a = in[*a]; //construct set
                    for( std::unordered_set<int>::iterator it = out[*a].begin(); it != out[*a].end(); it++)
                        if( valid[*it] )
                            observed_neighs_a.insert(*it);

                    for( std::unordered_set<int>::iterator b = observed_neighs_a.begin(); b != observed_neighs_a.end(); b++ ) {
                        count[(label[*b]>0)]++;
                    }
                    int max_ind = -1, max_val = -1;
                    for( int j = 0; j <= label_max; j++ )
                        if( count[j] > max_val ) {
                            max_ind = j;
                            max_val = count[j];
                        }
                    M(feat_ind+max_ind,v_ind )++;
                }
                // most-fraction
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ )
                    M(feat_ind+2+val,v_ind) = (M(feat_ind+val,v_ind)+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
            } else if( x1.compare(0,4,"att0") == 0 && x2.compare("average") == 0 ) {
                double inc = 1.0/MAX(1,observed_neighs.size());
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    for( int j = 0; j < attribs[*a].size(); j++ )
                        if( attribs[*a][j] < nattribs )
                            M(feat_ind+attribs[*a][j],v_ind) += inc;
                }
            } else if( x2.compare("rw") == 0 && x1.compare("y") == 0 ) {
                //fprintf( stderr, "rw\n");
                int steps = atoi(x3.c_str());
                int times = atoi(x4.c_str());

                double sum = 0.0;
                for( int t = 0; t < times; t++ ) {
                    int rw_pos = v;
                    for( int s = 0; s < steps; s++ ) {
                        if( in[rw_pos].size() == 0 )
                            break;
                        double p_in;
                        p_in = in[rw_pos].size()/(in[rw_pos].size()+out[rw_pos].size());
                        p_in = 1.0;
                        if( drand48() <= p_in ) {
                            std::unordered_set<int>::iterator it = in[rw_pos].begin();
                            std::advance(it,(int)(drand48()*in[rw_pos].size()));
                            rw_pos = *it;
                        } else {
                            std::unordered_set<int>::iterator it = out[rw_pos].begin();
                            std::advance(it,(int)(drand48()*out[rw_pos].size()));
                            rw_pos = *it;
                        }
                    }
                    sum += label[rw_pos];
                }
                M(feat_ind,v_ind) = sum/times;
            }
            feat_ind++;
        }
        //for( int i = 0; i < feat_names.size(); i++ )
        //    feats[i] = x[i];
        //delete x;
        v_ind++;
    }
}

void Graph::addEdge( int u, int v ) {
    int max = (u>v) ? u : v;
    // make resizing more efficient
    if( this->out.size() < (max+1) ) {
        this->out.resize(max+1);
        this->in.resize(max+1);
        this->label.resize(max+1);
    }
    this->out[u].insert(v);
    this->in[v].insert(u);
    totalE++;
}


//void Graph::initFeatures( int ntypes, int target, std::vector<int> att_max ) {
void Graph::initFeatures( int ntypes, int target, int nattribs ) {
    long long int target2 = target;
    this->nattribs = nattribs;

    //2) counts of neighbors
    for( long long int i=0; i < ntypes; i++ ) {
        feat_names.push_back( std::string("y,counts,") + std::to_string(i));
        feat_isFraction.push_back( false );
    }

    //1) fraction of neighbors
    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("y,counts-fraction,") + std::to_string(i));
        feat_isFraction.push_back( true );
    }

    if( nattribs < 0 ) return;

    feat_names.push_back( std::string("y,triangles") + ",~"+ std::to_string(target2)+",~"+std::to_string(target2));
    feat_names.push_back( std::string("y,triangles") + ",~"+ std::to_string(target2)+","+std::to_string(target2));
    //feat_names.push_back( std::string("y,triangles") + ","+ std::to_string(target2)+",~"+std::to_string(target2));
    feat_names.push_back( std::string("y,triangles") + ","+ std::to_string(target2)+","+std::to_string(target2));
    for( long long int i=0; i < 3; i++ )
        feat_isFraction.push_back( false );

    feat_names.push_back( std::string("y,tri-fraction") + ",~"+ std::to_string(target2)+",~"+std::to_string(target2));
    feat_names.push_back( std::string("y,tri-fraction") + ",~"+ std::to_string(target2)+","+std::to_string(target2));
    for( long long int i=0; i < 2; i++ )
        feat_isFraction.push_back( true );
    //feat_names.push_back( std::string("y,tri-fraction") + ","+ std::to_string(target2)+",~"+std::to_string(target2));

    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("yi,most,") + std::to_string(i));
        feat_isFraction.push_back( false );
    }

    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("yi,most-fraction,") + std::to_string(i));
        feat_isFraction.push_back( true );
    }

    if( INCLUDE_RW ) {
        feat_names.push_back( std::string("y,rw,2,5"));
        feat_isFraction.push_back( true );
    }

    for( long long int att_id=0; att_id < nattribs; att_id++ ) {
        feat_names.push_back( std::string("att")+std::to_string(att_id)+",average");
        feat_isFraction.push_back( true );
    }
}


// (renno) Computes features for all nodes at once.
SpMat Graph::computeFeaturesForAll(int verbose)
{
  std::unordered_set<int> toBeComputed;
  for (unsigned int i = 0; i < this->valid.size(); i++) {
    if (this->valid[i]) {
      toBeComputed.insert(i);
    }
  }
  
  fprintf(stdout, "There are %lu nodes in the graph.\n", toBeComputed.size());
  
  int nrows, ncols;
  nrows = feat_names.size() + 2;
  ncols = toBeComputed.size();
  
  SpMat M(nrows, ncols);
  
  this->computeFeaturesVerbose = (verbose > 0);
  computeFeaturesSparse(toBeComputed, M);
  
  return M;
}


RCPP_MODULE(yada){
    using namespace Rcpp ;
	
    class_<Graph>( "Graph" )
    // expose the default constructor
    .constructor<bool>()
    
    .method( "getE", &Graph::getE     , "get number of edges" )
    .method( "addEdge", &Graph::addEdge , "add directed edge" )
    .method( "getN", &Graph::getN     , "get number of nodes" )
    .method( "getTargetSize", &Graph::getTargetSize     , "get size of target nodes set" )
    .method( "getRandomNode", &Graph::getRandomNode     , "get random node" )
    .method( "initFeatures", &Graph::initFeatures     , "initialize feature names" )
    .method( "getFeatureNames", &Graph::getFeatureNames     , "get feature names" )
    .method( "isFeatureFraction", &Graph::isFeatureFraction     , "returns bool vector indicating whether feature is binary" )
    .method( "getNeighbors", &Graph::getNeighbors     , "get neighbors of a node" )
    .method( "getInNeighbors", &Graph::getInNeighbors     , "get in-neighbors of a node" )
    .method( "getTargetNodes", &Graph::getTargetNodes     , "get a random sample of target nodes" )
    .method( "getLabel", &Graph::getLabel     , "get label of a node" )
    .method( "getAttributes", &Graph::getAttributes     , "get attributes of a node" )
    .method( "getInfo", &Graph::getInfo     , "get attributes of a node" )
    .method( "readGraph", &Graph::readGraph     , "read graph from file" )
    .method( "readCommunities2", &Graph::readCommunities2     , "read communities from file as attributes" )
    .method( "readCommunities3", &Graph::readCommunities3     , "read communities from file as attributes" )
    .method( "writeAttributesToFile", &Graph::writeAttributesToFile     , "write communities to file" )
    
    .method( "computeFeaturesForAll", &Graph::computeFeaturesForAll, "computes features for all nodes in the graph" )

    ;
}                     
