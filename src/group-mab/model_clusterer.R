library(R6)
library(RBGL)

##
 #
 ##
ModelClusterer <- R6Class("ModelClusterer",
                          
    public = list(
      
        ##
         # 
         ##
        initialize = function(settings=NA)
        {
          private$settings <- settings
          nmodels <- settings$nmodels
          nranks <- settings$nranks
          
          private$correlations <- matrix(NA, nrow=(nmodels^2), ncol=nranks)
          private$current_rank <- 1
        },
        
        ##
         # 
         ##
        addRanks = function(ranks=NA)
        {
          # Asserts appropriate rankings from all models are available.
          if (is.null(ranks) || any(is.na(ranks)) || (length(ranks[[1]]) < 3)) {
            return (FALSE)
          }
          # If the correlations matrix is full, removes the first (oldest) column and adds a new one.
          if (private$current_rank > private$settings$nranks) {
            private$correlations <- cbind(private$correlations[,-1], rep(NA, nrow(private$correlations)))
            private$current_rank <- private$settings$nranks
          }
          # Calculates all pairwise correlations.
          ridx <- 0
          cidx <- private$current_rank
          for (midx1 in 1:private$settings$nmodels) {
            for (midx2 in 1:private$settings$nmodels) {
              ridx <- 1 + ridx
              private$correlations[ridx,cidx] <- suppressWarnings(cor.test(ranks[[midx1]], 
                                                                           ranks[[midx2]],
                                                                           method="spearman",
                                                                           alternative="two.sided")$estimate)
            }
          }
          private$current_rank <- 1 + private$current_rank
          return (TRUE)
        },
        
        ##
         #
         ##
        getModelClusters = function()
        {
          cgraph <- private$getCorrelationGraph()
          # clusters <- private$hcs(cgraph, private$settings$min_cor)
          clusters <- private$ccPivot(cgraph, private$settings$min_cor)
          return (clusters)
        }
    ),
    
    private = list(
      
        settings = NA,
        correlations = NA,
        current_rank = NA,
        
        ##
         #
         ##
        hcs = function(graph=NA, threshold=NA)
        {
          # TODO: the graph must be processed so as to remove the necessary edges.
          # clusters <- Map(as.numeric, highlyConnSG(cgraph)$clusters)
        },
        
        ##
         #
         ##
        ccPivot = function(graph=NA, threshold=NA)
        {
          clusters <- list()
          g <- graph
          while (length(unlist(clusters)) < length(graph::nodes(graph))) {
            v <- graph::nodes(g)
            pivot <- sample(v, 1)
            weights <- edgeWeights(g)[[pivot]]
            cluster <- c(pivot, names(unlist(Filter(function(w) w > threshold, weights))))
            clusters[[1+length(clusters)]] <- cluster
            g <- removeNode(cluster, g)
          }
          return (Map(as.numeric, clusters))
        },

        ##
         #
         ##
        getCorrelationGraph = function()
        {
          cmatrix <- private$getCorrelationMatrix()
          cgraph <- graphNEL(nodes=character(), edgeL=list(), edgemode="undirected")
          cgraph <- addNode(as.character(1:private$settings$nmodels), cgraph)
          for (m1 in 1:private$settings$nmodels) {
            for (m2 in 1:private$settings$nmodels) {
              if (m2 > m1) {
                cgraph <- addEdge(as.character(m1), as.character(m2), graph=cgraph, weights=cmatrix[m1,m2])
              }
            }
          }
          return (cgraph)
        },

        ##
         #
         ##
        getCorrelationMatrix = function()
        {
          series <- private$correlations

          # Mean of the N most recent.
          # nrecent <- 10
          # series <- series[, (ncol(series)-nrecent+1):ncol(series)]
          # correlations <- apply(series, 1, mean)
          
          # Mean of all.
          correlations <- apply(series, 1, mean)
          
          # w <- rep(0.9, ncol(series)) ^ seq(ncol(series)-1, 0)
          # correlations <- apply(series, 1, function(r) sum(r*w)/sum(w))
          
          squared <- matrix(as.vector(t(correlations)), nrow=private$settings$nmodels, byrow=TRUE)
          return (squared)
        }
    )
)
