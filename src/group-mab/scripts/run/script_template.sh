#!/bin/bash
#
#PBS -l nodes=NODES:ppn=PPN
#
#PBS -q QUEUENAME
#PBS -l walltime=WALLTIME
#PBS -l mem=MEM
#PBS -l naccesspolicy=singleuser
#
#PBS -j oe
#
#

source /etc/profile.d/modules.sh
module load r
module load octave
module load python
module load devel
echo " "
echo " "
echo "Job started on `hostname` at `date`"
#
#    User code starts now
#

echo "Replaced placeholders:"
echo NODES
echo PPN
echo QUEUENAME
echo WALLTIME
echo MEM
echo DATASET
echo TEST_RANGE
echo MSC
echo SCOREMAPPER
echo FEATURESET
echo SETTINGS

CODEDIR=~/pay2recruit/project/src/group-mab/
cd $CODEDIR

if [[ ! -d "OUTPATH" ]]; then
    echo "Creating output folder OUTPATH"
    mkdir -p OUTPATH
else
    echo "Saving results to OUTPATH"
fi

Rscript scripts/run/test.R DATASET TEST_RANGE NSEEDS HEURISTIC COLDPULLS NRANKS MIN_COR FEATURESET MSC ORACLE_MAB ORACLE_MS SCOREMAPPER inits/settings/SETTINGS.settings.init.R OUTPATH PPN

#
#    End user code
#
echo "Job ended on `hostname` at `date`"
