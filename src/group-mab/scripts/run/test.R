###
printArgument <- function(name, value)
{
  cat(sprintf("%17s = %s\n", name, as.character(value)))
}

###
confirmArguments <- function(proc_args)
{
  cat("\n")
  for (arg in names(proc_args)) {
    printArgument(arg, proc_args[[arg]])
  }
#   cat("\nPress [enter] to run")
#   scan("stdin", raw(), n=1, blank.lines.skip=FALSE)
  cat("\n\n")
}

###############################################################################

raw_args <- commandArgs(trailingOnly = TRUE)

# main <- function(problem_name, test_range=NA, nseeds=NA, heuristic=NA, ncold=NA, nranks=NA, min_cor=NA, feature_set=NA, mab_policy=NA, 
#                  score_mapper=NA, settings_init=NA, output_path=NA, nproc=1, output_to_file=FALSE, observation_file=NA, save_observations=FALSE)
proc_args <- list(
  dataset            = raw_args[1],
  test_range_start   = as.numeric(raw_args[2]),
  test_range_end     = as.numeric(raw_args[3]),
  nseeds             = as.numeric(raw_args[4]),
  heuristic          = raw_args[5],
  ncold              = as.numeric(raw_args[6]),
  nranks             = as.numeric(raw_args[7]),
  min_cor            = as.numeric(raw_args[8]),
  feature_set        = raw_args[9],
  mab_policy         = raw_args[10],
  oracle_mab         = as.logical(as.numeric(raw_args[11])),
  oracle_ms          = as.logical(as.numeric(raw_args[12])),
  score_mapper       = raw_args[13],
  settings_init      = raw_args[14],
  output_path        = raw_args[15],
  nproc              = as.numeric(raw_args[16]))

confirmArguments(proc_args)

source("main.R")
main(proc_args$dataset,
     test_range = c(proc_args$test_range_start, proc_args$test_range_end),
     nseeds = proc_args$nseeds,
     heuristic = proc_args$heuristic,
     ncold = proc_args$ncold,
     nranks = proc_args$nranks,
     min_cor = proc_args$min_cor,
     feature_set = proc_args$feature_set,
     mab_policy = proc_args$mab_policy,
     oracle_mab = proc_args$oracle_mab,
     oracle_ms = proc_args$oracle_ms,
     score_mapper = proc_args$score_mapper,
     settings_init = proc_args$settings_init,
     output_path = proc_args$output_path, 
     nproc = proc_args$nproc)

