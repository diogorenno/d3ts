data {
  int<lower=0> M; // number of features
  int<lower=0> N; // total number of observations 
  int<lower=0> K; // number of regions/models
  row_vector[M] x[N];  // unit-level predictors
  real y[N];    // estimated treatment effects
  int<lower=1, upper=K> ll[N];
  vector[M] mu_0; // prior mean
  real<lower=0> sigma_0; // prior variance
}
parameters {
  vector[M] mu;     // grand-mean
  row_vector[M] beta[K]; // model-specific coefficients
  //real<lower=0,upper=pi()/2> u_k[K];    // model-specific variance
  //real<lower=0,upper=pi()/2> u[M];
  real<lower=0> sigma[M];
  real<lower=0> sigma_k[K];    // model-specific variance
}
transformed parameters {
  vector[N] theta;
  for (n in 1:N)
      theta[n] <- dot_product(x[n],beta[ll[n]]);
  //for (m in 1:M)
  //  sigma[m] <- 25*tan(u[m]);
  //for (k in 1:K)
  //  sigma_k[k] <- 25*tan(u_k[k]);
}
model {
  mu ~ normal(mu_0,sigma_0);
  //u ~ uniform(0,pi()/2);
  //u_k ~ uniform(0,pi()/2);
  sigma ~ cauchy(0,25);
  sigma_k ~ cauchy(0,25);
  for (k in 1:K)
      beta[k] ~ normal(mu, to_vector(sigma));
  for (n in 1:N)
    y[n] ~ normal(theta[n], sigma_k[ll[n]]);
}
