#-*- coding: utf-8 -*-

#
#

import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

dirs = {"raw": sys.argv[1]}

for name, path in dirs.items():

  filenames = map(lambda x: path + x, os.listdir(path))

  for filename in filenames:

    tokens = filename.split("/")[-1].split("_")
    dataset = tokens[0]
    ntests = int(tokens[1][1:])
    nattempts = int(tokens[2][1:])
    nlocals = int(tokens[3][1:])
    kind = tokens[5].split(".")[0]

    with open(filename) as handler:
    
      values = dict()
    
      for line in handler:
        tokens = line.split()
        
        if not len(tokens): 
          continue
          
        if tokens[0] == "Results":
          turn = int(tokens[3][:-1])
          
          for i, line in enumerate(handler):
            if i > 1:
              break
            if i == 1:
              tokens = line.split()
              values[turn] = {"mean": float(tokens[0]), "sd": float(tokens[2])}
          
          output_filename = "{}_{}_{}.txt".format(dataset, nlocals, kind)
          with open(output_filename, "w") as output:
            print >> output, "turn\t\tmean\t\tsd"
            for key in sorted(values.keys()):
              print >> output, "{}\t\t{}\t\t{}".format(key, values[key]["mean"], values[key]["sd"])


