#-*- coding: utf-8 -*-

#
#

import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

dirs = {"raw": sys.argv[1]}

for name, path in dirs.items():

  dirnames = map(lambda x: path + x, os.listdir(path))
  
  for dirname in dirnames:

    is_svm = (dirname.split("/")[-1] == "svm")

    if (not is_svm):
      beta = dirname.split("_")[0].split("beta")[-1]
      lamb = dirname.split("_")[1].split("/")[0].split("lambda")[-1]
    
    filenames = map(lambda x: dirname + "/" + x, os.listdir(dirname))

    for filename in filenames:

      tokens = filename.split("/")[-1].split("_")
      dataset = tokens[0]
      ntests = int(tokens[1][1:])
      nattempts = int(tokens[2][1:])
      nmodels = int(tokens[3][1:])
      nseeds = int(tokens[4][1:])
      kind = tokens[5].split(".")[0]
      
      if (not is_svm) and (nmodels != 4):
        continue

      with open(filename) as handler:
      
        values = dict()
      
        for line in handler:
          tokens = line.split()
          
          if not len(tokens): 
            continue
            
          if tokens[0] == "Results":
            turn = int(tokens[3][:-1])
            
            for i, line in enumerate(handler):
              if i > 1:
                break
              if i == 1:
                tokens = line.split()
                values[turn] = {"mean": float(tokens[0]), "sd": float(tokens[2])}
            
            if not is_svm:
              output_filename = "extracted/{}_m{}_b{}_l{}_{}.txt".format(dataset, nmodels, beta, lamb, kind)
            else:
              output_filename = "extracted/{}_m{}_{}.txt".format(dataset, nmodels, kind)
            
            with open(output_filename, "w") as output:
              print >> output, "turn\t\tmean\t\tsd"
              for key in sorted(values.keys()):
                print >> output, "{}\t\t{}\t\t{}".format(key, values[key]["mean"], values[key]["sd"])


