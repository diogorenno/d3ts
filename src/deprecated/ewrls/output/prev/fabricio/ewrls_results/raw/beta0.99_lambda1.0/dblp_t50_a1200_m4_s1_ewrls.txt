Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		30.3 ± 7.7572
	Mean payoff:
		0.6184 ± 0.1583
	Total number of recruited nodes:
		49 ± 0

Results at turn 96:
	Total payoff:
		56.62 ± 16.2077
	Mean payoff:
		0.5837 ± 0.1671
	Total number of recruited nodes:
		97 ± 0

Results at turn 144:
	Total payoff:
		84 ± 22.7722
	Mean payoff:
		0.5793 ± 0.157
	Total number of recruited nodes:
		145 ± 0

Results at turn 192:
	Total payoff:
		112.1 ± 27.8159
	Mean payoff:
		0.5808 ± 0.1441
	Total number of recruited nodes:
		193 ± 0

Results at turn 240:
	Total payoff:
		141.6 ± 32.7925
	Mean payoff:
		0.5876 ± 0.1361
	Total number of recruited nodes:
		241 ± 0

Results at turn 288:
	Total payoff:
		169.74 ± 36.005
	Mean payoff:
		0.5873 ± 0.1246
	Total number of recruited nodes:
		289 ± 0

Results at turn 336:
	Total payoff:
		200.84 ± 36.1479
	Mean payoff:
		0.596 ± 0.1073
	Total number of recruited nodes:
		337 ± 0

Results at turn 384:
	Total payoff:
		231.48 ± 36.3179
	Mean payoff:
		0.6012 ± 0.0943
	Total number of recruited nodes:
		385 ± 0

Results at turn 432:
	Total payoff:
		261.02 ± 35.9293
	Mean payoff:
		0.6028 ± 0.083
	Total number of recruited nodes:
		433 ± 0

Results at turn 480:
	Total payoff:
		290.66 ± 36.1536
	Mean payoff:
		0.6043 ± 0.0752
	Total number of recruited nodes:
		481 ± 0

Results at turn 528:
	Total payoff:
		320.3 ± 37.0643
	Mean payoff:
		0.6055 ± 0.0701
	Total number of recruited nodes:
		529 ± 0

Results at turn 576:
	Total payoff:
		348.5 ± 36.8373
	Mean payoff:
		0.604 ± 0.0638
	Total number of recruited nodes:
		577 ± 0

Results at turn 624:
	Total payoff:
		375.2 ± 39.5572
	Mean payoff:
		0.6003 ± 0.0633
	Total number of recruited nodes:
		625 ± 0

Results at turn 672:
	Total payoff:
		401.52 ± 40.7948
	Mean payoff:
		0.5966 ± 0.0606
	Total number of recruited nodes:
		673 ± 0

Results at turn 720:
	Total payoff:
		427.5 ± 40.8368
	Mean payoff:
		0.5929 ± 0.0566
	Total number of recruited nodes:
		721 ± 0

Results at turn 768:
	Total payoff:
		451.56 ± 41.2202
	Mean payoff:
		0.5872 ± 0.0536
	Total number of recruited nodes:
		769 ± 0

Results at turn 816:
	Total payoff:
		475.1 ± 40.7357
	Mean payoff:
		0.5815 ± 0.0499
	Total number of recruited nodes:
		817 ± 0

Results at turn 864:
	Total payoff:
		499.56 ± 40.4778
	Mean payoff:
		0.5775 ± 0.0468
	Total number of recruited nodes:
		865 ± 0

Results at turn 912:
	Total payoff:
		523.48 ± 40.5409
	Mean payoff:
		0.5734 ± 0.0444
	Total number of recruited nodes:
		913 ± 0

Results at turn 960:
	Total payoff:
		545.04 ± 40.5709
	Mean payoff:
		0.5672 ± 0.0422
	Total number of recruited nodes:
		961 ± 0

Results at turn 1008:
	Total payoff:
		567.84 ± 42.9691
	Mean payoff:
		0.5628 ± 0.0426
	Total number of recruited nodes:
		1009 ± 0

Results at turn 1056:
	Total payoff:
		588.06 ± 46.953
	Mean payoff:
		0.5563 ± 0.0444
	Total number of recruited nodes:
		1057 ± 0

Results at turn 1104:
	Total payoff:
		608.76 ± 49.2845
	Mean payoff:
		0.5509 ± 0.0446
	Total number of recruited nodes:
		1105 ± 0

Results at turn 1152:
	Total payoff:
		631.66 ± 51.179
	Mean payoff:
		0.5478 ± 0.0444
	Total number of recruited nodes:
		1153 ± 0

Results at turn 1200:
	Total payoff:
		652 ± 52.0377
	Mean payoff:
		0.5433 ± 0.0434
	Total number of recruited nodes:
		1200 ± 0


