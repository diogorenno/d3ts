Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		30.44 ± 7.6536
	Mean payoff:
		0.6212 ± 0.1562
	Total number of recruited nodes:
		49 ± 0

Results at turn 96:
	Total payoff:
		57.74 ± 16.278
	Mean payoff:
		0.5953 ± 0.1678
	Total number of recruited nodes:
		97 ± 0

Results at turn 144:
	Total payoff:
		84.56 ± 22.3991
	Mean payoff:
		0.5832 ± 0.1545
	Total number of recruited nodes:
		145 ± 0

Results at turn 192:
	Total payoff:
		111.22 ± 29.4184
	Mean payoff:
		0.5763 ± 0.1524
	Total number of recruited nodes:
		193 ± 0

Results at turn 240:
	Total payoff:
		140.16 ± 33.116
	Mean payoff:
		0.5816 ± 0.1374
	Total number of recruited nodes:
		241 ± 0

Results at turn 288:
	Total payoff:
		170.96 ± 36.944
	Mean payoff:
		0.5916 ± 0.1278
	Total number of recruited nodes:
		289 ± 0

Results at turn 336:
	Total payoff:
		203.16 ± 38.0007
	Mean payoff:
		0.6028 ± 0.1128
	Total number of recruited nodes:
		337 ± 0

Results at turn 384:
	Total payoff:
		234.54 ± 38.5535
	Mean payoff:
		0.6092 ± 0.1001
	Total number of recruited nodes:
		385 ± 0

Results at turn 432:
	Total payoff:
		266.04 ± 39.275
	Mean payoff:
		0.6144 ± 0.0907
	Total number of recruited nodes:
		433 ± 0

Results at turn 480:
	Total payoff:
		297.86 ± 39.5469
	Mean payoff:
		0.6193 ± 0.0822
	Total number of recruited nodes:
		481 ± 0

Results at turn 528:
	Total payoff:
		329.64 ± 39.291
	Mean payoff:
		0.6231 ± 0.0743
	Total number of recruited nodes:
		529 ± 0

Results at turn 576:
	Total payoff:
		362.06 ± 39.1655
	Mean payoff:
		0.6275 ± 0.0679
	Total number of recruited nodes:
		577 ± 0

Results at turn 624:
	Total payoff:
		392.46 ± 38.3045
	Mean payoff:
		0.6279 ± 0.0613
	Total number of recruited nodes:
		625 ± 0

Results at turn 672:
	Total payoff:
		423.52 ± 37.1078
	Mean payoff:
		0.6293 ± 0.0551
	Total number of recruited nodes:
		673 ± 0

Results at turn 720:
	Total payoff:
		453.9 ± 35.9729
	Mean payoff:
		0.6295 ± 0.0499
	Total number of recruited nodes:
		721 ± 0

Results at turn 768:
	Total payoff:
		484.6 ± 34.3856
	Mean payoff:
		0.6302 ± 0.0447
	Total number of recruited nodes:
		769 ± 0

Results at turn 816:
	Total payoff:
		514 ± 35.1341
	Mean payoff:
		0.6291 ± 0.043
	Total number of recruited nodes:
		817 ± 0

Results at turn 864:
	Total payoff:
		544.3 ± 36.0517
	Mean payoff:
		0.6292 ± 0.0417
	Total number of recruited nodes:
		865 ± 0

Results at turn 912:
	Total payoff:
		574.06 ± 38.9098
	Mean payoff:
		0.6288 ± 0.0426
	Total number of recruited nodes:
		913 ± 0

Results at turn 960:
	Total payoff:
		603.98 ± 39.6291
	Mean payoff:
		0.6285 ± 0.0412
	Total number of recruited nodes:
		961 ± 0

Results at turn 1008:
	Total payoff:
		633.18 ± 40.821
	Mean payoff:
		0.6275 ± 0.0405
	Total number of recruited nodes:
		1009 ± 0

Results at turn 1056:
	Total payoff:
		662.16 ± 43.6205
	Mean payoff:
		0.6265 ± 0.0413
	Total number of recruited nodes:
		1057 ± 0

Results at turn 1104:
	Total payoff:
		691.46 ± 45.9992
	Mean payoff:
		0.6258 ± 0.0416
	Total number of recruited nodes:
		1105 ± 0

Results at turn 1152:
	Total payoff:
		721.82 ± 48.7993
	Mean payoff:
		0.626 ± 0.0423
	Total number of recruited nodes:
		1153 ± 0

Results at turn 1200:
	Total payoff:
		750.66 ± 52.8381
	Mean payoff:
		0.6256 ± 0.044
	Total number of recruited nodes:
		1200 ± 0


