Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 300
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 12:
	Total payoff:
		7.3 ± 1.4463
	Mean payoff:
		0.5615 ± 0.1113
	Total number of recruited nodes:
		13 ± 0

Results at turn 24:
	Total payoff:
		10.96 ± 2.4324
	Mean payoff:
		0.4384 ± 0.0973
	Total number of recruited nodes:
		25 ± 0

Results at turn 36:
	Total payoff:
		14.06 ± 3.3467
	Mean payoff:
		0.38 ± 0.0905
	Total number of recruited nodes:
		37 ± 0

Results at turn 48:
	Total payoff:
		16.04 ± 4.0303
	Mean payoff:
		0.3273 ± 0.0823
	Total number of recruited nodes:
		49 ± 0

Results at turn 60:
	Total payoff:
		17.96 ± 4.2183
	Mean payoff:
		0.2944 ± 0.0692
	Total number of recruited nodes:
		61 ± 0

Results at turn 72:
	Total payoff:
		19.94 ± 5.1997
	Mean payoff:
		0.2732 ± 0.0712
	Total number of recruited nodes:
		73 ± 0

Results at turn 84:
	Total payoff:
		21.9 ± 5.3117
	Mean payoff:
		0.2576 ± 0.0625
	Total number of recruited nodes:
		85 ± 0

Results at turn 96:
	Total payoff:
		23.54 ± 5.563
	Mean payoff:
		0.2427 ± 0.0574
	Total number of recruited nodes:
		97 ± 0

Results at turn 108:
	Total payoff:
		24.92 ± 5.7918
	Mean payoff:
		0.2286 ± 0.0531
	Total number of recruited nodes:
		109 ± 0

Results at turn 120:
	Total payoff:
		26.3 ± 6.0178
	Mean payoff:
		0.2174 ± 0.0497
	Total number of recruited nodes:
		121 ± 0

Results at turn 132:
	Total payoff:
		27.6 ± 6.0474
	Mean payoff:
		0.2075 ± 0.0455
	Total number of recruited nodes:
		133 ± 0

Results at turn 144:
	Total payoff:
		29.06 ± 6.2903
	Mean payoff:
		0.2004 ± 0.0434
	Total number of recruited nodes:
		145 ± 0

Results at turn 156:
	Total payoff:
		30.48 ± 6.3156
	Mean payoff:
		0.1941 ± 0.0402
	Total number of recruited nodes:
		157 ± 0

Results at turn 168:
	Total payoff:
		31.66 ± 6.3811
	Mean payoff:
		0.1873 ± 0.0378
	Total number of recruited nodes:
		169 ± 0

Results at turn 180:
	Total payoff:
		33.12 ± 6.3524
	Mean payoff:
		0.183 ± 0.0351
	Total number of recruited nodes:
		181 ± 0

Results at turn 192:
	Total payoff:
		34.78 ± 6.1588
	Mean payoff:
		0.1802 ± 0.0319
	Total number of recruited nodes:
		193 ± 0

Results at turn 204:
	Total payoff:
		36 ± 6
	Mean payoff:
		0.1756 ± 0.0293
	Total number of recruited nodes:
		205 ± 0

Results at turn 216:
	Total payoff:
		37.22 ± 6.2867
	Mean payoff:
		0.1715 ± 0.029
	Total number of recruited nodes:
		217 ± 0

Results at turn 228:
	Total payoff:
		38.46 ± 5.7043
	Mean payoff:
		0.1679 ± 0.0249
	Total number of recruited nodes:
		229 ± 0

Results at turn 240:
	Total payoff:
		39.56 ± 5.5775
	Mean payoff:
		0.1641 ± 0.0231
	Total number of recruited nodes:
		241 ± 0

Results at turn 252:
	Total payoff:
		40.56 ± 5.3915
	Mean payoff:
		0.1603 ± 0.0213
	Total number of recruited nodes:
		253 ± 0

Results at turn 264:
	Total payoff:
		41.42 ± 5.1392
	Mean payoff:
		0.1563 ± 0.0194
	Total number of recruited nodes:
		265 ± 0

Results at turn 276:
	Total payoff:
		42.46 ± 5.1079
	Mean payoff:
		0.1533 ± 0.0184
	Total number of recruited nodes:
		277 ± 0

Results at turn 288:
	Total payoff:
		43.28 ± 5.2529
	Mean payoff:
		0.1498 ± 0.0182
	Total number of recruited nodes:
		289 ± 0

Results at turn 300:
	Total payoff:
		43.88 ± 5.263
	Mean payoff:
		0.1463 ± 0.0175
	Total number of recruited nodes:
		300 ± 0


