Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1000
    nmodels: 1
    nseeds: 4
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 40:
	Total payoff:
		19.05 ± 5.8443
	Mean payoff:
		0.4646 ± 0.1425
	Total number of recruited nodes:
		41 ± 0

Results at turn 80:
	Total payoff:
		33.75 ± 11.0781
	Mean payoff:
		0.4167 ± 0.1368
	Total number of recruited nodes:
		81 ± 0

Results at turn 120:
	Total payoff:
		48.5 ± 16.0049
	Mean payoff:
		0.4008 ± 0.1323
	Total number of recruited nodes:
		121 ± 0

Results at turn 160:
	Total payoff:
		66.4 ± 21.6172
	Mean payoff:
		0.4124 ± 0.1343
	Total number of recruited nodes:
		161 ± 0

Results at turn 200:
	Total payoff:
		87.45 ± 25.8853
	Mean payoff:
		0.4351 ± 0.1288
	Total number of recruited nodes:
		201 ± 0

Results at turn 240:
	Total payoff:
		108.35 ± 32.1907
	Mean payoff:
		0.4496 ± 0.1336
	Total number of recruited nodes:
		241 ± 0

Results at turn 280:
	Total payoff:
		129.2 ± 35.2354
	Mean payoff:
		0.4598 ± 0.1254
	Total number of recruited nodes:
		281 ± 0

Results at turn 320:
	Total payoff:
		154.55 ± 35.3709
	Mean payoff:
		0.4815 ± 0.1102
	Total number of recruited nodes:
		321 ± 0

Results at turn 360:
	Total payoff:
		176.3 ± 34.6366
	Mean payoff:
		0.4884 ± 0.0959
	Total number of recruited nodes:
		361 ± 0

Results at turn 400:
	Total payoff:
		200.05 ± 36.3875
	Mean payoff:
		0.4989 ± 0.0907
	Total number of recruited nodes:
		401 ± 0

Results at turn 440:
	Total payoff:
		223.6 ± 37.7992
	Mean payoff:
		0.507 ± 0.0857
	Total number of recruited nodes:
		441 ± 0

Results at turn 480:
	Total payoff:
		247 ± 36.2811
	Mean payoff:
		0.5135 ± 0.0754
	Total number of recruited nodes:
		481 ± 0

Results at turn 520:
	Total payoff:
		271 ± 36.9167
	Mean payoff:
		0.5202 ± 0.0709
	Total number of recruited nodes:
		521 ± 0

Results at turn 560:
	Total payoff:
		293.5 ± 37.5507
	Mean payoff:
		0.5232 ± 0.0669
	Total number of recruited nodes:
		561 ± 0

Results at turn 600:
	Total payoff:
		316.95 ± 37.3666
	Mean payoff:
		0.5274 ± 0.0622
	Total number of recruited nodes:
		601 ± 0

Results at turn 640:
	Total payoff:
		338.2 ± 37.268
	Mean payoff:
		0.5276 ± 0.0581
	Total number of recruited nodes:
		641 ± 0

Results at turn 680:
	Total payoff:
		360.2 ± 37.0896
	Mean payoff:
		0.5289 ± 0.0545
	Total number of recruited nodes:
		681 ± 0

Results at turn 720:
	Total payoff:
		383.4 ± 36.0926
	Mean payoff:
		0.5318 ± 0.0501
	Total number of recruited nodes:
		721 ± 0

Results at turn 760:
	Total payoff:
		404 ± 36.3651
	Mean payoff:
		0.5309 ± 0.0478
	Total number of recruited nodes:
		761 ± 0

Results at turn 800:
	Total payoff:
		424.5 ± 35.6777
	Mean payoff:
		0.53 ± 0.0445
	Total number of recruited nodes:
		801 ± 0

Results at turn 840:
	Total payoff:
		446.45 ± 35.8762
	Mean payoff:
		0.5309 ± 0.0427
	Total number of recruited nodes:
		841 ± 0

Results at turn 880:
	Total payoff:
		467.25 ± 36.5022
	Mean payoff:
		0.5304 ± 0.0414
	Total number of recruited nodes:
		881 ± 0

Results at turn 920:
	Total payoff:
		488.2 ± 35.3681
	Mean payoff:
		0.5301 ± 0.0384
	Total number of recruited nodes:
		921 ± 0

Results at turn 960:
	Total payoff:
		509.85 ± 38.3561
	Mean payoff:
		0.5305 ± 0.0399
	Total number of recruited nodes:
		961 ± 0

Results at turn 1000:
	Total payoff:
		530.55 ± 39.4521
	Mean payoff:
		0.5306 ± 0.0395
	Total number of recruited nodes:
		1000 ± 0


