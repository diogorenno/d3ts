Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 1
    nseeds: 4
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		32.64 ± 12.5059
	Mean payoff:
		0.6661 ± 0.2552
	Total number of recruited nodes:
		49 ± 0

Results at turn 96:
	Total payoff:
		60.74 ± 23.2026
	Mean payoff:
		0.6262 ± 0.2392
	Total number of recruited nodes:
		97 ± 0

Results at turn 144:
	Total payoff:
		86.08 ± 31.9527
	Mean payoff:
		0.5937 ± 0.2204
	Total number of recruited nodes:
		145 ± 0

Results at turn 192:
	Total payoff:
		108.8 ± 38.3682
	Mean payoff:
		0.5637 ± 0.1988
	Total number of recruited nodes:
		193 ± 0

Results at turn 240:
	Total payoff:
		130.34 ± 45.6024
	Mean payoff:
		0.5408 ± 0.1892
	Total number of recruited nodes:
		241 ± 0

Results at turn 288:
	Total payoff:
		149.24 ± 51.9291
	Mean payoff:
		0.5164 ± 0.1797
	Total number of recruited nodes:
		289 ± 0

Results at turn 336:
	Total payoff:
		167.36 ± 58.0428
	Mean payoff:
		0.4966 ± 0.1722
	Total number of recruited nodes:
		337 ± 0

Results at turn 384:
	Total payoff:
		182.14 ± 62.4591
	Mean payoff:
		0.4731 ± 0.1622
	Total number of recruited nodes:
		385 ± 0

Results at turn 432:
	Total payoff:
		190.4 ± 65.192
	Mean payoff:
		0.4397 ± 0.1506
	Total number of recruited nodes:
		433 ± 0

Results at turn 480:
	Total payoff:
		196.26 ± 67.0111
	Mean payoff:
		0.408 ± 0.1393
	Total number of recruited nodes:
		481 ± 0

Results at turn 528:
	Total payoff:
		203.32 ± 69.4948
	Mean payoff:
		0.3843 ± 0.1314
	Total number of recruited nodes:
		529 ± 0

Results at turn 576:
	Total payoff:
		208.64 ± 71.4536
	Mean payoff:
		0.3616 ± 0.1238
	Total number of recruited nodes:
		577 ± 0

Results at turn 624:
	Total payoff:
		213.92 ± 72.9528
	Mean payoff:
		0.3423 ± 0.1167
	Total number of recruited nodes:
		625 ± 0

Results at turn 672:
	Total payoff:
		219.38 ± 74.9375
	Mean payoff:
		0.326 ± 0.1113
	Total number of recruited nodes:
		673 ± 0

Results at turn 720:
	Total payoff:
		224.12 ± 76.2862
	Mean payoff:
		0.3108 ± 0.1058
	Total number of recruited nodes:
		721 ± 0

Results at turn 768:
	Total payoff:
		228.94 ± 77.7771
	Mean payoff:
		0.2977 ± 0.1011
	Total number of recruited nodes:
		769 ± 0

Results at turn 816:
	Total payoff:
		233.3 ± 79.6316
	Mean payoff:
		0.2856 ± 0.0975
	Total number of recruited nodes:
		817 ± 0

Results at turn 864:
	Total payoff:
		238.72 ± 82.2636
	Mean payoff:
		0.276 ± 0.0951
	Total number of recruited nodes:
		865 ± 0

Results at turn 912:
	Total payoff:
		242.92 ± 83.8847
	Mean payoff:
		0.2661 ± 0.0919
	Total number of recruited nodes:
		913 ± 0

Results at turn 960:
	Total payoff:
		247.12 ± 85.1365
	Mean payoff:
		0.2571 ± 0.0886
	Total number of recruited nodes:
		961 ± 0

Results at turn 1008:
	Total payoff:
		251.24 ± 86.1085
	Mean payoff:
		0.249 ± 0.0853
	Total number of recruited nodes:
		1009 ± 0

Results at turn 1056:
	Total payoff:
		254.7 ± 87.1331
	Mean payoff:
		0.241 ± 0.0824
	Total number of recruited nodes:
		1057 ± 0

Results at turn 1104:
	Total payoff:
		258.9 ± 88.7867
	Mean payoff:
		0.2343 ± 0.0803
	Total number of recruited nodes:
		1105 ± 0

Results at turn 1152:
	Total payoff:
		263.48 ± 90.3393
	Mean payoff:
		0.2285 ± 0.0784
	Total number of recruited nodes:
		1153 ± 0

Results at turn 1200:
	Total payoff:
		267.48 ± 92.2314
	Mean payoff:
		0.2229 ± 0.0769
	Total number of recruited nodes:
		1200 ± 0


