Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		36.68 ± 4.7313
	Mean payoff:
		0.7486 ± 0.0966
	Total number of recruited nodes:
		49 ± 0

Results at turn 96:
	Total payoff:
		71.14 ± 8.6615
	Mean payoff:
		0.7334 ± 0.0893
	Total number of recruited nodes:
		97 ± 0

Results at turn 144:
	Total payoff:
		101.28 ± 11.1832
	Mean payoff:
		0.6985 ± 0.0771
	Total number of recruited nodes:
		145 ± 0

Results at turn 192:
	Total payoff:
		127.44 ± 15.6494
	Mean payoff:
		0.6603 ± 0.0811
	Total number of recruited nodes:
		193 ± 0

Results at turn 240:
	Total payoff:
		151.14 ± 19.097
	Mean payoff:
		0.6271 ± 0.0792
	Total number of recruited nodes:
		241 ± 0

Results at turn 288:
	Total payoff:
		173.38 ± 22.6616
	Mean payoff:
		0.5999 ± 0.0784
	Total number of recruited nodes:
		289 ± 0

Results at turn 336:
	Total payoff:
		191.56 ± 24.6991
	Mean payoff:
		0.5684 ± 0.0733
	Total number of recruited nodes:
		337 ± 0

Results at turn 384:
	Total payoff:
		210.92 ± 26.6204
	Mean payoff:
		0.5478 ± 0.0691
	Total number of recruited nodes:
		385 ± 0

Results at turn 432:
	Total payoff:
		225.66 ± 29.6019
	Mean payoff:
		0.5212 ± 0.0684
	Total number of recruited nodes:
		433 ± 0

Results at turn 480:
	Total payoff:
		238.62 ± 33.9903
	Mean payoff:
		0.4961 ± 0.0707
	Total number of recruited nodes:
		481 ± 0

Results at turn 528:
	Total payoff:
		250.66 ± 37.5334
	Mean payoff:
		0.4738 ± 0.071
	Total number of recruited nodes:
		529 ± 0

Results at turn 576:
	Total payoff:
		262.38 ± 38.725
	Mean payoff:
		0.4547 ± 0.0671
	Total number of recruited nodes:
		577 ± 0

Results at turn 624:
	Total payoff:
		273.56 ± 39.7252
	Mean payoff:
		0.4377 ± 0.0636
	Total number of recruited nodes:
		625 ± 0

Results at turn 672:
	Total payoff:
		284.02 ± 38.9487
	Mean payoff:
		0.422 ± 0.0579
	Total number of recruited nodes:
		673 ± 0

Results at turn 720:
	Total payoff:
		293.8 ± 40.4425
	Mean payoff:
		0.4075 ± 0.0561
	Total number of recruited nodes:
		721 ± 0

Results at turn 768:
	Total payoff:
		302.08 ± 39.7717
	Mean payoff:
		0.3928 ± 0.0517
	Total number of recruited nodes:
		769 ± 0

Results at turn 816:
	Total payoff:
		311.58 ± 38.0591
	Mean payoff:
		0.3814 ± 0.0466
	Total number of recruited nodes:
		817 ± 0

Results at turn 864:
	Total payoff:
		320.1 ± 38.0087
	Mean payoff:
		0.3701 ± 0.0439
	Total number of recruited nodes:
		865 ± 0

Results at turn 912:
	Total payoff:
		326.9 ± 38.3982
	Mean payoff:
		0.3581 ± 0.0421
	Total number of recruited nodes:
		913 ± 0

Results at turn 960:
	Total payoff:
		335.02 ± 40.5697
	Mean payoff:
		0.3486 ± 0.0422
	Total number of recruited nodes:
		961 ± 0

Results at turn 1008:
	Total payoff:
		343.12 ± 43.1309
	Mean payoff:
		0.3401 ± 0.0427
	Total number of recruited nodes:
		1009 ± 0

Results at turn 1056:
	Total payoff:
		350.6 ± 45.0555
	Mean payoff:
		0.3317 ± 0.0426
	Total number of recruited nodes:
		1057 ± 0

Results at turn 1104:
	Total payoff:
		357.8 ± 45.9889
	Mean payoff:
		0.3238 ± 0.0416
	Total number of recruited nodes:
		1105 ± 0

Results at turn 1152:
	Total payoff:
		365.3 ± 47.9537
	Mean payoff:
		0.3168 ± 0.0416
	Total number of recruited nodes:
		1153 ± 0

Results at turn 1200:
	Total payoff:
		372.72 ± 49.9947
	Mean payoff:
		0.3106 ± 0.0417
	Total number of recruited nodes:
		1200 ± 0


