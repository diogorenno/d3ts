Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 300
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 12:
	Total payoff:
		7.32 ± 1.6835
	Mean payoff:
		0.5631 ± 0.1295
	Total number of recruited nodes:
		13 ± 0

Results at turn 24:
	Total payoff:
		12.62 ± 2.989
	Mean payoff:
		0.5048 ± 0.1196
	Total number of recruited nodes:
		25 ± 0

Results at turn 36:
	Total payoff:
		17.88 ± 3.5665
	Mean payoff:
		0.4832 ± 0.0964
	Total number of recruited nodes:
		37 ± 0

Results at turn 48:
	Total payoff:
		22 ± 4.2714
	Mean payoff:
		0.449 ± 0.0872
	Total number of recruited nodes:
		49 ± 0

Results at turn 60:
	Total payoff:
		26.2 ± 4.8529
	Mean payoff:
		0.4295 ± 0.0796
	Total number of recruited nodes:
		61 ± 0

Results at turn 72:
	Total payoff:
		29.84 ± 4.846
	Mean payoff:
		0.4088 ± 0.0664
	Total number of recruited nodes:
		73 ± 0

Results at turn 84:
	Total payoff:
		33.58 ± 4.4359
	Mean payoff:
		0.3951 ± 0.0522
	Total number of recruited nodes:
		85 ± 0

Results at turn 96:
	Total payoff:
		36.74 ± 4.7286
	Mean payoff:
		0.3788 ± 0.0487
	Total number of recruited nodes:
		97 ± 0

Results at turn 108:
	Total payoff:
		39.38 ± 4.8525
	Mean payoff:
		0.3613 ± 0.0445
	Total number of recruited nodes:
		109 ± 0

Results at turn 120:
	Total payoff:
		41.08 ± 4.9191
	Mean payoff:
		0.3395 ± 0.0407
	Total number of recruited nodes:
		121 ± 0

Results at turn 132:
	Total payoff:
		42.42 ± 5.0228
	Mean payoff:
		0.3189 ± 0.0378
	Total number of recruited nodes:
		133 ± 0

Results at turn 144:
	Total payoff:
		43.92 ± 4.89
	Mean payoff:
		0.3029 ± 0.0337
	Total number of recruited nodes:
		145 ± 0

Results at turn 156:
	Total payoff:
		45 ± 4.798
	Mean payoff:
		0.2866 ± 0.0306
	Total number of recruited nodes:
		157 ± 0

Results at turn 168:
	Total payoff:
		45.74 ± 4.8226
	Mean payoff:
		0.2707 ± 0.0285
	Total number of recruited nodes:
		169 ± 0

Results at turn 180:
	Total payoff:
		46.38 ± 4.7246
	Mean payoff:
		0.2562 ± 0.0261
	Total number of recruited nodes:
		181 ± 0

Results at turn 192:
	Total payoff:
		47.24 ± 4.5471
	Mean payoff:
		0.2448 ± 0.0236
	Total number of recruited nodes:
		193 ± 0

Results at turn 204:
	Total payoff:
		48 ± 4.2523
	Mean payoff:
		0.2341 ± 0.0207
	Total number of recruited nodes:
		205 ± 0

Results at turn 216:
	Total payoff:
		48.54 ± 4.3105
	Mean payoff:
		0.2237 ± 0.0199
	Total number of recruited nodes:
		217 ± 0

Results at turn 228:
	Total payoff:
		48.98 ± 4.2833
	Mean payoff:
		0.2139 ± 0.0187
	Total number of recruited nodes:
		229 ± 0

Results at turn 240:
	Total payoff:
		49.54 ± 3.6823
	Mean payoff:
		0.2056 ± 0.0153
	Total number of recruited nodes:
		241 ± 0

Results at turn 252:
	Total payoff:
		50.2 ± 3.0772
	Mean payoff:
		0.1984 ± 0.0122
	Total number of recruited nodes:
		253 ± 0

Results at turn 264:
	Total payoff:
		50.88 ± 2.4465
	Mean payoff:
		0.192 ± 0.0092
	Total number of recruited nodes:
		265 ± 0

Results at turn 276:
	Total payoff:
		51.14 ± 2.3991
	Mean payoff:
		0.1846 ± 0.0087
	Total number of recruited nodes:
		277 ± 0

Results at turn 288:
	Total payoff:
		51.32 ± 2.2985
	Mean payoff:
		0.1776 ± 0.008
	Total number of recruited nodes:
		289 ± 0

Results at turn 300:
	Total payoff:
		51.64 ± 2.2293
	Mean payoff:
		0.1721 ± 0.0074
	Total number of recruited nodes:
		300 ± 0


