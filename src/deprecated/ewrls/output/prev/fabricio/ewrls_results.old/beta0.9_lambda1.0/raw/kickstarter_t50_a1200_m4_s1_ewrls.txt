Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		16.5 ± 5.3347
	Mean payoff:
		0.3367 ± 0.1089
	Total number of recruited nodes:
		49 ± 0

Results at turn 96:
	Total payoff:
		28.42 ± 7.913
	Mean payoff:
		0.293 ± 0.0816
	Total number of recruited nodes:
		97 ± 0

Results at turn 144:
	Total payoff:
		38.14 ± 9.8623
	Mean payoff:
		0.263 ± 0.068
	Total number of recruited nodes:
		145 ± 0

Results at turn 192:
	Total payoff:
		47.02 ± 10.987
	Mean payoff:
		0.2436 ± 0.0569
	Total number of recruited nodes:
		193 ± 0

Results at turn 240:
	Total payoff:
		56.28 ± 12.0137
	Mean payoff:
		0.2335 ± 0.0498
	Total number of recruited nodes:
		241 ± 0

Results at turn 288:
	Total payoff:
		65.3 ± 13.4472
	Mean payoff:
		0.226 ± 0.0465
	Total number of recruited nodes:
		289 ± 0

Results at turn 336:
	Total payoff:
		72.7 ± 13.6131
	Mean payoff:
		0.2157 ± 0.0404
	Total number of recruited nodes:
		337 ± 0

Results at turn 384:
	Total payoff:
		80.22 ± 14.3018
	Mean payoff:
		0.2084 ± 0.0371
	Total number of recruited nodes:
		385 ± 0

Results at turn 432:
	Total payoff:
		87.48 ± 13.9931
	Mean payoff:
		0.202 ± 0.0323
	Total number of recruited nodes:
		433 ± 0

Results at turn 480:
	Total payoff:
		94.82 ± 14.338
	Mean payoff:
		0.1971 ± 0.0298
	Total number of recruited nodes:
		481 ± 0

Results at turn 528:
	Total payoff:
		101.08 ± 14.3326
	Mean payoff:
		0.1911 ± 0.0271
	Total number of recruited nodes:
		529 ± 0

Results at turn 576:
	Total payoff:
		107.14 ± 14.9598
	Mean payoff:
		0.1857 ± 0.0259
	Total number of recruited nodes:
		577 ± 0

Results at turn 624:
	Total payoff:
		113.58 ± 15.8205
	Mean payoff:
		0.1817 ± 0.0253
	Total number of recruited nodes:
		625 ± 0

Results at turn 672:
	Total payoff:
		119.62 ± 16.8
	Mean payoff:
		0.1777 ± 0.025
	Total number of recruited nodes:
		673 ± 0

Results at turn 720:
	Total payoff:
		126.12 ± 17.4457
	Mean payoff:
		0.1749 ± 0.0242
	Total number of recruited nodes:
		721 ± 0

Results at turn 768:
	Total payoff:
		132.3 ± 17.6407
	Mean payoff:
		0.172 ± 0.0229
	Total number of recruited nodes:
		769 ± 0

Results at turn 816:
	Total payoff:
		138.92 ± 18.0202
	Mean payoff:
		0.17 ± 0.0221
	Total number of recruited nodes:
		817 ± 0

Results at turn 864:
	Total payoff:
		144.82 ± 18.2396
	Mean payoff:
		0.1674 ± 0.0211
	Total number of recruited nodes:
		865 ± 0

Results at turn 912:
	Total payoff:
		151.46 ± 18.5365
	Mean payoff:
		0.1659 ± 0.0203
	Total number of recruited nodes:
		913 ± 0

Results at turn 960:
	Total payoff:
		157.6 ± 18.3814
	Mean payoff:
		0.164 ± 0.0191
	Total number of recruited nodes:
		961 ± 0

Results at turn 1008:
	Total payoff:
		164.94 ± 19.2188
	Mean payoff:
		0.1635 ± 0.019
	Total number of recruited nodes:
		1009 ± 0

Results at turn 1056:
	Total payoff:
		171.24 ± 19.6827
	Mean payoff:
		0.162 ± 0.0186
	Total number of recruited nodes:
		1057 ± 0

Results at turn 1104:
	Total payoff:
		177.62 ± 19.3832
	Mean payoff:
		0.1607 ± 0.0175
	Total number of recruited nodes:
		1105 ± 0

Results at turn 1152:
	Total payoff:
		184.16 ± 20.0574
	Mean payoff:
		0.1597 ± 0.0174
	Total number of recruited nodes:
		1153 ± 0

Results at turn 1200:
	Total payoff:
		189.68 ± 19.9033
	Mean payoff:
		0.1581 ± 0.0166
	Total number of recruited nodes:
		1200 ± 0


