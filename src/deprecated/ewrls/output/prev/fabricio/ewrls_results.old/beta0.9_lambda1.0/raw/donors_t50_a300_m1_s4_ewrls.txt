Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 300
    nmodels: 1
    nseeds: 4
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 12:
	Total payoff:
		7.48 ± 1.8762
	Mean payoff:
		0.5754 ± 0.1443
	Total number of recruited nodes:
		13 ± 0

Results at turn 24:
	Total payoff:
		11.12 ± 3.4561
	Mean payoff:
		0.4448 ± 0.1382
	Total number of recruited nodes:
		25 ± 0

Results at turn 36:
	Total payoff:
		13.92 ± 4.1593
	Mean payoff:
		0.3762 ± 0.1124
	Total number of recruited nodes:
		37 ± 0

Results at turn 48:
	Total payoff:
		16.1 ± 4.4687
	Mean payoff:
		0.3286 ± 0.0912
	Total number of recruited nodes:
		49 ± 0

Results at turn 60:
	Total payoff:
		18.34 ± 5.0004
	Mean payoff:
		0.3007 ± 0.082
	Total number of recruited nodes:
		61 ± 0

Results at turn 72:
	Total payoff:
		20.24 ± 5.6697
	Mean payoff:
		0.2773 ± 0.0777
	Total number of recruited nodes:
		73 ± 0

Results at turn 84:
	Total payoff:
		21.88 ± 6.1864
	Mean payoff:
		0.2574 ± 0.0728
	Total number of recruited nodes:
		85 ± 0

Results at turn 96:
	Total payoff:
		23.46 ± 6.3574
	Mean payoff:
		0.2419 ± 0.0655
	Total number of recruited nodes:
		97 ± 0

Results at turn 108:
	Total payoff:
		24.64 ± 6.6968
	Mean payoff:
		0.2261 ± 0.0614
	Total number of recruited nodes:
		109 ± 0

Results at turn 120:
	Total payoff:
		26.06 ± 6.7018
	Mean payoff:
		0.2154 ± 0.0554
	Total number of recruited nodes:
		121 ± 0

Results at turn 132:
	Total payoff:
		27.08 ± 6.6297
	Mean payoff:
		0.2036 ± 0.0498
	Total number of recruited nodes:
		133 ± 0

Results at turn 144:
	Total payoff:
		27.98 ± 6.5387
	Mean payoff:
		0.193 ± 0.0451
	Total number of recruited nodes:
		145 ± 0

Results at turn 156:
	Total payoff:
		28.98 ± 6.1095
	Mean payoff:
		0.1846 ± 0.0389
	Total number of recruited nodes:
		157 ± 0

Results at turn 168:
	Total payoff:
		29.84 ± 5.9978
	Mean payoff:
		0.1766 ± 0.0355
	Total number of recruited nodes:
		169 ± 0

Results at turn 180:
	Total payoff:
		30.88 ± 5.7805
	Mean payoff:
		0.1706 ± 0.0319
	Total number of recruited nodes:
		181 ± 0

Results at turn 192:
	Total payoff:
		31.48 ± 5.7543
	Mean payoff:
		0.1631 ± 0.0298
	Total number of recruited nodes:
		193 ± 0

Results at turn 204:
	Total payoff:
		32.48 ± 5.4669
	Mean payoff:
		0.1584 ± 0.0267
	Total number of recruited nodes:
		205 ± 0

Results at turn 216:
	Total payoff:
		33.52 ± 5.4967
	Mean payoff:
		0.1545 ± 0.0253
	Total number of recruited nodes:
		217 ± 0

Results at turn 228:
	Total payoff:
		34.78 ± 5.3768
	Mean payoff:
		0.1519 ± 0.0235
	Total number of recruited nodes:
		229 ± 0

Results at turn 240:
	Total payoff:
		35.66 ± 5.6192
	Mean payoff:
		0.148 ± 0.0233
	Total number of recruited nodes:
		241 ± 0

Results at turn 252:
	Total payoff:
		36.42 ± 5.7217
	Mean payoff:
		0.144 ± 0.0226
	Total number of recruited nodes:
		253 ± 0

Results at turn 264:
	Total payoff:
		37.38 ± 5.5875
	Mean payoff:
		0.1411 ± 0.0211
	Total number of recruited nodes:
		265 ± 0

Results at turn 276:
	Total payoff:
		38.22 ± 5.3463
	Mean payoff:
		0.138 ± 0.0193
	Total number of recruited nodes:
		277 ± 0

Results at turn 288:
	Total payoff:
		39.08 ± 5.0823
	Mean payoff:
		0.1352 ± 0.0176
	Total number of recruited nodes:
		289 ± 0

Results at turn 300:
	Total payoff:
		39.62 ± 5.0745
	Mean payoff:
		0.1321 ± 0.0169
	Total number of recruited nodes:
		300 ± 0


