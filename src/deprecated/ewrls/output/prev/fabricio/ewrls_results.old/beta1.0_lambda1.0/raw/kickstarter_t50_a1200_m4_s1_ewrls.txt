Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		16.32 ± 5.2971
	Mean payoff:
		0.3331 ± 0.1081
	Total number of recruited nodes:
		49 ± 0

Results at turn 96:
	Total payoff:
		26.5 ± 8.1472
	Mean payoff:
		0.2732 ± 0.084
	Total number of recruited nodes:
		97 ± 0

Results at turn 144:
	Total payoff:
		36.46 ± 10.0208
	Mean payoff:
		0.2514 ± 0.0691
	Total number of recruited nodes:
		145 ± 0

Results at turn 192:
	Total payoff:
		46.16 ± 11.4113
	Mean payoff:
		0.2392 ± 0.0591
	Total number of recruited nodes:
		193 ± 0

Results at turn 240:
	Total payoff:
		55.78 ± 13.7993
	Mean payoff:
		0.2315 ± 0.0573
	Total number of recruited nodes:
		241 ± 0

Results at turn 288:
	Total payoff:
		64.5 ± 14.6723
	Mean payoff:
		0.2232 ± 0.0508
	Total number of recruited nodes:
		289 ± 0

Results at turn 336:
	Total payoff:
		73.16 ± 16.2246
	Mean payoff:
		0.2171 ± 0.0481
	Total number of recruited nodes:
		337 ± 0

Results at turn 384:
	Total payoff:
		82.08 ± 18.1949
	Mean payoff:
		0.2132 ± 0.0473
	Total number of recruited nodes:
		385 ± 0

Results at turn 432:
	Total payoff:
		90.66 ± 20.3275
	Mean payoff:
		0.2094 ± 0.0469
	Total number of recruited nodes:
		433 ± 0

Results at turn 480:
	Total payoff:
		99.54 ± 22.252
	Mean payoff:
		0.2069 ± 0.0463
	Total number of recruited nodes:
		481 ± 0

Results at turn 528:
	Total payoff:
		107.4 ± 23.9114
	Mean payoff:
		0.203 ± 0.0452
	Total number of recruited nodes:
		529 ± 0

Results at turn 576:
	Total payoff:
		115.46 ± 24.99
	Mean payoff:
		0.2001 ± 0.0433
	Total number of recruited nodes:
		577 ± 0

Results at turn 624:
	Total payoff:
		123.88 ± 26.2598
	Mean payoff:
		0.1982 ± 0.042
	Total number of recruited nodes:
		625 ± 0

Results at turn 672:
	Total payoff:
		131.52 ± 27.8635
	Mean payoff:
		0.1954 ± 0.0414
	Total number of recruited nodes:
		673 ± 0

Results at turn 720:
	Total payoff:
		138.3 ± 28.1347
	Mean payoff:
		0.1918 ± 0.039
	Total number of recruited nodes:
		721 ± 0

Results at turn 768:
	Total payoff:
		145.5 ± 28.6515
	Mean payoff:
		0.1892 ± 0.0373
	Total number of recruited nodes:
		769 ± 0

Results at turn 816:
	Total payoff:
		153.58 ± 29.3731
	Mean payoff:
		0.188 ± 0.036
	Total number of recruited nodes:
		817 ± 0

Results at turn 864:
	Total payoff:
		161.36 ± 30.0671
	Mean payoff:
		0.1865 ± 0.0348
	Total number of recruited nodes:
		865 ± 0

Results at turn 912:
	Total payoff:
		169 ± 31.318
	Mean payoff:
		0.1851 ± 0.0343
	Total number of recruited nodes:
		913 ± 0

Results at turn 960:
	Total payoff:
		176 ± 32.1108
	Mean payoff:
		0.1831 ± 0.0334
	Total number of recruited nodes:
		961 ± 0

Results at turn 1008:
	Total payoff:
		182.98 ± 34.0549
	Mean payoff:
		0.1813 ± 0.0338
	Total number of recruited nodes:
		1009 ± 0

Results at turn 1056:
	Total payoff:
		189.56 ± 35.3808
	Mean payoff:
		0.1793 ± 0.0335
	Total number of recruited nodes:
		1057 ± 0

Results at turn 1104:
	Total payoff:
		196.22 ± 36.7897
	Mean payoff:
		0.1776 ± 0.0333
	Total number of recruited nodes:
		1105 ± 0

Results at turn 1152:
	Total payoff:
		203.36 ± 38.1045
	Mean payoff:
		0.1764 ± 0.033
	Total number of recruited nodes:
		1153 ± 0

Results at turn 1200:
	Total payoff:
		210.6 ± 39.3223
	Mean payoff:
		0.1755 ± 0.0328
	Total number of recruited nodes:
		1200 ± 0


