Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1000
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 40:
	Total payoff:
		13.2 ± 3.8471
	Mean payoff:
		0.322 ± 0.0938
	Total number of recruited nodes:
		41 ± 0

Results at turn 80:
	Total payoff:
		23.5 ± 6.9244
	Mean payoff:
		0.2901 ± 0.0855
	Total number of recruited nodes:
		81 ± 0

Results at turn 120:
	Total payoff:
		33.15 ± 8.4808
	Mean payoff:
		0.274 ± 0.0701
	Total number of recruited nodes:
		121 ± 0

Results at turn 160:
	Total payoff:
		43.15 ± 9.6151
	Mean payoff:
		0.268 ± 0.0597
	Total number of recruited nodes:
		161 ± 0

Results at turn 200:
	Total payoff:
		53.2 ± 12.7056
	Mean payoff:
		0.2647 ± 0.0632
	Total number of recruited nodes:
		201 ± 0

Results at turn 240:
	Total payoff:
		61.75 ± 15.3207
	Mean payoff:
		0.2562 ± 0.0636
	Total number of recruited nodes:
		241 ± 0

Results at turn 280:
	Total payoff:
		70.95 ± 19.3866
	Mean payoff:
		0.2525 ± 0.069
	Total number of recruited nodes:
		281 ± 0

Results at turn 320:
	Total payoff:
		80.1 ± 20.8955
	Mean payoff:
		0.2495 ± 0.0651
	Total number of recruited nodes:
		321 ± 0

Results at turn 360:
	Total payoff:
		89.05 ± 22.0489
	Mean payoff:
		0.2467 ± 0.0611
	Total number of recruited nodes:
		361 ± 0

Results at turn 400:
	Total payoff:
		98.15 ± 23.9325
	Mean payoff:
		0.2448 ± 0.0597
	Total number of recruited nodes:
		401 ± 0

Results at turn 440:
	Total payoff:
		107.85 ± 23.7426
	Mean payoff:
		0.2446 ± 0.0538
	Total number of recruited nodes:
		441 ± 0

Results at turn 480:
	Total payoff:
		118.45 ± 26.871
	Mean payoff:
		0.2463 ± 0.0559
	Total number of recruited nodes:
		481 ± 0

Results at turn 520:
	Total payoff:
		126.85 ± 28.6251
	Mean payoff:
		0.2435 ± 0.0549
	Total number of recruited nodes:
		521 ± 0

Results at turn 560:
	Total payoff:
		136.2 ± 29.1233
	Mean payoff:
		0.2428 ± 0.0519
	Total number of recruited nodes:
		561 ± 0

Results at turn 600:
	Total payoff:
		146.65 ± 32.1203
	Mean payoff:
		0.244 ± 0.0534
	Total number of recruited nodes:
		601 ± 0

Results at turn 640:
	Total payoff:
		155.5 ± 34.0765
	Mean payoff:
		0.2426 ± 0.0532
	Total number of recruited nodes:
		641 ± 0

Results at turn 680:
	Total payoff:
		166.15 ± 34.57
	Mean payoff:
		0.244 ± 0.0508
	Total number of recruited nodes:
		681 ± 0

Results at turn 720:
	Total payoff:
		176.9 ± 35.2075
	Mean payoff:
		0.2454 ± 0.0488
	Total number of recruited nodes:
		721 ± 0

Results at turn 760:
	Total payoff:
		186.8 ± 37.0371
	Mean payoff:
		0.2455 ± 0.0487
	Total number of recruited nodes:
		761 ± 0

Results at turn 800:
	Total payoff:
		195.6 ± 38.597
	Mean payoff:
		0.2442 ± 0.0482
	Total number of recruited nodes:
		801 ± 0

Results at turn 840:
	Total payoff:
		203.65 ± 39.3343
	Mean payoff:
		0.2422 ± 0.0468
	Total number of recruited nodes:
		841 ± 0

Results at turn 880:
	Total payoff:
		213 ± 41.4272
	Mean payoff:
		0.2418 ± 0.047
	Total number of recruited nodes:
		881 ± 0

Results at turn 920:
	Total payoff:
		222.8 ± 43.0759
	Mean payoff:
		0.2419 ± 0.0468
	Total number of recruited nodes:
		921 ± 0

Results at turn 960:
	Total payoff:
		232.05 ± 45.8894
	Mean payoff:
		0.2415 ± 0.0478
	Total number of recruited nodes:
		961 ± 0

Results at turn 1000:
	Total payoff:
		240.15 ± 48.0189
	Mean payoff:
		0.2402 ± 0.048
	Total number of recruited nodes:
		1000 ± 0


