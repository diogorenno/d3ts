Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		32.78 ± 6.3961
	Mean payoff:
		0.669 ± 0.1305
	Total number of recruited nodes:
		49 ± 0

Results at turn 96:
	Total payoff:
		64.3 ± 11.6203
	Mean payoff:
		0.6629 ± 0.1198
	Total number of recruited nodes:
		97 ± 0

Results at turn 144:
	Total payoff:
		95.54 ± 16.2408
	Mean payoff:
		0.6589 ± 0.112
	Total number of recruited nodes:
		145 ± 0

Results at turn 192:
	Total payoff:
		127.76 ± 21.3499
	Mean payoff:
		0.662 ± 0.1106
	Total number of recruited nodes:
		193 ± 0

Results at turn 240:
	Total payoff:
		160.78 ± 25.6645
	Mean payoff:
		0.6671 ± 0.1065
	Total number of recruited nodes:
		241 ± 0

Results at turn 288:
	Total payoff:
		193.46 ± 29.3092
	Mean payoff:
		0.6694 ± 0.1014
	Total number of recruited nodes:
		289 ± 0

Results at turn 336:
	Total payoff:
		226.28 ± 31.5963
	Mean payoff:
		0.6715 ± 0.0938
	Total number of recruited nodes:
		337 ± 0

Results at turn 384:
	Total payoff:
		259.58 ± 32.0287
	Mean payoff:
		0.6742 ± 0.0832
	Total number of recruited nodes:
		385 ± 0

Results at turn 432:
	Total payoff:
		291.54 ± 33.5457
	Mean payoff:
		0.6733 ± 0.0775
	Total number of recruited nodes:
		433 ± 0

Results at turn 480:
	Total payoff:
		323.4 ± 35.5729
	Mean payoff:
		0.6723 ± 0.074
	Total number of recruited nodes:
		481 ± 0

Results at turn 528:
	Total payoff:
		354.36 ± 36.7228
	Mean payoff:
		0.6699 ± 0.0694
	Total number of recruited nodes:
		529 ± 0

Results at turn 576:
	Total payoff:
		385.74 ± 38.648
	Mean payoff:
		0.6685 ± 0.067
	Total number of recruited nodes:
		577 ± 0

Results at turn 624:
	Total payoff:
		415.96 ± 40.0545
	Mean payoff:
		0.6655 ± 0.0641
	Total number of recruited nodes:
		625 ± 0

Results at turn 672:
	Total payoff:
		445.5 ± 41.1142
	Mean payoff:
		0.662 ± 0.0611
	Total number of recruited nodes:
		673 ± 0

Results at turn 720:
	Total payoff:
		476.16 ± 41.99
	Mean payoff:
		0.6604 ± 0.0582
	Total number of recruited nodes:
		721 ± 0

Results at turn 768:
	Total payoff:
		507.26 ± 41.6569
	Mean payoff:
		0.6596 ± 0.0542
	Total number of recruited nodes:
		769 ± 0

Results at turn 816:
	Total payoff:
		537.56 ± 41.332
	Mean payoff:
		0.658 ± 0.0506
	Total number of recruited nodes:
		817 ± 0

Results at turn 864:
	Total payoff:
		566.88 ± 40.2343
	Mean payoff:
		0.6554 ± 0.0465
	Total number of recruited nodes:
		865 ± 0

Results at turn 912:
	Total payoff:
		595.62 ± 40.2299
	Mean payoff:
		0.6524 ± 0.0441
	Total number of recruited nodes:
		913 ± 0

Results at turn 960:
	Total payoff:
		626.68 ± 39.5162
	Mean payoff:
		0.6521 ± 0.0411
	Total number of recruited nodes:
		961 ± 0

Results at turn 1008:
	Total payoff:
		658.18 ± 38.8076
	Mean payoff:
		0.6523 ± 0.0385
	Total number of recruited nodes:
		1009 ± 0

Results at turn 1056:
	Total payoff:
		688.72 ± 39.6485
	Mean payoff:
		0.6516 ± 0.0375
	Total number of recruited nodes:
		1057 ± 0

Results at turn 1104:
	Total payoff:
		718.62 ± 40.2674
	Mean payoff:
		0.6503 ± 0.0364
	Total number of recruited nodes:
		1105 ± 0

Results at turn 1152:
	Total payoff:
		748.18 ± 41.5401
	Mean payoff:
		0.6489 ± 0.036
	Total number of recruited nodes:
		1153 ± 0

Results at turn 1200:
	Total payoff:
		778.02 ± 43.3917
	Mean payoff:
		0.6484 ± 0.0362
	Total number of recruited nodes:
		1200 ± 0


