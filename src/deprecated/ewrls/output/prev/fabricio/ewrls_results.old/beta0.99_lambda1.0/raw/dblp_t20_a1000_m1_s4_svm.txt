Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1000
    nmodels: 1
    nseeds: 4
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 40:
	Total payoff:
		18.75 ± 5.4471
	Mean payoff:
		0.4573 ± 0.1329
	Total number of recruited nodes:
		41 ± 0

Results at turn 80:
	Total payoff:
		34.3 ± 10.2859
	Mean payoff:
		0.4235 ± 0.127
	Total number of recruited nodes:
		81 ± 0

Results at turn 120:
	Total payoff:
		54.9 ± 14.4328
	Mean payoff:
		0.4537 ± 0.1193
	Total number of recruited nodes:
		121 ± 0

Results at turn 160:
	Total payoff:
		72.6 ± 20.127
	Mean payoff:
		0.4509 ± 0.125
	Total number of recruited nodes:
		161 ± 0

Results at turn 200:
	Total payoff:
		93.05 ± 26.9082
	Mean payoff:
		0.4629 ± 0.1339
	Total number of recruited nodes:
		201 ± 0

Results at turn 240:
	Total payoff:
		114.7 ± 30.8751
	Mean payoff:
		0.4759 ± 0.1281
	Total number of recruited nodes:
		241 ± 0

Results at turn 280:
	Total payoff:
		137.9 ± 32.6334
	Mean payoff:
		0.4907 ± 0.1161
	Total number of recruited nodes:
		281 ± 0

Results at turn 320:
	Total payoff:
		157.95 ± 35.6968
	Mean payoff:
		0.4921 ± 0.1112
	Total number of recruited nodes:
		321 ± 0

Results at turn 360:
	Total payoff:
		182.8 ± 39.1873
	Mean payoff:
		0.5064 ± 0.1086
	Total number of recruited nodes:
		361 ± 0

Results at turn 400:
	Total payoff:
		204.8 ± 42.5089
	Mean payoff:
		0.5107 ± 0.106
	Total number of recruited nodes:
		401 ± 0

Results at turn 440:
	Total payoff:
		226.8 ± 46.7272
	Mean payoff:
		0.5143 ± 0.106
	Total number of recruited nodes:
		441 ± 0

Results at turn 480:
	Total payoff:
		252.45 ± 49.6424
	Mean payoff:
		0.5248 ± 0.1032
	Total number of recruited nodes:
		481 ± 0

Results at turn 520:
	Total payoff:
		276.15 ± 51.3136
	Mean payoff:
		0.53 ± 0.0985
	Total number of recruited nodes:
		521 ± 0

Results at turn 560:
	Total payoff:
		300.55 ± 51.4991
	Mean payoff:
		0.5357 ± 0.0918
	Total number of recruited nodes:
		561 ± 0

Results at turn 600:
	Total payoff:
		323.65 ± 49.2291
	Mean payoff:
		0.5385 ± 0.0819
	Total number of recruited nodes:
		601 ± 0

Results at turn 640:
	Total payoff:
		346.7 ± 47.0577
	Mean payoff:
		0.5409 ± 0.0734
	Total number of recruited nodes:
		641 ± 0

Results at turn 680:
	Total payoff:
		370.05 ± 45.6076
	Mean payoff:
		0.5434 ± 0.067
	Total number of recruited nodes:
		681 ± 0

Results at turn 720:
	Total payoff:
		392.3 ± 44.1685
	Mean payoff:
		0.5441 ± 0.0613
	Total number of recruited nodes:
		721 ± 0

Results at turn 760:
	Total payoff:
		414.2 ± 44.6325
	Mean payoff:
		0.5443 ± 0.0586
	Total number of recruited nodes:
		761 ± 0

Results at turn 800:
	Total payoff:
		435.4 ± 43.0158
	Mean payoff:
		0.5436 ± 0.0537
	Total number of recruited nodes:
		801 ± 0

Results at turn 840:
	Total payoff:
		457.55 ± 43.0856
	Mean payoff:
		0.5441 ± 0.0512
	Total number of recruited nodes:
		841 ± 0

Results at turn 880:
	Total payoff:
		480.7 ± 44.0013
	Mean payoff:
		0.5456 ± 0.0499
	Total number of recruited nodes:
		881 ± 0

Results at turn 920:
	Total payoff:
		504.15 ± 44.546
	Mean payoff:
		0.5474 ± 0.0484
	Total number of recruited nodes:
		921 ± 0

Results at turn 960:
	Total payoff:
		525.85 ± 44.6828
	Mean payoff:
		0.5472 ± 0.0465
	Total number of recruited nodes:
		961 ± 0

Results at turn 1000:
	Total payoff:
		547.8 ± 44.7268
	Mean payoff:
		0.5478 ± 0.0447
	Total number of recruited nodes:
		1000 ± 0


