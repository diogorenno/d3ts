Valid tests: 40 out of 40

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 300
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 12:
	Total payoff:
		6.85 ± 1.3691
	Mean payoff:
		0.5269 ± 0.1053
	Total number of recruited nodes:
		13 ± 0

Results at turn 24:
	Total payoff:
		11.6 ± 2.134
	Mean payoff:
		0.464 ± 0.0854
	Total number of recruited nodes:
		25 ± 0

Results at turn 36:
	Total payoff:
		14.4 ± 2.6679
	Mean payoff:
		0.3892 ± 0.0721
	Total number of recruited nodes:
		37 ± 0

Results at turn 48:
	Total payoff:
		16.275 ± 2.9956
	Mean payoff:
		0.3321 ± 0.0611
	Total number of recruited nodes:
		49 ± 0

Results at turn 60:
	Total payoff:
		18.2 ± 3.546
	Mean payoff:
		0.2984 ± 0.0581
	Total number of recruited nodes:
		61 ± 0

Results at turn 72:
	Total payoff:
		19.825 ± 3.396
	Mean payoff:
		0.2716 ± 0.0465
	Total number of recruited nodes:
		73 ± 0

Results at turn 84:
	Total payoff:
		21.35 ± 3.4682
	Mean payoff:
		0.2512 ± 0.0408
	Total number of recruited nodes:
		85 ± 0

Results at turn 96:
	Total payoff:
		22.575 ± 3.4559
	Mean payoff:
		0.2327 ± 0.0356
	Total number of recruited nodes:
		97 ± 0

Results at turn 108:
	Total payoff:
		24.225 ± 3.7994
	Mean payoff:
		0.2222 ± 0.0349
	Total number of recruited nodes:
		109 ± 0

Results at turn 120:
	Total payoff:
		25.375 ± 4.0044
	Mean payoff:
		0.2097 ± 0.0331
	Total number of recruited nodes:
		121 ± 0

Results at turn 132:
	Total payoff:
		27.175 ± 4.1068
	Mean payoff:
		0.2043 ± 0.0309
	Total number of recruited nodes:
		133 ± 0

Results at turn 144:
	Total payoff:
		29.225 ± 4.5601
	Mean payoff:
		0.2016 ± 0.0314
	Total number of recruited nodes:
		145 ± 0

Results at turn 156:
	Total payoff:
		30.725 ± 4.6464
	Mean payoff:
		0.1957 ± 0.0296
	Total number of recruited nodes:
		157 ± 0

Results at turn 168:
	Total payoff:
		32.525 ± 4.5234
	Mean payoff:
		0.1925 ± 0.0268
	Total number of recruited nodes:
		169 ± 0

Results at turn 180:
	Total payoff:
		34.125 ± 4.4732
	Mean payoff:
		0.1885 ± 0.0247
	Total number of recruited nodes:
		181 ± 0

Results at turn 192:
	Total payoff:
		35.725 ± 4.5006
	Mean payoff:
		0.1851 ± 0.0233
	Total number of recruited nodes:
		193 ± 0

Results at turn 204:
	Total payoff:
		37.475 ± 4.2966
	Mean payoff:
		0.1828 ± 0.021
	Total number of recruited nodes:
		205 ± 0

Results at turn 216:
	Total payoff:
		39.475 ± 4.0824
	Mean payoff:
		0.1819 ± 0.0188
	Total number of recruited nodes:
		217 ± 0

Results at turn 228:
	Total payoff:
		41.225 ± 4.2153
	Mean payoff:
		0.18 ± 0.0184
	Total number of recruited nodes:
		229 ± 0

Results at turn 240:
	Total payoff:
		42.85 ± 4.0859
	Mean payoff:
		0.1778 ± 0.017
	Total number of recruited nodes:
		241 ± 0

Results at turn 252:
	Total payoff:
		44.4 ± 4.448
	Mean payoff:
		0.1755 ± 0.0176
	Total number of recruited nodes:
		253 ± 0

Results at turn 264:
	Total payoff:
		45.525 ± 3.9157
	Mean payoff:
		0.1718 ± 0.0148
	Total number of recruited nodes:
		265 ± 0

Results at turn 276:
	Total payoff:
		46.55 ± 3.5515
	Mean payoff:
		0.1681 ± 0.0128
	Total number of recruited nodes:
		277 ± 0

Results at turn 288:
	Total payoff:
		47.425 ± 3.0457
	Mean payoff:
		0.1641 ± 0.0105
	Total number of recruited nodes:
		289 ± 0

Results at turn 300:
	Total payoff:
		48.175 ± 2.9516
	Mean payoff:
		0.1606 ± 0.0098
	Total number of recruited nodes:
		300 ± 0


