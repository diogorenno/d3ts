library(R6)

library(Pay2Recruit)

source("model.R")
source("problem.R")
source("settings.R")

##
 #
 ##
Simulator <- R6Class("Simulator",

    public = list(
    
        name = NA,
        settings = NA,
    
        ##
         # Initializes a new instance.
         ##
        initialize = function(name, graph, settings)
        {
          stopifnot(settings$validate())
          private$graph <- graph
          self$settings <- settings
          self$name <- name
        },
        
        ##
         #
         ##
        simulate = function(seeds, nturns)
        {
          private$restart(seeds)
          private$runTurns(nturns - private$turn)
        },
        
        ##
         # Returns results of the simulation at the given turn.
         ##
        getSimulationResultsAtTurn = function(turn)
        {
          payoff <- self$getTotalPayoffAtTurn(turn)
          nrecruited <- length(private$getRecruitedUpToTurn(turn))
          return(SimulationResult$new(payoff, nrecruited, payoff / nrecruited))
        },
        
        ##
         # Returns the total payoff of nodes recruited up to the given turn.
         ##
        getTotalPayoffAtTurn = function(turn)
        {
          total <- 0.0
          nodes <- private$getRecruitedUpToTurn(turn)
          for (i in 1:length(nodes)) {
            id <- nodes[i]
            label <- private$recruited[[id]]$label
            total <- total + label
          }
          return (total)
        },
        
        ##
         # Allows internal access to the instance for testing/debugging purposes.
         ##
        browse = function()
        {
          browser()
        }
    ),
    
    private = list(
    
        graph = NA,       # (Graph) The full graph (underlying graph).
        models = NA,      # (list) The list of models used during the recruitment process.
        recruited = NA,   # (list) A list with the IDs of all nodes recruited during the simulation.
        turn = NA,        # (integer) The simulation turn counter.
        
        NEWLY_RECRUITED = 1,
        ALREADY_RECRUITED = 2,
        EMPTY_BORDER = 3,

        ##
         # Prints a message from this simulator.
         ##
        display = function(msg)
        {
          cat(paste(self$name, ": ", msg, sep=""))
        },

        ##
         # Returns TRUE if the node with the given ID has been recruited.
         # Returns FALSE otherwise.
         ##
        hasRecruited = function(id)
        {
          return (id %in% names(private$recruited))
        },

        ##
         # Adds a new node to the recruited list.
         ##
        addRecruited = function(id, info)
        {
          stopifnot(!private$hasRecruited(id))
          private$recruited[[id]] <- list(turn = private$turn, label = info$label)
        },
        
        ##
         # Returns the IDs of all nodes recruited up to the given turn.
         ##
        getRecruitedUpToTurn = function(turn)
        {
          return (names(Filter(function(x) x$turn <= turn, private$recruited)))
        },

        ##
         # Starts the simulator.
         # Creates the models from new seeds and cold-start them with additional samples.
         ##
        restart = function(seeds)
        {
          nseeds <- self$settings$nseeds
          nmodels <- self$settings$nmodels
          stopifnot(length(seeds) == nmodels * nseeds)
        
          # Initializes internal data structures.
          private$models <- list()
          private$recruited <- list()
          private$turn <- 1
        
          # Splits the seeds among the models.
          seeds <- split(seeds, ceiling(seq_along(seeds) / (length(seeds) / nmodels)))
          private$display("Seeds:\n"); print(seeds)
          
          # Creates the models.
          private$display("Creating models...\n")
          for (i in 1:nmodels) {
            private$models[[i]] <- Model$new(self$settings)
            for (j in 1:nseeds) {
              seed_id <- seeds[[i]][j]
              seed_info <- private$getNodeInfo(seed_id)
              private$models[[i]]$recruitNode(seed_id, seed_info, border_check=FALSE)
              private$addRecruited(seed_id, seed_info)
            }
          }

          # Updates the turn counter.
          private$turn <- length(private$recruited)

          # Cold start: recruits additional nodes for each model.
          private$display("Cold-starting...\n")
          for (i in 1:self$settings$cold_pulls) {
            for (j in 1:nmodels) {
              ret <- private$recruitNextNode(model_ids=c(j))
              if (ret == private$NEWLY_RECRUITED) {
                private$turn <- 1 + private$turn
              }
            }
          }
        },
        
        ##
         #
         ##
        runTurns = function(nturns)
        {
          stopifnot(self$settings$validate())
        
          # IDs of models which are able to recruit (having non-empty border sets).
          candidates <- 1:self$settings$nmodels
        
          # Recruits the most prominent node in each turn, until there are no more turns or no more nodes.
          iteration <- 0
          final_turn <- private$turn + nturns
          while (private$turn < final_turn) {
          
            iteration <- iteration + 1
            private$display(sprintf("Turn %03d, iteration %03d\r", private$turn, iteration))

            # Stops if no more models are able to recruit.
            if (length(candidates) == 0) {
              private$display(paste("\nNo more border nodes to recruit (turn ", private$turn, ").\n", sep=""))
              break
            }
            
            # Ranks each model's border and recruits the most prominent node.
            ret <- private$recruitNextNode(model_ids=candidates)

            if (ret == private$NEWLY_RECRUITED) {
              # Advances to the next turn.
              private$turn <- 1 + private$turn
            } else if (ret == private$ALREADY_RECRUITED) {
              # Since this node had already been recruited and hence its information was already known,
              # the turn counter is not advanced.
            } else if (ret == private$EMPTY_BORDER) {
              # Removes the models from the candidates list since we can no longer recruit with them.
              candidates <- NULL
            }
          }
          private$display("\n")
        },

        ##
         #
         ##
        recruitNextNode = function(model_ids)
        {
         # Evaluates each model's border and recruits the most prominent node.
          best_node <- NA; best_model <- NA; best_payoff <- -Inf
          for (model_id in model_ids) {
            ret <- private$rankWithModel(model_id)
            if (is.na(ret)[1]) {
              # This model's border is empty.
              next
            } 
            if (ret$payoff > best_payoff) {
              best_payoff <- ret$payoff
              best_model <- model_id
              best_node <- ret$node_id
            }
          }
          if (is.na(best_node)) {
            # No more nodes to recruit.
            return (private$EMPTY_BORDER)
          }
          return (private$recruitWithModel(best_model, best_node))
        },

        ##
         #
         ##
        rankWithModel = function(model_id)
        {
          # Uses the model to estimate the payoff of its border nodes.
          rank <- private$models[[model_id]]$evaluateBorder()
          #
          if (!is.null(rank)) {
            best <- names(which.max(rank))[1]
            return (list(node_id=best, payoff=rank[best]))
          }
          # This model's border is empty.
          return (NA)
        },

        ##
         # Recruits the given node with the specified model.
         # Returns ALREADY_RECRUITED if the recruited node had already been recruited by another model.
         # Returns NEWLY_RECRUITED if the node has been recruited for the first time.
         ##
        recruitWithModel = function(model_id, node_id)
        {
          # Adds the node to the local and global models.
          info <- private$getNodeInfo(node_id)
          private$models[[model_id]]$recruitNode(node_id, info)
          
          # Checks if the node had already been recruited by another model.
          # If so, its info would be already known to us.
          if (private$hasRecruited(node_id)) {
            return (private$ALREADY_RECRUITED)
          } else {
            private$addRecruited(node_id, info)
            return (private$NEWLY_RECRUITED)
          }
        },
        
        ##
         # Wrapper for the Graph::getInfo() method.
         # Returns a list with information about the node with the given ID.
         # TODO: maybe use a cache here?
         ##
        getNodeInfo = function(character_id)
        {
          id <- as.numeric(character_id)
          info <- private$graph$getInfo(id)
          info$neighbors <- as.character(info$neighbors)
          return (info)
        }
    )
)


##
 #
 ##
SimulationResult <- R6Class("SimulationResult",

    public = list(
    
        total_payoff = NA,
        nrecruited = NA,
        mean_payoff = NA,
    
        initialize = function(total_payoff, nrecruited, mean_payoff)
        {
          self$total_payoff <- total_payoff
          self$nrecruited <- nrecruited
          self$mean_payoff <- mean_payoff
        }
    )
)

