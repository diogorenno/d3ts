source("problem.R")

PROBLEMS <- list(

    dbpedia = Problem$new("dbpedia",
        "../../../data/datasets/wang2013/dbpedia/done/dbpedia.ungraph.txt.gz",
        "../../../data/datasets/wang2013/dbpedia/done/dbpedia.attribs.txt.gz",
        accept_trait = 0, target_size = 725, nattribs = 5),

    citeseer = Problem$new("citeseer",
        "../../../data/datasets/wang2013/citeseer/done/citeseer.ungraph.txt.gz",
        "../../../data/datasets/wang2013/citeseer/done/citeseer.attribs.txt.gz",
        accept_trait = 0, target_size = 1844, nattribs = 1),

    wikipedia = Problem$new("wikipedia",
        "../../../data/datasets/wang2013/wikipedia/done/wikipedia.ungraph.txt.gz",
        "../../../data/datasets/wang2013/wikipedia/done/wikipedia.attribs.txt.gz",
        accept_trait = 48, target_size = 202, nattribs = 93),

    # ---------------------------------------------------------------------------

    donors = Problem$new("donors", 
        "../../../data/datasets/donors/donors.ungraph.txt.gz",
        "../../../data/datasets/donors/donors.attribs.txt.gz",
        accept_trait = 284, target_size = 57, nattribs = 285),
        
    dblp = Problem$new("dblp",
        "../../../data/datasets/dblp/com-dblp.ungraph.txt.gz",
        "../../../data/datasets/dblp/com-dblp.top5000.cmty.txt.gz",
        accept_trait = 4972, target_size = 7556, nattribs = 5000),
    
    amazon = Problem$new("amazon",
        "../../../data/datasets/amazon/com-amazon.ungraph.txt.gz",
        "../../../data/datasets/amazon/com-amazon.top5000.cmty.txt.gz",
        accept_trait = 4832, target_size = 328, nattribs = 5000),
    
    lj = Problem$new("lj",
        "../../../data/datasets/lj/com-lj.ungraph.txt.gz",
        "../../../data/datasets/lj/com-lj.top5000.cmty.txt.gz",
        accept_trait = 4915, target_size = 1441, nattribs = 5000),
    
    kickstarter = Problem$new("kickstarter",
        "../../../data/datasets/kickstarter/kickstarter.ungraph.txt.gz",
        "../../../data/datasets/kickstarter/kickstarter.attribs.txt.gz",
        accept_trait = 180, target_size = 1457, nattribs = 181),
        
    # ---------------------------------------------------------------------------
        
    cesna_c2a1 = Problem$new("cesna_c2a1",
        "../../cesna/datasets/002/cesna_c2a1.ungraph.run01.txt.gz",
        "../../cesna/datasets/002/cesna_c2a1.attribs.run01.txt.gz",
        accept_trait = 0, target_size = 148, nattribs = 1),
        
    cesna_c2a2 = Problem$new("cesna_c2a2",
        "../../cesna/datasets/002/cesna_c2a2.ungraph.run01.txt.gz",
        "../../cesna/datasets/002/cesna_c2a2.attribs.run01.txt.gz",
        accept_trait = 0, target_size = 264, nattribs = 2),

    cesna_c2a10 = Problem$new("cesna_c2a10",
        "../../cesna/datasets/002/cesna_c2a10.ungraph.run01.txt.gz",
        "../../cesna/datasets/002/cesna_c2a10.attribs.run01.txt.gz",
        accept_trait = 0, target_size = 247, nattribs = 10),

    cesna_c3a1 = Problem$new("cesna_c3a1",
        "../../cesna/datasets/002/cesna_c3a1.ungraph.run01.txt.gz",
        "../../cesna/datasets/002/cesna_c3a1.attribs.run01.txt.gz",
        accept_trait = 0, target_size = 140, nattribs = 1),
        
    cesna_c3a15 = Problem$new("cesna_c3a15",
        "../../cesna/datasets/002/cesna_c3a15.ungraph.run01.txt.gz",
        "../../cesna/datasets/002/cesna_c3a15.attribs.run01.txt.gz",
        accept_trait = 0, target_size = 135, nattribs = 15),

    cesna_c6a30 = Problem$new("cesna_c6a30",
        "../../cesna/datasets/002/cesna_c6a30.ungraph.run01.txt.gz",
        "../../cesna/datasets/002/cesna_c6a30.attribs.run01.txt.gz",
        accept_trait = 0, target_size = 236, nattribs = 30)
)

