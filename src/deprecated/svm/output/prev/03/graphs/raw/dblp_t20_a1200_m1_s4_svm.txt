Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1200
    nmodels: 1
    nseeds: 4
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 40:
	Total payoff:
		22.15 ± 7.5343
	Mean payoff:
		0.5402 ± 0.1838
	Total number of recruited nodes:
		41 ± 0

Results at turn 80:
	Total payoff:
		38.75 ± 16.7893
	Mean payoff:
		0.4866 ± 0.1967
	Total number of recruited nodes:
		78.7 ± 8.4111

Results at turn 120:
	Total payoff:
		58.05 ± 25.8324
	Mean payoff:
		0.4933 ± 0.1932
	Total number of recruited nodes:
		114.7 ± 19.9159

Results at turn 160:
	Total payoff:
		77.45 ± 35.589
	Mean payoff:
		0.4972 ± 0.1957
	Total number of recruited nodes:
		150.7 ± 32.0265

Results at turn 200:
	Total payoff:
		97.15 ± 43.5035
	Mean payoff:
		0.5011 ± 0.1865
	Total number of recruited nodes:
		186.7 ± 44.2482

Results at turn 240:
	Total payoff:
		119.3 ± 50.5976
	Mean payoff:
		0.5139 ± 0.1748
	Total number of recruited nodes:
		222.7 ± 56.5091

Results at turn 280:
	Total payoff:
		141.35 ± 56.498
	Mean payoff:
		0.5226 ± 0.1607
	Total number of recruited nodes:
		258.7 ± 68.7881

Results at turn 320:
	Total payoff:
		164.15 ± 64.4518
	Mean payoff:
		0.5316 ± 0.1574
	Total number of recruited nodes:
		294.7 ± 81.077

Results at turn 360:
	Total payoff:
		187.8 ± 72.5633
	Mean payoff:
		0.5409 ± 0.1549
	Total number of recruited nodes:
		330.7 ± 93.372

Results at turn 400:
	Total payoff:
		211.7 ± 80.329
	Mean payoff:
		0.5489 ± 0.1517
	Total number of recruited nodes:
		366.7 ± 105.6708

Results at turn 440:
	Total payoff:
		234.45 ± 88.3227
	Mean payoff:
		0.5529 ± 0.15
	Total number of recruited nodes:
		402.7 ± 117.9724

Results at turn 480:
	Total payoff:
		255.95 ± 96.4455
	Mean payoff:
		0.5537 ± 0.1493
	Total number of recruited nodes:
		438.7 ± 130.2759

Results at turn 520:
	Total payoff:
		276.85 ± 104.4269
	Mean payoff:
		0.5531 ± 0.1486
	Total number of recruited nodes:
		474.7 ± 142.5808

Results at turn 560:
	Total payoff:
		298.2 ± 111.4321
	Mean payoff:
		0.5535 ± 0.1454
	Total number of recruited nodes:
		510.7 ± 154.8867

Results at turn 600:
	Total payoff:
		323.35 ± 120.7011
	Mean payoff:
		0.5601 ± 0.1468
	Total number of recruited nodes:
		546.7 ± 167.1936

Results at turn 640:
	Total payoff:
		347.4 ± 129.4435
	Mean payoff:
		0.5642 ± 0.1472
	Total number of recruited nodes:
		582.7 ± 179.5011

Results at turn 680:
	Total payoff:
		369 ± 137.905
	Mean payoff:
		0.5642 ± 0.1475
	Total number of recruited nodes:
		618.7 ± 191.8091

Results at turn 720:
	Total payoff:
		390.3 ± 145.664
	Mean payoff:
		0.5638 ± 0.1465
	Total number of recruited nodes:
		654.7 ± 204.1176

Results at turn 760:
	Total payoff:
		410.85 ± 153.2787
	Mean payoff:
		0.5624 ± 0.1455
	Total number of recruited nodes:
		690.7 ± 216.4265

Results at turn 800:
	Total payoff:
		432.9 ± 160.2908
	Mean payoff:
		0.5631 ± 0.1433
	Total number of recruited nodes:
		726.7 ± 228.7357

Results at turn 840:
	Total payoff:
		455 ± 167.9825
	Mean payoff:
		0.5637 ± 0.1424
	Total number of recruited nodes:
		762.7 ± 241.0451

Results at turn 880:
	Total payoff:
		479.15 ± 176.9687
	Mean payoff:
		0.5666 ± 0.1432
	Total number of recruited nodes:
		798.7 ± 253.3548

Results at turn 920:
	Total payoff:
		502.1 ± 184.9196
	Mean payoff:
		0.568 ± 0.1426
	Total number of recruited nodes:
		834.7 ± 265.6646

Results at turn 960:
	Total payoff:
		523 ± 193.2996
	Mean payoff:
		0.5671 ± 0.143
	Total number of recruited nodes:
		870.7 ± 277.9746

Results at turn 1000:
	Total payoff:
		544.9 ± 201.3771
	Mean payoff:
		0.5673 ± 0.1428
	Total number of recruited nodes:
		906.7 ± 290.2848

Results at turn 1040:
	Total payoff:
		565.35 ± 208.0557
	Mean payoff:
		0.5661 ± 0.141
	Total number of recruited nodes:
		942.7 ± 302.5951

Results at turn 1080:
	Total payoff:
		589.35 ± 215.8426
	Mean payoff:
		0.5682 ± 0.1401
	Total number of recruited nodes:
		978.7 ± 314.9055

Results at turn 1120:
	Total payoff:
		611.9 ± 223.755
	Mean payoff:
		0.5689 ± 0.1398
	Total number of recruited nodes:
		1014.7 ± 327.216

Results at turn 1160:
	Total payoff:
		632.9 ± 230.799
	Mean payoff:
		0.5683 ± 0.1386
	Total number of recruited nodes:
		1050.7 ± 339.5266

Results at turn 1200:
	Total payoff:
		655.25 ± 238.6481
	Mean payoff:
		0.5692 ± 0.1384
	Total number of recruited nodes:
		1085.8 ± 351.5295


