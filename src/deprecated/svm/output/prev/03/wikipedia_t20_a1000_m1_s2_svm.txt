Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1000
    nmodels: 1
    nseeds: 2
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 33:
	Total payoff:
		9.6 ± 5.7161
	Mean payoff:
		0.2824 ± 0.1681
	Total number of recruited nodes:
		34 ± 0

Results at turn 66:
	Total payoff:
		18 ± 10.6326
	Mean payoff:
		0.2687 ± 0.1587
	Total number of recruited nodes:
		67 ± 0

Results at turn 100:
	Total payoff:
		26.25 ± 16.3091
	Mean payoff:
		0.2599 ± 0.1615
	Total number of recruited nodes:
		101 ± 0

Results at turn 133:
	Total payoff:
		32.55 ± 20.1376
	Mean payoff:
		0.2429 ± 0.1503
	Total number of recruited nodes:
		134 ± 0

Results at turn 166:
	Total payoff:
		36.75 ± 22.0499
	Mean payoff:
		0.2201 ± 0.132
	Total number of recruited nodes:
		167 ± 0

Results at turn 200:
	Total payoff:
		39.85 ± 23.6115
	Mean payoff:
		0.1983 ± 0.1175
	Total number of recruited nodes:
		201 ± 0

Results at turn 233:
	Total payoff:
		44.3 ± 24.8217
	Mean payoff:
		0.1893 ± 0.1061
	Total number of recruited nodes:
		234 ± 0

Results at turn 266:
	Total payoff:
		48.3 ± 26.5966
	Mean payoff:
		0.1809 ± 0.0996
	Total number of recruited nodes:
		267 ± 0

Results at turn 300:
	Total payoff:
		53.1 ± 26.8307
	Mean payoff:
		0.1764 ± 0.0891
	Total number of recruited nodes:
		301 ± 0

Results at turn 333:
	Total payoff:
		57.95 ± 27.5919
	Mean payoff:
		0.1735 ± 0.0826
	Total number of recruited nodes:
		334 ± 0

Results at turn 366:
	Total payoff:
		65.1 ± 29.6042
	Mean payoff:
		0.1774 ± 0.0807
	Total number of recruited nodes:
		367 ± 0

Results at turn 400:
	Total payoff:
		70.95 ± 31.1794
	Mean payoff:
		0.1769 ± 0.0778
	Total number of recruited nodes:
		401 ± 0

Results at turn 433:
	Total payoff:
		77 ± 31.9868
	Mean payoff:
		0.1774 ± 0.0737
	Total number of recruited nodes:
		434 ± 0

Results at turn 466:
	Total payoff:
		82.35 ± 33.8344
	Mean payoff:
		0.1763 ± 0.0725
	Total number of recruited nodes:
		467 ± 0

Results at turn 500:
	Total payoff:
		86.75 ± 35.2583
	Mean payoff:
		0.1732 ± 0.0704
	Total number of recruited nodes:
		501 ± 0

Results at turn 533:
	Total payoff:
		89.95 ± 34.8326
	Mean payoff:
		0.1684 ± 0.0652
	Total number of recruited nodes:
		534 ± 0

Results at turn 566:
	Total payoff:
		93.9 ± 35.2732
	Mean payoff:
		0.1656 ± 0.0622
	Total number of recruited nodes:
		567 ± 0

Results at turn 600:
	Total payoff:
		97.5 ± 35.8454
	Mean payoff:
		0.1622 ± 0.0596
	Total number of recruited nodes:
		601 ± 0

Results at turn 633:
	Total payoff:
		100.3 ± 36.2333
	Mean payoff:
		0.1582 ± 0.0572
	Total number of recruited nodes:
		634 ± 0

Results at turn 666:
	Total payoff:
		103.55 ± 37.0568
	Mean payoff:
		0.1552 ± 0.0556
	Total number of recruited nodes:
		667 ± 0

Results at turn 700:
	Total payoff:
		107.15 ± 37.4493
	Mean payoff:
		0.1529 ± 0.0534
	Total number of recruited nodes:
		701 ± 0

Results at turn 733:
	Total payoff:
		110.35 ± 37.563
	Mean payoff:
		0.1503 ± 0.0512
	Total number of recruited nodes:
		734 ± 0

Results at turn 766:
	Total payoff:
		112.2 ± 37.3597
	Mean payoff:
		0.1463 ± 0.0487
	Total number of recruited nodes:
		767 ± 0

Results at turn 800:
	Total payoff:
		115.8 ± 37.4582
	Mean payoff:
		0.1446 ± 0.0468
	Total number of recruited nodes:
		801 ± 0

Results at turn 833:
	Total payoff:
		119.5 ± 37.9078
	Mean payoff:
		0.1433 ± 0.0455
	Total number of recruited nodes:
		834 ± 0

Results at turn 866:
	Total payoff:
		122.3 ± 38.1163
	Mean payoff:
		0.1411 ± 0.044
	Total number of recruited nodes:
		867 ± 0

Results at turn 900:
	Total payoff:
		123.85 ± 37.9422
	Mean payoff:
		0.1375 ± 0.0421
	Total number of recruited nodes:
		901 ± 0

Results at turn 933:
	Total payoff:
		125.7 ± 37.9211
	Mean payoff:
		0.1346 ± 0.0406
	Total number of recruited nodes:
		934 ± 0

Results at turn 966:
	Total payoff:
		127 ± 38.0595
	Mean payoff:
		0.1313 ± 0.0394
	Total number of recruited nodes:
		967 ± 0

Results at turn 1000:
	Total payoff:
		128.5 ± 38.4181
	Mean payoff:
		0.1285 ± 0.0384
	Total number of recruited nodes:
		1000 ± 0


