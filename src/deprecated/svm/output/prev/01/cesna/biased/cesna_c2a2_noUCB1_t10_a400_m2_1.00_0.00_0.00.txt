Valid tests: 10 out of 10

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    model_weights: 1 0 0
    nattempts: 400
    nclusters: 1
    nmodels: 2
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: list
    use_UCB1: FALSE
    validate: function


Results at turn 16:
	Total payoff:
		10.9 ± 1.8529
	Mean payoff:
		0.6812 ± 0.1158

Results at turn 32:
	Total payoff:
		22.3 ± 2.3594
	Mean payoff:
		0.6969 ± 0.0737

Results at turn 48:
	Total payoff:
		33.2 ± 4.158
	Mean payoff:
		0.6917 ± 0.0866

Results at turn 64:
	Total payoff:
		44.4 ± 4.4522
	Mean payoff:
		0.6938 ± 0.0696

Results at turn 80:
	Total payoff:
		54.7 ± 4.4981
	Mean payoff:
		0.6838 ± 0.0562

Results at turn 96:
	Total payoff:
		65 ± 5.099
	Mean payoff:
		0.6771 ± 0.0531

Results at turn 112:
	Total payoff:
		74.6 ± 5.4406
	Mean payoff:
		0.6661 ± 0.0486

Results at turn 128:
	Total payoff:
		85.7 ± 5.3135
	Mean payoff:
		0.6695 ± 0.0415

Results at turn 144:
	Total payoff:
		96.1 ± 6.3849
	Mean payoff:
		0.6674 ± 0.0443

Results at turn 160:
	Total payoff:
		106.9 ± 6.5396
	Mean payoff:
		0.6681 ± 0.0409

Results at turn 176:
	Total payoff:
		117.6 ± 5.8916
	Mean payoff:
		0.6682 ± 0.0335

Results at turn 192:
	Total payoff:
		128.5 ± 5.4416
	Mean payoff:
		0.6693 ± 0.0283

Results at turn 208:
	Total payoff:
		138.5 ± 5.8926
	Mean payoff:
		0.6659 ± 0.0283

Results at turn 224:
	Total payoff:
		149.2 ± 5.8652
	Mean payoff:
		0.6661 ± 0.0262

Results at turn 240:
	Total payoff:
		159.6 ± 5.4201
	Mean payoff:
		0.665 ± 0.0226

Results at turn 256:
	Total payoff:
		170.2 ± 5.0509
	Mean payoff:
		0.6648 ± 0.0197

Results at turn 272:
	Total payoff:
		180.4 ± 4.9486
	Mean payoff:
		0.6632 ± 0.0182

Results at turn 288:
	Total payoff:
		191.2 ± 3.7947
	Mean payoff:
		0.6639 ± 0.0132

Results at turn 304:
	Total payoff:
		201.6 ± 4.115
	Mean payoff:
		0.6632 ± 0.0135

Results at turn 320:
	Total payoff:
		210.2 ± 3.6757
	Mean payoff:
		0.6618 ± 0.0144

Results at turn 336:
	Total payoff:
		216.1 ± 5.646
	Mean payoff:
		0.6596 ± 0.0155

Results at turn 352:
	Total payoff:
		222.2 ± 10.2827
	Mean payoff:
		0.6593 ± 0.0154

Results at turn 368:
	Total payoff:
		227.3 ± 15.0853
	Mean payoff:
		0.6614 ± 0.0142

Results at turn 384:
	Total payoff:
		230.3 ± 19.0733
	Mean payoff:
		0.6622 ± 0.0149

Results at turn 400:
	Total payoff:
		231.3 ± 20.6938
	Mean payoff:
		0.6617 ± 0.0149


