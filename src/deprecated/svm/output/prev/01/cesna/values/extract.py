#-*- coding: utf-8 -*-

#
# | renno@ubuntu ⬥ ~/Documents/ufmg/mestrado/project/renno/src/local+inter+global/output/run_03 |
# $ python ../values/extract.py *.txt
#

import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

dirs = {"biased": sys.argv[1]}

for name, path in dirs.items():

  filenames = map(lambda x: path + x, os.listdir(path))

  for filename in filenames:

    tokens = filename.split("/")[-1].split("_")
    dataset = tokens[1]
    no_UCB1 = (tokens[2] == "noUCB1")
    ntests = int(tokens[-6][1:])
    nattempts = int(tokens[-5][1:])
    nlocals = int(tokens[-4][1:])
    wlocal = float(tokens[-3])
    winter = float(tokens[-2])
    wglobal = float(tokens[-1][:-4])

    with open(filename) as handler:
    
      values = dict()
    
      for line in handler:
        tokens = line.split()
        
        if not len(tokens): 
          continue
          
        if tokens[0] == "Results":
          turn = int(tokens[3][:-1])
          
          for i, line in enumerate(handler):
            if i > 1:
              break
            if i == 1:
              tokens = line.split()
              values[turn] = {"mean": float(tokens[0]), "sd": float(tokens[2])}
          
          weights = "{}_{}_{}".format(wlocal, winter, wglobal)
          if no_UCB1:
            output_filename = "{}_noUCB1_upper_bound_{}.txt".format(dataset, name)
          else:
            assert (False)
            
          with open(output_filename, "w") as output:
            print >> output, "turn\t\tmean\t\tsd"
            for key in sorted(values.keys()):
              print >> output, "{}\t\t{}\t\t{}".format(key, values[key]["mean"], values[key]["sd"])


