Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    model_weights: 1 0 0
    nattempts: 400
    nclusters: 2
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    use_UCB1: FALSE
    validate: function


Results at turn 16:
	Total payoff:
		8.3 ± 1.6255
	Mean payoff:
		0.5188 ± 0.1016

Results at turn 32:
	Total payoff:
		12.95 ± 2.6848
	Mean payoff:
		0.4047 ± 0.0839

Results at turn 48:
	Total payoff:
		15.5 ± 3.4105
	Mean payoff:
		0.3229 ± 0.0711

Results at turn 64:
	Total payoff:
		18.6 ± 3.1855
	Mean payoff:
		0.2906 ± 0.0498

Results at turn 80:
	Total payoff:
		20.75 ± 3.1098
	Mean payoff:
		0.2594 ± 0.0389

Results at turn 96:
	Total payoff:
		22.35 ± 2.7961
	Mean payoff:
		0.2328 ± 0.0291

Results at turn 112:
	Total payoff:
		24.05 ± 2.8373
	Mean payoff:
		0.2147 ± 0.0253

Results at turn 128:
	Total payoff:
		25.5 ± 2.929
	Mean payoff:
		0.1992 ± 0.0229

Results at turn 144:
	Total payoff:
		26.55 ± 2.9643
	Mean payoff:
		0.1844 ± 0.0206

Results at turn 160:
	Total payoff:
		27.35 ± 2.7582
	Mean payoff:
		0.1709 ± 0.0172

Results at turn 176:
	Total payoff:
		27.7 ± 2.9037
	Mean payoff:
		0.1574 ± 0.0165

Results at turn 192:
	Total payoff:
		28.55 ± 2.8373
	Mean payoff:
		0.1487 ± 0.0148

Results at turn 208:
	Total payoff:
		29.8 ± 3.0018
	Mean payoff:
		0.1433 ± 0.0144

Results at turn 224:
	Total payoff:
		31.2 ± 3.3023
	Mean payoff:
		0.1393 ± 0.0147

Results at turn 240:
	Total payoff:
		32.35 ± 3.1334
	Mean payoff:
		0.1348 ± 0.0131

Results at turn 256:
	Total payoff:
		33.25 ± 3.0586
	Mean payoff:
		0.1299 ± 0.0119

Results at turn 272:
	Total payoff:
		34.3 ± 3.0625
	Mean payoff:
		0.1261 ± 0.0113

Results at turn 288:
	Total payoff:
		35.1 ± 2.9362
	Mean payoff:
		0.1219 ± 0.0102

Results at turn 304:
	Total payoff:
		36.05 ± 2.9996
	Mean payoff:
		0.1192 ± 0.0104

Results at turn 320:
	Total payoff:
		36.6 ± 3.2671
	Mean payoff:
		0.1178 ± 0.0116

Results at turn 336:
	Total payoff:
		36.75 ± 3.3067
	Mean payoff:
		0.1176 ± 0.0119

Results at turn 352:
	Total payoff:
		36.75 ± 3.3067
	Mean payoff:
		0.1176 ± 0.0119

Results at turn 368:
	Total payoff:
		36.75 ± 3.3067
	Mean payoff:
		0.1176 ± 0.0119

Results at turn 384:
	Total payoff:
		36.75 ± 3.3067
	Mean payoff:
		0.1176 ± 0.0119

Results at turn 400:
	Total payoff:
		36.75 ± 3.3067
	Mean payoff:
		0.1176 ± 0.0119


