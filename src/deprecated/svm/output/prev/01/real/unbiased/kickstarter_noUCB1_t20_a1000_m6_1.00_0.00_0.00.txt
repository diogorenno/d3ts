Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    model_weights: 1 0 0
    nattempts: 1000
    nclusters: 3
    nmodels: 6
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    use_UCB1: FALSE
    validate: function


Results at turn 40:
	Total payoff:
		14.5 ± 3.6056
	Mean payoff:
		0.3625 ± 0.0901

Results at turn 80:
	Total payoff:
		24.7 ± 3.9617
	Mean payoff:
		0.3088 ± 0.0495

Results at turn 120:
	Total payoff:
		34.5 ± 5.2365
	Mean payoff:
		0.2875 ± 0.0436

Results at turn 160:
	Total payoff:
		43.05 ± 6.1171
	Mean payoff:
		0.2691 ± 0.0382

Results at turn 200:
	Total payoff:
		51.2 ± 7.0158
	Mean payoff:
		0.256 ± 0.0351

Results at turn 240:
	Total payoff:
		61.35 ± 6.0548
	Mean payoff:
		0.2556 ± 0.0252

Results at turn 280:
	Total payoff:
		69.2 ± 5.8183
	Mean payoff:
		0.2471 ± 0.0208

Results at turn 320:
	Total payoff:
		76.9 ± 7.033
	Mean payoff:
		0.2403 ± 0.022

Results at turn 360:
	Total payoff:
		84.8 ± 6.8102
	Mean payoff:
		0.2356 ± 0.0189

Results at turn 400:
	Total payoff:
		93.2 ± 8.489
	Mean payoff:
		0.233 ± 0.0212

Results at turn 440:
	Total payoff:
		101.4 ± 9.1214
	Mean payoff:
		0.2305 ± 0.0207

Results at turn 480:
	Total payoff:
		108.85 ± 10.8204
	Mean payoff:
		0.2268 ± 0.0225

Results at turn 520:
	Total payoff:
		117.5 ± 12.2882
	Mean payoff:
		0.226 ± 0.0236

Results at turn 560:
	Total payoff:
		125.9 ± 13.8674
	Mean payoff:
		0.2248 ± 0.0248

Results at turn 600:
	Total payoff:
		134.2 ± 14.5117
	Mean payoff:
		0.2237 ± 0.0242

Results at turn 640:
	Total payoff:
		141.15 ± 15.4009
	Mean payoff:
		0.2205 ± 0.0241

Results at turn 680:
	Total payoff:
		149.35 ± 16.5475
	Mean payoff:
		0.2196 ± 0.0243

Results at turn 720:
	Total payoff:
		156.75 ± 18.2089
	Mean payoff:
		0.2177 ± 0.0253

Results at turn 760:
	Total payoff:
		163.7 ± 19.429
	Mean payoff:
		0.2154 ± 0.0256

Results at turn 800:
	Total payoff:
		170.6 ± 20.6076
	Mean payoff:
		0.2132 ± 0.0258

Results at turn 840:
	Total payoff:
		177.2 ± 19.5841
	Mean payoff:
		0.2117 ± 0.0248

Results at turn 880:
	Total payoff:
		181.8 ± 17.4163
	Mean payoff:
		0.2099 ± 0.0251

Results at turn 920:
	Total payoff:
		184.05 ± 15.558
	Mean payoff:
		0.2083 ± 0.0259

Results at turn 960:
	Total payoff:
		184.4 ± 15.3088
	Mean payoff:
		0.2079 ± 0.0263

Results at turn 1000:
	Total payoff:
		184.4 ± 15.3088
	Mean payoff:
		0.2079 ± 0.0263


