Valid tests: 15 out of 15

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    model_weights: 1 0 0
    nattempts: 1000
    nclusters: 3
    nmodels: 6
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    use_UCB1: FALSE
    validate: function


Results at turn 40:
	Total payoff:
		23.2 ± 4.0743
	Mean payoff:
		0.58 ± 0.1019

Results at turn 80:
	Total payoff:
		34.2667 ± 4.0614
	Mean payoff:
		0.4283 ± 0.0508

Results at turn 120:
	Total payoff:
		46.6667 ± 8.2433
	Mean payoff:
		0.3889 ± 0.0687

Results at turn 160:
	Total payoff:
		59.0667 ± 11.9849
	Mean payoff:
		0.3692 ± 0.0749

Results at turn 200:
	Total payoff:
		75.4 ± 17.3938
	Mean payoff:
		0.377 ± 0.087

Results at turn 240:
	Total payoff:
		94.2667 ± 23.575
	Mean payoff:
		0.3928 ± 0.0982

Results at turn 280:
	Total payoff:
		111.5333 ± 27.946
	Mean payoff:
		0.3983 ± 0.0998

Results at turn 320:
	Total payoff:
		129.1333 ± 33.1423
	Mean payoff:
		0.4035 ± 0.1036

Results at turn 360:
	Total payoff:
		151.0667 ± 33.8916
	Mean payoff:
		0.4196 ± 0.0941

Results at turn 400:
	Total payoff:
		175.9333 ± 31.7658
	Mean payoff:
		0.4398 ± 0.0794

Results at turn 440:
	Total payoff:
		199.3333 ± 33.3417
	Mean payoff:
		0.453 ± 0.0758

Results at turn 480:
	Total payoff:
		223 ± 36.4711
	Mean payoff:
		0.4646 ± 0.076

Results at turn 520:
	Total payoff:
		246 ± 38.0582
	Mean payoff:
		0.4731 ± 0.0732

Results at turn 560:
	Total payoff:
		269.2667 ± 37.2587
	Mean payoff:
		0.4808 ± 0.0665

Results at turn 600:
	Total payoff:
		293.2 ± 37.8403
	Mean payoff:
		0.4887 ± 0.0631

Results at turn 640:
	Total payoff:
		316.4 ± 36.7983
	Mean payoff:
		0.4944 ± 0.0575

Results at turn 680:
	Total payoff:
		339.4667 ± 35.7988
	Mean payoff:
		0.4992 ± 0.0526

Results at turn 720:
	Total payoff:
		362.7333 ± 33.6313
	Mean payoff:
		0.5038 ± 0.0467

Results at turn 760:
	Total payoff:
		383.6667 ± 33.1548
	Mean payoff:
		0.5048 ± 0.0436

Results at turn 800:
	Total payoff:
		406.4 ± 32.9996
	Mean payoff:
		0.508 ± 0.0412

Results at turn 840:
	Total payoff:
		429.5333 ± 34.2342
	Mean payoff:
		0.5113 ± 0.0408

Results at turn 880:
	Total payoff:
		450.8667 ± 34.3051
	Mean payoff:
		0.5123 ± 0.039

Results at turn 920:
	Total payoff:
		470.1333 ± 33.3999
	Mean payoff:
		0.5124 ± 0.0387

Results at turn 960:
	Total payoff:
		490.3333 ± 32.2372
	Mean payoff:
		0.5137 ± 0.0378

Results at turn 1000:
	Total payoff:
		509 ± 31.0644
	Mean payoff:
		0.5138 ± 0.0362


