Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1000
    nmodels: 1
    nseeds: 4
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 40:
	Total payoff:
		14.5 ± 3.3482
	Mean payoff:
		0.3537 ± 0.0817
	Total number of recruited nodes:
		41 ± 0

Results at turn 80:
	Total payoff:
		23.7 ± 5.4299
	Mean payoff:
		0.2926 ± 0.067
	Total number of recruited nodes:
		81 ± 0

Results at turn 120:
	Total payoff:
		31.35 ± 6.9151
	Mean payoff:
		0.2591 ± 0.0571
	Total number of recruited nodes:
		121 ± 0

Results at turn 160:
	Total payoff:
		39.35 ± 8.7375
	Mean payoff:
		0.2444 ± 0.0543
	Total number of recruited nodes:
		161 ± 0

Results at turn 200:
	Total payoff:
		48.3 ± 11.0839
	Mean payoff:
		0.2403 ± 0.0551
	Total number of recruited nodes:
		201 ± 0

Results at turn 240:
	Total payoff:
		57.15 ± 12.1321
	Mean payoff:
		0.2371 ± 0.0503
	Total number of recruited nodes:
		241 ± 0

Results at turn 280:
	Total payoff:
		67.05 ± 13.1928
	Mean payoff:
		0.2386 ± 0.0469
	Total number of recruited nodes:
		281 ± 0

Results at turn 320:
	Total payoff:
		74.75 ± 16.3799
	Mean payoff:
		0.2329 ± 0.051
	Total number of recruited nodes:
		321 ± 0

Results at turn 360:
	Total payoff:
		81.75 ± 19.4554
	Mean payoff:
		0.2265 ± 0.0539
	Total number of recruited nodes:
		361 ± 0

Results at turn 400:
	Total payoff:
		88.1 ± 22.4216
	Mean payoff:
		0.2197 ± 0.0559
	Total number of recruited nodes:
		401 ± 0

Results at turn 440:
	Total payoff:
		97 ± 25.67
	Mean payoff:
		0.22 ± 0.0582
	Total number of recruited nodes:
		441 ± 0

Results at turn 480:
	Total payoff:
		106.2 ± 28.1156
	Mean payoff:
		0.2208 ± 0.0585
	Total number of recruited nodes:
		481 ± 0

Results at turn 520:
	Total payoff:
		115.7 ± 30.2709
	Mean payoff:
		0.2221 ± 0.0581
	Total number of recruited nodes:
		521 ± 0

Results at turn 560:
	Total payoff:
		125.35 ± 32.9821
	Mean payoff:
		0.2234 ± 0.0588
	Total number of recruited nodes:
		561 ± 0

Results at turn 600:
	Total payoff:
		134.7 ± 37.9114
	Mean payoff:
		0.2241 ± 0.0631
	Total number of recruited nodes:
		601 ± 0

Results at turn 640:
	Total payoff:
		144 ± 41.107
	Mean payoff:
		0.2246 ± 0.0641
	Total number of recruited nodes:
		641 ± 0

Results at turn 680:
	Total payoff:
		152.2 ± 44.1047
	Mean payoff:
		0.2235 ± 0.0648
	Total number of recruited nodes:
		681 ± 0

Results at turn 720:
	Total payoff:
		160.95 ± 46.8238
	Mean payoff:
		0.2232 ± 0.0649
	Total number of recruited nodes:
		721 ± 0

Results at turn 760:
	Total payoff:
		170.2 ± 49.8868
	Mean payoff:
		0.2237 ± 0.0656
	Total number of recruited nodes:
		761 ± 0

Results at turn 800:
	Total payoff:
		178.7 ± 53.2245
	Mean payoff:
		0.2231 ± 0.0664
	Total number of recruited nodes:
		801 ± 0

Results at turn 840:
	Total payoff:
		188.25 ± 54.6114
	Mean payoff:
		0.2238 ± 0.0649
	Total number of recruited nodes:
		841 ± 0

Results at turn 880:
	Total payoff:
		197.9 ± 56.225
	Mean payoff:
		0.2246 ± 0.0638
	Total number of recruited nodes:
		881 ± 0

Results at turn 920:
	Total payoff:
		211.5 ± 58.3641
	Mean payoff:
		0.2296 ± 0.0634
	Total number of recruited nodes:
		921 ± 0

Results at turn 960:
	Total payoff:
		222.4 ± 59.6202
	Mean payoff:
		0.2314 ± 0.062
	Total number of recruited nodes:
		961 ± 0

Results at turn 1000:
	Total payoff:
		231.85 ± 61.1601
	Mean payoff:
		0.2318 ± 0.0612
	Total number of recruited nodes:
		1000 ± 0


