Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1000
    nmodels: 4
    nseeds: 1
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 40:
	Total payoff:
		19.6 ± 4.4768
	Mean payoff:
		0.478 ± 0.1092
	Total number of recruited nodes:
		41 ± 0

Results at turn 80:
	Total payoff:
		29.5 ± 7.3735
	Mean payoff:
		0.3642 ± 0.091
	Total number of recruited nodes:
		81 ± 0

Results at turn 120:
	Total payoff:
		40.3 ± 10.0896
	Mean payoff:
		0.3331 ± 0.0834
	Total number of recruited nodes:
		121 ± 0

Results at turn 160:
	Total payoff:
		55.5 ± 10.5606
	Mean payoff:
		0.3447 ± 0.0656
	Total number of recruited nodes:
		161 ± 0

Results at turn 200:
	Total payoff:
		72.7 ± 13.0146
	Mean payoff:
		0.3617 ± 0.0647
	Total number of recruited nodes:
		201 ± 0

Results at turn 240:
	Total payoff:
		89.95 ± 18.2338
	Mean payoff:
		0.3732 ± 0.0757
	Total number of recruited nodes:
		241 ± 0

Results at turn 280:
	Total payoff:
		109.05 ± 21.3504
	Mean payoff:
		0.3881 ± 0.076
	Total number of recruited nodes:
		281 ± 0

Results at turn 320:
	Total payoff:
		129.6 ± 26.5853
	Mean payoff:
		0.4037 ± 0.0828
	Total number of recruited nodes:
		321 ± 0

Results at turn 360:
	Total payoff:
		150.85 ± 33.022
	Mean payoff:
		0.4179 ± 0.0915
	Total number of recruited nodes:
		361 ± 0

Results at turn 400:
	Total payoff:
		172.25 ± 36.5699
	Mean payoff:
		0.4296 ± 0.0912
	Total number of recruited nodes:
		401 ± 0

Results at turn 440:
	Total payoff:
		195.4 ± 39.3224
	Mean payoff:
		0.4431 ± 0.0892
	Total number of recruited nodes:
		441 ± 0

Results at turn 480:
	Total payoff:
		218 ± 40.9891
	Mean payoff:
		0.4532 ± 0.0852
	Total number of recruited nodes:
		481 ± 0

Results at turn 520:
	Total payoff:
		239.85 ± 41.2454
	Mean payoff:
		0.4604 ± 0.0792
	Total number of recruited nodes:
		521 ± 0

Results at turn 560:
	Total payoff:
		261.4 ± 42.7002
	Mean payoff:
		0.466 ± 0.0761
	Total number of recruited nodes:
		561 ± 0

Results at turn 600:
	Total payoff:
		285.1 ± 44.8857
	Mean payoff:
		0.4744 ± 0.0747
	Total number of recruited nodes:
		601 ± 0

Results at turn 640:
	Total payoff:
		307.25 ± 46.4621
	Mean payoff:
		0.4793 ± 0.0725
	Total number of recruited nodes:
		641 ± 0

Results at turn 680:
	Total payoff:
		329.5 ± 47.6903
	Mean payoff:
		0.4838 ± 0.07
	Total number of recruited nodes:
		681 ± 0

Results at turn 720:
	Total payoff:
		350.4 ± 47.3413
	Mean payoff:
		0.486 ± 0.0657
	Total number of recruited nodes:
		721 ± 0

Results at turn 760:
	Total payoff:
		371.85 ± 48.0376
	Mean payoff:
		0.4886 ± 0.0631
	Total number of recruited nodes:
		761 ± 0

Results at turn 800:
	Total payoff:
		394.85 ± 48.0726
	Mean payoff:
		0.4929 ± 0.06
	Total number of recruited nodes:
		801 ± 0

Results at turn 840:
	Total payoff:
		417.65 ± 47.0132
	Mean payoff:
		0.4966 ± 0.0559
	Total number of recruited nodes:
		841 ± 0

Results at turn 880:
	Total payoff:
		438.4 ± 45.3888
	Mean payoff:
		0.4976 ± 0.0515
	Total number of recruited nodes:
		881 ± 0

Results at turn 920:
	Total payoff:
		461.6 ± 44.3875
	Mean payoff:
		0.5012 ± 0.0482
	Total number of recruited nodes:
		921 ± 0

Results at turn 960:
	Total payoff:
		484.1 ± 43.5502
	Mean payoff:
		0.5037 ± 0.0453
	Total number of recruited nodes:
		961 ± 0

Results at turn 1000:
	Total payoff:
		505.35 ± 41.6581
	Mean payoff:
		0.5054 ± 0.0417
	Total number of recruited nodes:
		1000 ± 0


