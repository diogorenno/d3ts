library(R6)

source("model_selector.R")

##
 # 
 ##
ArmSelector <- R6Class("ArmSelector",
                 
    public = list(
     
        ##
         # Initializes a new selector instance.
         ##
        initialize = function(settings=NA, models=NA, clusters=NA)
        {
          private$settings <- settings
          private$models <- models
          private$nclusters <- length(clusters)
          
          private$mab_policy <- settings$mab_policy$clone()
          private$mab_policy$configure(private$nclusters)
          
          private$model_selector <- ModelSelector$new(settings, clusters)
          private$action_selector <- settings$action_selector
        },

        ##
         # Returns the ID of the border node selected by the underlying policy.
         # If the border set is empty, returns NA.
         ##
        selectNextAction = function(simulation_state=NA)
        {
          #
          selected_arm <- private$mab_policy$selectArm(simulation_state)
          selected_model <- private$model_selector$selectModelFromCluster(cluster_id=selected_arm)
          scores <- private$getModelScores(selected_model)
          
          # Checks if the border set is empty.
          if (is.null(scores)) return (NA)
          
          # Selects the next action based on the mapping of the scores.
          action <- private$action_selector$selectAction(scores, simulation_state)
          selected_node <- action$action_id

          return (list(node_id = selected_node,
                       model_id = selected_model,
                       arm_id = selected_arm))
        },
        
        ##
         # Registers feedback for the given model.
         ##
        addFeedback = function(arm_id=NA, model_id=NA, correct=NA)
        {
          stopifnot(is.logical(correct) && (!is.na(correct)))
          stopifnot((arm_id >= 1) && (arm_id <= private$nclusters))
          stopifnot((model_id >= 1) && (model_id <= private$settings$nmodels))
          
          private$mab_policy$addFeedback(arm_id, correct)
          private$model_selector$addFeedback(model_id, correct)
        }
    ),
    
    private = list(
      
        settings = NA,              # (Settings) The simulation settings.
        models = NA,                # (list<Model>) A list referencing the same models as that in the Simulator.
        nclusters = NA,             # (integer > 0) The current number of clusters.
        mab_policy = NA,            # A reference to an initialized instance of a MAB policy class.
        action_selector = NA,       # (ActionSelector) A reference to an initialized instance of the ActionSelector class.
        model_selector = NA,        # (ModelSelector) A reference to an initialized instance of the ModelSelector class.
        
        ##
         # Gets the scores of all border nodes, as estimated by the specified model.
         # Returns NA if the border set is found to be empty.
         ##
        getModelScores = function(model_id=NA)
        {
          rank <- private$models[[model_id]]$evaluateBorder()
          if (is.null(rank)) {return (NULL)}
          return (rank)
        }
    )
)
