library(R6)
library(RBGL)

source("temp/rr.R")

##
 # selector class.
 # ###
 ##
RoundRobinModelSelector <- R6Class("RoundRobinModelSelector",
                          
    public = list(
      
        # (list<list<numeric>>) Entry border_scores[[t]][[m]] holds the scores given by model m to each border node at turn t.
        border_scores = NA,
        
        ##
         # Initializes a new round-robin selector instance.
         ##
        initialize = function(settings=NA, models=NA)
        {
          private$models <- models
          private$settings <- settings
          self$border_scores <- list()
          
          private$selector <- PolicyRR$new()
          private$selector$init(private$settings)
        },
        
        ##
         # #####
         ##
        selectNextAction = function(simulation_state=NA)
        {
          # Selects which models will be considered for action selection.
          model_ids = 1:private$settings$nmodels
          selected_models <- private$selector$selectModels(model_ids, simulation_state)
          
          # Evaluates the border set using the models.
          evals <- Map(function(model_id) private$getModelScores(model_id), model_ids)
          
          # Checks if the border set is empty.
          if (any(unlist(Map(function(x) is.null(x), evals)))) {
            stop("Border set is empty.")
          }
          
          # Selects the next action using the policy.
          action <- private$selector$selectAction(evals[selected_models], simulation_state)
          selected_model <- model_ids[selected_models[action$vector_id]]
          selected_node <- action$action_id
          
          # #####
          turn_scores <- as.list(rep(NA, length(private$models)))
          for (m in 1:length(model_ids)) {
            turn_scores[[model_ids[m]]] <- evals[[m]]
          }
          self$border_scores[[simulation_state$current_turn]] <- turn_scores
          
          return (list(node_id = selected_node,
                       model_id = selected_model,
                       agreers = selected_model))
        },
        
        ##
         #
         ##
        getCorrelatedModels = function()
        {
          cgraph <- private$getCorrelationGraph()
          
          # clusters <- private$hcs(cgraph, private$settings$min_cor)
          clusters <- private$ccPivot(cgraph, private$settings$min_cor)
          
          return (clusters)
        }
    ),
    
    private = list(
      
        settings = NA,          # (Settings) The simulation settings.
        models = NA,            # (list<Model>) A list referencing the same models as that in the Simulator.
        selector = NA,          # A reference to an initialized instance of a supported model selector class.
        
        ##
         # Gets the scores of all border nodes, as estimated by the specified model.
         # Returns NA if the border set is found to be empty.
         ##
        getModelScores = function(model_id=NA)
        {
          rank <- private$models[[model_id]]$evaluateBorder()
          if (is.null(rank)) {return (NULL)}
          return (rank)
        },
        
        ##
         # Returns a list with the indices of all models in private$models which are currently valid.
         ##
        getValidModels = function()
        {
          return (unlist(Filter(function(i) private$models[[i]]$isValid(),
                                1:private$settings$nmodels)))
        },
        
        ##
         #
         ##
        hcs = function(graph=NA, threshold=NA)
        {
          # TODO: the graph must be processed so as to remove the necessary edges.
          # clusters <- Map(as.numeric, highlyConnSG(cgraph)$clusters)
        },
        
        ##
         #
         ##
        ccPivot = function(graph=NA, threshold=NA)
        {
          clusters <- list()
          g <- graph
          while (length(unlist(clusters)) < length(graph::nodes(graph))) {
            v <- graph::nodes(g)
            pivot <- sample(v, 1)
            weights <- edgeWeights(g)[[pivot]]
            cluster <- c(pivot, names(unlist(Filter(function(w) w > threshold, weights))))
            clusters[[1+length(clusters)]] <- cluster
            g <- removeNode(cluster, g)
          }
          return (Map(as.numeric, clusters))
        },

        ##
         #
         ##
        getCorrelationGraph = function()
        {
          cmatrix <- private$getCorrelationMatrix()
          cgraph <- graphNEL(nodes=character(), edgeL=list(), edgemode="undirected")
          cgraph <- addNode(as.character(1:private$settings$nmodels), cgraph)
          for (m1 in 1:private$settings$nmodels) {
            for (m2 in 1:private$settings$nmodels) {
              if (m2 > m1) {
                cgraph <- addEdge(as.character(m1), as.character(m2), graph=cgraph, weights=cmatrix[m1,m2])
              }
            }
          }
          return (cgraph)
        },

        ##
         #
         ##
        getCorrelationMatrix = function()
        {
          series <- private$getCorrelationSeries()
          
          # nrecent <- 10
          # series <- series[, (ncol(series)-nrecent+1):ncol(series)]
          # correlations <- apply(series, 1, mean)
          
          w <- rep(0.9, ncol(series)) ^ seq(ncol(series)-1, 0)
          correlations <- apply(series, 1, function(r) sum(r*w)/sum(w))
          
          squared <- matrix(as.vector(t(correlations)), nrow=private$settings$nmodels, byrow=TRUE)
          return (squared)
        },
        
        ##
         #
         ##
        getCorrelationSeries = function()
        {
          nturns <- length(self$border_scores)
          nmodels <- private$settings$nmodels
          
          stopifnot(nturns > 0)
          stopifnot(nmodels > 1)
          
          correlations <- matrix(NA, ncol=nturns, nrow=nmodels*nmodels)
          
          # Calculates pairwise ranking correlations along the turns.
          for (turn in 1:nturns) {
            scores <- self$border_scores[[turn]]
            # Asserts appropriate rankings from all models are available at this turn.
            if (is.null(scores)) next
            if (any(is.na(scores))) next
            if (length(scores[[1]]) < 3) next
            # Calculates all pairwise correlations at the current turn.
            ridx <- 0
            for (midx1 in 1:nmodels) {
              for (midx2 in 1:nmodels) {
                ridx <- 1 + ridx
                correlations[ridx,turn] <- suppressWarnings(cor.test(scores[[midx1]],
                                                                     scores[[midx2]],
                                                                     method="spearman",
                                                                     alternative="two.sided")$estimate)
              }
            }
          }

          # Selects only the turns in which scores from all models were available.
          correlations <- correlations[, unlist(Filter(function(tidx) !any(is.na(correlations[,tidx])), 1:nturns))]
          return (correlations)
        }
    )
)
