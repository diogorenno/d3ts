
datasets <- c("c2a1", "c2a2", "c2a10", "c3a1", "c3a15", "c6a30")
nmodels <- list(2, 2, 2, 3, 3, 6)

colors <- rainbow(3)

for (i in 1:length(datasets)) {

  pdf(sprintf("%s.pdf", datasets[i]), height=8, width=8)
  par(mfrow=c(1, 1))

  singles <- unlist(Map(function(x) sprintf("%s_1_%d_%s.txt", datasets[i], nmodels[[i]], x), c("prediction", "UB-local", "UB-global")))
  locals  <- unlist(Map(function(x) sprintf("%s_%d_1_%s.txt", datasets[i], nmodels[[i]], x), c("prediction", "UB-local", "UB-global")))
  
  labels <- unlist(Map(
      function(x) c(sprintf("Single model, %d seeds, %s", nmodels[[i]], x), sprintf("%d local models, %s", nmodels[[i]], x)), 
      c("prediction", "local UB", "global UB")))

  legend_colors <- unlist(Map(function(x) c(x, x), colors))
  legend_ltys <- rep(c("solid", "dashed"), length(singles))

  for (j in 1:length(singles)) {
    both <- list(read.table(singles[j], header=TRUE), read.table(locals[j], header=TRUE))
    for (k in 1:2) {
      data <- both[[k]]
      if ((j == 1) & (k == 1)) {
        ylim = c(0, max(80, 1.1 * max(data$mean)))
        plot(data$turn, data$mean, type="l", lwd=1.3, lty="solid", col=colors[j], main=datasets[i], xlab="", ylab="", ylim=ylim)
        title(xlab="Turn", ylab="Number of recruited targets")
        legend(0.05*max(data$turn), 0.95*ylim[2], labels, lty=legend_ltys, col=legend_colors)
      } else {
        if (k == 1) {
          lines(data$turn, data$mean, col=colors[j], lwd=1.3, lty="solid")
        } else {
          lines(data$turn, data$mean, col=colors[j], lwd=1.5, lty="dashed")
        }
      }
    }
  }

  ###############################################################################

  par(mfrow=c(2, 2))  
  filenames <- append(singles, locals)
  for (j in 1:length(filenames)) {
    data <- read.table(filenames[j], header=TRUE)
    ylim <- c(0, max(data$mean) + max(data$sd))
    plot(data$turn, data$mean, type="p", main=labels[j], ylim=ylim, xlab="", ylab="")
    title(xlab="Turn", ylab="Number of recruited targets")
    for (k in 1:length(data$turn)) {
      min_val <- max(0, data[k,]$mean - data[k,]$sd)
      max_val <- data[k,]$mean + data[k,]$sd
      lines(c(data$turn[k], data$turn[k]), c(min_val, max_val))
    }
  }
}

dev.off()

