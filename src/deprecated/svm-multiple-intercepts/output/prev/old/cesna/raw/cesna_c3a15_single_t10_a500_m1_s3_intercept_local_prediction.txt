Valid tests: 10 out of 10

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 500
    nmodels: 1
    nseeds: 3
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: list
    validate: function


Results at turn 20:
	Total payoff:
		11 ± 2.4944
	Mean payoff:
		0.2895 ± 0.0656
	Total number of recruited nodes:
		38 ± 0

Results at turn 40:
	Total payoff:
		16.7 ± 3.9735
	Mean payoff:
		0.2879 ± 0.0685
	Total number of recruited nodes:
		58 ± 0

Results at turn 60:
	Total payoff:
		21.5 ± 4.17
	Mean payoff:
		0.2756 ± 0.0535
	Total number of recruited nodes:
		78 ± 0

Results at turn 80:
	Total payoff:
		27 ± 6.5149
	Mean payoff:
		0.2755 ± 0.0665
	Total number of recruited nodes:
		98 ± 0

Results at turn 100:
	Total payoff:
		33 ± 6.2893
	Mean payoff:
		0.2797 ± 0.0533
	Total number of recruited nodes:
		118 ± 0

Results at turn 120:
	Total payoff:
		39.6 ± 5.9479
	Mean payoff:
		0.287 ± 0.0431
	Total number of recruited nodes:
		138 ± 0

Results at turn 140:
	Total payoff:
		44.6 ± 5.7966
	Mean payoff:
		0.2823 ± 0.0367
	Total number of recruited nodes:
		158 ± 0

Results at turn 160:
	Total payoff:
		51.3 ± 6.5836
	Mean payoff:
		0.2882 ± 0.037
	Total number of recruited nodes:
		178 ± 0

Results at turn 180:
	Total payoff:
		57.3 ± 7.15
	Mean payoff:
		0.2894 ± 0.0361
	Total number of recruited nodes:
		198 ± 0

Results at turn 200:
	Total payoff:
		63.5 ± 5.7591
	Mean payoff:
		0.2913 ± 0.0264
	Total number of recruited nodes:
		218 ± 0

Results at turn 220:
	Total payoff:
		68.8 ± 5.2873
	Mean payoff:
		0.2891 ± 0.0222
	Total number of recruited nodes:
		238 ± 0

Results at turn 240:
	Total payoff:
		74 ± 4.899
	Mean payoff:
		0.2868 ± 0.019
	Total number of recruited nodes:
		258 ± 0

Results at turn 260:
	Total payoff:
		78.6 ± 5.3583
	Mean payoff:
		0.2827 ± 0.0193
	Total number of recruited nodes:
		278 ± 0

Results at turn 280:
	Total payoff:
		83.9 ± 4.7481
	Mean payoff:
		0.2815 ± 0.0159
	Total number of recruited nodes:
		298 ± 0

Results at turn 300:
	Total payoff:
		88.5 ± 3.9511
	Mean payoff:
		0.2783 ± 0.0124
	Total number of recruited nodes:
		318 ± 0

Results at turn 320:
	Total payoff:
		93.4 ± 3.6576
	Mean payoff:
		0.2763 ± 0.0108
	Total number of recruited nodes:
		338 ± 0

Results at turn 340:
	Total payoff:
		97.9 ± 3.1429
	Mean payoff:
		0.2735 ± 0.0088
	Total number of recruited nodes:
		358 ± 0

Results at turn 360:
	Total payoff:
		102.9 ± 2.5144
	Mean payoff:
		0.2722 ± 0.0067
	Total number of recruited nodes:
		378 ± 0

Results at turn 380:
	Total payoff:
		108.4 ± 2.4129
	Mean payoff:
		0.2724 ± 0.0061
	Total number of recruited nodes:
		398 ± 0

Results at turn 400:
	Total payoff:
		113.7 ± 2.1108
	Mean payoff:
		0.272 ± 0.005
	Total number of recruited nodes:
		418 ± 0

Results at turn 420:
	Total payoff:
		116.8 ± 1.8135
	Mean payoff:
		0.2667 ± 0.0041
	Total number of recruited nodes:
		438 ± 0

Results at turn 440:
	Total payoff:
		120.4 ± 1.7127
	Mean payoff:
		0.2629 ± 0.0037
	Total number of recruited nodes:
		458 ± 0

Results at turn 460:
	Total payoff:
		122.5 ± 1.4337
	Mean payoff:
		0.2563 ± 0.003
	Total number of recruited nodes:
		478 ± 0

Results at turn 480:
	Total payoff:
		125.2 ± 1.8738
	Mean payoff:
		0.2514 ± 0.0038
	Total number of recruited nodes:
		498 ± 0

Results at turn 500:
	Total payoff:
		127 ± 1.3333
	Mean payoff:
		0.2452 ± 0.0026
	Total number of recruited nodes:
		518 ± 0


