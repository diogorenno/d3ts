Valid tests: 10 out of 10

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 400
    nmodels: 1
    nseeds: 2
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: list
    validate: function


Results at turn 16:
	Total payoff:
		11.3 ± 2.6687
	Mean payoff:
		0.4036 ± 0.0953
	Total number of recruited nodes:
		28 ± 0

Results at turn 32:
	Total payoff:
		16.6 ± 3.3066
	Mean payoff:
		0.3773 ± 0.0751
	Total number of recruited nodes:
		44 ± 0

Results at turn 48:
	Total payoff:
		23.5 ± 3.44
	Mean payoff:
		0.3917 ± 0.0573
	Total number of recruited nodes:
		60 ± 0

Results at turn 64:
	Total payoff:
		29.5 ± 2.8382
	Mean payoff:
		0.3882 ± 0.0373
	Total number of recruited nodes:
		76 ± 0

Results at turn 80:
	Total payoff:
		36 ± 3.3993
	Mean payoff:
		0.3913 ± 0.0369
	Total number of recruited nodes:
		92 ± 0

Results at turn 96:
	Total payoff:
		41.7 ± 3.653
	Mean payoff:
		0.3861 ± 0.0338
	Total number of recruited nodes:
		108 ± 0

Results at turn 112:
	Total payoff:
		46.9 ± 2.9231
	Mean payoff:
		0.3782 ± 0.0236
	Total number of recruited nodes:
		124 ± 0

Results at turn 128:
	Total payoff:
		53.8 ± 2.6998
	Mean payoff:
		0.3843 ± 0.0193
	Total number of recruited nodes:
		140 ± 0

Results at turn 144:
	Total payoff:
		60.7 ± 3.164
	Mean payoff:
		0.3891 ± 0.0203
	Total number of recruited nodes:
		156 ± 0

Results at turn 160:
	Total payoff:
		67 ± 2.7889
	Mean payoff:
		0.3895 ± 0.0162
	Total number of recruited nodes:
		172 ± 0

Results at turn 176:
	Total payoff:
		74.5 ± 1.354
	Mean payoff:
		0.3963 ± 0.0072
	Total number of recruited nodes:
		188 ± 0

Results at turn 192:
	Total payoff:
		81.4 ± 2.319
	Mean payoff:
		0.399 ± 0.0114
	Total number of recruited nodes:
		204 ± 0

Results at turn 208:
	Total payoff:
		87.5 ± 2.6771
	Mean payoff:
		0.3977 ± 0.0122
	Total number of recruited nodes:
		220 ± 0

Results at turn 224:
	Total payoff:
		94.2 ± 2.6998
	Mean payoff:
		0.3992 ± 0.0114
	Total number of recruited nodes:
		236 ± 0

Results at turn 240:
	Total payoff:
		99.9 ± 2.6854
	Mean payoff:
		0.3964 ± 0.0107
	Total number of recruited nodes:
		252 ± 0

Results at turn 256:
	Total payoff:
		106.4 ± 2.9889
	Mean payoff:
		0.397 ± 0.0112
	Total number of recruited nodes:
		268 ± 0

Results at turn 272:
	Total payoff:
		112.1 ± 3.1429
	Mean payoff:
		0.3947 ± 0.0111
	Total number of recruited nodes:
		284 ± 0

Results at turn 288:
	Total payoff:
		119 ± 1.8856
	Mean payoff:
		0.3967 ± 0.0063
	Total number of recruited nodes:
		300 ± 0

Results at turn 304:
	Total payoff:
		124.7 ± 1.8886
	Mean payoff:
		0.3946 ± 0.006
	Total number of recruited nodes:
		316 ± 0

Results at turn 320:
	Total payoff:
		129.5 ± 2.4608
	Mean payoff:
		0.3901 ± 0.0074
	Total number of recruited nodes:
		332 ± 0

Results at turn 336:
	Total payoff:
		134.8 ± 2.2998
	Mean payoff:
		0.3874 ± 0.0066
	Total number of recruited nodes:
		348 ± 0

Results at turn 352:
	Total payoff:
		139.2 ± 0.7888
	Mean payoff:
		0.3824 ± 0.0022
	Total number of recruited nodes:
		364 ± 0

Results at turn 368:
	Total payoff:
		143.4 ± 0.9661
	Mean payoff:
		0.3774 ± 0.0025
	Total number of recruited nodes:
		380 ± 0

Results at turn 384:
	Total payoff:
		147.2 ± 0.6325
	Mean payoff:
		0.3717 ± 0.0016
	Total number of recruited nodes:
		396 ± 0

Results at turn 400:
	Total payoff:
		148 ± 0
	Mean payoff:
		0.37 ± 0
	Total number of recruited nodes:
		400 ± 0


