Valid tests: 10 out of 10

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: upper_bound
    nattempts: 1000
    nmodels: 1
    nseeds: 6
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: list
    validate: function


Results at turn 40:
	Total payoff:
		21.5 ± 2.4152
	Mean payoff:
		0.2829 ± 0.0318
	Total number of recruited nodes:
		76 ± 0

Results at turn 80:
	Total payoff:
		30 ± 2.4495
	Mean payoff:
		0.2586 ± 0.0211
	Total number of recruited nodes:
		116 ± 0

Results at turn 120:
	Total payoff:
		40.2 ± 4.9171
	Mean payoff:
		0.2577 ± 0.0315
	Total number of recruited nodes:
		156 ± 0

Results at turn 160:
	Total payoff:
		48.9 ± 5.5066
	Mean payoff:
		0.2495 ± 0.0281
	Total number of recruited nodes:
		196 ± 0

Results at turn 200:
	Total payoff:
		59.7 ± 5.2504
	Mean payoff:
		0.253 ± 0.0222
	Total number of recruited nodes:
		236 ± 0

Results at turn 240:
	Total payoff:
		72.2 ± 6.2147
	Mean payoff:
		0.2616 ± 0.0225
	Total number of recruited nodes:
		276 ± 0

Results at turn 280:
	Total payoff:
		83.6 ± 6.4498
	Mean payoff:
		0.2646 ± 0.0204
	Total number of recruited nodes:
		316 ± 0

Results at turn 320:
	Total payoff:
		95.3 ± 6.8969
	Mean payoff:
		0.2677 ± 0.0194
	Total number of recruited nodes:
		356 ± 0

Results at turn 360:
	Total payoff:
		105.4 ± 7.0742
	Mean payoff:
		0.2662 ± 0.0179
	Total number of recruited nodes:
		396 ± 0

Results at turn 400:
	Total payoff:
		115.2 ± 6.7626
	Mean payoff:
		0.2642 ± 0.0155
	Total number of recruited nodes:
		436 ± 0

Results at turn 440:
	Total payoff:
		125.6 ± 3.7178
	Mean payoff:
		0.2639 ± 0.0078
	Total number of recruited nodes:
		476 ± 0

Results at turn 480:
	Total payoff:
		138.4 ± 5.2111
	Mean payoff:
		0.2682 ± 0.0101
	Total number of recruited nodes:
		516 ± 0

Results at turn 520:
	Total payoff:
		149.7 ± 5.7937
	Mean payoff:
		0.2692 ± 0.0104
	Total number of recruited nodes:
		556 ± 0

Results at turn 560:
	Total payoff:
		159.5 ± 5.4212
	Mean payoff:
		0.2676 ± 0.0091
	Total number of recruited nodes:
		596 ± 0

Results at turn 600:
	Total payoff:
		169 ± 4.4472
	Mean payoff:
		0.2657 ± 0.007
	Total number of recruited nodes:
		636 ± 0

Results at turn 640:
	Total payoff:
		178.8 ± 4.1312
	Mean payoff:
		0.2645 ± 0.0061
	Total number of recruited nodes:
		676 ± 0

Results at turn 680:
	Total payoff:
		187.9 ± 3.6347
	Mean payoff:
		0.2624 ± 0.0051
	Total number of recruited nodes:
		716 ± 0

Results at turn 720:
	Total payoff:
		196.3 ± 2.7909
	Mean payoff:
		0.2597 ± 0.0037
	Total number of recruited nodes:
		756 ± 0

Results at turn 760:
	Total payoff:
		202.9 ± 3.7253
	Mean payoff:
		0.2549 ± 0.0047
	Total number of recruited nodes:
		796 ± 0

Results at turn 800:
	Total payoff:
		207.5 ± 3.6591
	Mean payoff:
		0.2482 ± 0.0044
	Total number of recruited nodes:
		836 ± 0

Results at turn 840:
	Total payoff:
		211.3 ± 2.9458
	Mean payoff:
		0.2412 ± 0.0034
	Total number of recruited nodes:
		876 ± 0

Results at turn 880:
	Total payoff:
		215.4 ± 2.2706
	Mean payoff:
		0.2352 ± 0.0025
	Total number of recruited nodes:
		916 ± 0

Results at turn 920:
	Total payoff:
		219.4 ± 1.7127
	Mean payoff:
		0.2295 ± 0.0018
	Total number of recruited nodes:
		956 ± 0

Results at turn 960:
	Total payoff:
		224 ± 2.1082
	Mean payoff:
		0.2249 ± 0.0021
	Total number of recruited nodes:
		996 ± 0

Results at turn 1000:
	Total payoff:
		226.5 ± 1.9579
	Mean payoff:
		0.2186 ± 0.0019
	Total number of recruited nodes:
		1036 ± 0


