Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 1200
    nmodels: 1
    nseeds: 4
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		33.9 ± 12.6529
	Mean payoff:
		0.4708 ± 0.1757
	Total number of recruited nodes:
		72 ± 0

Results at turn 96:
	Total payoff:
		57.7 ± 20.2565
	Mean payoff:
		0.4808 ± 0.1688
	Total number of recruited nodes:
		120 ± 0

Results at turn 144:
	Total payoff:
		83.55 ± 25.9097
	Mean payoff:
		0.4973 ± 0.1542
	Total number of recruited nodes:
		168 ± 0

Results at turn 192:
	Total payoff:
		112 ± 32.1739
	Mean payoff:
		0.5185 ± 0.149
	Total number of recruited nodes:
		216 ± 0

Results at turn 240:
	Total payoff:
		144.4 ± 34.7236
	Mean payoff:
		0.547 ± 0.1315
	Total number of recruited nodes:
		264 ± 0

Results at turn 288:
	Total payoff:
		174.05 ± 37.4496
	Mean payoff:
		0.5579 ± 0.12
	Total number of recruited nodes:
		312 ± 0

Results at turn 336:
	Total payoff:
		204.2 ± 40.5224
	Mean payoff:
		0.5672 ± 0.1126
	Total number of recruited nodes:
		360 ± 0

Results at turn 384:
	Total payoff:
		238.6 ± 42.2628
	Mean payoff:
		0.5848 ± 0.1036
	Total number of recruited nodes:
		408 ± 0

Results at turn 432:
	Total payoff:
		271.8 ± 43.6645
	Mean payoff:
		0.5961 ± 0.0958
	Total number of recruited nodes:
		456 ± 0

Results at turn 480:
	Total payoff:
		304.5 ± 43.3365
	Mean payoff:
		0.6042 ± 0.086
	Total number of recruited nodes:
		504 ± 0

Results at turn 528:
	Total payoff:
		337.35 ± 42.6519
	Mean payoff:
		0.6111 ± 0.0773
	Total number of recruited nodes:
		552 ± 0

Results at turn 576:
	Total payoff:
		368.95 ± 40.8327
	Mean payoff:
		0.6149 ± 0.0681
	Total number of recruited nodes:
		600 ± 0

Results at turn 624:
	Total payoff:
		399.5 ± 41.7278
	Mean payoff:
		0.6165 ± 0.0644
	Total number of recruited nodes:
		648 ± 0

Results at turn 672:
	Total payoff:
		428.65 ± 41.4682
	Mean payoff:
		0.6159 ± 0.0596
	Total number of recruited nodes:
		696 ± 0

Results at turn 720:
	Total payoff:
		457.9 ± 43.1349
	Mean payoff:
		0.6155 ± 0.058
	Total number of recruited nodes:
		744 ± 0

Results at turn 768:
	Total payoff:
		486.3 ± 43.9989
	Mean payoff:
		0.614 ± 0.0556
	Total number of recruited nodes:
		792 ± 0

Results at turn 816:
	Total payoff:
		515.25 ± 44.2634
	Mean payoff:
		0.6134 ± 0.0527
	Total number of recruited nodes:
		840 ± 0

Results at turn 864:
	Total payoff:
		545.5 ± 44.689
	Mean payoff:
		0.6143 ± 0.0503
	Total number of recruited nodes:
		888 ± 0

Results at turn 912:
	Total payoff:
		574.2 ± 47.6131
	Mean payoff:
		0.6135 ± 0.0509
	Total number of recruited nodes:
		936 ± 0

Results at turn 960:
	Total payoff:
		601.85 ± 47.6735
	Mean payoff:
		0.6116 ± 0.0484
	Total number of recruited nodes:
		984 ± 0

Results at turn 1008:
	Total payoff:
		631.35 ± 48.9664
	Mean payoff:
		0.6118 ± 0.0474
	Total number of recruited nodes:
		1032 ± 0

Results at turn 1056:
	Total payoff:
		659.4 ± 49.0235
	Mean payoff:
		0.6106 ± 0.0454
	Total number of recruited nodes:
		1080 ± 0

Results at turn 1104:
	Total payoff:
		689.5 ± 52.4741
	Mean payoff:
		0.6113 ± 0.0465
	Total number of recruited nodes:
		1128 ± 0

Results at turn 1152:
	Total payoff:
		721 ± 52.8941
	Mean payoff:
		0.6131 ± 0.045
	Total number of recruited nodes:
		1176 ± 0

Results at turn 1200:
	Total payoff:
		751.25 ± 51.9715
	Mean payoff:
		0.6138 ± 0.0425
	Total number of recruited nodes:
		1224 ± 0


