Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 300
    nmodels: 4
    nseeds: 1
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 12:
	Total payoff:
		13.25 ± 2.9714
	Mean payoff:
		0.383 ± 0.0915
	Total number of recruited nodes:
		34.75 ± 1.4824

Results at turn 24:
	Total payoff:
		16.2 ± 4.3237
	Mean payoff:
		0.3476 ± 0.0956
	Total number of recruited nodes:
		46.75 ± 1.4824

Results at turn 36:
	Total payoff:
		19.25 ± 5.2202
	Mean payoff:
		0.3282 ± 0.09
	Total number of recruited nodes:
		58.75 ± 1.4824

Results at turn 48:
	Total payoff:
		21.2 ± 6.0053
	Mean payoff:
		0.2999 ± 0.0853
	Total number of recruited nodes:
		70.75 ± 1.4824

Results at turn 60:
	Total payoff:
		24 ± 6.8364
	Mean payoff:
		0.2901 ± 0.0826
	Total number of recruited nodes:
		82.75 ± 1.4824

Results at turn 72:
	Total payoff:
		25.9 ± 7.5107
	Mean payoff:
		0.2733 ± 0.0789
	Total number of recruited nodes:
		94.75 ± 1.4824

Results at turn 84:
	Total payoff:
		27.05 ± 7.7898
	Mean payoff:
		0.2534 ± 0.0728
	Total number of recruited nodes:
		106.75 ± 1.4824

Results at turn 96:
	Total payoff:
		29 ± 7.8539
	Mean payoff:
		0.2442 ± 0.066
	Total number of recruited nodes:
		118.75 ± 1.4824

Results at turn 108:
	Total payoff:
		31.15 ± 7.7342
	Mean payoff:
		0.2382 ± 0.059
	Total number of recruited nodes:
		130.75 ± 1.4824

Results at turn 120:
	Total payoff:
		32.9 ± 7.4615
	Mean payoff:
		0.2304 ± 0.0519
	Total number of recruited nodes:
		142.75 ± 1.4824

Results at turn 132:
	Total payoff:
		34.5 ± 7.4091
	Mean payoff:
		0.2229 ± 0.0476
	Total number of recruited nodes:
		154.75 ± 1.4824

Results at turn 144:
	Total payoff:
		36.6 ± 7.1185
	Mean payoff:
		0.2194 ± 0.0423
	Total number of recruited nodes:
		166.75 ± 1.4824

Results at turn 156:
	Total payoff:
		38.35 ± 7.1692
	Mean payoff:
		0.2145 ± 0.0396
	Total number of recruited nodes:
		178.75 ± 1.4824

Results at turn 168:
	Total payoff:
		39.9 ± 7.0926
	Mean payoff:
		0.2091 ± 0.0367
	Total number of recruited nodes:
		190.75 ± 1.4824

Results at turn 180:
	Total payoff:
		41.25 ± 6.7891
	Mean payoff:
		0.2034 ± 0.033
	Total number of recruited nodes:
		202.75 ± 1.4824

Results at turn 192:
	Total payoff:
		43.2 ± 6.1866
	Mean payoff:
		0.2011 ± 0.0283
	Total number of recruited nodes:
		214.75 ± 1.4824

Results at turn 204:
	Total payoff:
		44.05 ± 5.6891
	Mean payoff:
		0.1942 ± 0.0246
	Total number of recruited nodes:
		226.75 ± 1.4824

Results at turn 216:
	Total payoff:
		45.65 ± 5.4219
	Mean payoff:
		0.1912 ± 0.0224
	Total number of recruited nodes:
		238.75 ± 1.4824

Results at turn 228:
	Total payoff:
		46.4 ± 5.5479
	Mean payoff:
		0.185 ± 0.0219
	Total number of recruited nodes:
		250.75 ± 1.4824

Results at turn 240:
	Total payoff:
		46.9 ± 5.457
	Mean payoff:
		0.1785 ± 0.0206
	Total number of recruited nodes:
		262.75 ± 1.4824

Results at turn 252:
	Total payoff:
		47.75 ± 5.1183
	Mean payoff:
		0.1738 ± 0.0184
	Total number of recruited nodes:
		274.75 ± 1.4824

Results at turn 264:
	Total payoff:
		48.45 ± 5.0207
	Mean payoff:
		0.1689 ± 0.0173
	Total number of recruited nodes:
		286.75 ± 1.4824

Results at turn 276:
	Total payoff:
		49 ± 4.8232
	Mean payoff:
		0.164 ± 0.016
	Total number of recruited nodes:
		298.75 ± 1.4824

Results at turn 288:
	Total payoff:
		49.15 ± 4.8588
	Mean payoff:
		0.1582 ± 0.0155
	Total number of recruited nodes:
		310.75 ± 1.4824

Results at turn 300:
	Total payoff:
		49.6 ± 4.3818
	Mean payoff:
		0.1537 ± 0.0135
	Total number of recruited nodes:
		322.75 ± 1.4824


