Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 1200
    nmodels: 1
    nseeds: 5
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		21.95 ± 7.5077
	Mean payoff:
		0.2814 ± 0.0963
	Total number of recruited nodes:
		78 ± 0

Results at turn 96:
	Total payoff:
		33.45 ± 10.6696
	Mean payoff:
		0.2655 ± 0.0847
	Total number of recruited nodes:
		126 ± 0

Results at turn 144:
	Total payoff:
		45.3 ± 15.8417
	Mean payoff:
		0.2603 ± 0.091
	Total number of recruited nodes:
		174 ± 0

Results at turn 192:
	Total payoff:
		59.35 ± 21.3351
	Mean payoff:
		0.2673 ± 0.0961
	Total number of recruited nodes:
		222 ± 0

Results at turn 240:
	Total payoff:
		71.15 ± 25.4895
	Mean payoff:
		0.2635 ± 0.0944
	Total number of recruited nodes:
		270 ± 0

Results at turn 288:
	Total payoff:
		82.8 ± 29.1739
	Mean payoff:
		0.2604 ± 0.0917
	Total number of recruited nodes:
		318 ± 0

Results at turn 336:
	Total payoff:
		98.15 ± 31.2903
	Mean payoff:
		0.2682 ± 0.0855
	Total number of recruited nodes:
		366 ± 0

Results at turn 384:
	Total payoff:
		112.8 ± 35.1083
	Mean payoff:
		0.2725 ± 0.0848
	Total number of recruited nodes:
		414 ± 0

Results at turn 432:
	Total payoff:
		128.15 ± 39.6528
	Mean payoff:
		0.2774 ± 0.0858
	Total number of recruited nodes:
		462 ± 0

Results at turn 480:
	Total payoff:
		139.95 ± 43.6258
	Mean payoff:
		0.2744 ± 0.0855
	Total number of recruited nodes:
		510 ± 0

Results at turn 528:
	Total payoff:
		151.5 ± 48.667
	Mean payoff:
		0.2715 ± 0.0872
	Total number of recruited nodes:
		558 ± 0

Results at turn 576:
	Total payoff:
		164.85 ± 52.6361
	Mean payoff:
		0.272 ± 0.0869
	Total number of recruited nodes:
		606 ± 0

Results at turn 624:
	Total payoff:
		177.25 ± 55.1256
	Mean payoff:
		0.271 ± 0.0843
	Total number of recruited nodes:
		654 ± 0

Results at turn 672:
	Total payoff:
		188 ± 58.4339
	Mean payoff:
		0.2678 ± 0.0832
	Total number of recruited nodes:
		702 ± 0

Results at turn 720:
	Total payoff:
		198.2 ± 59.3833
	Mean payoff:
		0.2643 ± 0.0792
	Total number of recruited nodes:
		750 ± 0

Results at turn 768:
	Total payoff:
		210.65 ± 59.1806
	Mean payoff:
		0.264 ± 0.0742
	Total number of recruited nodes:
		798 ± 0

Results at turn 816:
	Total payoff:
		224.25 ± 57.2399
	Mean payoff:
		0.2651 ± 0.0677
	Total number of recruited nodes:
		846 ± 0

Results at turn 864:
	Total payoff:
		237.6 ± 55.1184
	Mean payoff:
		0.2658 ± 0.0617
	Total number of recruited nodes:
		894 ± 0

Results at turn 912:
	Total payoff:
		249.85 ± 51.2068
	Mean payoff:
		0.2652 ± 0.0544
	Total number of recruited nodes:
		942 ± 0

Results at turn 960:
	Total payoff:
		261.5 ± 48.3882
	Mean payoff:
		0.2641 ± 0.0489
	Total number of recruited nodes:
		990 ± 0

Results at turn 1008:
	Total payoff:
		273.55 ± 45.6433
	Mean payoff:
		0.2635 ± 0.044
	Total number of recruited nodes:
		1038 ± 0

Results at turn 1056:
	Total payoff:
		286.9 ± 44.8353
	Mean payoff:
		0.2642 ± 0.0413
	Total number of recruited nodes:
		1086 ± 0

Results at turn 1104:
	Total payoff:
		298.25 ± 44.693
	Mean payoff:
		0.263 ± 0.0394
	Total number of recruited nodes:
		1134 ± 0

Results at turn 1152:
	Total payoff:
		311 ± 42.932
	Mean payoff:
		0.2631 ± 0.0363
	Total number of recruited nodes:
		1182 ± 0

Results at turn 1200:
	Total payoff:
		322.7 ± 43.5033
	Mean payoff:
		0.2624 ± 0.0354
	Total number of recruited nodes:
		1230 ± 0


