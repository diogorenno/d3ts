Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: upper_bound
    nattempts: 1200
    nmodels: 1
    nseeds: 4
    predictor: global
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		28.6 ± 10.3943
	Mean payoff:
		0.3972 ± 0.1444
	Total number of recruited nodes:
		72 ± 0

Results at turn 96:
	Total payoff:
		47.4 ± 13.3116
	Mean payoff:
		0.395 ± 0.1109
	Total number of recruited nodes:
		120 ± 0

Results at turn 144:
	Total payoff:
		70.05 ± 18.925
	Mean payoff:
		0.417 ± 0.1126
	Total number of recruited nodes:
		168 ± 0

Results at turn 192:
	Total payoff:
		96 ± 23.4229
	Mean payoff:
		0.4444 ± 0.1084
	Total number of recruited nodes:
		216 ± 0

Results at turn 240:
	Total payoff:
		122.6 ± 26.5675
	Mean payoff:
		0.4644 ± 0.1006
	Total number of recruited nodes:
		264 ± 0

Results at turn 288:
	Total payoff:
		152.25 ± 26.0644
	Mean payoff:
		0.488 ± 0.0835
	Total number of recruited nodes:
		312 ± 0

Results at turn 336:
	Total payoff:
		183.15 ± 27.9949
	Mean payoff:
		0.5088 ± 0.0778
	Total number of recruited nodes:
		360 ± 0

Results at turn 384:
	Total payoff:
		211.2 ± 31.0087
	Mean payoff:
		0.5176 ± 0.076
	Total number of recruited nodes:
		408 ± 0

Results at turn 432:
	Total payoff:
		239.1 ± 30.1049
	Mean payoff:
		0.5243 ± 0.066
	Total number of recruited nodes:
		456 ± 0

Results at turn 480:
	Total payoff:
		269.7 ± 31.1602
	Mean payoff:
		0.5351 ± 0.0618
	Total number of recruited nodes:
		504 ± 0

Results at turn 528:
	Total payoff:
		296.5 ± 31.8822
	Mean payoff:
		0.5371 ± 0.0578
	Total number of recruited nodes:
		552 ± 0

Results at turn 576:
	Total payoff:
		322 ± 33.9039
	Mean payoff:
		0.5367 ± 0.0565
	Total number of recruited nodes:
		600 ± 0

Results at turn 624:
	Total payoff:
		348.25 ± 32.9335
	Mean payoff:
		0.5374 ± 0.0508
	Total number of recruited nodes:
		648 ± 0

Results at turn 672:
	Total payoff:
		373.05 ± 35.2099
	Mean payoff:
		0.536 ± 0.0506
	Total number of recruited nodes:
		696 ± 0

Results at turn 720:
	Total payoff:
		398.6 ± 36.9814
	Mean payoff:
		0.5358 ± 0.0497
	Total number of recruited nodes:
		744 ± 0

Results at turn 768:
	Total payoff:
		424.35 ± 41.0305
	Mean payoff:
		0.5358 ± 0.0518
	Total number of recruited nodes:
		792 ± 0

Results at turn 816:
	Total payoff:
		450.65 ± 41.1023
	Mean payoff:
		0.5365 ± 0.0489
	Total number of recruited nodes:
		840 ± 0

Results at turn 864:
	Total payoff:
		477 ± 42.2661
	Mean payoff:
		0.5372 ± 0.0476
	Total number of recruited nodes:
		888 ± 0

Results at turn 912:
	Total payoff:
		503.8 ± 40.4379
	Mean payoff:
		0.5382 ± 0.0432
	Total number of recruited nodes:
		936 ± 0

Results at turn 960:
	Total payoff:
		530.6 ± 41.0884
	Mean payoff:
		0.5392 ± 0.0418
	Total number of recruited nodes:
		984 ± 0

Results at turn 1008:
	Total payoff:
		556 ± 43.2021
	Mean payoff:
		0.5388 ± 0.0419
	Total number of recruited nodes:
		1032 ± 0

Results at turn 1056:
	Total payoff:
		581.2 ± 41.3771
	Mean payoff:
		0.5381 ± 0.0383
	Total number of recruited nodes:
		1080 ± 0

Results at turn 1104:
	Total payoff:
		605.1 ± 40.3314
	Mean payoff:
		0.5364 ± 0.0358
	Total number of recruited nodes:
		1128 ± 0

Results at turn 1152:
	Total payoff:
		630.25 ± 41.4715
	Mean payoff:
		0.5359 ± 0.0353
	Total number of recruited nodes:
		1176 ± 0

Results at turn 1200:
	Total payoff:
		655.85 ± 41.4046
	Mean payoff:
		0.5358 ± 0.0338
	Total number of recruited nodes:
		1224 ± 0


