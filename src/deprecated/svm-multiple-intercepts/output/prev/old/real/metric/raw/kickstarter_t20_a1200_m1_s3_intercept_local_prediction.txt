Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 1200
    nmodels: 1
    nseeds: 3
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		18.1 ± 5.8481
	Mean payoff:
		0.2742 ± 0.0886
	Total number of recruited nodes:
		66 ± 0

Results at turn 96:
	Total payoff:
		29.85 ± 7.6038
	Mean payoff:
		0.2618 ± 0.0667
	Total number of recruited nodes:
		114 ± 0

Results at turn 144:
	Total payoff:
		42.75 ± 12.0825
	Mean payoff:
		0.2639 ± 0.0746
	Total number of recruited nodes:
		162 ± 0

Results at turn 192:
	Total payoff:
		57.85 ± 17.7654
	Mean payoff:
		0.2755 ± 0.0846
	Total number of recruited nodes:
		210 ± 0

Results at turn 240:
	Total payoff:
		75.95 ± 23.0616
	Mean payoff:
		0.2944 ± 0.0894
	Total number of recruited nodes:
		258 ± 0

Results at turn 288:
	Total payoff:
		91.8 ± 28.5096
	Mean payoff:
		0.3 ± 0.0932
	Total number of recruited nodes:
		306 ± 0

Results at turn 336:
	Total payoff:
		105.05 ± 32.3931
	Mean payoff:
		0.2968 ± 0.0915
	Total number of recruited nodes:
		354 ± 0

Results at turn 384:
	Total payoff:
		119.7 ± 35.8169
	Mean payoff:
		0.2978 ± 0.0891
	Total number of recruited nodes:
		402 ± 0

Results at turn 432:
	Total payoff:
		135.7 ± 37.4181
	Mean payoff:
		0.3016 ± 0.0832
	Total number of recruited nodes:
		450 ± 0

Results at turn 480:
	Total payoff:
		152.2 ± 38.1377
	Mean payoff:
		0.3056 ± 0.0766
	Total number of recruited nodes:
		498 ± 0

Results at turn 528:
	Total payoff:
		168.75 ± 38.5853
	Mean payoff:
		0.3091 ± 0.0707
	Total number of recruited nodes:
		546 ± 0

Results at turn 576:
	Total payoff:
		184.25 ± 38.5744
	Mean payoff:
		0.3102 ± 0.0649
	Total number of recruited nodes:
		594 ± 0

Results at turn 624:
	Total payoff:
		197.3 ± 39.5249
	Mean payoff:
		0.3073 ± 0.0616
	Total number of recruited nodes:
		642 ± 0

Results at turn 672:
	Total payoff:
		209.85 ± 40.1999
	Mean payoff:
		0.3041 ± 0.0583
	Total number of recruited nodes:
		690 ± 0

Results at turn 720:
	Total payoff:
		226.4 ± 39.3238
	Mean payoff:
		0.3068 ± 0.0533
	Total number of recruited nodes:
		738 ± 0

Results at turn 768:
	Total payoff:
		239.05 ± 37.5731
	Mean payoff:
		0.3041 ± 0.0478
	Total number of recruited nodes:
		786 ± 0

Results at turn 816:
	Total payoff:
		250.55 ± 38.6366
	Mean payoff:
		0.3004 ± 0.0463
	Total number of recruited nodes:
		834 ± 0

Results at turn 864:
	Total payoff:
		263.3 ± 39.0992
	Mean payoff:
		0.2985 ± 0.0443
	Total number of recruited nodes:
		882 ± 0

Results at turn 912:
	Total payoff:
		274.1 ± 40.2844
	Mean payoff:
		0.2947 ± 0.0433
	Total number of recruited nodes:
		930 ± 0

Results at turn 960:
	Total payoff:
		286 ± 38.5528
	Mean payoff:
		0.2924 ± 0.0394
	Total number of recruited nodes:
		978 ± 0

Results at turn 1008:
	Total payoff:
		297.25 ± 37.6869
	Mean payoff:
		0.2897 ± 0.0367
	Total number of recruited nodes:
		1026 ± 0

Results at turn 1056:
	Total payoff:
		309.6 ± 33.8874
	Mean payoff:
		0.2883 ± 0.0316
	Total number of recruited nodes:
		1074 ± 0

Results at turn 1104:
	Total payoff:
		321 ± 30.2411
	Mean payoff:
		0.2861 ± 0.027
	Total number of recruited nodes:
		1122 ± 0

Results at turn 1152:
	Total payoff:
		331.9 ± 27.6366
	Mean payoff:
		0.2837 ± 0.0236
	Total number of recruited nodes:
		1170 ± 0

Results at turn 1200:
	Total payoff:
		342.55 ± 28.0741
	Mean payoff:
		0.2812 ± 0.023
	Total number of recruited nodes:
		1218 ± 0


