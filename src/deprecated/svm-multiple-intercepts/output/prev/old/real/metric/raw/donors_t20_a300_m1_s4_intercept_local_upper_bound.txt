Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: upper_bound
    nattempts: 300
    nmodels: 1
    nseeds: 4
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 12:
	Total payoff:
		12.05 ± 2.1392
	Mean payoff:
		0.3347 ± 0.0594
	Total number of recruited nodes:
		36 ± 0

Results at turn 24:
	Total payoff:
		14.15 ± 2.7582
	Mean payoff:
		0.2948 ± 0.0575
	Total number of recruited nodes:
		48 ± 0

Results at turn 36:
	Total payoff:
		16.25 ± 3.0586
	Mean payoff:
		0.2708 ± 0.051
	Total number of recruited nodes:
		60 ± 0

Results at turn 48:
	Total payoff:
		18.85 ± 2.9961
	Mean payoff:
		0.2618 ± 0.0416
	Total number of recruited nodes:
		72 ± 0

Results at turn 60:
	Total payoff:
		20.6 ± 2.9451
	Mean payoff:
		0.2452 ± 0.0351
	Total number of recruited nodes:
		84 ± 0

Results at turn 72:
	Total payoff:
		22.95 ± 3.5314
	Mean payoff:
		0.2391 ± 0.0368
	Total number of recruited nodes:
		96 ± 0

Results at turn 84:
	Total payoff:
		25.35 ± 3.6458
	Mean payoff:
		0.2347 ± 0.0338
	Total number of recruited nodes:
		108 ± 0

Results at turn 96:
	Total payoff:
		27.35 ± 4.0429
	Mean payoff:
		0.2279 ± 0.0337
	Total number of recruited nodes:
		120 ± 0

Results at turn 108:
	Total payoff:
		30 ± 4.6679
	Mean payoff:
		0.2273 ± 0.0354
	Total number of recruited nodes:
		132 ± 0

Results at turn 120:
	Total payoff:
		32.95 ± 4.8175
	Mean payoff:
		0.2288 ± 0.0335
	Total number of recruited nodes:
		144 ± 0

Results at turn 132:
	Total payoff:
		35.45 ± 4.4186
	Mean payoff:
		0.2272 ± 0.0283
	Total number of recruited nodes:
		156 ± 0

Results at turn 144:
	Total payoff:
		37.7 ± 3.7989
	Mean payoff:
		0.2244 ± 0.0226
	Total number of recruited nodes:
		168 ± 0

Results at turn 156:
	Total payoff:
		40.1 ± 3.81
	Mean payoff:
		0.2228 ± 0.0212
	Total number of recruited nodes:
		180 ± 0

Results at turn 168:
	Total payoff:
		41.35 ± 4.1074
	Mean payoff:
		0.2154 ± 0.0214
	Total number of recruited nodes:
		192 ± 0

Results at turn 180:
	Total payoff:
		42.05 ± 4.2485
	Mean payoff:
		0.2061 ± 0.0208
	Total number of recruited nodes:
		204 ± 0

Results at turn 192:
	Total payoff:
		43.15 ± 4.2212
	Mean payoff:
		0.1998 ± 0.0195
	Total number of recruited nodes:
		216 ± 0

Results at turn 204:
	Total payoff:
		43.75 ± 4.2535
	Mean payoff:
		0.1919 ± 0.0187
	Total number of recruited nodes:
		228 ± 0

Results at turn 216:
	Total payoff:
		44.45 ± 4.4186
	Mean payoff:
		0.1852 ± 0.0184
	Total number of recruited nodes:
		240 ± 0

Results at turn 228:
	Total payoff:
		44.8 ± 4.4319
	Mean payoff:
		0.1778 ± 0.0176
	Total number of recruited nodes:
		252 ± 0

Results at turn 240:
	Total payoff:
		45.3 ± 4.6915
	Mean payoff:
		0.1716 ± 0.0178
	Total number of recruited nodes:
		264 ± 0

Results at turn 252:
	Total payoff:
		45.75 ± 4.5754
	Mean payoff:
		0.1658 ± 0.0166
	Total number of recruited nodes:
		276 ± 0

Results at turn 264:
	Total payoff:
		46.1 ± 4.3878
	Mean payoff:
		0.1601 ± 0.0152
	Total number of recruited nodes:
		288 ± 0

Results at turn 276:
	Total payoff:
		46.3 ± 4.3782
	Mean payoff:
		0.1543 ± 0.0146
	Total number of recruited nodes:
		300 ± 0

Results at turn 288:
	Total payoff:
		46.6 ± 4.4414
	Mean payoff:
		0.1494 ± 0.0142
	Total number of recruited nodes:
		312 ± 0

Results at turn 300:
	Total payoff:
		46.8 ± 4.4319
	Mean payoff:
		0.1444 ± 0.0137
	Total number of recruited nodes:
		324 ± 0


