Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 1200
    nmodels: 1
    nseeds: 7
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 48:
	Total payoff:
		24.7 ± 7.6096
	Mean payoff:
		0.2744 ± 0.0846
	Total number of recruited nodes:
		90 ± 0

Results at turn 96:
	Total payoff:
		33.55 ± 9.3947
	Mean payoff:
		0.2431 ± 0.0681
	Total number of recruited nodes:
		138 ± 0

Results at turn 144:
	Total payoff:
		42.6 ± 12.1845
	Mean payoff:
		0.229 ± 0.0655
	Total number of recruited nodes:
		186 ± 0

Results at turn 192:
	Total payoff:
		50.9 ± 16.4153
	Mean payoff:
		0.2175 ± 0.0702
	Total number of recruited nodes:
		234 ± 0

Results at turn 240:
	Total payoff:
		61.85 ± 22.0031
	Mean payoff:
		0.2193 ± 0.078
	Total number of recruited nodes:
		282 ± 0

Results at turn 288:
	Total payoff:
		72.45 ± 26.7099
	Mean payoff:
		0.2195 ± 0.0809
	Total number of recruited nodes:
		330 ± 0

Results at turn 336:
	Total payoff:
		86.2 ± 30.4192
	Mean payoff:
		0.228 ± 0.0805
	Total number of recruited nodes:
		378 ± 0

Results at turn 384:
	Total payoff:
		102.2 ± 31.7964
	Mean payoff:
		0.2399 ± 0.0746
	Total number of recruited nodes:
		426 ± 0

Results at turn 432:
	Total payoff:
		118.45 ± 32.7036
	Mean payoff:
		0.2499 ± 0.069
	Total number of recruited nodes:
		474 ± 0

Results at turn 480:
	Total payoff:
		136.15 ± 34.2948
	Mean payoff:
		0.2608 ± 0.0657
	Total number of recruited nodes:
		522 ± 0

Results at turn 528:
	Total payoff:
		154.95 ± 34.3396
	Mean payoff:
		0.2718 ± 0.0602
	Total number of recruited nodes:
		570 ± 0

Results at turn 576:
	Total payoff:
		173.15 ± 30.8481
	Mean payoff:
		0.2802 ± 0.0499
	Total number of recruited nodes:
		618 ± 0

Results at turn 624:
	Total payoff:
		190.3 ± 27.7111
	Mean payoff:
		0.2857 ± 0.0416
	Total number of recruited nodes:
		666 ± 0

Results at turn 672:
	Total payoff:
		206.75 ± 24.9776
	Mean payoff:
		0.2896 ± 0.035
	Total number of recruited nodes:
		714 ± 0

Results at turn 720:
	Total payoff:
		222.85 ± 23.7714
	Mean payoff:
		0.2925 ± 0.0312
	Total number of recruited nodes:
		762 ± 0

Results at turn 768:
	Total payoff:
		235.3 ± 24.1837
	Mean payoff:
		0.2905 ± 0.0299
	Total number of recruited nodes:
		810 ± 0

Results at turn 816:
	Total payoff:
		249.15 ± 24.9067
	Mean payoff:
		0.2904 ± 0.029
	Total number of recruited nodes:
		858 ± 0

Results at turn 864:
	Total payoff:
		260.9 ± 25.3479
	Mean payoff:
		0.288 ± 0.028
	Total number of recruited nodes:
		906 ± 0

Results at turn 912:
	Total payoff:
		274.05 ± 24.9135
	Mean payoff:
		0.2873 ± 0.0261
	Total number of recruited nodes:
		954 ± 0

Results at turn 960:
	Total payoff:
		286.7 ± 22.8337
	Mean payoff:
		0.2861 ± 0.0228
	Total number of recruited nodes:
		1002 ± 0

Results at turn 1008:
	Total payoff:
		297.7 ± 23.5776
	Mean payoff:
		0.2835 ± 0.0225
	Total number of recruited nodes:
		1050 ± 0

Results at turn 1056:
	Total payoff:
		309.2 ± 23.3995
	Mean payoff:
		0.2816 ± 0.0213
	Total number of recruited nodes:
		1098 ± 0

Results at turn 1104:
	Total payoff:
		321 ± 24.0963
	Mean payoff:
		0.2801 ± 0.021
	Total number of recruited nodes:
		1146 ± 0

Results at turn 1152:
	Total payoff:
		332.35 ± 24.9278
	Mean payoff:
		0.2784 ± 0.0209
	Total number of recruited nodes:
		1194 ± 0

Results at turn 1200:
	Total payoff:
		342.15 ± 25.7606
	Mean payoff:
		0.2755 ± 0.0207
	Total number of recruited nodes:
		1242 ± 0


