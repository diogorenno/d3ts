Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 400
    nmodels: 5
    nseeds: 1
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 16:
	Total payoff:
		43.55 ± 1.4681
	Mean payoff:
		0.9689 ± 0.0218
	Total number of recruited nodes:
		44.95 ± 1.191

Results at turn 32:
	Total payoff:
		59 ± 1.3377
	Mean payoff:
		0.9682 ± 0.0212
	Total number of recruited nodes:
		60.95 ± 1.191

Results at turn 48:
	Total payoff:
		74.5 ± 1.5728
	Mean payoff:
		0.9683 ± 0.0209
	Total number of recruited nodes:
		76.95 ± 1.191

Results at turn 64:
	Total payoff:
		89.75 ± 1.6182
	Mean payoff:
		0.9657 ± 0.0178
	Total number of recruited nodes:
		92.95 ± 1.191

Results at turn 80:
	Total payoff:
		105.4 ± 1.5009
	Mean payoff:
		0.9675 ± 0.0136
	Total number of recruited nodes:
		108.95 ± 1.191

Results at turn 96:
	Total payoff:
		121 ± 1.451
	Mean payoff:
		0.9684 ± 0.0124
	Total number of recruited nodes:
		124.95 ± 1.191

Results at turn 112:
	Total payoff:
		136.3 ± 1.78
	Mean payoff:
		0.9671 ± 0.0144
	Total number of recruited nodes:
		140.95 ± 1.191

Results at turn 128:
	Total payoff:
		151.3 ± 2.5567
	Mean payoff:
		0.9641 ± 0.018
	Total number of recruited nodes:
		156.95 ± 1.191

Results at turn 144:
	Total payoff:
		166.75 ± 2.8261
	Mean payoff:
		0.9642 ± 0.0171
	Total number of recruited nodes:
		172.95 ± 1.191

Results at turn 160:
	Total payoff:
		181.85 ± 2.4121
	Mean payoff:
		0.9624 ± 0.0131
	Total number of recruited nodes:
		188.95 ± 1.191

Results at turn 176:
	Total payoff:
		195.8 ± 3.6792
	Mean payoff:
		0.9554 ± 0.0174
	Total number of recruited nodes:
		204.95 ± 1.191

Results at turn 192:
	Total payoff:
		210.1 ± 3.5079
	Mean payoff:
		0.9509 ± 0.0159
	Total number of recruited nodes:
		220.95 ± 1.191

Results at turn 208:
	Total payoff:
		224.3 ± 4.0144
	Mean payoff:
		0.9466 ± 0.0163
	Total number of recruited nodes:
		236.95 ± 1.191

Results at turn 224:
	Total payoff:
		239.1 ± 4.0249
	Mean payoff:
		0.9452 ± 0.0153
	Total number of recruited nodes:
		252.95 ± 1.191

Results at turn 240:
	Total payoff:
		252.7 ± 3.9216
	Mean payoff:
		0.9396 ± 0.0144
	Total number of recruited nodes:
		268.95 ± 1.191

Results at turn 256:
	Total payoff:
		265.3 ± 4.4497
	Mean payoff:
		0.931 ± 0.0148
	Total number of recruited nodes:
		284.95 ± 1.191

Results at turn 272:
	Total payoff:
		279.55 ± 4.7956
	Mean payoff:
		0.9289 ± 0.015
	Total number of recruited nodes:
		300.95 ± 1.191

Results at turn 288:
	Total payoff:
		294.5 ± 4.7072
	Mean payoff:
		0.9292 ± 0.0139
	Total number of recruited nodes:
		316.95 ± 1.191

Results at turn 304:
	Total payoff:
		307.35 ± 3.265
	Mean payoff:
		0.9231 ± 0.0085
	Total number of recruited nodes:
		332.95 ± 1.191

Results at turn 320:
	Total payoff:
		320.05 ± 1.6376
	Mean payoff:
		0.9172 ± 0.0046
	Total number of recruited nodes:
		348.95 ± 1.191

Results at turn 336:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.8988 ± 0.0029
	Total number of recruited nodes:
		364.95 ± 1.191

Results at turn 352:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.861 ± 0.0027
	Total number of recruited nodes:
		380.95 ± 1.191

Results at turn 368:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.8263 ± 0.0025
	Total number of recruited nodes:
		396.95 ± 1.191

Results at turn 384:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.7943 ± 0.0023
	Total number of recruited nodes:
		412.95 ± 1.191

Results at turn 400:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.7647 ± 0.0021
	Total number of recruited nodes:
		428.95 ± 1.191


