Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 300
    nmodels: 1
    nseeds: 2
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 12:
	Total payoff:
		9.25 ± 2.7697
	Mean payoff:
		0.3854 ± 0.1154
	Total number of recruited nodes:
		24 ± 0

Results at turn 24:
	Total payoff:
		12.1 ± 3.9323
	Mean payoff:
		0.3361 ± 0.1092
	Total number of recruited nodes:
		36 ± 0

Results at turn 36:
	Total payoff:
		15 ± 5.4095
	Mean payoff:
		0.3125 ± 0.1127
	Total number of recruited nodes:
		48 ± 0

Results at turn 48:
	Total payoff:
		17.45 ± 6.5331
	Mean payoff:
		0.2908 ± 0.1089
	Total number of recruited nodes:
		60 ± 0

Results at turn 60:
	Total payoff:
		19.5 ± 7.5915
	Mean payoff:
		0.2708 ± 0.1054
	Total number of recruited nodes:
		72 ± 0

Results at turn 72:
	Total payoff:
		21.8 ± 8.9419
	Mean payoff:
		0.2595 ± 0.1065
	Total number of recruited nodes:
		84 ± 0

Results at turn 84:
	Total payoff:
		23.2 ± 9.8012
	Mean payoff:
		0.2417 ± 0.1021
	Total number of recruited nodes:
		96 ± 0

Results at turn 96:
	Total payoff:
		25.05 ± 9.6925
	Mean payoff:
		0.2319 ± 0.0897
	Total number of recruited nodes:
		108 ± 0

Results at turn 108:
	Total payoff:
		26.95 ± 9.5888
	Mean payoff:
		0.2246 ± 0.0799
	Total number of recruited nodes:
		120 ± 0

Results at turn 120:
	Total payoff:
		28.2 ± 9.4735
	Mean payoff:
		0.2136 ± 0.0718
	Total number of recruited nodes:
		132 ± 0

Results at turn 132:
	Total payoff:
		30 ± 9.3358
	Mean payoff:
		0.2083 ± 0.0648
	Total number of recruited nodes:
		144 ± 0

Results at turn 144:
	Total payoff:
		31.25 ± 9.5415
	Mean payoff:
		0.2003 ± 0.0612
	Total number of recruited nodes:
		156 ± 0

Results at turn 156:
	Total payoff:
		32.45 ± 9.5282
	Mean payoff:
		0.1932 ± 0.0567
	Total number of recruited nodes:
		168 ± 0

Results at turn 168:
	Total payoff:
		33.3 ± 9.3308
	Mean payoff:
		0.185 ± 0.0518
	Total number of recruited nodes:
		180 ± 0

Results at turn 180:
	Total payoff:
		34.4 ± 9.4668
	Mean payoff:
		0.1792 ± 0.0493
	Total number of recruited nodes:
		192 ± 0

Results at turn 192:
	Total payoff:
		35.1 ± 9.1818
	Mean payoff:
		0.1721 ± 0.045
	Total number of recruited nodes:
		204 ± 0

Results at turn 204:
	Total payoff:
		35.8 ± 9.3392
	Mean payoff:
		0.1657 ± 0.0432
	Total number of recruited nodes:
		216 ± 0

Results at turn 216:
	Total payoff:
		36.45 ± 9.5282
	Mean payoff:
		0.1599 ± 0.0418
	Total number of recruited nodes:
		228 ± 0

Results at turn 228:
	Total payoff:
		37.45 ± 9.0987
	Mean payoff:
		0.156 ± 0.0379
	Total number of recruited nodes:
		240 ± 0

Results at turn 240:
	Total payoff:
		38.2 ± 8.5692
	Mean payoff:
		0.1516 ± 0.034
	Total number of recruited nodes:
		252 ± 0

Results at turn 252:
	Total payoff:
		39.2 ± 8.0302
	Mean payoff:
		0.1485 ± 0.0304
	Total number of recruited nodes:
		264 ± 0

Results at turn 264:
	Total payoff:
		39.85 ± 7.8222
	Mean payoff:
		0.1444 ± 0.0283
	Total number of recruited nodes:
		276 ± 0

Results at turn 276:
	Total payoff:
		40.75 ± 7.2248
	Mean payoff:
		0.1415 ± 0.0251
	Total number of recruited nodes:
		288 ± 0

Results at turn 288:
	Total payoff:
		41.5 ± 7.1635
	Mean payoff:
		0.1383 ± 0.0239
	Total number of recruited nodes:
		300 ± 0

Results at turn 300:
	Total payoff:
		42 ± 7.2765
	Mean payoff:
		0.1346 ± 0.0233
	Total number of recruited nodes:
		312 ± 0


