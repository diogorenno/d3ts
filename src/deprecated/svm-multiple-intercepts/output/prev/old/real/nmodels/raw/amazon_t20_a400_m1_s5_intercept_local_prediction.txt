Valid tests: 20 out of 20

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    metric: prediction
    nattempts: 400
    nmodels: 1
    nseeds: 5
    predictor: local
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 16:
	Total payoff:
		44.35 ± 1.9541
	Mean payoff:
		0.9641 ± 0.0425
	Total number of recruited nodes:
		46 ± 0

Results at turn 32:
	Total payoff:
		59.8 ± 2.285
	Mean payoff:
		0.9645 ± 0.0369
	Total number of recruited nodes:
		62 ± 0

Results at turn 48:
	Total payoff:
		74.85 ± 2.9961
	Mean payoff:
		0.9596 ± 0.0384
	Total number of recruited nodes:
		78 ± 0

Results at turn 64:
	Total payoff:
		90.25 ± 3.0066
	Mean payoff:
		0.9601 ± 0.032
	Total number of recruited nodes:
		94 ± 0

Results at turn 80:
	Total payoff:
		105.65 ± 3.1166
	Mean payoff:
		0.9605 ± 0.0283
	Total number of recruited nodes:
		110 ± 0

Results at turn 96:
	Total payoff:
		120.95 ± 2.9996
	Mean payoff:
		0.9599 ± 0.0238
	Total number of recruited nodes:
		126 ± 0

Results at turn 112:
	Total payoff:
		136.4 ± 2.7985
	Mean payoff:
		0.9606 ± 0.0197
	Total number of recruited nodes:
		142 ± 0

Results at turn 128:
	Total payoff:
		151.85 ± 2.9429
	Mean payoff:
		0.9611 ± 0.0186
	Total number of recruited nodes:
		158 ± 0

Results at turn 144:
	Total payoff:
		167.35 ± 3.0997
	Mean payoff:
		0.9618 ± 0.0178
	Total number of recruited nodes:
		174 ± 0

Results at turn 160:
	Total payoff:
		182.85 ± 3.0136
	Mean payoff:
		0.9624 ± 0.0159
	Total number of recruited nodes:
		190 ± 0

Results at turn 176:
	Total payoff:
		197.9 ± 2.6931
	Mean payoff:
		0.9607 ± 0.0131
	Total number of recruited nodes:
		206 ± 0

Results at turn 192:
	Total payoff:
		212.95 ± 2.7237
	Mean payoff:
		0.9592 ± 0.0123
	Total number of recruited nodes:
		222 ± 0

Results at turn 208:
	Total payoff:
		227.5 ± 3.3795
	Mean payoff:
		0.9559 ± 0.0142
	Total number of recruited nodes:
		238 ± 0

Results at turn 224:
	Total payoff:
		241.7 ± 4.342
	Mean payoff:
		0.9516 ± 0.0171
	Total number of recruited nodes:
		254 ± 0

Results at turn 240:
	Total payoff:
		254.45 ± 4.0843
	Mean payoff:
		0.9424 ± 0.0151
	Total number of recruited nodes:
		270 ± 0

Results at turn 256:
	Total payoff:
		266.95 ± 5.4626
	Mean payoff:
		0.9334 ± 0.0191
	Total number of recruited nodes:
		286 ± 0

Results at turn 272:
	Total payoff:
		280.9 ± 5.3499
	Mean payoff:
		0.9301 ± 0.0177
	Total number of recruited nodes:
		302 ± 0

Results at turn 288:
	Total payoff:
		295.3 ± 4.6803
	Mean payoff:
		0.9286 ± 0.0147
	Total number of recruited nodes:
		318 ± 0

Results at turn 304:
	Total payoff:
		308.75 ± 4.3392
	Mean payoff:
		0.9244 ± 0.013
	Total number of recruited nodes:
		334 ± 0

Results at turn 320:
	Total payoff:
		320.95 ± 2.3503
	Mean payoff:
		0.917 ± 0.0067
	Total number of recruited nodes:
		350 ± 0

Results at turn 336:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.8962 ± 0
	Total number of recruited nodes:
		366 ± 0

Results at turn 352:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.8586 ± 0
	Total number of recruited nodes:
		382 ± 0

Results at turn 368:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.8241 ± 0
	Total number of recruited nodes:
		398 ± 0

Results at turn 384:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.7923 ± 0
	Total number of recruited nodes:
		414 ± 0

Results at turn 400:
	Total payoff:
		328 ± 0
	Mean payoff:
		0.7628 ± 0
	Total number of recruited nodes:
		430 ± 0


