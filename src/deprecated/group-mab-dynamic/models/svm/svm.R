library(R6)

library(LiblineaR)

##
 #
 ##
SVM <- R6Class("SVM",

    public = list(
    
        ##
         # Initializes the model instance.
         # Default parameter values are those from LiblineaR.
         ##
        initialize = function(C=1, bias=TRUE)
        {
          private$heuristicC <- (C == "heuristic")
          private$C <- C
          private$bias <- bias
        },
        
        ##
         #
         ##
        isValid = function()
        {
          return (TRUE)
        },
        
        ##
        #
        ##
        getName = function()
        {
          name <- sprintf("SVM, C=%s, bias=%s", 
                          as.character(private$C),
                          as.character(private$bias))
          return (name)
        },
        
        ##
         #
         ##
        fit = function(x, y)
        {
          cost <- if (private$heuristicC) heuristicC(x) else private$C
          private$model <- LiblineaR(data=x, target=y, type=11, svr_eps=0.1, bias=private$bias, cost=cost)
        },
        
        ##
         # 
         ##
        predict = function(x)
        {
          yhat <- predict(private$model, x)$predictions
          names(yhat) <- rownames(x)
          return (yhat)
        }
    ),

    private = list(
        
        model = NA,
        
        heuristicC = NA,
        C = NA,
        bias = NA
    )
)
