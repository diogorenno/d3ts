#!/bin/bash

# cluster parameters
NODES=1
PPN=20
QUEUENAME=debug
QUEUENAME=ribeirob
QUEUENAME=standby
WALLTIME="4:00:00"
MEM=200gb

# simulation parameters
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

# =============================================================
DATASET=$1
NTESTS=40

MSC="dts.5"
SCOREMAPPER="max"

FEATURESET="all"
SETTINGS="shorter.config3"

NRANKS_PCT=0.1
MIN_COR=0.7

NSEEDS=1
COLDPULLS=20
HEURISTIC=mod
# =============================================================

# dataset specific settings
MAXTESTS=1000
case $DATASET in
    dbpedia)
        MAXTESTS=725
        MEMPERTSK=3
        NTURNS=700
	;;
    citeseer)
        MAXTESTS=1583
        MEMPERTSK=3
        NTURNS=1500
	;;
    wikipedia)
        MAXTESTS=202
        MEMPERTSK=3
        NTURNS=400
	;;
    donors)
        MAXTESTS=56
        MEMPERTSK=1
        NTURNS=200
	;;
    dblp)
        MAXTESTS=7556
        MEMPERTSK=5
	;;
    lj)
        MAXTESTS=1441
        MEMPERTSK=10
	;;
    kickstarter)
        MAXTESTS=1457
        MEMPERTSK=3
        NTURNS=700
	;;
    *)
        echo "Invalid dataset $1"
        quit
        ;;
esac

# Number of turns before model reclustering.
NRANKS=$(python -c "print int($NRANKS_PCT*$NTURNS)")

if [[ $NTESTS -gt $MAXTESTS ]]; then
    echo "Overriding NTESTS; now set to $MAXTESTS"
    NTESTS=$MAXTESTS
fi

# ribeirob specific settings
if [[ $QUEUENAME == "ribeirob" ]]; then
    NODES=1
    PPN=20
    WALLTIME="60:00:00"
elif [[ $QUEUENAME == "debug" ]]; then
    WALLTIME="00:30:00"
fi

# "acconte" for the fact that conte has only 16 cores 
HOSTNAME=`hostname | cut -d- -f1`
if [[ $HOSTNAME == "conte" ]]; then
    case $DATASET in
        dblp)
            PPN=10
            ;;
        lj)
            PPN=5
            ;;
        *)
            PPN=16
            ;;
    esac
elif [[ $QUEUENAME == "debug" ]]; then
    echo "ERROR: debug queue only available in Conte Cluster. Exiting ..."
    exit 1
fi


START_TEST=1
while [[ $START_TEST -le $NTESTS ]]; do

    # determine end test index
    if [[ $((START_TEST+PPN-1)) -gt $NTESTS ]]; then
        END_TEST=$NTESTS
        PPN=$((END_TEST-START_TEST+1))
    else
        END_TEST=$((START_TEST+PPN-1))
    fi

    TEST_RANGE="$START_TEST $END_TEST"
    TEST_RANGE_STR=`printf "%03d_%03d" $START_TEST $END_TEST`
    
    MEM="$((PPN*MEMPERTSK))gb"
    
    OUTFILE=${MSC}.${SCOREMAPPER}.${NRANKS_PCT}.${MIN_COR}.${SETTINGS}.${FEATURESET}.${DATASET}.${TEST_RANGE_STR}.sh
    
    OUTPATH=/scratch/lustreE/d/drennoro/p2r_results/group-mab/dynamic/ccpivot/mean-all/${MSC}_${SCOREMAPPER}_${NRANKS_PCT}_${MIN_COR}_${SETTINGS}_${FEATURESET}/

    cat script_template.sh | sed \
        -e "s/NODES/$NODES/" \
        -e "s/PPN/$PPN/" \
        -e "s/QUEUENAME/$QUEUENAME/" \
        -e "s/WALLTIME/$WALLTIME/" \
        -e "s/MEM/$MEM/" \
        -e "s/DATASET/$DATASET/" \
        -e "s/TEST_RANGE/$TEST_RANGE/" \
        -e "s/MSC/$MSC/" \
        -e "s/SCOREMAPPER/$SCOREMAPPER/" \
        -e "s/NRANKS/$NRANKS/" \
        -e "s/MIN_COR/$MIN_COR/" \
        -e "s/FEATURESET/$FEATURESET/" \
        -e "s/SETTINGS/$SETTINGS/" \
        -e "s/NSEEDS/$NSEEDS/" \
        -e "s/COLDPULLS/$COLDPULLS/" \
        -e "s/HEURISTIC/$HEURISTIC/" \
        -e "s@OUTPATH@$OUTPATH@" \
        > $OUTFILE

    echo qsub `pwd`/$OUTFILE
    qsub $OUTFILE

    START_TEST=$((END_TEST+1))
done
