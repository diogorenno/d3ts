library(R6)

##
 # General model class.
 # Encapsulates specific model implementations.
 ##
Model <- R6Class("Model",

    public = list(
    
        ##
         # Initializes the model and general settings.
         ##
        initialize = function(settings, model, observed_graph, external_feats)
        {
          private$settings <- settings
          private$model <- model$clone()
          private$type <- class(model)[1]
          private$graph <- observed_graph
          private$must_fit <- FALSE
          private$external_feats <- external_feats
        },
        
        ##
         # Fits a new model on the current samples or updates it with the new sample.
         ##
        recruitNode = function(id)
        {
          if (length(private$graph$recruited) > 1) {
            private$update(id)
          } else {
            private$fit()
          }
        },
        
        ##
         # Returns TRUE if the model is valid (can be used for prediction).
         # Returns FALSE otherwise.
         ##
        isValid = function()
        {
          if (private$must_fit) private$fit()
          return (private$model$isValid())
        },
        
        ##
         # Returns a character string identifying the underlying model.
         ##
        getName = function()
        {
          return (private$model$getName())
        },
        
        ##
         # 
         ##
        evaluateBorder = function()
        {
          return (private$evaluateNodes(private$graph$border))
        },
        
        ##
         # Allows internal access to the instance for testing/debugging purposes.
         ##
        browse = function()
        {
          browser()
        }
    ),

    private = list(
        
        model = NA,           # A reference to an initialized instance of a supported model class.
        type = NA,            # (character) Name of the R6 class which implements private$model.
        settings = NA,        # (Settings) General settings.
        graph = NA,           # (ObservedGraph) Reference to the currently observed graph.
        must_fit = NA,        # (logical) Indicates whether fit() must be called prior to predicting.
        external_feats = NA,  # (matrix) Rows 1:i compose external design matrix at turn i.
        
        # Supported models. Each constant value must be the name of the corresponding R6 class.
        TYPE_SVM = "SVM",
        TYPE_EWRLS = "EWRLS",
        TYPE_MFOREST = "MForest",
        TYPE_RFOREST_RF = "RForest.rf",
        TYPE_RFOREST_P = "RForest.p",
        TYPE_LISTNET = "ListNet",
        TYPE_ACTIVESEARCH = "ActiveSearch",
        TYPE_SIGMAOPT = "SigmaOpt",
        TYPE_LOGISTIC = "Logistic",
        TYPE_MOD = "MOD",
        
        ##
         #
         ##
        fit = function()
        {
          # Unsets the lazy fit flag.
          private$must_fit <- FALSE

          # Gets training data from recruited nodes or the external features matrix. 
          N = length(private$graph$recruited)
          if(!is.null(private$external_feats)) {
            x = as.matrix(private$external_feats[1:N, -1, drop=FALSE])
            y = as.vector(private$external_feats[1:N, "response", drop=FALSE])
          } else {
            x = as.matrix(private$graph$features[private$graph$recruited, -1, drop=FALSE])
            y = as.vector(private$graph$features[private$graph$recruited, "response"])
          }
          
          # EWRLS.
          if (private$type == private$TYPE_EWRLS) {
            private$model$fit(
                x = t(cbind(x, rep(1, N))),
                y = y)
          }

          # ListNet.
          if (private$type == private$TYPE_LISTNET) {
            private$model$fit(
                x = cbind(x, rep(1, N)),
                y = y)
          }

          # ActiveSearch.
          if (private$type %in% c(private$TYPE_ACTIVESEARCH,private$TYPE_SIGMAOPT)) {}
          
          # SVM.
          if (private$type == private$TYPE_SVM) {
            private$model$fit(
                x = x,
                y = y)
          }

          # Logistic.
          if (private$type == private$TYPE_LOGISTIC) {
            private$model$fit(
                x = x,
                y = y)
          }
          
          # RForest.
          if (private$type == private$TYPE_RFOREST_RF || private$type == private$TYPE_RFOREST_P) {
            private$model$fit(
                x = x,
                y = y)
          }
          
          # MForest.
          if (private$type == private$TYPE_MFOREST) {
            private$model$fit(
                x = x,
                y = y)
          }
          
          # MOD.
          if (private$type == private$TYPE_MOD) {}
        },

        ##
         #
         ##
        update = function(id)
        {
          stopifnot(id %in% private$graph$recruited)

          # Gets node data from recruited nodes or the external features matrix.
          N = length(private$graph$recruited)
          if(!is.null(private$external_feats)) {
            x = private$external_feats[N, -1]
            y = private$external_feats[N, "response"]
          } else {
            x = private$graph$features[id, -1]
            y = private$graph$features[id, "response"]
          }
          
          # EWRLS.
          if (private$type == private$TYPE_EWRLS) {
            private$model$update(x=c(x, 1), y=y)
          }

          # ListNet.
          if (private$type == private$TYPE_LISTNET) {
            private$flagLazyFit()
          }

          # ActiveSearch.
          if (private$type %in% c(private$TYPE_ACTIVESEARCH,private$TYPE_SIGMAOPT)) {}
          
          # SVM.
          if (private$type == private$TYPE_SVM) {
            private$flagLazyFit()
          }

          # Logistic.
          if (private$type == private$TYPE_LOGISTIC) {
            private$flagLazyFit()
          }

          # RForest.
          if (private$type == private$TYPE_RFOREST_RF || private$type == private$TYPE_RFOREST_P) {
            private$flagLazyFit()
          }
          
          # MForest.
          if (private$type == private$TYPE_MFOREST) {
            if (self$isValid()) {
              private$model$update(x=x, y=y)
            } else {
              private$fit()
            }
          }
          
          # MOD.
          if (private$type == private$TYPE_MOD) {}
        },
        
        ##
         #
         ##
        predict = function(ids)
        {
          # Refits the model if flagged.
          if (private$must_fit) {
            private$fit()
          }
          
          # EWRLS.
          if (private$type == private$TYPE_EWRLS) {
            return (private$model$predict(x = t(private$graph$getDesignMatrix(ids))))
          }

          # ListNet.
          if (private$type == private$TYPE_LISTNET) {
            return (private$model$predict(x = private$graph$getDesignMatrix(ids))[,1])
          }

          # ActiveSearch.
          if (private$type %in% c(private$TYPE_ACTIVESEARCH,private$TYPE_SIGMAOPT)) {
            params <- private$graph$getAdjacencyMatrix()

            isRecruited <- rep(FALSE,length(params$node_ids))
            names(isRecruited) <- params$node_ids
            responses <- isRecruited

            isRecruited[private$graph$recruited] <- TRUE
            responses[private$graph$recruited] <- private$graph$features[private$graph$recruited, "response"]

            yhat <- private$model$predict(params$edge_matrix, isRecruited, responses)
            return(yhat)
          }
          
          # SVM.
          if (private$type == private$TYPE_SVM) {
            return (private$model$predict(x = as.matrix(private$graph$features[ids, -1, drop=FALSE])))
          }

          # Logistic.
          if (private$type == private$TYPE_LOGISTIC) {
            return (private$model$predict(x = as.matrix(private$graph$features[ids, -1, drop=FALSE])))
          }

          # RForest.
          if (private$type == private$TYPE_RFOREST_RF || private$type == private$TYPE_RFOREST_P) {
            return (private$model$predict(x = as.matrix(private$graph$features[ids, -1, drop=FALSE])))
          }
          
          # MForest.
          if (private$type == private$TYPE_MFOREST) {
            return (private$model$predict(x = as.matrix(private$graph$features[ids, -1, drop=FALSE])))
          }
          
          # MOD.
          if (private$type == private$TYPE_MOD) {
            return (private$graph$getPositiveNeighborsCount(ids))
          }
        },
        
        ##
         # 
         ##
        evaluateNodes = function(ids)
        {
          stopifnot(self$isValid())
          if (length(ids) == 0) {
            return (NULL)
          }
          return (private$predict(ids))
        },
        
        ##
         # If the model is valid, it is simply flagged to be refitted prior to predicting.
         # If the model is not yet valid, always retrains it.
         ##
        flagLazyFit = function()
        {
          if (self$isValid()) {
            private$must_fit <- TRUE
          } else {
            private$fit()
          }
        }
    )
)
