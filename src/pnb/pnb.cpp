#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <set>
#include <iostream>
#include <fstream>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <inttypes.h>
//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>
#include <unordered_set>
#include "RandomLib/Random.hpp"
//#include "linear.h"
#include <cassert>
#include <sys/stat.h>

using namespace std;

#define MIN( x, y ) ( (x)<(y) ? (x) : (y) )
#define MAX( x, y ) ( (x)>(y) ? (x) : (y) )
#define DEBUG 0
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

#define BUDGET 1
#define TARGET 2

#define NRUNS 1
#define NTHREADS 1
#define RUN_ID_OFFSET 0;

#define GSAMPLES 100
#define BURN_IN  50

class Problem {
    public:

    string name, graph_file, cmty_file;
    int target_cmty, seed_id, budget;

    Problem( string name_, string graph_file_, string cmty_file_, int target_cmty_, int seed_id_, int budget_ ) :
        name(name_), graph_file(graph_file_), cmty_file(cmty_file_),
        target_cmty(target_cmty_), seed_id(seed_id_), budget(budget_) {};
    Problem();
    void setProperties(string name_, string graph_file_, string cmty_file_, int target_cmty_, int seed_id_, int budget_ ) {
        name = name_;
        graph_file = graph_file_;
        cmty_file = cmty_file_;
        target_cmty = target_cmty_;
        seed_id = seed_id_;
        budget = budget_;
    };
};



RandomLib::Random rnd(0);        // r created with random seed
inline int myrand(int max) {
    return rnd.IntegerC(0,max-1);
}

inline double myUnifrand() {
    // r.Fixed() is in the interval (0, 1)
    return rnd.Fixed();
}
class Graph {
public:
    Graph( bool undirected_ ) : undirected(undirected_), totalE(0), label_max(1), target_label(1) { }
    ~Graph() {}
    void addEdge( int u, int v );
    int getN() { return this->out.size(); } // FIXME
    int getE() { return this->totalE; }
    unordered_set<int>    getNeighbors( int i ) {return all[i]; }
    //vector<int>    getInNeighbors( int i ) { vector<int> ret(in[i].begin(),in[i].end()); return ret; }
    unordered_set<int>    getInNeighbors( int i ) { return in[i]; }
    int                 getLabel( int i ) { return label[i]; }
    int                 getRandomNode();
    //vector<double> getAttributes( int i ) { return attribs[i]; }
    vector<string> getFeatureNames() { return this->feat_names; }
    vector<bool> isFeatureFraction() { return this->feat_isFraction; }
    //void initFeatures( int ntypes, int target, vector<int> att_max );
    void initFeatures( int ntypes, int target, int nattribs );
    void addNode( int u, unordered_set<int> neighs, int label );
    void readGraph( string filename );
    int readCommunities( string filename, int target );
    void writeAttributesToFile( string filename );
    void pnb( int samples, int initial_node, char *output, int condition, int stop_at );
    std::vector<int>getTargetNodes( int nsamples );

private:
    // TODO: make vertex a class
    vector<unordered_set<int> > out;
    vector<unordered_set<int> > in;
    vector<unordered_set<int> > all;
    vector<char> valid;
    vector<char> label;
    //vector<vector<double> > attribs;

    int label_max, target_label;
    vector<string> feat_names;
    vector<bool> feat_isFraction;
    // TODO: map ids to save space
    //map<int,int> old2new;
    //map<int,int> new2old;
    int totalE;
    int nattribs;
    int id_offset, max_id;
    bool undirected;

    //void computeFeatures( int v, vector<double> &feat_values );
};

int32_t GibbsSampler( Graph *S, unordered_set<int32_t> border, int32_t *label, double theta, double gamma) {
    int gsamples = GSAMPLES, burnin=BURN_IN;
    vector<int32_t> border_ind(border.begin(),border.end());
    map<int32_t,double> sumLabel;

    if( DEBUG > 1 ) cerr << "Current theta " << theta << ", gamma " << gamma << endl;
    for( int i=0; i < gsamples; i++ ) {
        random_shuffle(border_ind.begin(),border_ind.end());
        for( int j=0; j<border_ind.size(); j++ ){
            double numer = theta*gamma;
            int32_t denom = gamma;
            int32_t u = border_ind[j]; // u-->v-->?
            unordered_set<int> neighs = S->getNeighbors(u);
            for( unordered_set<int>::iterator k=neighs.begin(); k != neighs.end(); k++ ) {
                int32_t v = *k;
                unordered_set<int> neighs2 = S->getNeighbors(v);
                for( unordered_set<int>::iterator m=neighs2.begin(); m != neighs2.end(); m++ ) {
                    int32_t w = *m;
                    if( u != w ) {
                        numer += label[w];
                        denom++;
                    }
                }
            }
            label[u] = 1*(myUnifrand() < numer/denom);
            if( DEBUG > 1 )
                cerr << "Node " << u << ", numer " << numer << ", denom " << denom <<
                    ", label " << label[u] << endl; 
            if( i >= burnin ) {
                sumLabel[u] += label[u];
            }
        }
    }
    double max = 0.0;
    vector<int32_t> max_indices;
    for( int j=0; j<border_ind.size(); j++ ) {
        int32_t u = border_ind[j];
        if( sumLabel[u] >= max ) {
            if( sumLabel[u] > max ) {
                max_indices.clear();
                max = sumLabel[u];
            }
            max_indices.push_back(u);
        }
    }
    return max_indices[myrand(max_indices.size())];


}

void Graph::pnb( int maxsamples, int initiator, char * outputfile, int condition, int stop_at ) {
    double gamma = 2.0, theta = 0.0;

    int32_t n = getN();
    Graph *S = new Graph( false );

    //assign random labels
    int32_t *vlabel = new int32_t [n];
    for( int32_t i=0; i < n; i++ )
        vlabel[i] = 1*(myUnifrand() <= 0.5);
    //S->attribute["label"] = label;
    int *recruited = new int [n]();

    unordered_set<int32_t> border;
    recruited[initiator] = 1;
    vlabel[initiator] = label[initiator];
    S->addNode( initiator, out[initiator], label[initiator] );
    border.insert(out[initiator].begin(),out[initiator].end());
    if( DEBUG ) {
        fprintf( stderr, "Adding node %d.\n Neighbors:", initiator );
        for( unordered_set<int>::iterator i = out[initiator].begin();
                i != out[initiator].end(); i++ )
            fprintf( stderr, " %d (lab %d)", *i, vlabel[*i] );
        fprintf( stderr, "\n\n" );
    }

    if( condition == BUDGET )
        maxsamples = stop_at;

    ofstream output, output2;
    string idfile(outputfile);
    int lastindex = idfile.find_last_of(".");
    idfile = idfile.substr(0,lastindex)+".lab";

    output.open( outputfile );
    output2.open( idfile );
    int32_t i, total = 0;
    for( i=0; i < maxsamples; i++ ) {

        if( DEBUG ) fprintf( stderr, "|B| = %d\n", (int) border.size() );
        while( border.empty() ) {
            initiator = myrand(n);
            if( valid[initiator] && recruited[initiator] == 0 ) {
                recruited[initiator] = 1;
                vlabel[initiator] = label[initiator];
                total += label[initiator];

                S->addNode( initiator, out[initiator], label[initiator] );
                for( unordered_set<int>::iterator j = out[initiator].begin();
                        j != out[initiator].end(); j++ )
                    if( recruited[*j] == 0 )
                        border.insert( *j );
            }
        }

        // evaluate border nodes
        int32_t selected = GibbsSampler( S, border, vlabel, theta, gamma );
        if( DEBUG )
            cerr << "Selected " << selected << ",vlabel " << vlabel[selected] <<
                ", lab " << (int)label[selected] << endl;

        border.erase(selected);
        recruited[selected] = 1;
        vlabel[selected] = label[selected];
        theta = (theta*(gamma+(i-1)+1)+label[selected])/(gamma+i+1);
        total += label[selected];

        //cerr << "i " << i << " " << (int)label[selected] << " total:" << total << endl;

        S->addNode( selected, out[selected], label[selected] );
        if( DEBUG )
            fprintf( stderr, "Adding node %d.\n Neighbors:", selected );
        for( unordered_set<int>::iterator j = out[selected].begin(); j != out[selected].end(); j++ )
            if( !recruited[*j] ) {
                border.insert(*j);
                if( DEBUG ) {
                    fprintf( stderr, " %d (lab %d),", *j, vlabel[*j] );
                }
            }
        if( DEBUG )
        fprintf( stderr, "\n\n" );

        output << (int)label[selected] << " ";
        output2 << selected+id_offset << " ";
        if( (i+1) % 1 == 0 ) {
            output.flush();
            output2.flush();
            cout << i << " samples acquired.\r";
            cout.flush();
        }

        if( condition == TARGET && total == stop_at )
            break;
    }
    cout << endl;

    output.close();
    output2.close();
    cout << total/(i+1.0) << endl;

    delete vlabel;
    delete recruited;
    delete S;
}

std::vector<int> Graph::getTargetNodes( int nsamples ) {
    int n = out.size();
    std::vector<int> target_nodes;
    for( int i=1; i <= n; i++ )
        if( valid[i] && label[i] == 1 )
            target_nodes.push_back(i);

    std::random_shuffle(target_nodes.begin(),target_nodes.end());
    target_nodes.resize(nsamples);

    return target_nodes;
}


int Graph::getRandomNode() {
    int id = (int)(drand48()*out.size());
    while( !valid[id] )
        id = (int)(drand48()*out.size());
    return id;
}

class FileHandler {
    public:
        bool use_gzip;
        FILE *f;

        FILE * open( string filename );
        void close();
};

FILE * FileHandler::open( string filename ) {
    use_gzip = filename.substr(filename.size()-3,3) == ".gz";
    if( use_gzip ) {
        char st[1000];
        sprintf( st, "gunzip -c %s", filename.c_str() ); //FIXME: use zcat instead
        f = popen(st, "r");
    } else {
        f = fopen(filename.c_str(), "r");
    }

    return f;
}

void FileHandler::close( void ) {
    if( use_gzip ) pclose(f);
    else fclose(f);
}

//TODO: modify the code to skip comment lines
void Graph::readGraph( string filename ) {
    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t min_id = 1000;
    max_id = -1;
    while(!feof(f)) {
        int32_t i, j;
        if( fscanf(f, "%d %d", &i, &j) ==  2 ) {
            if(i != j) {
                min_id = MIN(min_id,MIN(i,j));
                max_id = MAX(max_id,MAX(i,j));
            }
        }
    }
    fh->close();

    //id_offset = min_id-1;
    id_offset = 0;
    max_id -= id_offset;
    out.resize(max_id+1);
    in.resize(max_id+1);
    all.resize(max_id+1);
    //attribs.resize(max_id+1);
    label.resize(max_id+1);
    valid.resize(max_id+1);

    cerr << "id_offset " << id_offset << ", max_id " << max_id << endl;

    int32_t count = 0;
    f = fh->open( filename );
    while(!feof(f)) {
        int32_t i, j;
        if( fscanf(f, "%d %d", &i, &j) ==  2 ) {
            if(i != j) {
                i -= id_offset;
                j -= id_offset;
                out[i].insert(j);
                in[j].insert(i);
                totalE++;
                if( undirected ) {
                    out[j].insert(i);
                    in[i].insert(j);
                }
                all[i].insert(j);
                all[j].insert(i);
                valid[i] = 1;
                valid[j] = 1;

                if( totalE%100000 == 0 )
                    fprintf( stderr, "Processed %d edges.\n", totalE );
            }
        }
    }
    fh->close();
    delete fh;
}

// TODO: make target a expression
int Graph::readCommunities( string filename, int target ) {
    //FIXME: give the file one pass to find out number of communities
    //for( int i = 0; i <= max_id; i++ )
    //    attribs[i].resize(5000);

    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t community = 0, id, cmty_size = 0, nread;
    char *line;
    size_t max_line_len = 10000;
    line = (char *) malloc(max_line_len*sizeof(char));

    while( (nread = getline( &line, &max_line_len, f)) > -1 ) {
        const char *sep = " \t,";
        char *id_str, *last;

        if( line[0] == '#' ) // skip headers
            continue;

        for( id_str = strtok_r(line, sep, &last);
             id_str;
             id_str = strtok_r(NULL, sep, &last) ) {
            id = atoi(id_str);
            if( community == target ) {
                label[id-id_offset] = 1;
                cmty_size++;
            }
        }
        community++;
    }
    free(line);

    //fprintf( stdout, "Largest community: %d (%d members)\n", largest_community, max_k );

    //while(!feof(f)) {
    //    fprintf( stdout, "Community %d:", community );
    //    while( fscanf( f, "%d[^\n]", &id ) == 1 ) {
    //        fprintf( stdout, " %d", id);
    //        attribs[id-id_offset][community] = 1;

    //        if(community == target)
    //            label[id-id_offset]++;
    //    }
    //    fprintf( stdout, "\n");
    //    community++;
    //}
    fh->close();
    delete fh;

    this->nattribs = community+1;

    return cmty_size;
}


//TODO: if id's are sequential, drop parameter and use push_back instead of resize
void Graph::addNode( int u, unordered_set<int> neighs, int label ) {
    // get max and resize vectors
    int max = u;
    for( unordered_set<int>::iterator i = neighs.begin(); i != neighs.end(); i++ )
        max = MAX( max, *i );

    int cur_max = this->out.size();
    if( cur_max < (max+1) ) {
        int new_max = MAX(cur_max*2,max+1);
        this->out.resize(new_max);
        this->in.resize(new_max);
        this->all.resize(new_max);
        this->label.resize(new_max);
        this->valid.resize(new_max);
    }

    //fprintf(stderr, "A1\n" );
    valid[u] = 1; // node is recruited
    
    //unordered_set<int> toBeComputed;
    //add edges and mark border nodes to have its features computed
    out[u].insert(neighs.begin(),neighs.end());
    for( unordered_set<int>::iterator i = neighs.begin(); i != neighs.end(); i++ ) {
        this->in[*i].insert(u);

        this->all[*i].insert(u);
        this->all[u].insert(*i);
    }

    ////fprintf(stderr, "A2\n" );
    //// mark border nodes to have its features computed
    //for( int i = 0; i < neighs.size(); i++ ) {
    //    int v = neighs[i];
    //    // border node
    //    if( !valid[v] )
    //        toBeComputed.insert(v);
    //    // border neighbor of recruited neighbor
    //    else {
    //        for( unordered_set<int>::iterator j=out[v].begin(); j != out[v].end(); j++ )
    //            if( !valid[*j] )
    //                toBeComputed.insert(*j);
    //    }
    //}

    //fprintf(stderr, "A3\n" );
    //set label and attribs
    this->label[u] = label;
    //this->attribs[u] = attribs; //copy

    //vector<double> feat_values;
    //int i = 0;
    //for( unordered_set<int>::iterator it = toBeComputed.begin();
    //        it != toBeComputed.end(); it++ ) {
    //    // first element has node id
    //    feat_values.push_back(*it);
    //    computeFeatures(*it,feat_values);
    //    i++;
    //}
    ////fprintf(stderr, "A4\n" );
    ////return feat_values;
    //return feat_values;
}


struct task {
    Graph *G;
    int nruns;
    int *run_ids;
    int nsamples;
    int initial_node;
    char *basename;
    int condition;
    int stop_at;
    int seed_idx;
};

void *run(void *param) {
    struct task *t = (struct task *)param;
    for( int i=0; i < t->nruns; i++ ) {
      char outputfile[1024];
      // sprintf( outputfile, "%s_r%03d.txt", t->basename, t->run_ids[i] );
      sprintf( outputfile, "%s_s%02d.txt", t->basename, t->seed_idx );
      cerr << "Output will be saved in " << outputfile << endl;

      t->G->pnb(t->nsamples,  t->initial_node, outputfile,
                    t->condition, t->stop_at);
    }
    //t->G->logRegression(t->nsamples);

    return NULL;
}



int main( int argc, char **argv ){
    //string graphfile("binned.adj.gz");
    //string labelfile("binned.tsv.gz");
    srand48(0);
    srand(0);

    vector<Problem *> p;
    p.push_back(new Problem("dblp","data/datasets/dblp/com-dblp.ungraph.txt.gz","data/datasets/dblp/com-dblp.top5000.cmty.txt.gz",4972, 61+1, 1250));
    // p.push_back(new Problem("amazon","data/datasets/com-amazon.ungraph.txt.gz","data/datasets/com-amazon.top5000.cmty.txt.gz",4832, 278,300));
    // p.push_back(new Problem("amazon2","com-amazon2.ungraph.txt.gz","com-amazon2.top5750.cmty.txt.gz",5749, -1,1000));
    // p.push_back(new Problem("youtube","com-youtube.ungraph.txt.gz","com-youtube.top5000.cmty.txt.gz",45, 15,1000));
    p.push_back(new Problem("lj","data/datasets/lj/com-lj.ungraph.txt.gz","data/datasets/lj/com-lj.top5000.cmty.txt.gz",4915, 9732, 1250));
    // p.push_back(new Problem("orkut","com-orkut.ungraph.txt.gz","com-orkut.top5000.cmty.txt.gz",22, 27,1000));
    // p.push_back(new Problem("dogster","com-dogster.ungraph.txt.gz","com-dogster.top5000.cmty.txt.gz",0, 3,1000));
    // p.push_back(new Problem("catster","com-catster.ungraph.txt.gz","com-catster.top5000.cmty.txt.gz",8, 3,1000));
    // p.push_back(new Problem("delicious","com-delicious.ungraph.txt.gz","com-delicious.top2918.cmty.txt.gz",2394, -1,1000));
    p.push_back(new Problem("donors","data/datasets/gccs/donors/donors.gcc.txt.gz","data/datasets/gccs/donors/donors.att.txt.gz",284, -1, 300));
    p.push_back(new Problem("kickstarter","data/datasets/kickstarter/kickstarter.ungraph.txt.gz","data/datasets/kickstarter/kickstarter.attribs.txt.gz",180, -1, 1500));

    p.push_back(new Problem("dbpedia","data/datasets/wang2013/dbpedia/done/dbpedia.ungraph.txt.gz","data/datasets/wang2013/dbpedia/done/dbpedia.attribs.txt.gz",0, -1, 1000));
    p.push_back(new Problem("citeseer","data/datasets/gccs/citeseer/labeled/citeseer.gcc.txt.gz","data/datasets/gccs/citeseer/labeled/citeseer.att.txt.gz",2, -1, 1500));
    p.push_back(new Problem("wikipedia","data/datasets/wang2013/wikipedia/done/wikipedia.ungraph.txt.gz","data/datasets/wang2013/wikipedia/done/wikipedia.attribs.txt.gz",48, -1, 1000));


    if( argc != 4 ) {
        fprintf( stderr, "Incorrect number of arguments.");
        return 1;
    }
    int ind;
    for( ind = 0; ind < p.size(); ind++)
        if( p[ind]->name == argv[1] )
            break;
    if( ind == p.size() ) {
        fprintf( stderr, "Problem %s not found.", argv[1] );
        return 1;
    }
    fprintf( stderr, "Recovered ind = %d\n", ind );

    
    // Asserts output directory exists.
    char *output_dir = argv[3];
    struct stat sb;
    assert(stat(output_dir, &sb) == 0 && S_ISDIR(sb.st_mode));
    
    // Builds the basename for output files.
    char basename[2048];
    sprintf(basename, "%s%s", output_dir, p[ind]->name.c_str());
    
    // Parses the seed index for this run.
    unsigned int seed_idx = atoi(argv[2]);
    assert(seed_idx > 0);
        
    // Reads the seed (initial node) from the dataset's file.
    int seed_id = -1;
    char seeds_fn[2048];
    sprintf(seeds_fn, "seeds/%s.txt", argv[1]);
    ifstream seeds(seeds_fn);
    for (unsigned int i = 1; i <= seed_idx; ++i) {
      char buffer[2048];
      seeds.getline(buffer, 2048);
      assert(!seeds.eof());
      if (i == seed_idx) {
        seed_id = atoi(buffer);
        break;
      }
    }
    cerr << "Initial node: " << seed_id << endl;
    
    
    int cmty_size, nsamples, stop_at;


    //0. Read Graph
    Graph *G = new Graph( true );
    G->readGraph(p[ind]->graph_file);
    cmty_size = G->readCommunities(p[ind]->cmty_file, p[ind]->target_cmty);
    //stop_at  = (int)(cmty_size*0.95);
    stop_at  = (int)(cmty_size*1.00);
    nsamples = (int)(cmty_size*0.50);
    fprintf( stderr, "Target community size = %d\n", cmty_size );

        
    std::vector<int> seed_ids;
    seed_ids.push_back(seed_id);
    
    pthread_t threads[NTHREADS];
    int nruns_per_thread = NRUNS/NTHREADS;
    struct task t[NTHREADS];

    assert(seed_ids.size() == 1);
    assert(NTHREADS == 1);
    assert(NRUNS == 1);
        
    for( int i=0; i<NTHREADS; i++ ) {
        t[i].G = G;
        t[i].nruns = nruns_per_thread;
        t[i].run_ids = (int *) malloc( nruns_per_thread*sizeof(int) );
        for( int j=0; j < nruns_per_thread; j++ )
            t[i].run_ids[j] = i*nruns_per_thread+j+1+RUN_ID_OFFSET;
        t[i].nsamples = nsamples;
        t[i].initial_node = seed_ids[i];
        t[i].basename = basename;
        t[i].condition = BUDGET;
        t[i].stop_at = p[ind]->budget;
        t[i].seed_idx = seed_idx;
        pthread_create(&threads[i], NULL, run, &t[i] );
    }
    void *status;
    for( int i=0; i<NTHREADS; i++ )
        pthread_join(threads[i], &status );

    //delete output;
    
    pthread_exit(NULL);
    return 0;
}
