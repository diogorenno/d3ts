#!/bin/bash

# cluster parameters
NODES=1
PPN=1
QUEUENAME=ribeirob

# simulation parameters
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

DATASET=$1
START_TEST=1
END_TEST=20

# dataset specific settings
MAXTESTS=1000
case $DATASET in
    dbpedia)
        MAXTESTS=725
        MEMPERTSK=3
	;;
    citeseer)
        MAXTESTS=1583
        MEMPERTSK=3
	;;
    wikipedia)
        MAXTESTS=202
        MEMPERTSK=3
	;;
    blogcatalog)
        MAXTESTS=986
        MEMPERTSK=3
	;;
    donors)
        MAXTESTS=56
        MEMPERTSK=1
	;;
    dblp)
        MAXTESTS=7556
        MEMPERTSK=5
	;;
    lj)
        MAXTESTS=1441
        MEMPERTSK=10
	;;
    kickstarter)
        MAXTESTS=1457
        MEMPERTSK=3
	;;
    *)
        echo "Invalid dataset $1"
        quit
        ;;
esac


# ribeirob specific settings
if [[ $QUEUENAME == "ribeirob" ]]; then
    WALLTIME="90:00:00"
else 
    echo "Invalid queue $QUEUENAME"
    quit
fi


MEM="$((MEMPERTSK))gb"

# submits job batch
for TEST_IDX in $(seq $START_TEST $END_TEST);
do
    OUTFILE=jobs/${DATASET}.${TEST_IDX}.sh

    cat script_template.sh | sed \
        -e "s/NODES/$NODES/" \
        -e "s/PPN/$PPN/" \
        -e "s/QUEUENAME/$QUEUENAME/" \
        -e "s/WALLTIME/$WALLTIME/" \
        -e "s/MEM/$MEM/" \
        -e "s/DATASET/$DATASET/" \
        -e "s/SEED_IDX/$TEST_IDX/" \
        > $OUTFILE

    echo qsub `pwd`/$OUTFILE
    qsub $OUTFILE
done

