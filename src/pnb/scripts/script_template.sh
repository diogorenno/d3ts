#!/bin/bash

#
#PBS -l nodes=NODES:ppn=PPN
#
#PBS -q QUEUENAME
#PBS -l naccesspolicy=shared
#PBS -l walltime=WALLTIME
#PBS -l naccesspolicy=singleuser
#PBS -l mem=MEM
#
#PBS -j oe
#

source /etc/profile.d/modules.sh
module load devel
echo " "
echo " "
echo "Job started on `hostname` at `date`"
#
#    User code starts now
#

echo "Replaced placeholders:"
echo NODES
echo PPN
echo QUEUENAME
echo WALLTIME
echo MEM
echo DATASET
echo SEED_IDX

# Builds the output path.
OUTPATH=$RCAC_SCRATCH/p2r_results/pnb/DATASET/

# Changes to src/pnb directory.
CODEDIR=~/pay2recruit/project/src/pnb/
cd $CODEDIR

# Checks if output path exists (and creates it if needed).
if [[ ! -d "$OUTPATH" ]]; then
    echo "Creating output folder $OUTPATH"
    mkdir -p $OUTPATH
else
    echo "Saving results to $OUTPATH"
fi

# Runs PNB.
./pnb  DATASET  SEED_IDX  $OUTPATH

#
#    End user code
#
echo "Job ended on `hostname` at `date`"

