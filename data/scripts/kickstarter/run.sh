#!/bin/bash

cd "$(dirname "$0")"

# generate base files from mysql DB
if [ ! -f susceptible_backers.txt  ] || [ ! -f test_backers.txt  ]; then
    if [ ! $# -eq 2 ]; then
        echo "Incorrect number of parameters."
        echo "Usage:"
        echo "    $0 <mysql username> <mysql database>"
    else
        mysql  -u $USERNAME -p $DATABASE < kickstarter.sql
    fi
fi

# generate graph, based on cliques
python gen_graph.py

# extract giant connected component and filter attributes for these nodes
python gen_attribs.py

# delete files no longer used
rm kickstarter.adj.gz

# move the pre-processed dataset
mv kickstarter.*.txt.gz ../../datasets/kickstarter/

cd -
