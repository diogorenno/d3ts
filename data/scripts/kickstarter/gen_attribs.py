import fileinput
import csv
import gzip
import sys
import ipdb

attribs_file='susceptible_backers.txt'
new_attribs_file='kickstarter.attribs.txt.gz'
targets_file='test_backers.txt'

#target_id=7793


graph_file='kickstarter.adj.gz'
new_graph_file='kickstarter.ungraph.txt.gz'

def possibleTestsets():
    csv.field_size_limit(sys.maxsize)
    backers = []
    # get susceptible backers: all backers till end of 2012
    with open(attribs_file) as tsvin:
        tsvin = csv.reader(tsvin,delimiter='\t')
        for line in tsvin:
            backers += line

    backers = frozenset(backers)
    print len(backers)

    # count how many susceptible backers reappear in 2013 projects
    counts = []
    with open('test_backers.txt') as tsvin:
        tsvin = csv.reader(tsvin,delimiter='\t')
        for line in tsvin:
            tmp = len(backers.intersection(line))
            counts.append( (tmp*1.0/len(line),tmp,len(line), len(counts)) )

    counts.sort(reverse=True,key=lambda x: x[1])
    # print 'fraction\tabsolute\t|backers|\tproject_newid'
    # for t in counts:
    #     print '{:.2}\t{}\t{}\t{}'.format(t[0],t[1],t[2],t[3])
 
    print counts[0:3]
    return [x[3] for x in counts[0:3]]



old2new = dict()
target_project_ids = possibleTestsets()

# filter attributes for nodes that appear in the graph, while assigning contiguous ids
with open(attribs_file,'r') as tsvin, open(targets_file,'r') as tsvin2, gzip.open(new_attribs_file,'w') as tsvout:
    tsvin = csv.reader(tsvin,delimiter='\t')
    tsvin2 = csv.reader(tsvin2,delimiter='\t')
    tsvout = csv.writer(tsvout,delimiter='\t')
    print 'Processing attributes file'
    for line in tsvin:
        ids = map(int,line)
        for oldid in ids:
            if oldid not in old2new:
                old2new[oldid] = len(old2new)
        tsvout.writerow([old2new[oldid]+1 for oldid in ids])

    print 'Processing targets file'
    project2backers = dict()
    for idx,line in enumerate(tsvin2):
        if idx in target_project_ids:
            ids = map(int,line)
            project2backers[idx] = ids

    print 'Saving list of targets'
    for idx in target_project_ids:
        tsvout.writerow([old2new[oldid]+1 for oldid in project2backers[idx] if oldid in old2new])

maxid_attrib = len(old2new)
maxid_graph = -1

# save attributes to file
with gzip.open(graph_file,'r') as tsvin, gzip.open(new_graph_file,'w') as tsvout:
    tsvin = csv.reader(tsvin,delimiter='\t')
    tsvout = csv.writer(tsvout,delimiter='\t')
    print 'Processing graph file'
    for line in tsvin:
        ids = map(int,line)
        for oldid in ids:
            if oldid not in old2new:
                old2new[oldid] = len(old2new)
        newids=[old2new[oldid]+1 for oldid in ids]
        tsvout.writerow(newids)
        maxid_graph = max(newids+[maxid_graph])

    # if isolated target nodes appear with id larger than maxid_graph,
    # make sure to connect it to a stub node, to prevent indexing problems
    print 'max id from attrib file:', maxid_attrib, '\nmax id from graph file:', maxid_graph
    if maxid_attrib > maxid_graph:
        tsvout.writerow([maxid_attrib+1,maxid_attrib+2])
