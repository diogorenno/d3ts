set @split=2012;
set @maxbackers=1000;
set @minbackers=100;

SET group_concat_max_len=1500000;
SELECT bp.project_id
    FROM crawler_backer_project AS bp
    INNER JOIN crawler_project AS p
        ON bp.project_id = p.id
    WHERE YEAR(p.date_end) = @split AND p.date_end < p.date_scanned
    GROUP BY project_id 
    INTO outfile "/Users/fabricio/deleteme/test_projects.txt" FIELDS TERMINATED BY ','  LINES TERMINATED BY '\n';

SELECT GROUP_CONCAT(bp.backer_id SEPARATOR '\t') AS backers
    FROM crawler_backer_project AS bp
    INNER JOIN crawler_project AS p
        ON bp.project_id = p.id
    WHERE YEAR(p.date_end) = @split AND p.date_end < p.date_scanned
    GROUP BY project_id 
    INTO outfile "~/deleteme/test_backers.txt" FIELDS  TERMINATED BY ',' escaped BY '\\' LINES TERMINATED BY '\n';

SELECT bp.project_id
    FROM crawler_backer_project AS bp
    INNER JOIN crawler_project AS p
        ON bp.project_id = p.id
    WHERE p.raised < p.goal AND p.backers BETWEEN @minbackers AND @maxbackers AND YEAR(p.date_end) < @split
    GROUP BY project_id
    INTO outfile "~/deleteme/susceptible_projects.txt" FIELDS  TERMINATED BY ',' LINES TERMINATED BY '\n';

SELECT GROUP_CONCAT(bp.backer_id SEPARATOR '\t') AS backers
    FROM crawler_backer_project AS bp
    INNER JOIN crawler_project AS p
        ON bp.project_id = p.id
    WHERE p.raised < p.goal AND p.backers BETWEEN @minbackers AND @maxbackers AND YEAR(p.date_end) < @split
    GROUP BY project_id 
    INTO outfile "~/deleteme/susceptible_backers.txt" FIELDS  TERMINATED BY ',' escaped BY '\\' LINES TERMINATED BY '\n';

SELECT b.id,GROUP_CONCAT(date(p.date_end) ORDER BY p.date_end) FROM ( SELECT id FROM crawler_backer WHERE backed > 1 ) AS b inner join crawler_backer_project AS bp on b.id = bp.backer_id INNER JOIN crawler_project AS p on bp.project_id = p.id WHERE p.backers < 2000 group BY b.id limit 10;
