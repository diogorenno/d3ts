import projects
import donations
from datetime import datetime

# get maps: project->school, school->projects, project->date_posted
p2s,s2p,pd = projects.projects_by_school()

# get map: school->donation_count 
dc = donations.donations_per_school( p2s )

# find school S with most donations
school_id = max(dc,key=lambda k:dc[k])

# trainset: all projects from S posted before 2013
trainset = frozenset([p for p in s2p[school_id]  if pd[p] < datetime(2013,1,1) ])

# testset: all projects from S posted since 2013
testset  = frozenset([p for p in s2p[school_id]  if  datetime(2013,1,1) <= pd[p] < datetime(2014,1,1) ])

# use the trainset to generate a graph, use some projects in the testset to generate labels
p2d,targets,amounts = donations.gen_graph(trainset,testset)
