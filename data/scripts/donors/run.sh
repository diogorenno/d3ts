#!/bin/bash


cd "$(dirname "$0")"

# get network (topology and attributes)
# input: csv files
# output: donors.ungraph.txt.gz, donors.att.txt.gz
python get_network.py

# get giant connected component
# input: text file
# output: text file
gunzip donors.ungraph.txt.gz
python get_gcc.py donors.ungraph.txt donors.gcc.txt
gzip donors.gcc.txt

# list only attributes of nodes that appear in gcc graph
# input: gzipped graph, gzipped attributes
# output: gzipped attributes from nodes in gcc
python clean_attribs.py donors.gcc.txt.gz donors.attribs.txt.gz donors.att.txt.gz

# delete files no longer used
rm donors.ungraph.txt donors.attribs.txt.gz

# move the pre-processed dataset
mkdir -p ../../datasets/gccs/donors/
mv donors.*.txt.gz ../../datasets/gccs/donors/

cd -
