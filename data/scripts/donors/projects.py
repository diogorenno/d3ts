import csv
from collections import namedtuple, defaultdict
from datetime import datetime

def projects_by_school():
    filename='projects.csv'

    projects2school = dict()
    school2projects = defaultdict(set) # each entry stores a list pairs (timestamp,donor_id)
    dateposted = dict()

    print 'Obtaining info about projects'
    with open(filename,'r') as csvin:
        csvin = csv.reader(csvin)
        headers = csvin.next()
        Projects = namedtuple('Projects',headers)

        for p in map(Projects._make, csvin):
            projects2school[p.projectid] = p.schoolid
            school2projects[p.schoolid].add(p.projectid)
            dateposted[p.projectid] = datetime.strptime( p.date_posted[:10], "%Y-%m-%d")

    print 'Finding target school and its projects post dates'
    #school_id = max(school2projects, key=lambda k:len(school2projects[k]))
    #project_dates = [dateposted[p] for p in school2projects[school_id]]

    #trainset = frozenset([p for p in school2projects[school_id] if dateposted[p] < datetime(2013,1,1) ])
    #testset  = frozenset([p for p in school2projects[school_id] if datetime(2013,1,1) <= dateposted[p] < datetime(2014,1,1)])

    return (projects2school,school2projects,dateposted)

