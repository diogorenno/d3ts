#!/usr/bin/env python

""" Author: Fabricio Murai
    Creation date: 10/06/2015
    Last modified: 10/06/2015 11:39 AM EST
"""

import networkx as nx
import sys
import gzip
import csv

def main(infile, outfile, attfile, labind):
    """ Reads a directed network from a tsv file and writes its largest strongly connected component to a tsv file.
    """

    with gzip.open(attfile) as f:
        for i in xrange(labind+1):
            line = f.readline()

        hasLabelOne = frozenset([i for i in line.split('\t')])

    G = nx.read_edgelist(infile)
    cc_list = nx.connected_components(G)

    sizes = []
    for cc in cc_list:
        oneCounts = [i for i in cc if i in hasLabelOne]
        sizes.append((len(cc),len(oneCounts)))
    sizes = sorted(sizes,reverse=True)

    with open(outfile,'w') as tsv:
        tsv = csv.writer(tsv,delimiter='\t')
        for i in sizes:
            tsv.writerow(i)


if __name__ == '__main__':
    if len(sys.argv) == 5:
        infile = sys.argv[1]
        outfile = sys.argv[2]
        attfile = sys.argv[3]
        labind = int(sys.argv[4])
        main(infile, outfile, attfile, labind)
    else:
        sys.exit(1)
