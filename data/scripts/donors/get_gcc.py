#!/usr/bin/env python

""" Author: Fabricio Murai
    Creation date: 10/06/2015
    Last modified: 10/06/2015 11:39 AM EST
"""

import networkx as nx
import sys

def main(infile, outfile):
    """ Reads a directed network from a tsv file and writes its largest strongly connected component to a tsv file.
    """

    G = nx.read_edgelist(infile)
    maxid_original = max([int(i) for i in G.nodes()])

    largest_cc = max(nx.connected_components(G), key=len)
    maxid_gcc = max([int(i) for i in largest_cc])

    nx.write_edgelist(G.subgraph(largest_cc),outfile,data=False)

    if maxid_original == maxid_gcc:
        print "SUCCESS (max id inside gcc)"
    else:
        print "WARNING: max id not inside gcc"


if __name__ == '__main__':
    if len(sys.argv) == 3:
        infile = sys.argv[1]
        outfile = sys.argv[2]
        main(infile, outfile)
    else:
        sys.exit(1)
