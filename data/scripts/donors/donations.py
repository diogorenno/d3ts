import csv
from collections import namedtuple, defaultdict
from datetime import datetime, timedelta
import gzip
import numpy as np

def unique_donors():
    donors = list()
    filename='donations.csv'

    print 'Obtaining info about donations'
    with open(filename,'r') as csvin:
        csvin = csv.reader(csvin)
        headers = csvin.next()
        Donations = namedtuple('Donations',headers)
        for d in map(Donations._make, csvin):
            donors.append(d.donor_acctid)

    return frozenset(donors)

def donations_per_school( projects2school ):
    donation_counts = defaultdict(int)
    filename='donations.csv'

    print 'Obtaining info about donations'
    with open(filename,'r') as csvin:
        csvin = csv.reader(csvin)
        headers = csvin.next()
        Donations = namedtuple('Donations',headers)

        for d in map(Donations._make, csvin):
            donation_counts[projects2school[d.projectid]] += 1

    return donation_counts

def gen_graph( trainset, testset ):

    filename='donations.csv'
    ungraphfile='donors.ungraph.txt.gz'
    attribsfile='donors.attribs.txt.gz'
    max_interval=timedelta(days=2)

    #ungraphfile='donors2.ungraph.txt.gz'
    #attribsfile='donors2.attribs.txt.gz'
    #max_interval=timedelta(days=365)


    donor2newid = dict()
    project2donations = defaultdict(list) # each entry stores a list pairs (timestamp,donor_id)

    # var targets maps project->donors
    targets = defaultdict(set)
    newid2amount = defaultdict(float)

    print 'Obtaining info from donors'
    with open(filename,'r') as csvin:
        csvin = csv.reader(csvin)
        headers = csvin.next()
        Donations = namedtuple('Donations',headers)
        for d in map(Donations._make, csvin):
            if d.projectid in trainset:
                if d.donor_acctid not in donor2newid:
                    donor2newid[d.donor_acctid] = len(donor2newid)+1

                newid = donor2newid[d.donor_acctid]
                t = datetime.strptime( d.donation_timestamp[:19], "%Y-%m-%d %H:%M:%S")
                project2donations[d.projectid].append( (t,newid) )

            if d.projectid in testset:
                targets[d.projectid].add(d.donor_acctid)


    for projectid in targets.keys():
        targets[projectid] = [donor2newid[donor] for donor in targets[projectid] if donor in donor2newid]

    print 'Saving mapping from donor_acctid to new ids'
    with gzip.open('donor2newid.txt.gz','w') as tsvout:
        tsvout = csv.writer(tsvout,delimiter='\t')
        for item in donor2newid.items():
            tsvout.writerow(item)

    print 'Determining graph'
    graph = defaultdict(set)
    for p in project2donations:
        project2donations[p].sort()
        for idx,(t1,d1) in enumerate(project2donations[p]):
            for jdx in xrange(idx+1,len(project2donations[p])):
                t2,d2 = project2donations[p][jdx]
                if t2-t1 < max_interval:
                    if d1 != d2:
                        graph[min(d1,d2)].add(max(d1,d2))
                else:
                    break

    print 'Saving graph'
    with gzip.open(ungraphfile,'w') as tsvout:
        tsvout = csv.writer(tsvout,delimiter='\t')
        for u,neighbors in graph.items():
            for v in neighbors:
                tsvout.writerow([u,v]);

    project2donors = { k:frozenset([newid for t,newid in v]) for k,v in project2donations.items() }
    print 'Saving attribs'
    with gzip.open(attribsfile,'w') as tsvout:
        tsvout = csv.writer(tsvout,delimiter='\t')
        for k in project2donors:
            tsvout.writerow(list(project2donors[k]))

    # from testset, select project that has more targets
    projectids_sorted = sorted(targets, key=lambda k:-len(targets[k]))

    for target_projectid in projectids_sorted[:3]:
        print 'Target project id is', target_projectid, 'size', len(targets[target_projectid])

        amounts = defaultdict(float)
        #print 'Checking donated amounts'
        #with open(filename,'r') as csvin:
        #    csvin = csv.reader(csvin)
        #    headers = csvin.next()
        #    Donations = namedtuple('Donations',headers)
        #    for d in map(Donations._make, csvin):
        #        if d.projectid == target_projectid:
        #            amount = float(d.donation_total)
        #            amounts[d.donor_acctid] += amount
        #            if d.donor_acctid in donor2newid:
        #                newid2amount[donor2newid[d.donor_acctid]] += amount

        #amounts = amounts.values()


        #print 'Saving mapping new ids to donation amounts'
        #with gzip.open('newid2amount.txt.gz','w') as tsvout:
        #    tsvout = csv.writer(tsvout,delimiter='\t')
        #    for item in newid2amount.items():
        #        tsvout.writerow(item)


        print 'Saving target project '+target_projectid+', |projects|='+str(len(project2donors))+', |target|='+str(len(targets[target_projectid]))
        with gzip.open(attribsfile,'a') as tsvout:
            tsvout = csv.writer(tsvout,delimiter='\t')
            tsvout.writerow(targets[target_projectid]);
            #threshold = np.median(newid2amount.values())
            threshold = 0
            #tsvout.writerow([k for k,val in newid2amount.items() if val > threshold])

    return (project2donations,targets,amounts)
