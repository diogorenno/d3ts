#!/usr/bin/env python

""" Author: Fabricio Murai
    Creation date: 01/27/2015
    Last modified: 01/27/2015 11:10 AM EST
"""

import sys
import csv
import gzip

def main(ungraphfile, attribsfile, outfile):
    """ What does it do?
    """

    nodes = set()
    with gzip.open(ungraphfile, 'rb') as f:
        for line in f:
            for nodeid in line.split():
                nodes.add(nodeid)

    with gzip.open(attribsfile, 'rb') as fin, gzip.open(outfile,'wb') as fout:
        tsvout = csv.writer(fout,delimiter='\t')
        for line in fin:
            nodeids = [nodeid for nodeid in line.split() if nodeid in nodes]
            tsvout.writerow(nodeids)

if __name__ == '__main__':
    if len(sys.argv) == 4:
        ungraphfile = sys.argv[1]
        attribsfile = sys.argv[2]
        outfile = sys.argv[3]
        main(ungraphfile,attribsfile,outfile)
    else:
        sys.exit(1)
