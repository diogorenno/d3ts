import csv
import sys
import gzip
from collections import defaultdict, deque



def gen_graph():
    ungraphfile = 'citeseer.ungraph.txt.gz'
    gccfile = 'citeseer.gcc.txt.gz'

    print 'Reading cliques'
    graph = defaultdict(set)
    with gzip.open(ungraphfile) as tsvin:
        tsvin = csv.reader(tsvin,delimiter=' ')
        for i,line in enumerate(tsvin):
            ids = map(int,line)
            for idx in ids:
                for jdx in ids:
                    if idx != jdx:
                        graph[idx].add(jdx)
                        graph[jdx].add(idx)

    # run BFS from node 16
    marked = {v:False for v in graph}
    queue = deque()
    seed = 16
    queue.append(seed)
    marked[seed] = True

    while len(queue) > 0:
        v = queue.pop()
        for u in graph[v]:
            if not marked[u]:
                queue.append(u)
                marked[u] = True

    print 'Saving graph'
    with gzip.open(gccfile,'w') as tsvout:
        tsvout = csv.writer(tsvout,delimiter='\t')
        for v in graph:
            if marked[v]:
                for u in graph[v]:
                    if v < u:
                        tsvout.writerow([v,u])

if __name__ == "__main__":
    gen_graph()
    #possibleTestsets()
