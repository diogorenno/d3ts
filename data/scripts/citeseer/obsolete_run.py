import gzip
import csv
from collections import defaultdict, deque
import ipdb

# read edge list and extract giant connected component
def gen_graph(paper2conf):
    ungraphfile = 'citeseer.gcc.txt.gz'

    print 'Reading edges'
    graph = defaultdict(set)
    with open('edge_list') as tsvin:
        tsvin = csv.reader(tsvin,delimiter=' ')
        for i,line in enumerate(tsvin):
            ids = map(int,line)
            if paper2conf[ids[0]] <= 10 and paper2conf[ids[1]] <= 10:
                for idx in ids:
                    for jdx in ids:
                        if idx != jdx:
                            graph[idx].add(jdx)
                            graph[jdx].add(idx)

    # run BFS from node 1
    marked = {v:False for v in graph}
    queue = deque()
    seed = 1
    queue.append(seed)
    marked[seed] = True

    while len(queue) > 0:
        v = queue.pop()
        for u in graph[v]:
            if not marked[u]:
                queue.append(u)
                marked[u] = True

    print 'Saving graph'
    with gzip.open(ungraphfile,'w') as tsvout:
        tsvout = csv.writer(tsvout,delimiter='\t')
        for v in graph:
            if marked[v]:
                for u in graph[v]:
                    if v < u:
                        tsvout.writerow([v,u])

    return marked

def readConferences(K=10):
    paper2conf = dict()
    counts = defaultdict(int)
    with open('labels') as tsvin:
        tsvin = csv.reader(tsvin,delimiter=' ')
        for i,line in enumerate(tsvin):
            ids = map(int,line)
            paper2conf[ids[0]] = ids[1]
            counts[ids[1]] += 1

    sorted_keys = sorted(counts, key=lambda k: counts[k])
    top_keys = sorted_keys[-10:-1]
    return paper2conf


def gen_attribs(marked):
    group2id = defaultdict(set)
    with open('labels') as tsvin:
        tsvin = csv.reader(tsvin,delimiter=' ')
        for i,line in enumerate(tsvin):
            ids = map(int,line)
            if ids[0] in marked and marked[ids[0]]:
                group2id[ids[1]].add(ids[0])


    ipdb.set_trace()
    attfile = 'citeseer.att.txt.gz'
    with gzip.open(attfile,'w') as tsvout:
        tsvout = csv.writer(tsvout,delimiter='\t')
        for k in group2id.keys():
            tsvout.writerow(list(group2id[k]))





if __name__ == "__main__":
    paper2conf = readConferences()
    marked = gen_graph(paper2conf)
    gen_attribs(marked)