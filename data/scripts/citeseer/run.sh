#!/bin/bash

cd "$(dirname "$0")"

# generate graph and attributes
Rscript script.R

if [ "$?" -ne 0 ]; then
    echo "Error executing R script. Make sure you have R package 'hash'."
    echo "Open R and try:"
    echo "install.packages('hash')"
    exit 1
fi

# get the gcc
python gen_graph.py

# list only attributes of nodes that appear in gcc graph
# input: gzipped graph, gzipped attributes
# output: gzipped attributes from nodes in gcc
python clean_attribs.py citeseer.gcc.txt.gz citeseer.attribs.txt.gz citeseer.att.txt.gz

# delete files no longer used
 rm citeseer.ungraph.txt.gz citeseer.attribs.txt.gz

# move the pre-processed dataset
mkdir -p ../../datasets/gccs/citeseer/labeled/
 mv citeseer.*.txt.gz ../../datasets/gccs/citeseer/labeled/

cd -
