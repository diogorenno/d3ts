#!/bin/bash


octave -qf << EOT

% loads
% A: 5000x5000 adjacency matrix
% labels: 5000x1 categorical vector, indexed from 1 to 5 
load populated_places_5000.mat;

% save edges to E
[row,col] = find(A);
E = [row,col];
% matrix is symmetric, remove redundant entries
E = E(E(:,1)<=E(:,2),:);
E = sortrows(E);
% write to file
dlmwrite('dbpedia.ungraph.txt',E,' ');

outfile = fopen('dbpedia.attribs.txt','w');
for i=1:5
    z = find(labels==i);
    for j=1:length(z)
        fprintf(outfile,'%d\t',z(j));
    end
    fprintf(outfile,'\n');
end
fclose(outfile);

EOT

gzip dbpedia.*.txt

mkdir -p data/datasets/wang2013/dbpedia/done/
mv dbpedia.*.txt.gz data/datasets/wang2013/dbpedia/done/
