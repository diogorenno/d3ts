#!/bin/bash


octave -qf << EOT

% loads
% A: 5271x5271 adjacency matrix
% topic_vectors: 5271x200 matrix indicating strength of each topic 
load wikipedia_data.mat;

% save edges to E
[row,col] = find(A);
E = [row,col];
% matrix is symmetric, remove redundant entries
E = E(E(:,1)<=E(:,2),:);
E = sortrows(E);
% write to file
dlmwrite('wikipedia.ungraph.txt',E,' ');


% use threshold>=0.4 to determine whether each topic is present or not
topic_boolean = topic_vectors>=0.4;
outfile = fopen('wikipedia.attribs.txt','w');
for i=1:200
    z = find(topic_boolean(:,i));
    n = length(z);
    if n>0
        for j=1:n
            fprintf(outfile,'%d\t', z(j));
        end
        fprintf(outfile,'\n');
    end
end
fclose(outfile);

EOT

gzip wikipedia.*.txt

mkdir -p data/datasets/wang2013/wikipedia/done/
mv wikipedia.*.txt.gz data/datasets/wang2013/wikipedia/done/
