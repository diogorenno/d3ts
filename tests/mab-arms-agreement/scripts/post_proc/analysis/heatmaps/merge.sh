##
# Merges cropped heatmaps into a single PDF file for each dataset and method.
##

for method in pearson kendall spearman; do
    for dataset in donors citeseer dbpedia wikipedia kickstarter; do 
        pdfjam $(ls cropped/${dataset}_${method}*) --nup 4x6 --outfile merged/${dataset}_${method}.pdf &
    done
done

