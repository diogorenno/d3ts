#!/bin/bash
#
#PBS -l nodes=1:ppn=20
#
#PBS -q standby
#PBS -l walltime=3:50:00
#PBS -l naccesspolicy=singleuser
#PBS -l mem=200gb
#
#PBS -j oe
#

source /etc/profile.d/modules.sh
module load r
echo " "
echo " "
echo "Job started on `hostname` at `date`"
#
#    User code starts now
#

CODEDIR=~/pay2recruit/project/tests/mab-arms-agreement/scripts/post_proc/analysis/
cd $CODEDIR

Rscript run.R FILENAME  METHOD  NPROC

#
#    End user code
#
echo "Job ended on `hostname` at `date`"

