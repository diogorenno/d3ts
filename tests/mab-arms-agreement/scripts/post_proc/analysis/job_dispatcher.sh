#!/bin/bash

DATASETS=(donors dbpedia wikipedia citeseer kickstarter)
#METHODS=(spearman pearson kendall)
METHODS=(spearman)
NPROC=20

for DATASET in ${DATASETS[*]}; do
#    FILENAME=$(ls /scratch/lustreE/d/drennoro/p2r_results/mab-arms-agreement/dts.5_max_long.config2_all/extracted/${DATASET}_*.scores.RData)
    FILENAME=$(ls /scratch/lustreE/d/drennoro/p2r_results/mab-arms-agreement/rr_max_short.config2_all/extracted/${DATASET}_*.scores.RData)
    for METHOD in ${METHODS[*]}; do
        OUTFILE=pbs_job.${DATASET}.${METHOD}.sh
        cat job_template.sh | sed \
            -e "s@FILENAME@$FILENAME@" \
            -e "s/METHOD/$METHOD/" \
            -e "s/NPROC/$NPROC/" \
            > jobs/$OUTFILE
        echo qsub `pwd`/jobs/$OUTFILE
        qsub jobs/$OUTFILE
    done
done

