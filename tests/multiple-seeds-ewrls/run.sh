#!/bin/bash

ntests=2
nproc=2
nsteps=-1 # will be overriden
lambda=1.0
BACKUPRESTORE=0


#beta=1.0
for beta in 1.0; do
    if [[ $BACKUPRESTORE -eq 1 ]]; then
        mv model.R model.R.bkp
        nproc=10
    fi

    OUTPATH=beta${beta}_lambda${lambda}/
    if [[ ! -d "$OUTPATH" ]]; then
        mkdir -p $OUTPATH
    fi

    sed -e "s/XXX/$beta/g" -e "s/YYY/$lambda/g" model_base.R > model.R

    #for dataset in donors; do
    for dataset in donors; do
        Rscript test.R $dataset $ntests 1 1 $nsteps ${OUTPATH} $nproc &
    done

    sleep 5

    if [[ $BACKUPRESTORE -eq 1 ]]; then
        mv model.R.bkp model.R 
    fi
done
