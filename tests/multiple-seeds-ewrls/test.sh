#!/bin/bash

DATASET="$1"
NPROC="$2"

Rscript test.R $DATASET 50 20   1   2  "output/" $NPROC
Rscript test.R $DATASET 50 20   2   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1   3  "output/" $NPROC
Rscript test.R $DATASET 50 20   3   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1   4  "output/" $NPROC
Rscript test.R $DATASET 50 20   4   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1   5  "output/" $NPROC
Rscript test.R $DATASET 50 20   5   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1   6  "output/" $NPROC
Rscript test.R $DATASET 50 20   6   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1   7  "output/" $NPROC
Rscript test.R $DATASET 50 20   7   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1   8  "output/" $NPROC
Rscript test.R $DATASET 50 20   8   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1   9  "output/" $NPROC
Rscript test.R $DATASET 50 20   9   1  "output/" $NPROC
Rscript test.R $DATASET 50 20   1  10  "output/" $NPROC
Rscript test.R $DATASET 50 20  10   1  "output/" $NPROC


