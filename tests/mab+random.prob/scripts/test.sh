#!/bin/bash

DATASET="$1"
TEST_FIRST="$2"
TEST_FINAL="$3"
NPROC="$4"

# proc_args <- list(
#   dataset            = raw_args[1],
#   test_range_start   = as.numeric(raw_args[2]),
#   test_range_end     = as.numeric(raw_args[3]),
#   nseeds             = as.numeric(raw_args[4]),
#   msc                = raw_args[5],
#   ncold              = as.numeric(raw_args[6]),
#   heuristic          = raw_args[7],
#   parallel_updates   = as.logical(as.numeric(raw_args[8])),
#   settings_init      = raw_args[9],
#   output_path        = raw_args[10],
#   nproc              = as.numeric(raw_args[11]))


Rscript scripts/test.R  $DATASET  $TEST_FIRST  $TEST_FINAL  1  ucb1  20  mod  0  settings/svm.settings.init.R   output/svm_p.000/   $NPROC 0.000
Rscript scripts/test.R  $DATASET  $TEST_FIRST  $TEST_FINAL  1  ucb1  20  mod  0  settings/svm.settings.init.R   output/svm_p.025/   $NPROC 0.025
Rscript scripts/test.R  $DATASET  $TEST_FIRST  $TEST_FINAL  1  ucb1  20  mod  0  settings/svm.settings.init.R   output/svm_p.050/   $NPROC 0.050
Rscript scripts/test.R  $DATASET  $TEST_FIRST  $TEST_FINAL  1  ucb1  20  mod  0  settings/svm.settings.init.R   output/svm_p.100/   $NPROC 0.100
Rscript scripts/test.R  $DATASET  $TEST_FIRST  $TEST_FINAL  1  ucb1  20  mod  0  settings/svm.settings.init.R   output/svm_p.150/   $NPROC 0.150
Rscript scripts/test.R  $DATASET  $TEST_FIRST  $TEST_FINAL  1  ucb1  20  mod  0  settings/svm.settings.init.R   output/svm_p.200/   $NPROC 0.200


