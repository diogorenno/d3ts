#!/bin/bash

# cluster parameters
NODES=1
PPN=20
QUEUENAME=standby
WALLTIME="4:00:00"
MEM=200gb

# simulation parameters
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
fi
DATASET=$1
MSC="rr"
SETTINGS="config2"
NTESTS=50

# dataset specific settings
MAXTESTS=1000
case $DATASET in
    dbpedia)
        MAXTESTS=725
	;;
    citeseer)
        MAXTESTS=1583
	;;
    wikipedia)
        MAXTESTS=202
	;;
    blogcatalog)
        MAXTESTS=986
	;;
    donors)
        MAXTESTS=56
	;;
    dblp)
        MAXTESTS=7556
	;;
    lj)
        MAXTESTS=1441
	;;
    kickstarter)
        MAXTESTS=1457
	;;
    *)
        echo "Invalid dataset $1"
        quit
        ;;
esac

if [[ $NTESTS -gt $MAXTESTS ]]; then
    echo "Overriding NTESTS; now set to $MAXTESTS"
    NTESTS=$MAXTESTS
fi

# ribeirob specific settings
if [[ $QUEUENAME == "ribeirob" ]]; then
    NODES=1
    PPN=20
    WALLTIME="60:00:00"
fi

# "acconte" for the fact that conte has only 16 cores 
HOSTNAME=`hostname | cut -d- -f1`
if [[ $HOSTNAME == "conte" ]]; then
    PPN=16
fi



START_TEST=1
while [[ $START_TEST -le $NTESTS ]]; do

    # determine end test index
    if [[ $((START_TEST+PPN-1)) -gt $NTESTS ]]; then
        END_TEST=$NTESTS
    else
        END_TEST=$((START_TEST+PPN-1))
    fi

    TEST_RANGE="$START_TEST $END_TEST"
    TEST_RANGE_STR=`printf "%03d_%03d" $START_TEST $END_TEST`
    OUTFILE=${MSC}.${SETTINGS}.${DATASET}.${TEST_RANGE_STR}.sh

    cat script_template.sh | sed \
        -e "s/NODES/$NODES/" \
        -e "s/PPN/$PPN/" \
        -e "s/QUEUENAME/$QUEUENAME/" \
        -e "s/WALLTIME/$WALLTIME/" \
        -e "s/MEM/$MEM/" \
        -e "s/DATASET/$DATASET/" \
        -e "s/TEST_RANGE/$TEST_RANGE/" \
        -e "s/MSC/$MSC/" \
        -e "s/SETTINGS/$SETTINGS/" \
        > $OUTFILE

    qsub $OUTFILE

    START_TEST=$((END_TEST+1))
done
