library(R6)

library(nnet)
library(Pay2Recruit)

source("model.R")
source("observed.R")
source("settings.R")

##
 #
 ##
Simulator <- R6Class("Simulator",

    public = list(
    
        ##
         # Initializes a new instance.
         ##
        initialize = function(name, graph, settings, external_feats)
        {
          stopifnot(settings$validate())
          
          private$settings <- settings$clone()
          private$graph <- graph
          private$name <- name
          private$observed_graph <- ObservedGraph$new(private$settings)
          private$external_feats <- external_feats
        },
        
        ##
         #
         ##
        simulate = function(seeds, nturns)
        {
          stopifnot(private$settings$validate())
          private$restart(seeds)
          stopifnot(private$turn < nturns)
          private$runTurns(1 + nturns - private$turn)
        },
        
        ##
         # Returns results of the simulation at the given turn.
         ##
        getSimulationResultsAtTurn = function(turn)
        {
          stopifnot(turn < private$turn)
          
          payoff <- self$getTotalPayoffAtTurn(turn)
          recruited  <- private$selected_nodes[1:turn]
          nrecruited <- length(recruited)
          return (SimulationResult$new(payoff, 
                                       nrecruited, 
                                       payoff / nrecruited, 
                                       recruited[nrecruited],
                                       private$selected_models[turn],
                                       private$observed_feats[turn,]))
        },
        
        ##
         # Returns the total payoff of nodes recruited up to the given turn.
         ##
        getTotalPayoffAtTurn = function(turn)
        {
          total <- 0.0
          nodes <- private$getRecruitedUpToTurn(turn)
          for (i in 1:length(nodes)) {
            id <- nodes[i]
            label <- private$recruited[[id]]$label
            total <- total + label
          }
          return (total)
        },
        
        ##
         # Allows internal access to the instance for testing/debugging purposes.
         ##
        browse = function()
        {
          browser()
        }
    ),
    
    private = list(
    
        name = NA,               # (character) Simulation name.
        settings = NA,           # (Settings) Simulation and model settings.
        
        graph = NA,              # (Graph) The full graph (underlying graph).
        observed_graph = NA,     # (ObservedGraph) The currently observed graph.
        models = NA,             # (list) The list of models used during the recruitment process.

        turn = NA,               # (integer >= 0) The simulation turn counter.        
        recruited = NA,          # (list) A list with the IDs of all nodes recruited during the simulation.
        
        selected_nodes = NA,     # (vector) Vector with IDs of all recruited nodes, in recruitment order.
        selected_models = NA,    # (vector) Vector with the ID of the model selected in each turn.
        
        observed_feats = NA,     # (matrix) columns 1:i compose design matrix at turn i.
        external_feats = NA,     # (matrix) columns 1:i compose external design matrix at turn i.
        
        # Class constants.
        NEWLY_RECRUITED = 1,
        EMPTY_BORDER = 3,

        ##
         # Prints a message from this simulator.
         ##
        display = function(msg)
        {
          cat(paste(private$name, ": ", msg, sep=""))
        },

        ##
         # Returns TRUE if the node with the given ID has been recruited.
         # Returns FALSE otherwise.
         ##
        hasRecruited = function(id)
        {
          return (id %in% names(private$recruited))
        },

        ##
         # Recruits a new node and updates all models.
         # Returns the node info.
         ##
        recruitNode = function(node_id, border_check=TRUE)
        {
          stopifnot(!private$hasRecruited(node_id))
          
          # Gets the node info.
          #private$display(sprintf("node_id %s\n", node_id))
          node_info <- private$getNodeInfo(node_id)
          
          # Adds the new sample to the list of recruited nodes.
          private$selected_nodes <- c(private$selected_nodes, node_id)
          private$recruited[[node_id]] <- list(turn = private$turn, 
                                               label = node_info$label)
          
          # Adds the new node to the observed graph.
          private$observed_graph$recruitNode(node_id, node_info, border_check)

          # Adds the new design matrix to the list of observed design matrices.
          private$observed_feats <- rBind(private$observed_feats, private$observed_graph$features[node_id,])

          # Updates all models.
          Map(function(i) private$models[[i]]$recruitNode(node_id), 1:private$settings$nmodels)
          
          return (node_info)
        },
        
        ##
         # Returns the IDs of all nodes recruited up to the given turn.
         ##
        getRecruitedUpToTurn = function(turn)
        {
          return (names(Filter(function(x) x$turn <= turn, private$recruited)))
        },
        
        ##
         # Returns a list with the indices of all models in private$models which are currently valid.
         ##
        getValidModels = function()
        {
          return (unlist(Filter(function(i) private$models[[i]]$isValid(),
                                1:private$settings$nmodels)))
        },

        ##
         # Starts the simulator.
         # Creates the models from new seeds and cold-start them with additional samples.
         ##
        restart = function(seeds)
        {
          nseeds <- private$settings$nseeds
          nmodels <- private$settings$nmodels
        
          # Initializes internal data structures.
          private$models <- list()
          private$recruited <- list()
          private$turn <- 1
          private$observed_feats <- NULL
          private$selected_nodes <- NULL
          private$selected_models <- NULL
        
          # Creates the models.
          private$display("Creating models...\n")
          private$models <- Map(function(i) Model$new(private$settings, 
                                                      private$settings$models[[i]],
                                                      private$observed_graph,
                                                      private$external_feats),
                                1:nmodels)
          
          # Recruits the seeds.
          private$display("Seeds:\n"); print(seeds)
          for (i in 1:nseeds) {
            private$recruitNode(seeds[i], border_check=FALSE)
            private$turn <- 1 + private$turn
          }
          
          # Runs the cold start.
          i <- 0
          heuristic <- private$settings$initial_heuristic
          private$display(sprintf("Cold-start criterion: %s\n", heuristic))
          while ((i < private$settings$ncold) | (length(private$getValidModels()) == 0)) {
            # Recruits one more node using the configured heuristic.
            node <- private$recruitWithHeuristic(private$settings$initial_heuristic)
            private$turn <- 1 + private$turn
            # If "MOD until N zeros", increments i only when zero-labeled nodes are recruited.
            i <- i + (if (heuristic == "modunz") (node$node_info$label == 0) else 1)
            # Shows the status of the cold-start process.
            private$display(sprintf("Cold-start status: %d out of %d\r", i, private$settings$ncold))
          }
          private$display("\n")
        },
        
        ##
         # Recruits one node with the heuristic specified in the settings.
         ##
        recruitWithHeuristic = function(heuristic=NA)
        {
          node_id <- if (heuristic == "random") private$observed_graph$getRandomSuggestion()
                else if (heuristic %in% c("mod", "modunz")) private$observed_graph$getModSuggestion()
          node_info <- private$recruitNode(node_id)
          return (list(node_id=node_id, node_info=node_info))
        },
        
        ##
         #
         ##
        runTurns = function(nturns)
        {
          # Recruits the most prominent node in each turn, until there are no more turns or no more nodes.
          final_turn <- private$turn + nturns
          while (private$turn < final_turn) {
          
            # Reports simulation status.
            if ((!private$settings$output_to_file) |
                (private$settings$output_to_file & ((private$turn %% 10) == 0))) {
              private$display(sprintf("Turn %03d\r", private$turn))
            }
            
            # Performs model selection and recruits the most prominent border node.
            ret <- private$recruitNextNode()
            
            if (ret == private$NEWLY_RECRUITED) {
              # Advances to the next turn.
              private$turn <- 1 + private$turn
            } else if (ret == private$EMPTY_BORDER) {
              # Stops since there are no more nodes to recruit.
              private$display(paste("\nNo more border nodes to recruit (turn ", private$turn, ").\n", sep=""))
              break
            }
          }
          private$display("\n")
        },

        ##
         #
         ##
        recruitNextNode = function()
        {
          # Checks if the border set is empty.
          if (length(private$observed_graph$border) == 0) {
            return (private$EMPTY_BORDER)
          }
          
          # Might recruit a random border node according to the configured probability.
          if (private$settings$random_prob > 0) {
            if (runif(1) <= private$settings$random_prob) {
              private$recruitWithHeuristic("random")
              return (private$NEWLY_RECRUITED)
            }
          }
          
          # Lists which models are ready.
          valid_models <- private$getValidModels()
          
          # Performs model selection.
          selected_model <- private$selectNextModel(valid_models)
          selected_model_index <- which(valid_models == selected_model)
          private$selected_models <- c(private$selected_models, selected_model)
          
          # Evaluates the border nodes.
          if (private$settings$parallel_updates) {
            # If updating all possible models, evaluates using all valid models.
            evals <- Map(function(model_id) private$rankWithModel(model_id), valid_models)
          } else {
            # Otherwise, evaluates the border set using only the selected model.
            evals <- Map(function(model_id) if (model_id != selected_model) list()
                                            else private$rankWithModel(model_id),
                         valid_models)
          }
          
          # Recruits the most prominent node (as ranked by the selected model).
          selected_node <- evals[[selected_model_index]]$node_id
          node_info <- private$recruitNode(selected_node)
          
          # Updates model statistics.
          if (private$settings$parallel_updates) {
            # Updates all models which recommended the selected node.
            agreers <- which(unlist(Map(function(x) x$node_id, evals)) == selected_node)
            Map(function(i) private$models[[i]]$addFeedback(correct=(node_info$label > 0)),
                valid_models[agreers])
          } else {
            # Updates only the selected model.
            private$models[[selected_model]]$addFeedback(correct=(node_info$label > 0))
          }
          
          return (private$NEWLY_RECRUITED)
        },

        ##
         #
         ##
        rankWithModel = function(model_id=NA)
        {
          # Estimates border node payoffs with the specified model.
          rank <- private$models[[model_id]]$evaluateBorder()
          #
          if (!is.null(rank)) {
            best <- names(rank)[which.is.max(rank)]
            return (list(model_id=model_id,
                         node_id=best, 
                         payoff=rank[best]))
          }
          # This model's border is empty.
          return (NA)
        },

        ##
         #
         ##
        selectNextModel = function(models=NA)
        {
          if (private$settings$model_selection_method == "rr")
            return (models[private$turn %% length(models) +1])

          scores <- unlist(Map(function(i) private$models[[i]]$getModelSelectionScore(private$turn), 
                               models))
          return (models[which.is.max(scores)])
        },
        
        ##
         # Wrapper for the Graph::getInfo() method.
         # Returns a list with information about the node with the given ID.
         # TODO: maybe use a cache here?
         ##
        getNodeInfo = function(character_id=NA)
        {
          id <- as.numeric(character_id)
          info <- private$graph$getInfo(id)
          info$neighbors <- as.character(info$neighbors)
          return (info)
        }
    )
)


##
 #
 ##
SimulationResult <- R6Class("SimulationResult",

    public = list(
    
        total_payoff = NA,
        nrecruited = NA,
        mean_payoff = NA,
        recruited_id = NA,
        model_id = NA,
        observed_feats = NA,
    
        initialize = function(total_payoff=NA, nrecruited=NA, mean_payoff=NA, recruited_id=NA, model_id=NA, observed_feats=NA)
        {
          self$total_payoff <- total_payoff
          self$nrecruited <- nrecruited
          self$mean_payoff <- mean_payoff
          self$recruited_id <- recruited_id
          self$model_id <- model_id
          self$observed_feats <- observed_feats
        }
    )
)
