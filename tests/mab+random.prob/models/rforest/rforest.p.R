library(R6)

library(party)

##
 #
 ##
RForest.p <- R6Class("RForest.p",

    public = list(
    
        ##
         # Initializes the model instance.
         ##
        initialize = function(ntree=500)
        {
          private$ntree <- ntree
        },
        
        ##
         #
         ##
        isValid = function()
        {
          return (private$valid)
        },
        
        ##
        #
        ##
        getName = function()
        {
          name <- sprintf("RForest.p, ntree=%d", private$ntree)
          return (name)
        },
        
        ##
         #
         ##
        fit = function(x, y)
        {
          if (is.na(private$mtry))
            private$mtry = ceiling(sqrt(ncol(x)))

          y = as.factor(y)
          if (length(levels(y)) > 1) {
              private$model = cforest( y ~ ., data.frame(x,y), control=cforest_classical(ntree=private$ntree,mtry=private$mtry) )
              private$valid = TRUE
          }

        },
        
        ##
         # 
         ##
        predict = function(x)
        {
          if (private$valid) {
              yhat <- unlist(treeresponse( private$model, newdata=data.frame(x)))[2*(1:nrow(x))]
          } else {
              yhat <- rep(0,nrow(x))
          }
          names(yhat) <- rownames(x)
          return(yhat)
        }
    ),

    private = list(
        
        model = NA,
        mtry = NA,
        ntree = NA,
        valid = FALSE
    )
)
