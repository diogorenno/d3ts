#!/bin/bash

DATASET="$1"
NTESTS="$2"
NPROC="$3"

# proc_args <- list(
#    dataset            = raw_args[1],
#    ntests             = as.numeric(raw_args[2]),
#    nseeds             = as.numeric(raw_args[3]),
#    msc                = raw_args[4],
#    ncold              = as.numeric(raw_args[5]),
#    heuristic          = raw_args[6],
#    parallel_updates   = as.logical(as.numeric(raw_args[7])),
#    settings_init      = raw_args[8],
#    output_path        = raw_args[9],
#    nproc              = as.numeric(raw_args[10]))

##
 # Single-models.
 ##
#Rscript test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/mod.settings.init.R            output/single-model/mod/            $NPROC
#Rscript test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/svm.settings.init.R            output/single-model/svm/            $NPROC
#Rscript test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/ewrls.settings.init.R          output/single-model/ewrls/          $NPROC
#Rscript test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/logistic.settings.init.R       output/single-model/logistic/       $NPROC
#Rscript test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/activesearch.settings.init.R   output/single-model/activesearch/   $NPROC
#Rscript test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/listnet.settings.init.R        output/single-model/listnet/        $NPROC
#Rscript test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/rforest.settings.init.R        output/single-model/rforest/        $NPROC


Rscript scripts/test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/config1-as.settings.init.R   output/config1-as/   $NPROC
Rscript scripts/test.R  $DATASET  $NTESTS  1  ucb1  20  mod  0  settings/config1-ln.settings.init.R   output/config1-ln/   $NPROC


