library(R6)

##
 # General model class.
 # Encapsulates specific model implementations.
 ##
Model <- R6Class("Model",

    public = list(
        debug_mode = FALSE,
    
        ##
         # Initializes the model and general settings.
         ##
        initialize = function(settings, model, observed_graph)
        {
          private$settings <- settings
          private$model <- model$clone()
          private$type <- class(model)[1]
          private$graph <- observed_graph
          private$must_fit <- FALSE
        },
        
        ##
         # Fits a new model on the current samples or updates it with the new sample.
         ##
        recruitNode = function(id)
        {
          if (length(private$graph$recruited) > 1) {
            private$update(id)
          } else {
            private$fit()
          }
        },
        
        ##
         # Returns TRUE if the model is valid (can be used for prediction).
         # Returns FALSE otherwise.
         ##
        isValid = function()
        {
          return (private$model$isValid())
        },
        
        ##
         # Returns a character string identifying the underlying model.
         ##
        getName = function()
        {
          return (private$model$getName())
        },
        
        ##
         # 
         ##
        evaluateBorder = function()
        {
          return (private$evaluateNodes(private$graph$border))
        },
        
        ##
         # Returns this model's score according to the selection criterion in use.
         ##
        getModelSelectionScore = function(current_turn)
        {
          stopifnot(is.numeric(current_turn))
          stopifnot(current_turn > 0)
          score <- NA
          # UCB1.
          if (private$settings$model_selection_method == "ucb1") {
            upper_bound <- sqrt(2 * log(current_turn) / private$nused)
            score <- (private$ncorrect / private$nused) + upper_bound
          }
          # Thompson Sampling.
          if (private$settings$model_selection_method == "ts") {
            score <- rbeta(1, private$ncorrect, private$nwrong)
          }
          return (score)
        },
        
        ##
         #
         ##
        addFeedback = function(correct) {
          private$nused <- 1 + private$nused
          if (correct) private$ncorrect <- 1 + private$ncorrect
          else private$nwrong <- 1 + private$nwrong
        },
        
        ##
         # Allows internal access to the instance for testing/debugging purposes.
         ##
        browse = function()
        {
          browser()
        }
    ),

    private = list(
        
        model = NA,       # A reference to an initialized instance of a supported model class.
        type = NA,        # (character) Name of the R6 class which implements private$model.
        settings = NA,    # (Settings) General settings.
        graph = NA,       # (ObservedGraph) Reference to the currently observed graph.
        must_fit = NA,    # (logical) Indicates whether fit() must be called prior to predicting.
      
        # Statistics used for model selecion.
        ncorrect = 1,     # The number of times this model correctly labeled a sample.
        nwrong = 1,       # The number of times this model was wrong about a sample.
        nused = 2,        # The number of times this model was used.
        
        # Supported models. Each constant value must be the name of the corresponding R6 class.
        TYPE_SVM = "SVM",
        TYPE_EWRLS = "EWRLS",
        TYPE_RFOREST_RF = "RForest.rf",
        TYPE_RFOREST_P = "RForest.p",
        TYPE_LISTNET = "ListNet",
        TYPE_ACTIVESEARCH = "ActiveSearch",
        TYPE_LOGISTIC = "Logistic",
        TYPE_MOD = "MOD",
        
        ##
         #
         # TODO: centering, scaling?
         ##
        fit = function()
        {
          # Unsets the lazy fit flag.
          private$must_fit <- FALSE

          N = length(private$graph$recruited)
          x = as.matrix(private$graph$features[private$graph$recruited, -1, drop=FALSE])
          y = as.vector(private$graph$features[private$graph$recruited, "response"])
          
          # EWRLS.
          if (private$type == private$TYPE_EWRLS) {
            private$model$fit(
                x = t(cbind(x, rep(1, N))),
                y = y)
          }

          # ListNet.
          if (private$type == private$TYPE_LISTNET) {
            private$model$fit(
                x = cbind(x, rep(1, N)),
                y = y)
          }

          # ActiveSearch.
          if (private$type == private$TYPE_ACTIVESEARCH) {}
          
          # SVM.
          if (private$type == private$TYPE_SVM) {
            private$model$fit(
                x = x,
                y = y)
          }

          # Logistic.
          if (private$type == private$TYPE_LOGISTIC) {
            private$model$fit(
                x = x,
                y = y)
          }
          
          # RForest.
          if (private$type == private$TYPE_RFOREST_RF || private$type == private$TYPE_RFOREST_P) {
            private$model$fit(
                x = x,
                y = y)
          }
          
          # MOD.
          if (private$type == private$TYPE_MOD) {}
        },

        ##
         #
         ##
        update = function(id)
        {
          stopifnot(id %in% private$graph$recruited)

          # EWRLS.
          if (private$type == private$TYPE_EWRLS) {
            x = private$graph$features[id, -1]
            y = private$graph$features[id, "response"]
            private$model$update(
                x = c(x, 1),
                y = y)
          }

          # ListNet.
          if (private$type == private$TYPE_LISTNET) {
            private$flagLazyFit()
          }

          # ActiveSearch.
          if (private$type == private$TYPE_ACTIVESEARCH) {}
          
          # SVM.
          if (private$type == private$TYPE_SVM) {
            private$flagLazyFit()
          }

          # Logistic.
          if (private$type == private$TYPE_LOGISTIC) {
            private$flagLazyFit()
          }

          # RForest.
          if (private$type == private$TYPE_RFOREST_RF || private$type == private$TYPE_RFOREST_P) {
            private$flagLazyFit()
          }
          
          # MOD.
          if (private$type == private$TYPE_MOD) {}
        },
        
        ##
         #
         ##
        predict = function(ids)
        {
          # Refits the model if flagged.
          if (private$must_fit) {
            private$fit()
          }
          
          # EWRLS.
          if (private$type == private$TYPE_EWRLS) {
            return (private$model$predict(x = t(private$graph$getDesignMatrix(ids))))
          }

          # ListNet.
          if (private$type == private$TYPE_LISTNET) {
            return (private$model$predict(x = private$graph$getDesignMatrix(ids)))
          }

          # ActiveSearch.
          if (private$type == private$TYPE_ACTIVESEARCH) {
            params <- private$graph$getAdjacencyMatrix()

            isRecruited <- rep(FALSE,length(params$node_ids))
            names(isRecruited) <- params$node_ids
            responses <- isRecruited

            isRecruited[private$graph$recruited] <- TRUE
            responses[private$graph$recruited] <- private$graph$features[private$graph$recruited, "response"]

            yhat <- private$model$predict(params$edge_matrix, isRecruited, responses)
            return(yhat)
          }
          
          # SVM.
          if (private$type == private$TYPE_SVM) {
            return (private$model$predict(x = as.matrix(private$graph$features[ids, -1, drop=FALSE])))
          }

          # Logistic.
          if (private$type == private$TYPE_LOGISTIC) {
            return (private$model$predict(x = as.matrix(private$graph$features[ids, -1, drop=FALSE])))
          }

          # RForest.
          if (private$type == private$TYPE_RFOREST_RF || private$type == private$TYPE_RFOREST_P) {
            return (private$model$predict(x = as.matrix(private$graph$features[ids, -1, drop=FALSE])))
          }
          
          # MOD.
          if (private$type == private$TYPE_MOD) {
            suggested <- private$graph$getModSuggestion()
            stopifnot(suggested %in% ids)
            values <- rep(0, length(ids))
            values[which(ids == suggested)] <- 1
            names(values) <- ids
            return (values)
          }
        },
        
        ##
         # 
         ##
        evaluateNodes = function(ids)
        {
          stopifnot(self$isValid())
          if (length(ids) == 0) {
            return (NULL)
          }
          return (private$predict(ids))
        },
        
        ##
         # If the model is valid, then flags it to be refitted prior to predicting.
         # If the model is not yet valid, always retrains it.
         ##
        flagLazyFit = function()
        {
          if (self$isValid()) {
            private$must_fit <- TRUE
          } else {
            private$fit()
          }
        }
    )
)
