#!/bin/bash

DIRECTORIES=$@

for DIRNAME in ${DIRECTORIES}; do
	for dataset in donors dbpedia citeseer wikipedia kickstarter; do
        	OUTPATH=${DIRNAME}/extracted/
	        echo "------> $dataset $DIRNAME"
	        if [[ ! -d  $OUTPATH ]]; then
	            mkdir $OUTPATH
	        fi
	    Rscript  aggregate.R  $OUTPATH  ${DIRNAME}/${dataset}_*RData  &
  done
done
