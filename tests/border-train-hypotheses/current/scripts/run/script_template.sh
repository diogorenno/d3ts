#!/bin/bash
#
#PBS -l nodes=NODES:ppn=PPN
#
#PBS -q QUEUENAME
#PBS -l walltime=WALLTIME
#PBS -l naccesspolicy=singleuser
#PBS -l mem=MEM
#
#
#PBS -j oe
#

source /etc/profile.d/modules.sh
module load r
module load octave
module load python
echo " "
echo " "
echo "Job started on `hostname` at `date`"
#
#    User code starts now
#

echo "Replaced placeholders:"
echo NODES
echo PPN
echo QUEUENAME
echo WALLTIME
echo MEM
echo DATASET
echo TEST_RANGE
echo MSC
echo SCOREMAPPER
echo FEATURESET
echo SETTINGS

NSEEDS=1
COLDPULLS=20
OUTPATH=$RCAC_SCRATCH/p2r_results/border-train-hypotheses/experiments/MSC_SCOREMAPPER_SETTINGS_FEATURESET_p.PMODEL_obs.POBS_SRC/
HEURISTIC=mod
PARALLEL_UPDATES=0

CODEDIR=~/pay2recruit/project/tests/border-train-hypotheses/current/
cd $CODEDIR

if [[ ! -d "$OUTPATH" ]]; then
    echo "Creating output folder $OUTPATH"
    mkdir -p $OUTPATH
else
    echo "Saving results to $OUTPATH"
fi

Rscript scripts/run/test.R DATASET TEST_RANGE $NSEEDS $COLDPULLS $HEURISTIC FEATURESET MSC SCOREMAPPER inits/settings/SETTINGS.settings.init.R $PARALLEL_UPDATES $OUTPATH PPN PMODEL POBS_FILE

#
#    End user code
#
echo "Job ended on `hostname` at `date`"
