DATASETS=(donors wikipedia dbpedia citeseer kickstarter dblp lj)
METHODS=(rr_config2 activesearch listnet mod rforest svm)

for DATASET in ${DATASETS[@]}; do
  for METHOD in ${METHODS[@]}; do
	  echo; echo ${DATASET}, ${METHOD}; echo
	  Rscript script.R $DATASET /scratch1/renno/mestrado/p2r_results/camera-ready/${DATASET}/recruited_${METHOD}.RData /scratch1/renno/mestrado/p2r_results/prop-targets-border/${METHOD}/${DATASET}.RData 4
  done
done

