#!/bin/bash

apt-add-repository ppa:octave/stable
apt-get update
apt-get install octave liboctave-dev
apt-get install liblzma-dev
apt-get install libcurl4-openssl-dev

