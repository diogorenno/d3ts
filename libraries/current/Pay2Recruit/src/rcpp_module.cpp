#include <Rcpp.h>
#include <RcppEigen.h>
#include <stdio.h>
#include <unordered_set>
#include <fstream>
#include <queue>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <cassert>

using namespace Rcpp;

typedef Eigen::SparseMatrix<double> SpMat; 
#define MAX( a, b ) ( (a) > (b) ? (a) : (b) )
#define MIN( a, b ) ( (a) < (b) ? (a) : (b) )

#define DEBUG 0

#define P2R_VERSION "1.21"
/*
 * v1.1:  Implements calculated RW (Diogo).
 * v1.21: Falls back to estimated (simulated) RW, adds constant for choosing (Diogo).
 *        Standalone results for SVR and EWLS with calculated RW were very problematic; cause unknown ATM.
 */


class Graph {
public:
    Graph( bool undirected_ ) : undirected(undirected_), totalE(0), label_max(1), target_label(1) { srand48(0); }
    ~Graph() {}
    void addEdge( int u, int v );
    int getTargetSize() { return this->target_nodes.size(); }
    int getN() { return this->out.size(); } // FIXME
    int getE() { return this->totalE; }
    int                 getRandomNode();

    std::vector<int>    getNeighbors( int i ) { std::vector<int> ret(out[i].begin(),out[i].end()); return ret; }
    std::vector<int>    getInNeighbors( int i ) { std::vector<int> ret(in[i].begin(),in[i].end()); return ret; }
    int                 getLabel( int i ) { return label[i]; }
    IntegerVector       getAttributes( int i ) { return wrap(attribs[i]); }

    List getInfo(int i)
    {
      assert(share_neighs[i]);
      return List::create(Named("accept")     = (int) accept[i],
                          Named("neighbors")  = wrap(out[i]),
                          Named("label")      = (int) label[i],
                          Named("attributes") = wrap(attribs[i]));
    }
    
    // (renno)
    List getStats(int i)
    {
      assert(node_stats.count(i));
      return List::create(Named("queried_neighs_count") = node_stats[i].queried_neighs_count,
                          Named("target_neighs_count")  = node_stats[i].target_neighs_count);
    }
    
    //std::vector<double> getAttributes( int i ) { return attribs[i]; }
    std::vector<std::string> getFeatureNames() { return this->feat_names; }
    std::vector<bool> isFeatureFraction() { return this->feat_isFraction; }

    //void initFeatures( int ntypes, int target, std::vector<int> att_max );
    void initFeatures( int ntypes, int target, int nattribs );
    void initFeatures2( int ntypes, int target, int nattribs, bool include_structural,
        bool include_triangles, bool include_most, bool include_rw, bool include_counts,
        bool include_fractions, bool include_attrib_fractions );

    SpMat         addNode4( int u, IntegerVector neighs, int label, IntegerVector attribs );
    SpMat         addNode5( int u, int label, IntegerVector attribs ) {
        IntegerVector empty(0);
        return addNode4( u, empty, label, attribs );
    }

    void readGraph( std::string filename );
    void readCommunities3( std::string filename, int accept_trait, int target_trait,
            double p_accept_one=1.0, double p_accept_zero=0.0,
            double p_neighs_accept=1.0, double p_neighs_reject=0.0,
            std::string amount_file = std::string () );
    void readCommunities2( std::string filename, int accept_trait,
            double p_accept_one=1.0, double p_accept_zero=0.0, double p_neighs_accept=1.0, double p_neighs_reject=0.0 ) {
        readCommunities3( filename, accept_trait, accept_trait, p_accept_one, p_accept_zero, p_neighs_accept, p_neighs_reject );
    }
    std::vector<int>getTargetNodes( int nsamples );
    void writeAttributesToFile( std::string filename );
    
    // (renno)
    std::vector<int> getPositiveNeighborsCount(std::vector<int> node_ids);
    std::vector<int> getNeighborsCount(std::vector<int> node_ids);
    std::string getVersion() {return P2R_VERSION;};

private:
    // TODO: make vertex a class
    std::vector<std::unordered_set<int> > out;
    std::vector<std::unordered_set<int> > in;
    std::vector<char> valid;
    std::vector<int> label;
    std::vector<bool> accept;
    std::vector<bool> share_neighs;
    std::vector<std::vector<int> > attribs; // TODO: make this of type double

    int label_max, target_label;
    std::vector<std::string> feat_names;
    std::vector<bool> feat_isFraction;
    // TODO: map ids to save space
    //std::map<int,int> old2new;
    //std::map<int,int> new2old;
    int totalE;
    int nattribs;
    int id_offset, max_id;
    bool undirected;
    std::vector<int> target_nodes;
    
    // (renno)
    struct node_stats_t {
      int queried_neighs_count;
      int target_neighs_count;
      node_stats_t() : queried_neighs_count(0), target_neighs_count(0) {};
    };
    std::unordered_map<int, node_stats_t> node_stats;

    void computeFeaturesSparse( std::unordered_set<int> v, SpMat &feats );
    
    static const int ID_COUNTS = 0;
    static const int ID_FRACTION = 1;
    static const int ID_TRIANGLES = 2;
    static const int ID_TRIFRACTION = 3;
    static const int ID_MOST = 4;
    static const int ID_MOSTFRACTION = 5;
    static const int ID_RW = 6;
    static const int ID_ATTRIBS = 7;
    int SIZE_COUNTS;
    int SIZE_FRACTION;
    int SIZE_TRIANGLES;
    int SIZE_TRIFRACTION;
    int SIZE_MOST;
    int SIZE_MOSTFRACTION;
    int SIZE_RW;
    int SIZE_ATTRIBS;
    int OFFSET_COUNTS;
    int OFFSET_FRACTION;
    int OFFSET_TRIANGLES;
    int OFFSET_TRIFRACTION;
    int OFFSET_MOST;
    int OFFSET_MOSTFRACTION;
    int OFFSET_RW;
    int OFFSET_ATTRIBS;
    int RW_STEPS;
    int RW_RUNS;

    bool include_structural;
    bool include_triangles;
    bool include_most;
    bool include_rw;
    bool include_counts;
    bool include_fractions;
    bool include_attrib_fractions;
    
    static const bool USE_CALCULATED_RW = false;
};
RCPP_EXPOSED_CLASS(Graph);

std::vector<int> Graph::getTargetNodes( int nsamples ) {
    if( nsamples > target_nodes.size() ) {
        char err_msg[100];
        sprintf( err_msg, "Graph::getTargetNodes() -- nsamples > target_nodes.size() (%d > %lu)\n", nsamples, target_nodes.size()  );
        throw std::runtime_error( err_msg );
    }
    std::vector<int> ret( target_nodes.begin(), target_nodes.end() );
    std::random_shuffle(ret.begin(),ret.end());
    ret.resize(nsamples);

    return ret;
}


int Graph::getRandomNode() {
    int id = (int)(drand48()*out.size());
    while( !valid[id] )
        id = (int)(drand48()*out.size());
    return id;
}

class FileHandler {
    public:
        bool use_gzip;
        FILE *f;

        FILE * open( std::string filename );
        void close();
};

FILE * FileHandler::open( std::string filename ) {
    use_gzip = filename.substr(filename.size()-3,3) == ".gz";
    if( use_gzip ) {
        char st[1000];
        sprintf( st, "gunzip -c %s", filename.c_str() ); //FIXME: use zcat instead
        f = popen(st, "r");
    } else {
        f = fopen(filename.c_str(), "r");
    }

    return f;
}

void FileHandler::close( void ) {
    if( use_gzip ) pclose(f);
    else fclose(f);
}

//TODO: modify the code to skip comment lines
void Graph::readGraph( std::string filename ) {
    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t min_id = 1000;
    max_id = -1;
    char line[1000];
    while( fgets(line, sizeof(line), f) != NULL ) {
        if( line[0] != '#' ) {
            int32_t i, j;
            if( sscanf(line, "%d %d", &i, &j) ==  2 ) {
                if(i != j) {
                    min_id = MIN(min_id,MIN(i,j));
                    max_id = MAX(max_id,MAX(i,j));
                }
            }
        }
    }
    fh->close();

    id_offset = min_id-1;
    max_id -= id_offset;
    out.resize(max_id+1);
    in.resize(max_id+1);
    //attribs.resize(max_id+1);
    attribs.resize(max_id+1);
    label.resize(max_id+1,0);
    valid.resize(max_id+1,false);
    accept.resize(max_id+1,false);
    share_neighs.resize(max_id+1,false);

    std::cout << "id_offset " << id_offset << ", max_id " << max_id << std::endl;

    totalE = 0; // (renno)
    f = fh->open( filename );
    fprintf( stdout, "Processing edges...\n" );
    while( fgets(line, sizeof(line), f) != NULL ) {
        if( line[0] != '#' ) {
            int32_t i, j;
            if( sscanf(line, "%d %d", &i, &j) ==  2 ) {
                if(i != j) {
                    i -= id_offset;
                    j -= id_offset;
                    out[i].insert(j);
                    in[j].insert(i);
                    totalE++;
                    if( undirected ) {
                        out[j].insert(i);
                        in[i].insert(j);
                    }
                    valid[i] = 1;
                    valid[j] = 1;

                    if( totalE%1000000 == 0 )
                        fprintf( stdout, "Processed %d edges.\n", totalE );
                }
            }
        }
    }
    fprintf( stdout, "Processed %d edges.\n", totalE );
    fh->close();
    delete fh;
}

void Graph::readCommunities3( std::string filename, int accept_trait, int target_trait,
        double p_accept_one, double p_accept_zero, double p_neighs_accept, double p_neighs_reject,
        std::string amount_file ) {

    FileHandler *fh = new FileHandler();
    FILE *f = fh->open( filename );

    // pre-read: find min and max
    int32_t community = 0, id;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, f)) != -1) {
    
        const char *sep = " \t,";
        char *id_str, *last;

        if( line[0] == '#' ) // skip headers
            continue;

        for( id_str = strtok_r(line, sep, &last);
             id_str;
             id_str = strtok_r(NULL, sep, &last) ) {
            id = atoi(id_str);
            if(community == accept_trait) {
                accept[id-id_offset] = true; // att corresponding to label is not included, on purpose
            }
            if( community == target_trait ) {
                label[id-id_offset] = 1;
                target_nodes.push_back(id-id_offset);
            }
            if ((community != accept_trait) and (community != target_trait)) {  // (renno)
                attribs[id-id_offset].push_back(community);
            }
        }
        community++;
    }
    free(line);
    fh->close();
    delete fh;
}

void Graph::writeAttributesToFile( std::string filename ) {
    std::ofstream outfile;
    outfile.open(filename );
    for( int i = 0; i < out.size(); i++ ) {
        outfile << attribs[i][0];
        for( int j = 1; j < nattribs; j++ ) {
            outfile << "\t" << attribs[i][j];
        }
        if( i < out.size()-1 )
            outfile << "\n";
    }
    outfile.close();
}

SpMat      Graph::addNode4( int u, IntegerVector neighs, int label, IntegerVector attribs) {
    // get max and resize vectors
    int max = u;
    for( int i = 0; i < neighs.size(); i++ )
        max = MAX( max, neighs[i] );
    int cur_max = this->out.size();
    if( cur_max < (max+1) ) {
        //int new_max = MAX(cur_max*2,max+1);
        int new_max = max+1;
        this->out.resize(new_max);
        this->in.resize(new_max);
        this->label.resize(new_max,0);
        this->attribs.resize(new_max);
        this->valid.resize(new_max,false);
    }

    valid[u] = 1; // node is recruited
    
    std::unordered_set<int> toBeComputed;
    //add edges and mark border nodes to have its features computed
    for( int i = 0; i < neighs.size(); i++ ) {
        this->out[u].insert(neighs[i]);
        this->in[neighs[i]].insert(u);
    }
    // (renno) Marks the node itself to have its features computed.
    toBeComputed.insert(u);    

    // mark border nodes to have its features computed
    for( std::unordered_set<int>::iterator i=out[u].begin(); i != out[u].end(); i++ ) {
        int v = *i;
        // border node
        if( !valid[v] )
            toBeComputed.insert(v);
        //// border neighbor of recruited neighbor
        //else { //TODO: uncomment this
        //    for( std::unordered_set<int>::iterator j=out[v].begin(); j != out[v].end(); j++ )
        //        if( !valid[*j] )
        //            toBeComputed.insert(*j);
        //}
    }

    //set label and attribs
    this->label[u] = label;
    this->attribs[u] = as<std::vector<int> >(attribs); //copy
    
    node_stats_t new_stats;
    this->node_stats[u] = new_stats;
    // (renno) Updates u's neighbors' counts of queried neighbors and target neighbors.
    for (std::unordered_set<int>::iterator i = out[u].begin(); i != out[u].end(); ++i) {
      int v = *i;
      if (valid[v]) {
        // Updates the neighbor.
        this->node_stats[v].queried_neighs_count += 1;
        this->node_stats[v].target_neighs_count += (label > 0);
        // Updates u.
        this->node_stats[u].queried_neighs_count += 1;
        this->node_stats[u].target_neighs_count += (this->label[v] > 0);
      }
    }
        
    int i = 0;
    int nrows, ncols;
    nrows = feat_names.size()+1;
    ncols = toBeComputed.size();

    SpMat M(nrows,ncols);
    computeFeaturesSparse(toBeComputed,M);
    return M;
}


void Graph::computeFeaturesSparse( std::unordered_set<int> toBeComputed, SpMat &M ) { //colMajor version
    std::vector<int> entries;
    for( std::unordered_set<int>::iterator v=toBeComputed.begin(); v != toBeComputed.end(); v++ ) {
            int count = 12;
            for( std::unordered_set<int>::iterator u = in[*v].begin(); u != in[*v].end(); u++) {
                count += attribs[*u].size();
            }
            entries.push_back(count);
    }
    if(entries.empty()) return;
    else M.reserve(entries);

    int v_ind=0;
    for( std::unordered_set<int>::iterator it=toBeComputed.begin(); it != toBeComputed.end(); it++ ) {
        int v = *it;
        //double *x = new double[feat_names.size()]();
        double eps = 1e-10, epsilon;
        std::unordered_set<int> observed_neighs = in[v]; //construct set
        for( std::unordered_set<int>::iterator u = out[v].begin(); u != out[v].end(); u++)
            if( valid[*u] )
                observed_neighs.insert(*u);


        int feat_ind = 0;
        //printf("M[%d,%d] = %d\n", feat_ind,v_ind,v);
        M.insert(0,v_ind) = v;
        
        // Structural.
        if( include_structural ) {
            int counts[] = {0,0};
            for( std::unordered_set<int>::iterator u = observed_neighs.begin(); u != observed_neighs.end(); u++ )
                counts[label[*u]]++;

            // Counts.
            if( include_counts ) {
                for( int val = 0; val <= label_max; val++ )
                    M.insert(1+OFFSET_COUNTS+val,v_ind) = counts[val];
            }
            
            // Fractions.
            if( include_fractions ) {
                epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                for( int val = 0; val < label_max; val++ )
                    M.insert(1+OFFSET_FRACTION+val,v_ind ) = (counts[val]+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
            }
            
            // Triangles.
            if( include_triangles ) {
                int tmp = 0;
                int counts[] = {0,0,0};
                if( observed_neighs.size() > 0 )
                    for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                        for( std::unordered_set<int>::iterator b = a; b != observed_neighs.end(); b++ ) {
                            if( out[*a].find(*b) != out[*a].end() ||
                                out[*b].find(*a) != out[*b].end() ) {
                                int pos = (label[*a] == target_label ) + (label[*b] == target_label );
                                counts[pos]++;
                                tmp++;
                            }
                        }
                    }
                if( include_counts ) {
                    for( int val = 0; val <= 2; val++ )
                        M.insert(1+OFFSET_TRIANGLES+val,v_ind) = counts[val];
                }
                if( include_fractions ) {
                    epsilon = tmp > 0 ? 0.0 : eps;
                    for( int val = 0; val < 2; val++ )
                        M.insert(1+OFFSET_TRIFRACTION+val,v_ind) = (counts[val]+epsilon)/(tmp+3*epsilon);
                }
            }

            // Most.            
            if( include_most ) {
                int counts = 0;
                for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                    int tmp[] = {0,0};
                    for( std::unordered_set<int>::iterator b = in[*a].begin(); b != in[*a].end(); b++ )
                        tmp[label[*b]]++;

                    if (tmp[0] > tmp[1])
                        counts++;
                }
                if( include_counts ) {
                    M.insert(1+OFFSET_MOST,v_ind) = counts;
                }
                if( include_fractions ) {
                    epsilon = observed_neighs.size() > 0 ? 0.0 : eps;
                    for( int val = 0; val < label_max; val++ )
                        M.insert(1+OFFSET_MOSTFRACTION+val,v_ind) = (counts+epsilon)/(observed_neighs.size()+(label_max+1)*epsilon);
                }
            }
            
            // 2-step Random Walk.
            if( include_rw ) {
              
              if (USE_CALCULATED_RW) 
              {
                // Calculated RW.
                double num = 0.0;
                double den = 0.0;
                for (std::unordered_set<int>::iterator a = out[v].begin(); a != out[v].end(); ++a) {
                  int neigh = *a;
                  if (valid[neigh] and (node_stats[neigh].queried_neighs_count > 0)) {
                    num += node_stats[neigh].target_neighs_count / (double) node_stats[neigh].queried_neighs_count;
                    den += 1;
                  }
                }
  
                if (den > 0) M.insert(1+OFFSET_RW, v_ind) = num / den;
                else M.insert(1+OFFSET_RW, v_ind) = 0;
              }
              else 
              {
                // Estimated (simulated) RW.
                double sum = 0.0, denom = 0.0;
                for( int t = 0; t < RW_RUNS; t++ ) {
                    int rw_pos = v;
                    int s;
                    for( s = 0; s < RW_STEPS; s++ ) {
                        if( in[rw_pos].size() == 0 )
                            break;
                        double p_in;
                        p_in = in[rw_pos].size()/(in[rw_pos].size()+out[rw_pos].size());
                        p_in = 1.0;
                        if( drand48() <= p_in ) {
                            std::unordered_set<int>::iterator it = in[rw_pos].begin();
                            std::advance(it,(int)(drand48()*in[rw_pos].size()));
                            rw_pos = *it;
                        } else {
                            std::unordered_set<int>::iterator it = out[rw_pos].begin();
                            std::advance(it,(int)(drand48()*out[rw_pos].size()));
                            rw_pos = *it;
                        }
                    }
                    if( s == RW_STEPS ) {
                        sum += label[rw_pos];
                        denom += 1.0;
                    }
                }
                if( denom > 0 )
                    M.insert(1+OFFSET_RW,v_ind) = sum/denom;
              }
            }
        }
        
        // Attribute fractions.
        if( include_attrib_fractions ) {
            double inc = 1.0/MAX(1,observed_neighs.size());
            int *counts = new int [nattribs]();
            for( std::unordered_set<int>::iterator a = observed_neighs.begin(); a != observed_neighs.end(); a++ ) {
                for( int j = 0; j < attribs[*a].size(); j++ )
                    if( attribs[*a][j] < nattribs )
                        counts[attribs[*a][j]]++;
            }
            for( int j = 0; j < nattribs; j++ )
                if( counts[j]>0 ) {
                    M.insert(1+OFFSET_ATTRIBS+j,v_ind) = counts[j]*1.0*inc;
                }
            delete counts;
        }
        v_ind++;
    }
    M.makeCompressed();

    //IntegerVector coef(wrap(Eigen::MatrixXd(M.block(0,0,1,11))));
}


void Graph::addEdge( int u, int v ) {
    int max = (u>v) ? u : v;
    // make resizing more efficient
    if( this->out.size() < (max+1) ) {
        this->out.resize(max+1);
        this->in.resize(max+1);
        this->label.resize(max+1);
    }
    this->out[u].insert(v);
    this->in[v].insert(u);
    totalE++;
}


void Graph::initFeatures2( int ntypes, int target, int nattribs, bool include_structural,
        bool include_triangles, bool include_most, bool include_rw,
        bool include_counts, bool include_fractions, bool include_attrib_fractions ) 
{
    long long int target2 = target;
    this->nattribs = nattribs;

    this->include_structural = include_structural;
    this->include_triangles = include_triangles;
    this->include_most = include_most;
    this->include_rw = include_rw;
    this->include_counts = include_counts;
    this->include_fractions = include_fractions;
    this->include_attrib_fractions = include_attrib_fractions;

    SIZE_COUNTS = ntypes;
    SIZE_FRACTION = ntypes-1;
    SIZE_TRIANGLES = 3;
    SIZE_TRIFRACTION = 2;
    SIZE_MOST = ntypes-1;
    SIZE_MOSTFRACTION = ntypes-1;
    SIZE_RW = 1;
    SIZE_ATTRIBS = nattribs;

    int nfeatures = 0;
    if( include_structural ) {
        if( include_counts ) {
            OFFSET_COUNTS = nfeatures;
            nfeatures += SIZE_COUNTS;
        }
        if( include_fractions ) {
            OFFSET_FRACTION = nfeatures;
            nfeatures += SIZE_FRACTION;
        }
        if( include_triangles ) {
            if( include_counts ) {
                OFFSET_TRIANGLES = nfeatures;
                nfeatures += SIZE_TRIANGLES;
            }
            if( include_fractions ) {
                OFFSET_TRIFRACTION = nfeatures;
                nfeatures += SIZE_TRIFRACTION;
            }
        }
        if( include_most ) {
            if( include_counts ) {
                OFFSET_MOST = nfeatures;
                nfeatures += SIZE_MOST;
            }
            if( include_fractions ) {
                OFFSET_MOSTFRACTION = nfeatures;
                nfeatures += SIZE_MOSTFRACTION;
            }
        }
        if( include_rw ) {
            OFFSET_RW = nfeatures;
            nfeatures += SIZE_RW;
            RW_STEPS = 2;
            RW_RUNS = 5;
        }
    }
    if( include_attrib_fractions ) {
        OFFSET_ATTRIBS = nfeatures;
        nfeatures += SIZE_ATTRIBS;
    }
    fprintf(stderr, "OFFSET_ATTRIBS = %d\n", OFFSET_ATTRIBS );

    // Legacy code below
    if( include_structural && include_counts )
    for( long long int i=0; i < ntypes; i++ ) {
        feat_names.push_back( std::string("y,counts,") + std::to_string(i));
        feat_isFraction.push_back( false );
    }

    if( include_structural && include_fractions )
    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("y,counts-fraction,") + std::to_string(i));
        feat_isFraction.push_back( true );
    }

    //if( nattribs < 0 ) return;

    if( include_structural && include_triangles ) {
        if( include_counts ) {
            feat_names.push_back( std::string("y,triangles") + ",~"+ std::to_string(target2)+",~"+std::to_string(target2));
            feat_names.push_back( std::string("y,triangles") + ",~"+ std::to_string(target2)+","+std::to_string(target2));
            //feat_names.push_back( std::string("y,triangles") + ","+ std::to_string(target2)+",~"+std::to_string(target2));
            feat_names.push_back( std::string("y,triangles") + ","+ std::to_string(target2)+","+std::to_string(target2));
            for( long long int i=0; i < 3; i++ )
                feat_isFraction.push_back( false );
        }
        if( include_fractions ) {
            feat_names.push_back( std::string("y,tri-fraction") + ",~"+ std::to_string(target2)+",~"+std::to_string(target2));
            feat_names.push_back( std::string("y,tri-fraction") + ",~"+ std::to_string(target2)+","+std::to_string(target2));
            for( long long int i=0; i < 2; i++ )
                feat_isFraction.push_back( true );
        }
    }

    if( include_structural && include_most && include_counts )
    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("yi,most,") + std::to_string(i));
        feat_isFraction.push_back( false );
    }

    if( include_structural && include_most && include_fractions )
    for( long long int i=0; i < ntypes-1; i++ ) {
        feat_names.push_back( std::string("yi,most-fraction,") + std::to_string(i));
        feat_isFraction.push_back( true );
    }

    if( include_structural && include_rw ) {
        feat_names.push_back( std::string("y,rw,2,5"));
        feat_isFraction.push_back( true );
    }

    if( include_attrib_fractions )
    for( long long int att_id=0; att_id < nattribs; att_id++ ) {
        feat_names.push_back( std::string("att")+std::to_string(att_id)+",average");
        feat_isFraction.push_back( true );
    }
    fprintf( stderr, "feat_names.size() = %lu\n", feat_names.size() );
    
    // (renno)
    if (include_rw) {
      fprintf(stdout, "\nRandom-Walk feature: %s\n\n", USE_CALCULATED_RW ? "calculated" : "estimated");
    }
}

//void Graph::initFeatures( int ntypes, int target, std::vector<int> att_max ) {
void Graph::initFeatures( int ntypes, int target, int nattribs ) 
{
    initFeatures2( ntypes, target, nattribs, true, true, true, true, true, true, true );
}


// (renno)
std::vector<int> Graph::getPositiveNeighborsCount( std::vector<int> node_ids )
{
  std::vector<int> counts;
  // Counts the number of positive neighbors of each given node.
  for (unsigned int i = 0; i < node_ids.size(); i++) {
    int v = node_ids[i];
    // Builds v's set of neighbors.
    std::unordered_set<int> observed_neighs = in[v];
    for (std::unordered_set<int>::iterator u = out[v].begin(); u != out[v].end(); u++)
      if (valid[*u])
        observed_neighs.insert(*u);
    // Counts v's number of negative and positive neighbors.
    int v_counts[] = {0,0};
    for (std::unordered_set<int>::iterator u = observed_neighs.begin(); u != observed_neighs.end(); u++)
      v_counts[label[*u]]++;
    // Registers v's count of positive neighbors.
    counts.push_back(v_counts[1]);
  }
  return(counts);
}


// (renno)
std::vector<int> Graph::getNeighborsCount( std::vector<int> node_ids )
{
  std::vector<int> counts;
  // Counts the number of neighbors of each given node.
  for (unsigned int i = 0; i < node_ids.size(); i++) {
    int v = node_ids[i];
    // Builds v's set of neighbors.
    std::unordered_set<int> observed_neighs = in[v];
    for (std::unordered_set<int>::iterator u = out[v].begin(); u != out[v].end(); u++)
      if (valid[*u])
        observed_neighs.insert(*u);
      // Registers v's neighbor count.
      counts.push_back(observed_neighs.size());
  }
  return(counts);
}


RCPP_MODULE(yada){
    using namespace Rcpp ;
	
    class_<Graph>( "Graph" )
    // expose the default constructor
    .constructor<bool>()
        
    .method( "getVersion", &Graph::getVersion   , "get version of Pay2Recruit library")
    .method( "getE", &Graph::getE     , "get number of edges" )
    .method( "addEdge", &Graph::addEdge , "add directed edge" )
    .method( "addNode4", &Graph::addNode4 , "add node" )
    .method( "addNode5", &Graph::addNode5 , "add node" )
    .method( "getN", &Graph::getN     , "get number of nodes" )
    .method( "getTargetSize", &Graph::getTargetSize     , "get size of target nodes set" )
    .method( "getRandomNode", &Graph::getRandomNode     , "get random node" )
    .method( "initFeatures", &Graph::initFeatures     , "initialize feature names" )
    .method( "initFeatures2", &Graph::initFeatures2     , "initialize feature names" )
    .method( "getFeatureNames", &Graph::getFeatureNames     , "get feature names" )
    .method( "isFeatureFraction", &Graph::isFeatureFraction     , "returns bool vector indicating whether feature is binary" )
    .method( "getNeighbors", &Graph::getNeighbors     , "get neighbors of a node" )
    .method( "getInNeighbors", &Graph::getInNeighbors     , "get in-neighbors of a node" )
    .method( "getTargetNodes", &Graph::getTargetNodes     , "get a random sample of target nodes" )
    .method( "getLabel", &Graph::getLabel     , "get label of a node" )
    .method( "getAttributes", &Graph::getAttributes     , "get attributes of a node" )
    .method( "getInfo", &Graph::getInfo     , "get attributes of a node" )
    .method( "getStats", &Graph::getStats   , "get current statistics about a node")
    .method( "readGraph", &Graph::readGraph     , "read graph from file" )
    .method( "readCommunities2", &Graph::readCommunities2     , "read communities from file as attributes" )
    .method( "readCommunities3", &Graph::readCommunities3     , "read communities from file as attributes" )
    .method( "writeAttributesToFile", &Graph::writeAttributesToFile     , "write communities to file" )
    .method( "getPositiveNeighborsCount", &Graph::getPositiveNeighborsCount    , "counts the number of positive node neighbors" )
    .method( "getNeighborsCount", &Graph::getNeighborsCount    , "counts the number of node neighbors" )
    ;
}

