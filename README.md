D^3TS: Directed Diversity Dynamic Thompson Sampling
===================================================
(A new method for finding target nodes in an unknown network)

This repository contains the source code of D3TS, a framework proposed in

	Fabricio Murai, Diogo Renno, Bruno Ribeiro, Gisele Pappa, Don Towsley and Krista Gile
	Selective Harvesting over Networks, http://arxiv.org/abs/1703.05082.


for collecting certain labeled nodes (target nodes) where the next node to be queried must be chosen among the neighbors of the current queried node set; the available training data for deciding which node to query is restricted to the subgraph induced by the queried set (and their node attributes) and their neighbors (currently without any node or edge attributes). The queried set is expanded by querying neighbors of nodes in the set.


How to run (basics):

0) Go to the desired folder. Ex.:

	cd ~/Projects


1) Download the repository

	git clone git@bitbucket.org:diogorenno/d3ts.git


2) Save the directory d3ts to a variable name D3TSDIR.

	export D3TSDIR=`pwd`/d3ts

3) Download the datasets. CURRENTLY, THE DATASETS MUST BE STORED IN A FOLDER
   AT THE SAME LEVEL AS THE d3ts FOLDER. THEREFORE, YOU MUST CREATE A LINK:

	ln -s d3ts/data

   Datasets Wikipedia, DBpedia and CiteSeer were collected by

	Wang X, Garnett R, Schneider J (2013) Active search on graphs. In: ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, ACM, pp 731–738

   Wikipedia and DBpedia can be downloaded directly from yifeim's Github (see 3.1). While this code repository also has a CiteSeer dataset, we use a more complete version with conference labels, directly provided by R. Garnett (see 3.2).

   Datasets DBLP and LiveJournal MUST BE downloaded from the Stanford SNAP Dataset Collection (see 3.3).

   Dataset KickStarter was collected by GitHub user neight-allen (see 3.4).

   The DonorsChoose dataset used in our paper is a pre-processed version of a dataset from Kaggle. To obtain the DonorsChoose dataset, you must agree to the rules of the corresponding Kaggle competition (see 3.5).

   3.1) Wikipedia and DBpedia can be obtained by downloading these files

	https://github.com/yifeim/active-search-gp-sopt/blob/export/public/data/wikipedia_data.mat
	https://github.com/yifeim/active-search-gp-sopt/blob/export/public/data/populated_places_5000.mat

   Move them to the current folder:

	mv <download path>/wikipedia_data.mat .
	mv <download path>/populated_places_5000.mat .

   then convert them to the right format by running:

	sh data/scripts/wikipedia/run.sh
	sh data/scripts/dbpedia/run.sh

   3.2) CiteSeer can be obtained by downloading these three files from Roman Garnett's Dropbox:

	http://tiny.cc/citeseer_data/

   Then you need to copy these files must be copied to data/scripts/citeseer:

	mv <download path>/{edge_list,labels,venue_names} data/scripts/citeseer

   Last, run the script to pre-process them:

	sh data/scripts/citeseer/run.sh

   3.3) DBLP and LiveJournal MUST BE obtained by running:

	mkdir -p data/datasets/{dblp,lj}

	wget https://snap.stanford.edu/data/bigdata/communities/com-dblp.ungraph.txt.gz
	wget https://snap.stanford.edu/data/bigdata/communities/com-dblp.top5000.cmty.txt.gz
	mv com-dblp*gz data/datasets/dblp

	wget https://snap.stanford.edu/data/bigdata/communities/com-lj.ungraph.txt.gz
	wget https://snap.stanford.edu/data/bigdata/communities/com-lj.top5000.cmty.txt.gz
	mv com-lj*gz data/datasets/lj


   3.4) [OPTIONAL] Alternatively, the KickStarter dataset can also be downloaded from

	https://docs.google.com/file/d/0Bx11M2JmRNl5RlB5Vm1MUUZWbjg/edit?usp=sharing

   WARNING: You will need a mysql server up and running to use this file. To import the data to a mysql db, run:

	mysql -u <username> -p <database_name>  <  <filename.sql>

   Then, the pre-processed dataset can be obtained by running:

	sh data/scripts/kickstarter/run.sh


   3.5) The DonorsChoose dataset MUST BE obtained from Kaggle:

	https://www.kaggle.com/c/kdd-cup-2014-predicting-excitement-at-donors-choose/data

   After downloading projects.csv and donations.csv, you need to run the scripts to pre-process the DonorsChoose dataset:

	mv <download path>/projects.csv data/scripts/donors/projects.csv
	mv <download path>/donations.csv data/scripts/donors/donations.csv
	sh data/scripts/donors/run.sh


4) Install the system dependencies.

4.1 If you are using apt, run

	sudo sh $D3TSDIR/libraries/dependencies_apt.sh

**NOTE:** If you are on a Mac, you will have to use Octave 4.2.2 (which is a previous release), since RcppOctave cannot be installed with newer Octave versions. We explain below how to accomplish this using Homebrew or Mac Ports. 

4.2 If you are using Homebrew, you will have to follow the next steps.

    - Install R:

        brew install R

    -  Back up the current Octave formula:

        cp /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core/Formula/octave.rb /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core/Formula/octave.rb.bkp

    - Replace it with the Homebrew official repository's formula for Octave 4.2.2:

        wget https://raw.githubusercontent.com/Homebrew/homebrew-core/ef045d07c15304e9c3fbb1d2a5f61d036d864a1c/Formula/octave.rb -O /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core/Formula/octave.rb

    - Unlink octave if it is already installed

        brew unlink octave

    - Install it from source (otherwise it will link against wrong library versions)

        brew install --build-from-source octave

    - Finally, install RcppOctave:

        R -e "install.packages('devtools'); library('devtools'); devtools::install_github('git-steb/RcppOctave',ref='develop')"

4.3 If you are using Mac Ports, you will have to follow the next steps.

    - Install R:

        ports install R

    - Use the following instructions to install Octave 4.2.2:

        https://trac.macports.org/wiki/howto/InstallingOlderPort

    The commit hash you will need for that is 5bad64c152eb418d42c5ede34f8db59703738bc4.

    - Finally, install RcppOctave:

        R -e "install.packages('devtools'); library('devtools'); devtools::install_github('git-steb/RcppOctave',ref='develop')"


5) Install R packages. Run

	Rscript $D3TSDIR/libraries/dependencies_r.R

If this is the first time you use R on this computer, you will get an error message saying that it wasn't possible to write to the system library. To solve this issue, install one of the packages manually (e.g., open R and type install.packages('Hmisc') and confirm when the prompt asks you to create a local library). Then try to run the command above again.

Then, you can install the library:

	cd $D3TSDIR/libraries/current
	R CMD INSTALL Pay2Recruit


6) The directory src/mab contains the code to reproduce the main simulations of the paper.
There is a script to run five standalone methods and another to run RR and D3TS, on the five smallest datasets.

	$D3TSDIR/src/mab/scripts/run/simulate_standalone.sh [nthreads]
	$D3TSDIR/src/mab/scripts/run/simulate_rr_d3ts.sh [nthreads]

where nthreads default is 1.
WARNING: This may take a couple of days to run when running single-threaded. You may want to change the variable NTESTS (which controls the number of runs of each simulation) from 100 to something smaller, say 10.

To process these results, run

    $D3TSDIR/src/mab/scripts/post_proc/folder_aggregate.sh  $D3TSDIR/src/mab/results/*

Then, to generate a table summarizing these results, run

    Rscript $D3TSDIR/src/mab/scripts/post_proc/table_gen.R $D3TSDIR/src/mab/results/

Next, to generate a plot for each dataset, run

    $D3TSDIR/src/mab/scripts/post_proc/plotAll.sh $D3TSDIR/src/mab/results/
