for warmup in 0.1 0.2; do
    for dataset in donors citeseer dbpedia wikipedia kickstarter; do
        Rscript graph_group-mab.R /scratch1/renno/mestrado/p2r_results/group-mab/performance/ccpivot/exp-0.9/dts.5_max_${warmup}_0.6_shorter.config3_all/extracted/${dataset}_t*.models.RData  ${dataset}_${warmup}.pdf  ${dataset} ${warmup}  &
    done
done
