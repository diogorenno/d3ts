library(caTools)
library(hash)

raw_args <- commandArgs(trailingOnly = TRUE)
stopifnot(length(raw_args) >= 3)
mab_models_history_fn <- raw_args[1]
output_filename <- raw_args[2]
main_title <- Reduce(function(y,x) paste(y,x), raw_args[3:length(raw_args)], "")

load(mab_models_history_fn)

# #####
dataset_name <- strsplit(tail(strsplit(mab_models_history_fn, "/", fixed=TRUE)[[1]], n=1), "_", fixed=TRUE)[[1]][1]
nturn_shortener <- list(donors=200, kickstarter=700, dbpedia=700, wikipedia=400, citeseer=1500)

# Manually changes some of the models's names.
models <- list_models$names
models <- unlist(Map(function(x) if (x == "EWRLS, beta=0.90, lambda=1.00")  "EWRLS (b=0.90)" else x, models))
models <- unlist(Map(function(x) if (x == "EWRLS, beta=0.99, lambda=1.00")  "EWRLS (b=0.99)" else x, models))
models <- unlist(Map(function(x) if (x == "EWRLS, beta=1.00, lambda=1.00")  "Lin.Reg." else x, models))

# Gets only the first part of models's names (before the first comma).
models <- unlist(Map(function(mname) strsplit(mname, ",")[[1]][1], models))

# Manually changes some of the models's names.
models <- unlist(Map(function(x) if (x == "RForest.p")  "RForest" else x, models))
models <- unlist(Map(function(x) if (x == "RForest.rf") "RForest" else x, models))
models <- unlist(Map(function(x) if (x == "SVM")  "SVR" else x, models))
models <- unlist(Map(function(x) if (x == "Logistic Regression")  "Log.Reg." else x, models))

nmodels <- length(models)
stopifnot(nmodels > 1)

runs <- list_models[1:(length(list_models) - 1)]
runs_matrix <- matrix(unlist(runs), byrow=TRUE, nrow=length(runs), ncol=length(runs[[1]]))

indices <- unlist(Filter(function(i) !is.na(runs[[1]][i]), 1:length(runs[[1]])))
# nturns <- length(indices)
nturns <- min(length(indices), nturn_shortener[[dataset_name]])
runs_matrix <- runs_matrix[,indices]

props_matrix <- matrix(NA, nrow=nmodels, ncol=nturns)
for (i in 1:nturns) {
  counts <- unlist(Map(function(mid) sum(as.numeric(runs_matrix[,i] == mid)), 1:nmodels))
  props  <- counts / length(runs)
  props_matrix[,i] <- props  
}

# Smooths each models curve using a sliding window running mean.
for (i in 1:nmodels) {
  props_matrix[i,] <- runmean(props_matrix[i,], 4, endrule="mean")
}

#
color_map <- hash()
color_map[["ListNet"]] <- "#f781bf"
color_map[["Log.Reg."]] <- "#1f78b4"
color_map[["SVR"]] <- "#b2df8a"
color_map[["RForest"]] <- "#33a02c"
color_map[["ActiveSearch"]] <- "#e31a1c"
color_map[["MOD"]] <- "#000000"
color_map[["EWRLS (b=0.90)"]] <- "#fdbf6f"
color_map[["EWRLS (b=0.99)"]] <- "#ff7f00"
color_map[["Lin.Reg."]] <- "#984ea3"

#
# colors <- c("#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00", "#e7298a", "#a65628")
colors <- unlist(Map(function(mname) color_map[[mname]], models))

pdf(sprintf("%s", output_filename), height=6, width=10, pointsize=12)
par(mfrow=c(1, 1), mar=c(6, 7, 3, 19), cex=1, cex.axis=2, cex.lab=2.1, cex.main=2.5, cex.sub=1,
    oma=c(0, 0, 0, 0), xpd=TRUE)

plot(1:nturns, type="n", main=main_title, ylim=c(0, 0.45), mgp=c(4, 1.5, 0), xlab="", ylab="")
title(xlab="# queried nodes (t)", line=4.2)
title(ylab="fraction of times", line=4)

# Adds numbers to the model names and plots the legend.
models <- unlist(Map(function(i) sprintf("%s. %s", i, models[i]), 1:length(models)))
legend(x=1.05*nturns, y=0.45, models, pch="+", col=colors[1:nmodels], cex=2, bty="n")

for (i in 1:nmodels) {
  points(1:nturns, props_matrix[i,], col=colors[i], type="p", pch="+")
}

dev.off()


