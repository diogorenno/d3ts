library(caTools)
source("script.R")

# config1
#dataset_configs <- list()
#dataset_configs$donors       <- list(ylim=c(0.05,0.65), legx=50, legy=0.65, leg.cex=1.4, ncol=2, name="DonorsChoose")
#dataset_configs$wikipedia    <- list(ylim=c(0.1,0.6), legx=0, legy=0.6, leg.cex=1.4, ncol=3, name="Wikipedia: OOP")
#dataset_configs$dbpedia      <- list(ylim=c(0.42,1.0), legx=0, legy=1.0, leg.cex=1.4, ncol=3, name="DBpedia: 5000 places")
#dataset_configs$citeseer     <- list(ylim=c(0.4,0.9), legx=0, legy=0.9, leg.cex=1.4, ncol=3, name="CiteSeer: NIPS")
#dataset_configs$kickstarter  <- list(ylim=c(0.05,0.55), legx=0, legy=0.55, leg.cex=1.4, ncol=3, name="Kickstarter")

# rr_config1
dataset_configs <- list()
dataset_configs$donors       <- list(ylim=c(0.00,0.60), legx=10, legy=0.60, leg.cex=1.4, ncol=2, name="DonorsChoose")
dataset_configs$wikipedia    <- list(ylim=c(0.00,0.70), legx=15, legy=0.70, leg.cex=1.4, ncol=3, name="Wikipedia: OOP")
dataset_configs$dbpedia      <- list(ylim=c(0.10,1.12), legx=10, legy=1.13, leg.cex=1.4, ncol=3, name="DBpedia: 5000 places")
dataset_configs$citeseer     <- list(ylim=c(0.28,0.95), legx=20, legy=0.95, leg.cex=1.4, ncol=3, name="CiteSeer: NIPS")
dataset_configs$kickstarter  <- list(ylim=c(0.05,0.65), legx=15, legy=0.65, leg.cex=1.4, ncol=3, name="Kickstarter")

config <- list()
config[["MAB"]]                  <- list(color="#000000", lty="solid")
config[["ActiveSearch"]]         <- list(dir="../output/activesearch", color="#984ea3", lty="solid")
config[["ListNet"]]              <- list(dir="../output/listnet", color="#e41a1c", lty="solid")
config[["Logistic Regression"]]  <- list(dir="../output/logistic", color="#377eb8", lty="solid")
config[["MOD"]]                  <- list(dir="../output/mod", color="#ff8000", lty="solid")
config[["RForest"]]              <- list(dir="../output/rforest_party", color="#4daf4a", lty="solid")
config[["rr_config1"]]           <- list(dir="../output/rr_config1", color="#000000", lty="solid", name="RR")

##
 #
 ##
main <- function(dataset)
{
  # Gets the number of turns.
  source('empty.settings.init.R')
  stopifnot(dataset %in% names(SETTINGS))
  nturns <- SETTINGS[[dataset]]$nattempts
  
  pdf(sprintf("%s.pdf", dataset), height=8, width=14)
  par(mfrow=c(1, 1), mar=c(6, 7, 3, 1), cex=1, cex.axis=2, cex.lab=3, cex.main=2.5, cex.sub=1)

  legend_list <- list()
  
  #
  for (index in length(config)) {
    
    name <- names(config)[index]
    if (name == "MAB") next
    print(name)
    
    full_fns <- list.files(config[[name]]$dir, full.names=TRUE, recursive=FALSE)
    fns <- list.files(config[[name]]$dir, full.names=FALSE, recursive=FALSE)
    indices <- which(unlist(Map(function(fn) strsplit(fn, "_", fixed=TRUE)[[1]][1], fns)) == dataset)
    models_ind <- which(unlist(Map(function(fn) "models" %in% unlist(strsplit(fn, ".", fixed=TRUE)), fns[indices])))
    recruited_ind <- which(unlist(Map(function(fn) "recruited" %in% unlist(strsplit(fn, ".", fixed=TRUE)), fns[indices])))
    models_file <- full_fns[indices[models_ind]]
    recruited_file <- full_fns[indices[recruited_ind]]
    
    config[[name]]$models_history_fn <- models_file
    config[[name]]$recruited_history_fn <- recruited_file
    
    hit_rates <- getHitRates(dataset, config[[name]]$models_history_fn, config[[name]]$recruited_history_fn)

    nmodels <- nrow(hit_rates)
    models <- rownames(hit_rates)[1:nmodels]
    models_short <- unlist(Map(function(mname) strsplit(mname, ",")[[1]][1], models))
    models_short <- unlist(Map(function(mname) strsplit(mname, ".", fixed=TRUE)[[1]][1], models_short))

    colors <- unlist(Map(function(mname) config[[mname]]$color, models_short))
    if (nmodels > 1) {
      legend_names <- unlist(Map(function(mname) sprintf("%s (%s)", mname, config[[name]]$name), models_short))
    } else {
      legend_names <- models_short[1]
    }
    legend_list[[index]] <- list(names=legend_names, lty=rep(config[[name]]$lty, nmodels), colors=colors)
    
    main_name <- sprintf("%s", dataset_configs[[dataset]]$name)
    plot(1:ncol(hit_rates), type="n", main=main_name, ylim=dataset_configs[[dataset]]$ylim, xlab="", ylab="", mgp=c(4, 1.5, 0))
    title(xlab="n-th model pull", line=4.2)
    title(ylab="smoothed hit proportion", line=4)

    for (i in 1:nmodels) {
      lines(1:ncol(hit_rates), hit_rates[i,], col=colors[i], lty=config[[name]]$lty, lwd=2.5)
    }
  }
  
  #
  legend_names  <- unlist(Map(function(item) item$names, legend_list))
  legend_colors <- unlist(Map(function(item) item$colors, legend_list))
  legend_ltys   <- unlist(Map(function(item) item$lty, legend_list))
  legend_x <- dataset_configs[[dataset]]$legx
  legend_y <- dataset_configs[[dataset]]$legy
  legend(x=legend_x, y=legend_y, legend_names, lty=legend_ltys, col=legend_colors, lwd=2.5,
         cex=dataset_configs[[dataset]]$leg.cex, ncol=dataset_configs[[dataset]]$ncol)
  
  dev.off()
}
