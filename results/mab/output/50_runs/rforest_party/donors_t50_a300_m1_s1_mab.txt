Valid tests: 50 out of 50

<Settings>
  Public:
    clone: function
    initial_heuristic: mod
    initialize: function
    model_selection_method: ucb1
    models: list
    nattempts: 300
    ncold: 20
    nmodels: 1
    nseeds: 1
    parallel_updates: TRUE
    problem: environment
    rng_seed: 112358
    seeds: NA
    validate: function


Results at turn 1:
	Total payoff:
		1 ± 0
	Mean payoff:
		1 ± 0
	Total number of recruited nodes:
		1 ± 0

Results at turn 2:
	Total payoff:
		1.6 ± 0.4949
	Mean payoff:
		0.8 ± 0.2474
	Total number of recruited nodes:
		2 ± 0

Results at turn 3:
	Total payoff:
		1.96 ± 0.6987
	Mean payoff:
		0.6533 ± 0.2329
	Total number of recruited nodes:
		3 ± 0

Results at turn 4:
	Total payoff:
		2.58 ± 1.012
	Mean payoff:
		0.645 ± 0.253
	Total number of recruited nodes:
		4 ± 0

Results at turn 5:
	Total payoff:
		3.12 ± 1.1891
	Mean payoff:
		0.624 ± 0.2378
	Total number of recruited nodes:
		5 ± 0

Results at turn 6:
	Total payoff:
		3.76 ± 1.3786
	Mean payoff:
		0.6267 ± 0.2298
	Total number of recruited nodes:
		6 ± 0

Results at turn 7:
	Total payoff:
		4.46 ± 1.5546
	Mean payoff:
		0.6371 ± 0.2221
	Total number of recruited nodes:
		7 ± 0

Results at turn 8:
	Total payoff:
		5.16 ± 1.8335
	Mean payoff:
		0.645 ± 0.2292
	Total number of recruited nodes:
		8 ± 0

Results at turn 9:
	Total payoff:
		5.8 ± 2.0898
	Mean payoff:
		0.6444 ± 0.2322
	Total number of recruited nodes:
		9 ± 0

Results at turn 10:
	Total payoff:
		6.48 ± 2.2879
	Mean payoff:
		0.648 ± 0.2288
	Total number of recruited nodes:
		10 ± 0

Results at turn 11:
	Total payoff:
		7.32 ± 2.4615
	Mean payoff:
		0.6655 ± 0.2238
	Total number of recruited nodes:
		11 ± 0

Results at turn 12:
	Total payoff:
		8.08 ± 2.6329
	Mean payoff:
		0.6733 ± 0.2194
	Total number of recruited nodes:
		12 ± 0

Results at turn 13:
	Total payoff:
		8.78 ± 2.7942
	Mean payoff:
		0.6754 ± 0.2149
	Total number of recruited nodes:
		13 ± 0

Results at turn 14:
	Total payoff:
		9.58 ± 2.9972
	Mean payoff:
		0.6843 ± 0.2141
	Total number of recruited nodes:
		14 ± 0

Results at turn 15:
	Total payoff:
		10.3 ± 3.1896
	Mean payoff:
		0.6867 ± 0.2126
	Total number of recruited nodes:
		15 ± 0

Results at turn 16:
	Total payoff:
		11.04 ± 3.4224
	Mean payoff:
		0.69 ± 0.2139
	Total number of recruited nodes:
		16 ± 0

Results at turn 17:
	Total payoff:
		11.6 ± 3.5283
	Mean payoff:
		0.6824 ± 0.2075
	Total number of recruited nodes:
		17 ± 0

Results at turn 18:
	Total payoff:
		12.24 ± 3.7393
	Mean payoff:
		0.68 ± 0.2077
	Total number of recruited nodes:
		18 ± 0

Results at turn 19:
	Total payoff:
		12.84 ± 3.7708
	Mean payoff:
		0.6758 ± 0.1985
	Total number of recruited nodes:
		19 ± 0

Results at turn 20:
	Total payoff:
		13.38 ± 3.9943
	Mean payoff:
		0.669 ± 0.1997
	Total number of recruited nodes:
		20 ± 0

Results at turn 21:
	Total payoff:
		13.92 ± 4.1837
	Mean payoff:
		0.6629 ± 0.1992
	Total number of recruited nodes:
		21 ± 0

Results at turn 22:
	Total payoff:
		14.18 ± 4.124
	Mean payoff:
		0.6445 ± 0.1875
	Total number of recruited nodes:
		22 ± 0

Results at turn 23:
	Total payoff:
		14.5 ± 4.1662
	Mean payoff:
		0.6304 ± 0.1811
	Total number of recruited nodes:
		23 ± 0

Results at turn 24:
	Total payoff:
		14.7 ± 4.2438
	Mean payoff:
		0.6125 ± 0.1768
	Total number of recruited nodes:
		24 ± 0

Results at turn 25:
	Total payoff:
		14.92 ± 4.3511
	Mean payoff:
		0.5968 ± 0.174
	Total number of recruited nodes:
		25 ± 0

Results at turn 26:
	Total payoff:
		15.2 ± 4.4493
	Mean payoff:
		0.5846 ± 0.1711
	Total number of recruited nodes:
		26 ± 0

Results at turn 27:
	Total payoff:
		15.44 ± 4.5855
	Mean payoff:
		0.5719 ± 0.1698
	Total number of recruited nodes:
		27 ± 0

Results at turn 28:
	Total payoff:
		15.76 ± 4.7275
	Mean payoff:
		0.5629 ± 0.1688
	Total number of recruited nodes:
		28 ± 0

Results at turn 29:
	Total payoff:
		16 ± 4.8403
	Mean payoff:
		0.5517 ± 0.1669
	Total number of recruited nodes:
		29 ± 0

Results at turn 30:
	Total payoff:
		16.26 ± 4.9479
	Mean payoff:
		0.542 ± 0.1649
	Total number of recruited nodes:
		30 ± 0

Results at turn 31:
	Total payoff:
		16.44 ± 5.0433
	Mean payoff:
		0.5303 ± 0.1627
	Total number of recruited nodes:
		31 ± 0

Results at turn 32:
	Total payoff:
		16.66 ± 5.1492
	Mean payoff:
		0.5206 ± 0.1609
	Total number of recruited nodes:
		32 ± 0

Results at turn 33:
	Total payoff:
		16.9 ± 5.203
	Mean payoff:
		0.5121 ± 0.1577
	Total number of recruited nodes:
		33 ± 0

Results at turn 34:
	Total payoff:
		17.22 ± 5.2772
	Mean payoff:
		0.5065 ± 0.1552
	Total number of recruited nodes:
		34 ± 0

Results at turn 35:
	Total payoff:
		17.4 ± 5.3681
	Mean payoff:
		0.4971 ± 0.1534
	Total number of recruited nodes:
		35 ± 0

Results at turn 36:
	Total payoff:
		17.62 ± 5.4282
	Mean payoff:
		0.4894 ± 0.1508
	Total number of recruited nodes:
		36 ± 0

Results at turn 37:
	Total payoff:
		18.02 ± 5.597
	Mean payoff:
		0.487 ± 0.1513
	Total number of recruited nodes:
		37 ± 0

Results at turn 38:
	Total payoff:
		18.18 ± 5.6881
	Mean payoff:
		0.4784 ± 0.1497
	Total number of recruited nodes:
		38 ± 0

Results at turn 39:
	Total payoff:
		18.4 ± 5.7923
	Mean payoff:
		0.4718 ± 0.1485
	Total number of recruited nodes:
		39 ± 0

Results at turn 40:
	Total payoff:
		18.74 ± 5.8513
	Mean payoff:
		0.4685 ± 0.1463
	Total number of recruited nodes:
		40 ± 0

Results at turn 41:
	Total payoff:
		18.98 ± 5.9401
	Mean payoff:
		0.4629 ± 0.1449
	Total number of recruited nodes:
		41 ± 0

Results at turn 42:
	Total payoff:
		19.14 ± 5.8902
	Mean payoff:
		0.4557 ± 0.1402
	Total number of recruited nodes:
		42 ± 0

Results at turn 43:
	Total payoff:
		19.38 ± 6.0098
	Mean payoff:
		0.4507 ± 0.1398
	Total number of recruited nodes:
		43 ± 0

Results at turn 44:
	Total payoff:
		19.62 ± 6.0774
	Mean payoff:
		0.4459 ± 0.1381
	Total number of recruited nodes:
		44 ± 0

Results at turn 45:
	Total payoff:
		19.9 ± 6.1221
	Mean payoff:
		0.4422 ± 0.136
	Total number of recruited nodes:
		45 ± 0

Results at turn 46:
	Total payoff:
		20.18 ± 6.1335
	Mean payoff:
		0.4387 ± 0.1333
	Total number of recruited nodes:
		46 ± 0

Results at turn 47:
	Total payoff:
		20.44 ± 6.283
	Mean payoff:
		0.4349 ± 0.1337
	Total number of recruited nodes:
		47 ± 0

Results at turn 48:
	Total payoff:
		20.76 ± 6.4192
	Mean payoff:
		0.4325 ± 0.1337
	Total number of recruited nodes:
		48 ± 0

Results at turn 49:
	Total payoff:
		21.06 ± 6.5509
	Mean payoff:
		0.4298 ± 0.1337
	Total number of recruited nodes:
		49 ± 0

Results at turn 50:
	Total payoff:
		21.46 ± 6.6799
	Mean payoff:
		0.4292 ± 0.1336
	Total number of recruited nodes:
		50 ± 0

Results at turn 51:
	Total payoff:
		21.72 ± 6.7764
	Mean payoff:
		0.4259 ± 0.1329
	Total number of recruited nodes:
		51 ± 0

Results at turn 52:
	Total payoff:
		21.92 ± 6.8149
	Mean payoff:
		0.4215 ± 0.1311
	Total number of recruited nodes:
		52 ± 0

Results at turn 53:
	Total payoff:
		22.3 ± 6.9671
	Mean payoff:
		0.4208 ± 0.1315
	Total number of recruited nodes:
		53 ± 0

Results at turn 54:
	Total payoff:
		22.56 ± 6.9873
	Mean payoff:
		0.4178 ± 0.1294
	Total number of recruited nodes:
		54 ± 0

Results at turn 55:
	Total payoff:
		22.82 ± 7.1133
	Mean payoff:
		0.4149 ± 0.1293
	Total number of recruited nodes:
		55 ± 0

Results at turn 56:
	Total payoff:
		23.14 ± 7.2055
	Mean payoff:
		0.4132 ± 0.1287
	Total number of recruited nodes:
		56 ± 0

Results at turn 57:
	Total payoff:
		23.42 ± 7.332
	Mean payoff:
		0.4109 ± 0.1286
	Total number of recruited nodes:
		57 ± 0

Results at turn 58:
	Total payoff:
		23.92 ± 7.2586
	Mean payoff:
		0.4124 ± 0.1251
	Total number of recruited nodes:
		58 ± 0

Results at turn 59:
	Total payoff:
		24.36 ± 7.4004
	Mean payoff:
		0.4129 ± 0.1254
	Total number of recruited nodes:
		59 ± 0

Results at turn 60:
	Total payoff:
		24.64 ± 7.4279
	Mean payoff:
		0.4107 ± 0.1238
	Total number of recruited nodes:
		60 ± 0

Results at turn 61:
	Total payoff:
		24.82 ± 7.4661
	Mean payoff:
		0.4069 ± 0.1224
	Total number of recruited nodes:
		61 ± 0

Results at turn 62:
	Total payoff:
		25.32 ± 7.4654
	Mean payoff:
		0.4084 ± 0.1204
	Total number of recruited nodes:
		62 ± 0

Results at turn 63:
	Total payoff:
		25.66 ± 7.4659
	Mean payoff:
		0.4073 ± 0.1185
	Total number of recruited nodes:
		63 ± 0

Results at turn 64:
	Total payoff:
		26.12 ± 7.6469
	Mean payoff:
		0.4081 ± 0.1195
	Total number of recruited nodes:
		64 ± 0

Results at turn 65:
	Total payoff:
		26.42 ± 7.6534
	Mean payoff:
		0.4065 ± 0.1177
	Total number of recruited nodes:
		65 ± 0

Results at turn 66:
	Total payoff:
		26.88 ± 7.6815
	Mean payoff:
		0.4073 ± 0.1164
	Total number of recruited nodes:
		66 ± 0

Results at turn 67:
	Total payoff:
		27.32 ± 7.633
	Mean payoff:
		0.4078 ± 0.1139
	Total number of recruited nodes:
		67 ± 0

Results at turn 68:
	Total payoff:
		27.64 ± 7.5478
	Mean payoff:
		0.4065 ± 0.111
	Total number of recruited nodes:
		68 ± 0

Results at turn 69:
	Total payoff:
		28.02 ± 7.5983
	Mean payoff:
		0.4061 ± 0.1101
	Total number of recruited nodes:
		69 ± 0

Results at turn 70:
	Total payoff:
		28.28 ± 7.6025
	Mean payoff:
		0.404 ± 0.1086
	Total number of recruited nodes:
		70 ± 0

Results at turn 71:
	Total payoff:
		28.52 ± 7.6405
	Mean payoff:
		0.4017 ± 0.1076
	Total number of recruited nodes:
		71 ± 0

Results at turn 72:
	Total payoff:
		28.8 ± 7.5889
	Mean payoff:
		0.4 ± 0.1054
	Total number of recruited nodes:
		72 ± 0

Results at turn 73:
	Total payoff:
		29.06 ± 7.5712
	Mean payoff:
		0.3981 ± 0.1037
	Total number of recruited nodes:
		73 ± 0

Results at turn 74:
	Total payoff:
		29.38 ± 7.5671
	Mean payoff:
		0.397 ± 0.1023
	Total number of recruited nodes:
		74 ± 0

Results at turn 75:
	Total payoff:
		29.66 ± 7.604
	Mean payoff:
		0.3955 ± 0.1014
	Total number of recruited nodes:
		75 ± 0

Results at turn 76:
	Total payoff:
		29.94 ± 7.7127
	Mean payoff:
		0.3939 ± 0.1015
	Total number of recruited nodes:
		76 ± 0

Results at turn 77:
	Total payoff:
		30.12 ± 7.7661
	Mean payoff:
		0.3912 ± 0.1009
	Total number of recruited nodes:
		77 ± 0

Results at turn 78:
	Total payoff:
		30.44 ± 7.8041
	Mean payoff:
		0.3903 ± 0.1001
	Total number of recruited nodes:
		78 ± 0

Results at turn 79:
	Total payoff:
		30.64 ± 7.7926
	Mean payoff:
		0.3878 ± 0.0986
	Total number of recruited nodes:
		79 ± 0

Results at turn 80:
	Total payoff:
		30.88 ± 7.8757
	Mean payoff:
		0.386 ± 0.0984
	Total number of recruited nodes:
		80 ± 0

Results at turn 81:
	Total payoff:
		31.14 ± 7.8481
	Mean payoff:
		0.3844 ± 0.0969
	Total number of recruited nodes:
		81 ± 0

Results at turn 82:
	Total payoff:
		31.4 ± 7.822
	Mean payoff:
		0.3829 ± 0.0954
	Total number of recruited nodes:
		82 ± 0

Results at turn 83:
	Total payoff:
		31.5 ± 7.8513
	Mean payoff:
		0.3795 ± 0.0946
	Total number of recruited nodes:
		83 ± 0

Results at turn 84:
	Total payoff:
		31.7 ± 7.9212
	Mean payoff:
		0.3774 ± 0.0943
	Total number of recruited nodes:
		84 ± 0

Results at turn 85:
	Total payoff:
		32 ± 7.9231
	Mean payoff:
		0.3765 ± 0.0932
	Total number of recruited nodes:
		85 ± 0

Results at turn 86:
	Total payoff:
		32.28 ± 7.9386
	Mean payoff:
		0.3753 ± 0.0923
	Total number of recruited nodes:
		86 ± 0

Results at turn 87:
	Total payoff:
		32.5 ± 7.9955
	Mean payoff:
		0.3736 ± 0.0919
	Total number of recruited nodes:
		87 ± 0

Results at turn 88:
	Total payoff:
		32.84 ± 8.0238
	Mean payoff:
		0.3732 ± 0.0912
	Total number of recruited nodes:
		88 ± 0

Results at turn 89:
	Total payoff:
		33.02 ± 8.0673
	Mean payoff:
		0.371 ± 0.0906
	Total number of recruited nodes:
		89 ± 0

Results at turn 90:
	Total payoff:
		33.22 ± 8.1423
	Mean payoff:
		0.3691 ± 0.0905
	Total number of recruited nodes:
		90 ± 0

Results at turn 91:
	Total payoff:
		33.42 ± 8.2117
	Mean payoff:
		0.3673 ± 0.0902
	Total number of recruited nodes:
		91 ± 0

Results at turn 92:
	Total payoff:
		33.68 ± 8.1652
	Mean payoff:
		0.3661 ± 0.0888
	Total number of recruited nodes:
		92 ± 0

Results at turn 93:
	Total payoff:
		33.94 ± 8.1727
	Mean payoff:
		0.3649 ± 0.0879
	Total number of recruited nodes:
		93 ± 0

Results at turn 94:
	Total payoff:
		34.14 ± 8.0888
	Mean payoff:
		0.3632 ± 0.0861
	Total number of recruited nodes:
		94 ± 0

Results at turn 95:
	Total payoff:
		34.26 ± 8.1537
	Mean payoff:
		0.3606 ± 0.0858
	Total number of recruited nodes:
		95 ± 0

Results at turn 96:
	Total payoff:
		34.44 ± 8.0563
	Mean payoff:
		0.3588 ± 0.0839
	Total number of recruited nodes:
		96 ± 0

Results at turn 97:
	Total payoff:
		34.64 ± 8.068
	Mean payoff:
		0.3571 ± 0.0832
	Total number of recruited nodes:
		97 ± 0

Results at turn 98:
	Total payoff:
		34.94 ± 8.0923
	Mean payoff:
		0.3565 ± 0.0826
	Total number of recruited nodes:
		98 ± 0

Results at turn 99:
	Total payoff:
		35.22 ± 8.1599
	Mean payoff:
		0.3558 ± 0.0824
	Total number of recruited nodes:
		99 ± 0

Results at turn 100:
	Total payoff:
		35.34 ± 8.1356
	Mean payoff:
		0.3534 ± 0.0814
	Total number of recruited nodes:
		100 ± 0

Results at turn 101:
	Total payoff:
		35.52 ± 8.1171
	Mean payoff:
		0.3517 ± 0.0804
	Total number of recruited nodes:
		101 ± 0

Results at turn 102:
	Total payoff:
		35.68 ± 8.1126
	Mean payoff:
		0.3498 ± 0.0795
	Total number of recruited nodes:
		102 ± 0

Results at turn 103:
	Total payoff:
		35.9 ± 8.0819
	Mean payoff:
		0.3485 ± 0.0785
	Total number of recruited nodes:
		103 ± 0

Results at turn 104:
	Total payoff:
		36.02 ± 8.0572
	Mean payoff:
		0.3463 ± 0.0775
	Total number of recruited nodes:
		104 ± 0

Results at turn 105:
	Total payoff:
		36.14 ± 7.9051
	Mean payoff:
		0.3442 ± 0.0753
	Total number of recruited nodes:
		105 ± 0

Results at turn 106:
	Total payoff:
		36.4 ± 7.8584
	Mean payoff:
		0.3434 ± 0.0741
	Total number of recruited nodes:
		106 ± 0

Results at turn 107:
	Total payoff:
		36.48 ± 7.8252
	Mean payoff:
		0.3409 ± 0.0731
	Total number of recruited nodes:
		107 ± 0

Results at turn 108:
	Total payoff:
		36.76 ± 7.7211
	Mean payoff:
		0.3404 ± 0.0715
	Total number of recruited nodes:
		108 ± 0

Results at turn 109:
	Total payoff:
		36.94 ± 7.7576
	Mean payoff:
		0.3389 ± 0.0712
	Total number of recruited nodes:
		109 ± 0

Results at turn 110:
	Total payoff:
		37.14 ± 7.6586
	Mean payoff:
		0.3376 ± 0.0696
	Total number of recruited nodes:
		110 ± 0

Results at turn 111:
	Total payoff:
		37.34 ± 7.5663
	Mean payoff:
		0.3364 ± 0.0682
	Total number of recruited nodes:
		111 ± 0

Results at turn 112:
	Total payoff:
		37.5 ± 7.5869
	Mean payoff:
		0.3348 ± 0.0677
	Total number of recruited nodes:
		112 ± 0

Results at turn 113:
	Total payoff:
		37.7 ± 7.4539
	Mean payoff:
		0.3336 ± 0.066
	Total number of recruited nodes:
		113 ± 0

Results at turn 114:
	Total payoff:
		37.96 ± 7.4037
	Mean payoff:
		0.333 ± 0.0649
	Total number of recruited nodes:
		114 ± 0

Results at turn 115:
	Total payoff:
		38.1 ± 7.3713
	Mean payoff:
		0.3313 ± 0.0641
	Total number of recruited nodes:
		115 ± 0

Results at turn 116:
	Total payoff:
		38.22 ± 7.3215
	Mean payoff:
		0.3295 ± 0.0631
	Total number of recruited nodes:
		116 ± 0

Results at turn 117:
	Total payoff:
		38.42 ± 7.2818
	Mean payoff:
		0.3284 ± 0.0622
	Total number of recruited nodes:
		117 ± 0

Results at turn 118:
	Total payoff:
		38.54 ± 7.2877
	Mean payoff:
		0.3266 ± 0.0618
	Total number of recruited nodes:
		118 ± 0

Results at turn 119:
	Total payoff:
		38.68 ± 7.2799
	Mean payoff:
		0.325 ± 0.0612
	Total number of recruited nodes:
		119 ± 0

Results at turn 120:
	Total payoff:
		38.84 ± 7.2853
	Mean payoff:
		0.3237 ± 0.0607
	Total number of recruited nodes:
		120 ± 0

Results at turn 121:
	Total payoff:
		39.02 ± 7.3025
	Mean payoff:
		0.3225 ± 0.0604
	Total number of recruited nodes:
		121 ± 0

Results at turn 122:
	Total payoff:
		39.22 ± 7.2907
	Mean payoff:
		0.3215 ± 0.0598
	Total number of recruited nodes:
		122 ± 0

Results at turn 123:
	Total payoff:
		39.38 ± 7.298
	Mean payoff:
		0.3202 ± 0.0593
	Total number of recruited nodes:
		123 ± 0

Results at turn 124:
	Total payoff:
		39.6 ± 7.2196
	Mean payoff:
		0.3194 ± 0.0582
	Total number of recruited nodes:
		124 ± 0

Results at turn 125:
	Total payoff:
		39.74 ± 7.2134
	Mean payoff:
		0.3179 ± 0.0577
	Total number of recruited nodes:
		125 ± 0

Results at turn 126:
	Total payoff:
		39.94 ± 7.2349
	Mean payoff:
		0.317 ± 0.0574
	Total number of recruited nodes:
		126 ± 0

Results at turn 127:
	Total payoff:
		40.04 ± 7.1227
	Mean payoff:
		0.3153 ± 0.0561
	Total number of recruited nodes:
		127 ± 0

Results at turn 128:
	Total payoff:
		40.28 ± 7.051
	Mean payoff:
		0.3147 ± 0.0551
	Total number of recruited nodes:
		128 ± 0

Results at turn 129:
	Total payoff:
		40.46 ± 7.0717
	Mean payoff:
		0.3136 ± 0.0548
	Total number of recruited nodes:
		129 ± 0

Results at turn 130:
	Total payoff:
		40.62 ± 6.9485
	Mean payoff:
		0.3125 ± 0.0534
	Total number of recruited nodes:
		130 ± 0

Results at turn 131:
	Total payoff:
		40.82 ± 6.8354
	Mean payoff:
		0.3116 ± 0.0522
	Total number of recruited nodes:
		131 ± 0

Results at turn 132:
	Total payoff:
		40.94 ± 6.7383
	Mean payoff:
		0.3102 ± 0.051
	Total number of recruited nodes:
		132 ± 0

Results at turn 133:
	Total payoff:
		41.08 ± 6.6635
	Mean payoff:
		0.3089 ± 0.0501
	Total number of recruited nodes:
		133 ± 0

Results at turn 134:
	Total payoff:
		41.3 ± 6.5846
	Mean payoff:
		0.3082 ± 0.0491
	Total number of recruited nodes:
		134 ± 0

Results at turn 135:
	Total payoff:
		41.48 ± 6.4055
	Mean payoff:
		0.3073 ± 0.0474
	Total number of recruited nodes:
		135 ± 0

Results at turn 136:
	Total payoff:
		41.54 ± 6.3026
	Mean payoff:
		0.3054 ± 0.0463
	Total number of recruited nodes:
		136 ± 0

Results at turn 137:
	Total payoff:
		41.72 ± 6.2173
	Mean payoff:
		0.3045 ± 0.0454
	Total number of recruited nodes:
		137 ± 0

Results at turn 138:
	Total payoff:
		41.86 ± 6.1545
	Mean payoff:
		0.3033 ± 0.0446
	Total number of recruited nodes:
		138 ± 0

Results at turn 139:
	Total payoff:
		42.02 ± 6.1095
	Mean payoff:
		0.3023 ± 0.044
	Total number of recruited nodes:
		139 ± 0

Results at turn 140:
	Total payoff:
		42.22 ± 6.0078
	Mean payoff:
		0.3016 ± 0.0429
	Total number of recruited nodes:
		140 ± 0

Results at turn 141:
	Total payoff:
		42.34 ± 5.9199
	Mean payoff:
		0.3003 ± 0.042
	Total number of recruited nodes:
		141 ± 0

Results at turn 142:
	Total payoff:
		42.5 ± 6.0009
	Mean payoff:
		0.2993 ± 0.0423
	Total number of recruited nodes:
		142 ± 0

Results at turn 143:
	Total payoff:
		42.66 ± 5.8714
	Mean payoff:
		0.2983 ± 0.0411
	Total number of recruited nodes:
		143 ± 0

Results at turn 144:
	Total payoff:
		42.76 ± 5.8504
	Mean payoff:
		0.2969 ± 0.0406
	Total number of recruited nodes:
		144 ± 0

Results at turn 145:
	Total payoff:
		42.96 ± 5.8553
	Mean payoff:
		0.2963 ± 0.0404
	Total number of recruited nodes:
		145 ± 0

Results at turn 146:
	Total payoff:
		43.12 ± 5.7946
	Mean payoff:
		0.2953 ± 0.0397
	Total number of recruited nodes:
		146 ± 0

Results at turn 147:
	Total payoff:
		43.24 ± 5.7199
	Mean payoff:
		0.2941 ± 0.0389
	Total number of recruited nodes:
		147 ± 0

Results at turn 148:
	Total payoff:
		43.32 ± 5.7337
	Mean payoff:
		0.2927 ± 0.0387
	Total number of recruited nodes:
		148 ± 0

Results at turn 149:
	Total payoff:
		43.48 ± 5.7151
	Mean payoff:
		0.2918 ± 0.0384
	Total number of recruited nodes:
		149 ± 0

Results at turn 150:
	Total payoff:
		43.58 ± 5.7538
	Mean payoff:
		0.2905 ± 0.0384
	Total number of recruited nodes:
		150 ± 0

Results at turn 151:
	Total payoff:
		43.7 ± 5.7543
	Mean payoff:
		0.2894 ± 0.0381
	Total number of recruited nodes:
		151 ± 0

Results at turn 152:
	Total payoff:
		43.88 ± 5.6556
	Mean payoff:
		0.2887 ± 0.0372
	Total number of recruited nodes:
		152 ± 0

Results at turn 153:
	Total payoff:
		44.06 ± 5.6112
	Mean payoff:
		0.288 ± 0.0367
	Total number of recruited nodes:
		153 ± 0

Results at turn 154:
	Total payoff:
		44.14 ± 5.5476
	Mean payoff:
		0.2866 ± 0.036
	Total number of recruited nodes:
		154 ± 0

Results at turn 155:
	Total payoff:
		44.26 ± 5.4128
	Mean payoff:
		0.2855 ± 0.0349
	Total number of recruited nodes:
		155 ± 0

Results at turn 156:
	Total payoff:
		44.38 ± 5.3677
	Mean payoff:
		0.2845 ± 0.0344
	Total number of recruited nodes:
		156 ± 0

Results at turn 157:
	Total payoff:
		44.56 ± 5.3037
	Mean payoff:
		0.2838 ± 0.0338
	Total number of recruited nodes:
		157 ± 0

Results at turn 158:
	Total payoff:
		44.64 ± 5.1854
	Mean payoff:
		0.2825 ± 0.0328
	Total number of recruited nodes:
		158 ± 0

Results at turn 159:
	Total payoff:
		44.78 ± 5.0114
	Mean payoff:
		0.2816 ± 0.0315
	Total number of recruited nodes:
		159 ± 0

Results at turn 160:
	Total payoff:
		44.92 ± 4.8733
	Mean payoff:
		0.2808 ± 0.0305
	Total number of recruited nodes:
		160 ± 0

Results at turn 161:
	Total payoff:
		45 ± 4.8445
	Mean payoff:
		0.2795 ± 0.0301
	Total number of recruited nodes:
		161 ± 0

Results at turn 162:
	Total payoff:
		45.08 ± 4.81
	Mean payoff:
		0.2783 ± 0.0297
	Total number of recruited nodes:
		162 ± 0

Results at turn 163:
	Total payoff:
		45.2 ± 4.8022
	Mean payoff:
		0.2773 ± 0.0295
	Total number of recruited nodes:
		163 ± 0

Results at turn 164:
	Total payoff:
		45.3 ± 4.799
	Mean payoff:
		0.2762 ± 0.0293
	Total number of recruited nodes:
		164 ± 0

Results at turn 165:
	Total payoff:
		45.38 ± 4.6506
	Mean payoff:
		0.275 ± 0.0282
	Total number of recruited nodes:
		165 ± 0

Results at turn 166:
	Total payoff:
		45.54 ± 4.532
	Mean payoff:
		0.2743 ± 0.0273
	Total number of recruited nodes:
		166 ± 0

Results at turn 167:
	Total payoff:
		45.66 ± 4.4704
	Mean payoff:
		0.2734 ± 0.0268
	Total number of recruited nodes:
		167 ± 0

Results at turn 168:
	Total payoff:
		45.8 ± 4.3142
	Mean payoff:
		0.2726 ± 0.0257
	Total number of recruited nodes:
		168 ± 0

Results at turn 169:
	Total payoff:
		45.98 ± 4.2402
	Mean payoff:
		0.2721 ± 0.0251
	Total number of recruited nodes:
		169 ± 0

Results at turn 170:
	Total payoff:
		46.1 ± 4.1466
	Mean payoff:
		0.2712 ± 0.0244
	Total number of recruited nodes:
		170 ± 0

Results at turn 171:
	Total payoff:
		46.2 ± 3.9383
	Mean payoff:
		0.2702 ± 0.023
	Total number of recruited nodes:
		171 ± 0

Results at turn 172:
	Total payoff:
		46.26 ± 3.8956
	Mean payoff:
		0.269 ± 0.0226
	Total number of recruited nodes:
		172 ± 0

Results at turn 173:
	Total payoff:
		46.32 ± 3.8621
	Mean payoff:
		0.2677 ± 0.0223
	Total number of recruited nodes:
		173 ± 0

Results at turn 174:
	Total payoff:
		46.44 ± 3.887
	Mean payoff:
		0.2669 ± 0.0223
	Total number of recruited nodes:
		174 ± 0

Results at turn 175:
	Total payoff:
		46.58 ± 3.9023
	Mean payoff:
		0.2662 ± 0.0223
	Total number of recruited nodes:
		175 ± 0

Results at turn 176:
	Total payoff:
		46.64 ± 3.8691
	Mean payoff:
		0.265 ± 0.022
	Total number of recruited nodes:
		176 ± 0

Results at turn 177:
	Total payoff:
		46.7 ± 3.8132
	Mean payoff:
		0.2638 ± 0.0215
	Total number of recruited nodes:
		177 ± 0

Results at turn 178:
	Total payoff:
		46.78 ± 3.6828
	Mean payoff:
		0.2628 ± 0.0207
	Total number of recruited nodes:
		178 ± 0

Results at turn 179:
	Total payoff:
		46.86 ± 3.6084
	Mean payoff:
		0.2618 ± 0.0202
	Total number of recruited nodes:
		179 ± 0

Results at turn 180:
	Total payoff:
		46.92 ± 3.5791
	Mean payoff:
		0.2607 ± 0.0199
	Total number of recruited nodes:
		180 ± 0

Results at turn 181:
	Total payoff:
		47.1 ± 3.5471
	Mean payoff:
		0.2602 ± 0.0196
	Total number of recruited nodes:
		181 ± 0

Results at turn 182:
	Total payoff:
		47.22 ± 3.4064
	Mean payoff:
		0.2595 ± 0.0187
	Total number of recruited nodes:
		182 ± 0

Results at turn 183:
	Total payoff:
		47.32 ± 3.3651
	Mean payoff:
		0.2586 ± 0.0184
	Total number of recruited nodes:
		183 ± 0

Results at turn 184:
	Total payoff:
		47.48 ± 3.2529
	Mean payoff:
		0.258 ± 0.0177
	Total number of recruited nodes:
		184 ± 0

Results at turn 185:
	Total payoff:
		47.52 ± 3.2277
	Mean payoff:
		0.2569 ± 0.0174
	Total number of recruited nodes:
		185 ± 0

Results at turn 186:
	Total payoff:
		47.6 ± 3.2325
	Mean payoff:
		0.2559 ± 0.0174
	Total number of recruited nodes:
		186 ± 0

Results at turn 187:
	Total payoff:
		47.72 ± 3.1039
	Mean payoff:
		0.2552 ± 0.0166
	Total number of recruited nodes:
		187 ± 0

Results at turn 188:
	Total payoff:
		47.82 ± 3.0818
	Mean payoff:
		0.2544 ± 0.0164
	Total number of recruited nodes:
		188 ± 0

Results at turn 189:
	Total payoff:
		47.92 ± 3.0428
	Mean payoff:
		0.2535 ± 0.0161
	Total number of recruited nodes:
		189 ± 0

Results at turn 190:
	Total payoff:
		47.98 ± 2.9657
	Mean payoff:
		0.2525 ± 0.0156
	Total number of recruited nodes:
		190 ± 0

Results at turn 191:
	Total payoff:
		48.04 ± 2.8994
	Mean payoff:
		0.2515 ± 0.0152
	Total number of recruited nodes:
		191 ± 0

Results at turn 192:
	Total payoff:
		48.14 ± 2.8357
	Mean payoff:
		0.2507 ± 0.0148
	Total number of recruited nodes:
		192 ± 0

Results at turn 193:
	Total payoff:
		48.24 ± 2.7521
	Mean payoff:
		0.2499 ± 0.0143
	Total number of recruited nodes:
		193 ± 0

Results at turn 194:
	Total payoff:
		48.32 ± 2.6836
	Mean payoff:
		0.2491 ± 0.0138
	Total number of recruited nodes:
		194 ± 0

Results at turn 195:
	Total payoff:
		48.44 ± 2.6891
	Mean payoff:
		0.2484 ± 0.0138
	Total number of recruited nodes:
		195 ± 0

Results at turn 196:
	Total payoff:
		48.48 ± 2.636
	Mean payoff:
		0.2473 ± 0.0134
	Total number of recruited nodes:
		196 ± 0

Results at turn 197:
	Total payoff:
		48.54 ± 2.6358
	Mean payoff:
		0.2464 ± 0.0134
	Total number of recruited nodes:
		197 ± 0

Results at turn 198:
	Total payoff:
		48.62 ± 2.6178
	Mean payoff:
		0.2456 ± 0.0132
	Total number of recruited nodes:
		198 ± 0

Results at turn 199:
	Total payoff:
		48.66 ± 2.6389
	Mean payoff:
		0.2445 ± 0.0133
	Total number of recruited nodes:
		199 ± 0

Results at turn 200:
	Total payoff:
		48.76 ± 2.6307
	Mean payoff:
		0.2438 ± 0.0132
	Total number of recruited nodes:
		200 ± 0

Results at turn 201:
	Total payoff:
		48.86 ± 2.5234
	Mean payoff:
		0.2431 ± 0.0126
	Total number of recruited nodes:
		201 ± 0

Results at turn 202:
	Total payoff:
		48.9 ± 2.501
	Mean payoff:
		0.2421 ± 0.0124
	Total number of recruited nodes:
		202 ± 0

Results at turn 203:
	Total payoff:
		48.94 ± 2.4695
	Mean payoff:
		0.2411 ± 0.0122
	Total number of recruited nodes:
		203 ± 0

Results at turn 204:
	Total payoff:
		49 ± 2.4661
	Mean payoff:
		0.2402 ± 0.0121
	Total number of recruited nodes:
		204 ± 0

Results at turn 205:
	Total payoff:
		49.04 ± 2.4408
	Mean payoff:
		0.2392 ± 0.0119
	Total number of recruited nodes:
		205 ± 0

Results at turn 206:
	Total payoff:
		49.16 ± 2.3592
	Mean payoff:
		0.2386 ± 0.0115
	Total number of recruited nodes:
		206 ± 0

Results at turn 207:
	Total payoff:
		49.28 ± 2.2861
	Mean payoff:
		0.2381 ± 0.011
	Total number of recruited nodes:
		207 ± 0

Results at turn 208:
	Total payoff:
		49.36 ± 2.2836
	Mean payoff:
		0.2373 ± 0.011
	Total number of recruited nodes:
		208 ± 0

Results at turn 209:
	Total payoff:
		49.4 ± 2.2678
	Mean payoff:
		0.2364 ± 0.0109
	Total number of recruited nodes:
		209 ± 0

Results at turn 210:
	Total payoff:
		49.46 ± 2.2334
	Mean payoff:
		0.2355 ± 0.0106
	Total number of recruited nodes:
		210 ± 0

Results at turn 211:
	Total payoff:
		49.52 ± 2.2519
	Mean payoff:
		0.2347 ± 0.0107
	Total number of recruited nodes:
		211 ± 0

Results at turn 212:
	Total payoff:
		49.56 ± 2.2054
	Mean payoff:
		0.2338 ± 0.0104
	Total number of recruited nodes:
		212 ± 0

Results at turn 213:
	Total payoff:
		49.58 ± 2.2047
	Mean payoff:
		0.2328 ± 0.0104
	Total number of recruited nodes:
		213 ± 0

Results at turn 214:
	Total payoff:
		49.58 ± 2.2047
	Mean payoff:
		0.2317 ± 0.0103
	Total number of recruited nodes:
		214 ± 0

Results at turn 215:
	Total payoff:
		49.6 ± 2.1759
	Mean payoff:
		0.2307 ± 0.0101
	Total number of recruited nodes:
		215 ± 0

Results at turn 216:
	Total payoff:
		49.68 ± 2.1135
	Mean payoff:
		0.23 ± 0.0098
	Total number of recruited nodes:
		216 ± 0

Results at turn 217:
	Total payoff:
		49.76 ± 2.1243
	Mean payoff:
		0.2293 ± 0.0098
	Total number of recruited nodes:
		217 ± 0

Results at turn 218:
	Total payoff:
		49.82 ± 2.1351
	Mean payoff:
		0.2285 ± 0.0098
	Total number of recruited nodes:
		218 ± 0

Results at turn 219:
	Total payoff:
		49.84 ± 2.132
	Mean payoff:
		0.2276 ± 0.0097
	Total number of recruited nodes:
		219 ± 0

Results at turn 220:
	Total payoff:
		49.9 ± 2.0228
	Mean payoff:
		0.2268 ± 0.0092
	Total number of recruited nodes:
		220 ± 0

Results at turn 221:
	Total payoff:
		49.96 ± 1.9791
	Mean payoff:
		0.2261 ± 0.009
	Total number of recruited nodes:
		221 ± 0

Results at turn 222:
	Total payoff:
		50 ± 1.9483
	Mean payoff:
		0.2252 ± 0.0088
	Total number of recruited nodes:
		222 ± 0

Results at turn 223:
	Total payoff:
		50.04 ± 1.9996
	Mean payoff:
		0.2244 ± 0.009
	Total number of recruited nodes:
		223 ± 0

Results at turn 224:
	Total payoff:
		50.1 ± 1.9509
	Mean payoff:
		0.2237 ± 0.0087
	Total number of recruited nodes:
		224 ± 0

Results at turn 225:
	Total payoff:
		50.12 ± 1.934
	Mean payoff:
		0.2228 ± 0.0086
	Total number of recruited nodes:
		225 ± 0

Results at turn 226:
	Total payoff:
		50.22 ± 1.9197
	Mean payoff:
		0.2222 ± 0.0085
	Total number of recruited nodes:
		226 ± 0

Results at turn 227:
	Total payoff:
		50.24 ± 1.9226
	Mean payoff:
		0.2213 ± 0.0085
	Total number of recruited nodes:
		227 ± 0

Results at turn 228:
	Total payoff:
		50.28 ± 1.9277
	Mean payoff:
		0.2205 ± 0.0085
	Total number of recruited nodes:
		228 ± 0

Results at turn 229:
	Total payoff:
		50.32 ± 1.9529
	Mean payoff:
		0.2197 ± 0.0085
	Total number of recruited nodes:
		229 ± 0

Results at turn 230:
	Total payoff:
		50.34 ± 1.9547
	Mean payoff:
		0.2189 ± 0.0085
	Total number of recruited nodes:
		230 ± 0

Results at turn 231:
	Total payoff:
		50.36 ± 1.9667
	Mean payoff:
		0.218 ± 0.0085
	Total number of recruited nodes:
		231 ± 0

Results at turn 232:
	Total payoff:
		50.46 ± 1.8975
	Mean payoff:
		0.2175 ± 0.0082
	Total number of recruited nodes:
		232 ± 0

Results at turn 233:
	Total payoff:
		50.5 ± 1.8098
	Mean payoff:
		0.2167 ± 0.0078
	Total number of recruited nodes:
		233 ± 0

Results at turn 234:
	Total payoff:
		50.56 ± 1.8424
	Mean payoff:
		0.2161 ± 0.0079
	Total number of recruited nodes:
		234 ± 0

Results at turn 235:
	Total payoff:
		50.62 ± 1.7944
	Mean payoff:
		0.2154 ± 0.0076
	Total number of recruited nodes:
		235 ± 0

Results at turn 236:
	Total payoff:
		50.68 ± 1.7664
	Mean payoff:
		0.2147 ± 0.0075
	Total number of recruited nodes:
		236 ± 0

Results at turn 237:
	Total payoff:
		50.74 ± 1.7357
	Mean payoff:
		0.2141 ± 0.0073
	Total number of recruited nodes:
		237 ± 0

Results at turn 238:
	Total payoff:
		50.76 ± 1.709
	Mean payoff:
		0.2133 ± 0.0072
	Total number of recruited nodes:
		238 ± 0

Results at turn 239:
	Total payoff:
		50.78 ± 1.6938
	Mean payoff:
		0.2125 ± 0.0071
	Total number of recruited nodes:
		239 ± 0

Results at turn 240:
	Total payoff:
		50.84 ± 1.6581
	Mean payoff:
		0.2118 ± 0.0069
	Total number of recruited nodes:
		240 ± 0

Results at turn 241:
	Total payoff:
		50.9 ± 1.6194
	Mean payoff:
		0.2112 ± 0.0067
	Total number of recruited nodes:
		241 ± 0

Results at turn 242:
	Total payoff:
		50.94 ± 1.5832
	Mean payoff:
		0.2105 ± 0.0065
	Total number of recruited nodes:
		242 ± 0

Results at turn 243:
	Total payoff:
		51.04 ± 1.4841
	Mean payoff:
		0.21 ± 0.0061
	Total number of recruited nodes:
		243 ± 0

Results at turn 244:
	Total payoff:
		51.12 ± 1.4234
	Mean payoff:
		0.2095 ± 0.0058
	Total number of recruited nodes:
		244 ± 0

Results at turn 245:
	Total payoff:
		51.22 ± 1.4184
	Mean payoff:
		0.2091 ± 0.0058
	Total number of recruited nodes:
		245 ± 0

Results at turn 246:
	Total payoff:
		51.26 ± 1.3969
	Mean payoff:
		0.2084 ± 0.0057
	Total number of recruited nodes:
		246 ± 0

Results at turn 247:
	Total payoff:
		51.38 ± 1.3384
	Mean payoff:
		0.208 ± 0.0054
	Total number of recruited nodes:
		247 ± 0

Results at turn 248:
	Total payoff:
		51.4 ± 1.3553
	Mean payoff:
		0.2073 ± 0.0055
	Total number of recruited nodes:
		248 ± 0

Results at turn 249:
	Total payoff:
		51.46 ± 1.3584
	Mean payoff:
		0.2067 ± 0.0055
	Total number of recruited nodes:
		249 ± 0

Results at turn 250:
	Total payoff:
		51.54 ± 1.3584
	Mean payoff:
		0.2062 ± 0.0054
	Total number of recruited nodes:
		250 ± 0

Results at turn 251:
	Total payoff:
		51.58 ± 1.3716
	Mean payoff:
		0.2055 ± 0.0055
	Total number of recruited nodes:
		251 ± 0

Results at turn 252:
	Total payoff:
		51.58 ± 1.3716
	Mean payoff:
		0.2047 ± 0.0054
	Total number of recruited nodes:
		252 ± 0

Results at turn 253:
	Total payoff:
		51.64 ± 1.3667
	Mean payoff:
		0.2041 ± 0.0054
	Total number of recruited nodes:
		253 ± 0

Results at turn 254:
	Total payoff:
		51.66 ± 1.3644
	Mean payoff:
		0.2034 ± 0.0054
	Total number of recruited nodes:
		254 ± 0

Results at turn 255:
	Total payoff:
		51.72 ± 1.2784
	Mean payoff:
		0.2028 ± 0.005
	Total number of recruited nodes:
		255 ± 0

Results at turn 256:
	Total payoff:
		51.74 ± 1.2586
	Mean payoff:
		0.2021 ± 0.0049
	Total number of recruited nodes:
		256 ± 0

Results at turn 257:
	Total payoff:
		51.78 ± 1.1657
	Mean payoff:
		0.2015 ± 0.0045
	Total number of recruited nodes:
		257 ± 0

Results at turn 258:
	Total payoff:
		51.86 ± 1.2124
	Mean payoff:
		0.201 ± 0.0047
	Total number of recruited nodes:
		258 ± 0

Results at turn 259:
	Total payoff:
		51.88 ± 1.2229
	Mean payoff:
		0.2003 ± 0.0047
	Total number of recruited nodes:
		259 ± 0

Results at turn 260:
	Total payoff:
		51.92 ± 1.2262
	Mean payoff:
		0.1997 ± 0.0047
	Total number of recruited nodes:
		260 ± 0

Results at turn 261:
	Total payoff:
		51.92 ± 1.2262
	Mean payoff:
		0.1989 ± 0.0047
	Total number of recruited nodes:
		261 ± 0

Results at turn 262:
	Total payoff:
		52 ± 1.178
	Mean payoff:
		0.1985 ± 0.0045
	Total number of recruited nodes:
		262 ± 0

Results at turn 263:
	Total payoff:
		52.02 ± 1.1516
	Mean payoff:
		0.1978 ± 0.0044
	Total number of recruited nodes:
		263 ± 0

Results at turn 264:
	Total payoff:
		52.06 ± 1.1502
	Mean payoff:
		0.1972 ± 0.0044
	Total number of recruited nodes:
		264 ± 0

Results at turn 265:
	Total payoff:
		52.12 ± 1.1364
	Mean payoff:
		0.1967 ± 0.0043
	Total number of recruited nodes:
		265 ± 0

Results at turn 266:
	Total payoff:
		52.22 ± 1.148
	Mean payoff:
		0.1963 ± 0.0043
	Total number of recruited nodes:
		266 ± 0

Results at turn 267:
	Total payoff:
		52.26 ± 1.1572
	Mean payoff:
		0.1957 ± 0.0043
	Total number of recruited nodes:
		267 ± 0

Results at turn 268:
	Total payoff:
		52.26 ± 1.1572
	Mean payoff:
		0.195 ± 0.0043
	Total number of recruited nodes:
		268 ± 0

Results at turn 269:
	Total payoff:
		52.32 ± 1.1683
	Mean payoff:
		0.1945 ± 0.0043
	Total number of recruited nodes:
		269 ± 0

Results at turn 270:
	Total payoff:
		52.34 ± 1.1359
	Mean payoff:
		0.1939 ± 0.0042
	Total number of recruited nodes:
		270 ± 0

Results at turn 271:
	Total payoff:
		52.36 ± 1.1386
	Mean payoff:
		0.1932 ± 0.0042
	Total number of recruited nodes:
		271 ± 0

Results at turn 272:
	Total payoff:
		52.38 ± 1.1045
	Mean payoff:
		0.1926 ± 0.0041
	Total number of recruited nodes:
		272 ± 0

Results at turn 273:
	Total payoff:
		52.38 ± 1.1045
	Mean payoff:
		0.1919 ± 0.004
	Total number of recruited nodes:
		273 ± 0

Results at turn 274:
	Total payoff:
		52.38 ± 1.1045
	Mean payoff:
		0.1912 ± 0.004
	Total number of recruited nodes:
		274 ± 0

Results at turn 275:
	Total payoff:
		52.4 ± 1.1066
	Mean payoff:
		0.1905 ± 0.004
	Total number of recruited nodes:
		275 ± 0

Results at turn 276:
	Total payoff:
		52.42 ± 1.0515
	Mean payoff:
		0.1899 ± 0.0038
	Total number of recruited nodes:
		276 ± 0

Results at turn 277:
	Total payoff:
		52.44 ± 1.0721
	Mean payoff:
		0.1893 ± 0.0039
	Total number of recruited nodes:
		277 ± 0

Results at turn 278:
	Total payoff:
		52.44 ± 1.0721
	Mean payoff:
		0.1886 ± 0.0039
	Total number of recruited nodes:
		278 ± 0

Results at turn 279:
	Total payoff:
		52.48 ± 1.111
	Mean payoff:
		0.1881 ± 0.004
	Total number of recruited nodes:
		279 ± 0

Results at turn 280:
	Total payoff:
		52.5 ± 1.1112
	Mean payoff:
		0.1875 ± 0.004
	Total number of recruited nodes:
		280 ± 0

Results at turn 281:
	Total payoff:
		52.58 ± 1.1445
	Mean payoff:
		0.1871 ± 0.0041
	Total number of recruited nodes:
		281 ± 0

Results at turn 282:
	Total payoff:
		52.64 ± 1.1911
	Mean payoff:
		0.1867 ± 0.0042
	Total number of recruited nodes:
		282 ± 0

Results at turn 283:
	Total payoff:
		52.64 ± 1.1911
	Mean payoff:
		0.186 ± 0.0042
	Total number of recruited nodes:
		283 ± 0

Results at turn 284:
	Total payoff:
		52.64 ± 1.1911
	Mean payoff:
		0.1854 ± 0.0042
	Total number of recruited nodes:
		284 ± 0

Results at turn 285:
	Total payoff:
		52.64 ± 1.1911
	Mean payoff:
		0.1847 ± 0.0042
	Total number of recruited nodes:
		285 ± 0

Results at turn 286:
	Total payoff:
		52.66 ± 1.1537
	Mean payoff:
		0.1841 ± 0.004
	Total number of recruited nodes:
		286 ± 0

Results at turn 287:
	Total payoff:
		52.72 ± 1.1787
	Mean payoff:
		0.1837 ± 0.0041
	Total number of recruited nodes:
		287 ± 0

Results at turn 288:
	Total payoff:
		52.78 ± 1.183
	Mean payoff:
		0.1833 ± 0.0041
	Total number of recruited nodes:
		288 ± 0

Results at turn 289:
	Total payoff:
		52.82 ± 1.1373
	Mean payoff:
		0.1828 ± 0.0039
	Total number of recruited nodes:
		289 ± 0

Results at turn 290:
	Total payoff:
		52.86 ± 1.143
	Mean payoff:
		0.1823 ± 0.0039
	Total number of recruited nodes:
		290 ± 0

Results at turn 291:
	Total payoff:
		52.94 ± 1.1502
	Mean payoff:
		0.1819 ± 0.004
	Total number of recruited nodes:
		291 ± 0

Results at turn 292:
	Total payoff:
		52.98 ± 1.1516
	Mean payoff:
		0.1814 ± 0.0039
	Total number of recruited nodes:
		292 ± 0

Results at turn 293:
	Total payoff:
		52.98 ± 1.1516
	Mean payoff:
		0.1808 ± 0.0039
	Total number of recruited nodes:
		293 ± 0

Results at turn 294:
	Total payoff:
		53.08 ± 1.122
	Mean payoff:
		0.1805 ± 0.0038
	Total number of recruited nodes:
		294 ± 0

Results at turn 295:
	Total payoff:
		53.08 ± 1.122
	Mean payoff:
		0.1799 ± 0.0038
	Total number of recruited nodes:
		295 ± 0

Results at turn 296:
	Total payoff:
		53.1 ± 1.1294
	Mean payoff:
		0.1794 ± 0.0038
	Total number of recruited nodes:
		296 ± 0

Results at turn 297:
	Total payoff:
		53.12 ± 1.1183
	Mean payoff:
		0.1789 ± 0.0038
	Total number of recruited nodes:
		297 ± 0

Results at turn 298:
	Total payoff:
		53.14 ± 1.1068
	Mean payoff:
		0.1783 ± 0.0037
	Total number of recruited nodes:
		298 ± 0

Results at turn 299:
	Total payoff:
		53.2 ± 1.088
	Mean payoff:
		0.1779 ± 0.0036
	Total number of recruited nodes:
		299 ± 0

Results at turn 300:
	Total payoff:
		53.24 ± 1.0797
	Mean payoff:
		0.1775 ± 0.0036
	Total number of recruited nodes:
		300 ± 0


