Results at turn 1:
	Total payoff:
		1 ± 0
	Mean payoff:
		1 ± 0
	Total number of recruited nodes:
		1 ± 0

Results at turn 2:
	Total payoff:
		1.4107 ± 0.4964
	Mean payoff:
		0.7054 ± 0.2482
	Total number of recruited nodes:
		2 ± 0

Results at turn 3:
	Total payoff:
		1.875 ± 0.7877
	Mean payoff:
		0.625 ± 0.2626
	Total number of recruited nodes:
		3 ± 0

Results at turn 4:
	Total payoff:
		2.4107 ± 1.1083
	Mean payoff:
		0.6027 ± 0.2771
	Total number of recruited nodes:
		4 ± 0

Results at turn 5:
	Total payoff:
		3 ± 1.3073
	Mean payoff:
		0.6 ± 0.2615
	Total number of recruited nodes:
		5 ± 0

Results at turn 6:
	Total payoff:
		3.6429 ± 1.4946
	Mean payoff:
		0.6071 ± 0.2491
	Total number of recruited nodes:
		6 ± 0

Results at turn 7:
	Total payoff:
		4.2321 ± 1.6292
	Mean payoff:
		0.6046 ± 0.2327
	Total number of recruited nodes:
		7 ± 0

Results at turn 8:
	Total payoff:
		4.875 ± 1.8297
	Mean payoff:
		0.6094 ± 0.2287
	Total number of recruited nodes:
		8 ± 0

Results at turn 9:
	Total payoff:
		5.6607 ± 1.9751
	Mean payoff:
		0.629 ± 0.2195
	Total number of recruited nodes:
		9 ± 0

Results at turn 10:
	Total payoff:
		6.4464 ± 2.2068
	Mean payoff:
		0.6446 ± 0.2207
	Total number of recruited nodes:
		10 ± 0

Results at turn 11:
	Total payoff:
		7.1607 ± 2.4404
	Mean payoff:
		0.651 ± 0.2219
	Total number of recruited nodes:
		11 ± 0

Results at turn 12:
	Total payoff:
		7.7679 ± 2.6215
	Mean payoff:
		0.6473 ± 0.2185
	Total number of recruited nodes:
		12 ± 0

Results at turn 13:
	Total payoff:
		8.3929 ± 2.8199
	Mean payoff:
		0.6456 ± 0.2169
	Total number of recruited nodes:
		13 ± 0

Results at turn 14:
	Total payoff:
		9.0179 ± 2.9817
	Mean payoff:
		0.6441 ± 0.213
	Total number of recruited nodes:
		14 ± 0

Results at turn 15:
	Total payoff:
		9.8036 ± 3.1647
	Mean payoff:
		0.6536 ± 0.211
	Total number of recruited nodes:
		15 ± 0

Results at turn 16:
	Total payoff:
		10.5179 ± 3.2529
	Mean payoff:
		0.6574 ± 0.2033
	Total number of recruited nodes:
		16 ± 0

Results at turn 17:
	Total payoff:
		11.1964 ± 3.3271
	Mean payoff:
		0.6586 ± 0.1957
	Total number of recruited nodes:
		17 ± 0

Results at turn 18:
	Total payoff:
		11.9821 ± 3.371
	Mean payoff:
		0.6657 ± 0.1873
	Total number of recruited nodes:
		18 ± 0

Results at turn 19:
	Total payoff:
		12.6607 ± 3.476
	Mean payoff:
		0.6664 ± 0.1829
	Total number of recruited nodes:
		19 ± 0

Results at turn 20:
	Total payoff:
		13.3929 ± 3.6416
	Mean payoff:
		0.6696 ± 0.1821
	Total number of recruited nodes:
		20 ± 0

Results at turn 21:
	Total payoff:
		14.0179 ± 3.7001
	Mean payoff:
		0.6675 ± 0.1762
	Total number of recruited nodes:
		21 ± 0

Results at turn 22:
	Total payoff:
		14.0893 ± 3.7186
	Mean payoff:
		0.6404 ± 0.169
	Total number of recruited nodes:
		22 ± 0

Results at turn 23:
	Total payoff:
		14.1607 ± 3.7889
	Mean payoff:
		0.6157 ± 0.1647
	Total number of recruited nodes:
		23 ± 0

Results at turn 24:
	Total payoff:
		14.25 ± 3.8388
	Mean payoff:
		0.5938 ± 0.1599
	Total number of recruited nodes:
		24 ± 0

Results at turn 25:
	Total payoff:
		14.2857 ± 3.8693
	Mean payoff:
		0.5714 ± 0.1548
	Total number of recruited nodes:
		25 ± 0

Results at turn 26:
	Total payoff:
		14.3036 ± 3.8937
	Mean payoff:
		0.5501 ± 0.1498
	Total number of recruited nodes:
		26 ± 0

Results at turn 27:
	Total payoff:
		14.375 ± 3.8733
	Mean payoff:
		0.5324 ± 0.1435
	Total number of recruited nodes:
		27 ± 0

Results at turn 28:
	Total payoff:
		14.5 ± 3.9358
	Mean payoff:
		0.5179 ± 0.1406
	Total number of recruited nodes:
		28 ± 0

Results at turn 29:
	Total payoff:
		14.6607 ± 3.951
	Mean payoff:
		0.5055 ± 0.1362
	Total number of recruited nodes:
		29 ± 0

Results at turn 30:
	Total payoff:
		14.8214 ± 3.9731
	Mean payoff:
		0.494 ± 0.1324
	Total number of recruited nodes:
		30 ± 0

Results at turn 31:
	Total payoff:
		14.9107 ± 3.9371
	Mean payoff:
		0.481 ± 0.127
	Total number of recruited nodes:
		31 ± 0

Results at turn 32:
	Total payoff:
		15 ± 4
	Mean payoff:
		0.4688 ± 0.125
	Total number of recruited nodes:
		32 ± 0

Results at turn 33:
	Total payoff:
		15.2143 ± 3.9987
	Mean payoff:
		0.461 ± 0.1212
	Total number of recruited nodes:
		33 ± 0

Results at turn 34:
	Total payoff:
		15.3929 ± 4.0572
	Mean payoff:
		0.4527 ± 0.1193
	Total number of recruited nodes:
		34 ± 0

Results at turn 35:
	Total payoff:
		15.4643 ± 4.036
	Mean payoff:
		0.4418 ± 0.1153
	Total number of recruited nodes:
		35 ± 0

Results at turn 36:
	Total payoff:
		15.6429 ± 4.0291
	Mean payoff:
		0.4345 ± 0.1119
	Total number of recruited nodes:
		36 ± 0

Results at turn 37:
	Total payoff:
		15.8214 ± 4.077
	Mean payoff:
		0.4276 ± 0.1102
	Total number of recruited nodes:
		37 ± 0

Results at turn 38:
	Total payoff:
		15.9107 ± 4.0688
	Mean payoff:
		0.4187 ± 0.1071
	Total number of recruited nodes:
		38 ± 0

Results at turn 39:
	Total payoff:
		16.0893 ± 4.1397
	Mean payoff:
		0.4125 ± 0.1061
	Total number of recruited nodes:
		39 ± 0

Results at turn 40:
	Total payoff:
		16.1964 ± 4.2057
	Mean payoff:
		0.4049 ± 0.1051
	Total number of recruited nodes:
		40 ± 0

Results at turn 41:
	Total payoff:
		16.3214 ± 4.2602
	Mean payoff:
		0.3981 ± 0.1039
	Total number of recruited nodes:
		41 ± 0

Results at turn 42:
	Total payoff:
		16.375 ± 4.2279
	Mean payoff:
		0.3899 ± 0.1007
	Total number of recruited nodes:
		42 ± 0

Results at turn 43:
	Total payoff:
		16.4464 ± 4.1513
	Mean payoff:
		0.3825 ± 0.0965
	Total number of recruited nodes:
		43 ± 0

Results at turn 44:
	Total payoff:
		16.5536 ± 4.1601
	Mean payoff:
		0.3762 ± 0.0945
	Total number of recruited nodes:
		44 ± 0

Results at turn 45:
	Total payoff:
		16.6429 ± 4.1797
	Mean payoff:
		0.3698 ± 0.0929
	Total number of recruited nodes:
		45 ± 0

Results at turn 46:
	Total payoff:
		16.6964 ± 4.2165
	Mean payoff:
		0.363 ± 0.0917
	Total number of recruited nodes:
		46 ± 0

Results at turn 47:
	Total payoff:
		16.7679 ± 4.2426
	Mean payoff:
		0.3568 ± 0.0903
	Total number of recruited nodes:
		47 ± 0

Results at turn 48:
	Total payoff:
		16.875 ± 4.2983
	Mean payoff:
		0.3516 ± 0.0895
	Total number of recruited nodes:
		48 ± 0

Results at turn 49:
	Total payoff:
		17 ± 4.3401
	Mean payoff:
		0.3469 ± 0.0886
	Total number of recruited nodes:
		49 ± 0

Results at turn 50:
	Total payoff:
		17.1071 ± 4.397
	Mean payoff:
		0.3421 ± 0.0879
	Total number of recruited nodes:
		50 ± 0

Results at turn 51:
	Total payoff:
		17.2143 ± 4.4342
	Mean payoff:
		0.3375 ± 0.0869
	Total number of recruited nodes:
		51 ± 0

Results at turn 52:
	Total payoff:
		17.3929 ± 4.4952
	Mean payoff:
		0.3345 ± 0.0864
	Total number of recruited nodes:
		52 ± 0

Results at turn 53:
	Total payoff:
		17.5536 ± 4.5324
	Mean payoff:
		0.3312 ± 0.0855
	Total number of recruited nodes:
		53 ± 0

Results at turn 54:
	Total payoff:
		17.7143 ± 4.5596
	Mean payoff:
		0.328 ± 0.0844
	Total number of recruited nodes:
		54 ± 0

Results at turn 55:
	Total payoff:
		17.8929 ± 4.5793
	Mean payoff:
		0.3253 ± 0.0833
	Total number of recruited nodes:
		55 ± 0

Results at turn 56:
	Total payoff:
		18.0714 ± 4.6196
	Mean payoff:
		0.3227 ± 0.0825
	Total number of recruited nodes:
		56 ± 0

Results at turn 57:
	Total payoff:
		18.2679 ± 4.5146
	Mean payoff:
		0.3205 ± 0.0792
	Total number of recruited nodes:
		57 ± 0

Results at turn 58:
	Total payoff:
		18.4286 ± 4.4797
	Mean payoff:
		0.3177 ± 0.0772
	Total number of recruited nodes:
		58 ± 0

Results at turn 59:
	Total payoff:
		18.5536 ± 4.488
	Mean payoff:
		0.3145 ± 0.0761
	Total number of recruited nodes:
		59 ± 0

Results at turn 60:
	Total payoff:
		18.6607 ± 4.4406
	Mean payoff:
		0.311 ± 0.074
	Total number of recruited nodes:
		60 ± 0

Results at turn 61:
	Total payoff:
		18.8036 ± 4.4616
	Mean payoff:
		0.3083 ± 0.0731
	Total number of recruited nodes:
		61 ± 0

Results at turn 62:
	Total payoff:
		18.875 ± 4.4192
	Mean payoff:
		0.3044 ± 0.0713
	Total number of recruited nodes:
		62 ± 0

Results at turn 63:
	Total payoff:
		19.0357 ± 4.3899
	Mean payoff:
		0.3022 ± 0.0697
	Total number of recruited nodes:
		63 ± 0

Results at turn 64:
	Total payoff:
		19.2321 ± 4.4517
	Mean payoff:
		0.3005 ± 0.0696
	Total number of recruited nodes:
		64 ± 0

Results at turn 65:
	Total payoff:
		19.4286 ± 4.4716
	Mean payoff:
		0.2989 ± 0.0688
	Total number of recruited nodes:
		65 ± 0

Results at turn 66:
	Total payoff:
		19.625 ± 4.4704
	Mean payoff:
		0.2973 ± 0.0677
	Total number of recruited nodes:
		66 ± 0

Results at turn 67:
	Total payoff:
		19.8393 ± 4.4672
	Mean payoff:
		0.2961 ± 0.0667
	Total number of recruited nodes:
		67 ± 0

Results at turn 68:
	Total payoff:
		20.0179 ± 4.4128
	Mean payoff:
		0.2944 ± 0.0649
	Total number of recruited nodes:
		68 ± 0

Results at turn 69:
	Total payoff:
		20.125 ± 4.4561
	Mean payoff:
		0.2917 ± 0.0646
	Total number of recruited nodes:
		69 ± 0

Results at turn 70:
	Total payoff:
		20.2857 ± 4.5155
	Mean payoff:
		0.2898 ± 0.0645
	Total number of recruited nodes:
		70 ± 0

Results at turn 71:
	Total payoff:
		20.5179 ± 4.5367
	Mean payoff:
		0.289 ± 0.0639
	Total number of recruited nodes:
		71 ± 0

Results at turn 72:
	Total payoff:
		20.6429 ± 4.5823
	Mean payoff:
		0.2867 ± 0.0636
	Total number of recruited nodes:
		72 ± 0

Results at turn 73:
	Total payoff:
		20.7857 ± 4.6111
	Mean payoff:
		0.2847 ± 0.0632
	Total number of recruited nodes:
		73 ± 0

Results at turn 74:
	Total payoff:
		20.8929 ± 4.6775
	Mean payoff:
		0.2823 ± 0.0632
	Total number of recruited nodes:
		74 ± 0

Results at turn 75:
	Total payoff:
		21 ± 4.7059
	Mean payoff:
		0.28 ± 0.0627
	Total number of recruited nodes:
		75 ± 0

Results at turn 76:
	Total payoff:
		21.1607 ± 4.7779
	Mean payoff:
		0.2784 ± 0.0629
	Total number of recruited nodes:
		76 ± 0

Results at turn 77:
	Total payoff:
		21.25 ± 4.8176
	Mean payoff:
		0.276 ± 0.0626
	Total number of recruited nodes:
		77 ± 0

Results at turn 78:
	Total payoff:
		21.3393 ± 4.8252
	Mean payoff:
		0.2736 ± 0.0619
	Total number of recruited nodes:
		78 ± 0

Results at turn 79:
	Total payoff:
		21.3929 ± 4.7775
	Mean payoff:
		0.2708 ± 0.0605
	Total number of recruited nodes:
		79 ± 0

Results at turn 80:
	Total payoff:
		21.4286 ± 4.7323
	Mean payoff:
		0.2679 ± 0.0592
	Total number of recruited nodes:
		80 ± 0

Results at turn 81:
	Total payoff:
		21.4643 ± 4.7058
	Mean payoff:
		0.265 ± 0.0581
	Total number of recruited nodes:
		81 ± 0

Results at turn 82:
	Total payoff:
		21.5 ± 4.6788
	Mean payoff:
		0.2622 ± 0.0571
	Total number of recruited nodes:
		82 ± 0

Results at turn 83:
	Total payoff:
		21.5536 ± 4.694
	Mean payoff:
		0.2597 ± 0.0566
	Total number of recruited nodes:
		83 ± 0

Results at turn 84:
	Total payoff:
		21.625 ± 4.7196
	Mean payoff:
		0.2574 ± 0.0562
	Total number of recruited nodes:
		84 ± 0

Results at turn 85:
	Total payoff:
		21.6786 ± 4.7256
	Mean payoff:
		0.255 ± 0.0556
	Total number of recruited nodes:
		85 ± 0

Results at turn 86:
	Total payoff:
		21.7679 ± 4.7749
	Mean payoff:
		0.2531 ± 0.0555
	Total number of recruited nodes:
		86 ± 0

Results at turn 87:
	Total payoff:
		21.875 ± 4.7523
	Mean payoff:
		0.2514 ± 0.0546
	Total number of recruited nodes:
		87 ± 0

Results at turn 88:
	Total payoff:
		21.9643 ± 4.7824
	Mean payoff:
		0.2496 ± 0.0543
	Total number of recruited nodes:
		88 ± 0

Results at turn 89:
	Total payoff:
		22.0536 ± 4.8107
	Mean payoff:
		0.2478 ± 0.0541
	Total number of recruited nodes:
		89 ± 0

Results at turn 90:
	Total payoff:
		22.0893 ± 4.7607
	Mean payoff:
		0.2454 ± 0.0529
	Total number of recruited nodes:
		90 ± 0

Results at turn 91:
	Total payoff:
		22.125 ± 4.779
	Mean payoff:
		0.2431 ± 0.0525
	Total number of recruited nodes:
		91 ± 0

Results at turn 92:
	Total payoff:
		22.2143 ± 4.7929
	Mean payoff:
		0.2415 ± 0.0521
	Total number of recruited nodes:
		92 ± 0

Results at turn 93:
	Total payoff:
		22.3036 ± 4.8126
	Mean payoff:
		0.2398 ± 0.0517
	Total number of recruited nodes:
		93 ± 0

Results at turn 94:
	Total payoff:
		22.3214 ± 4.8133
	Mean payoff:
		0.2375 ± 0.0512
	Total number of recruited nodes:
		94 ± 0

Results at turn 95:
	Total payoff:
		22.4107 ± 4.8422
	Mean payoff:
		0.2359 ± 0.051
	Total number of recruited nodes:
		95 ± 0

Results at turn 96:
	Total payoff:
		22.4286 ± 4.8387
	Mean payoff:
		0.2336 ± 0.0504
	Total number of recruited nodes:
		96 ± 0

Results at turn 97:
	Total payoff:
		22.4464 ± 4.8539
	Mean payoff:
		0.2314 ± 0.05
	Total number of recruited nodes:
		97 ± 0

Results at turn 98:
	Total payoff:
		22.5179 ± 4.8729
	Mean payoff:
		0.2298 ± 0.0497
	Total number of recruited nodes:
		98 ± 0

Results at turn 99:
	Total payoff:
		22.5536 ± 4.9172
	Mean payoff:
		0.2278 ± 0.0497
	Total number of recruited nodes:
		99 ± 0

Results at turn 100:
	Total payoff:
		22.6071 ± 4.9237
	Mean payoff:
		0.2261 ± 0.0492
	Total number of recruited nodes:
		100 ± 0

Results at turn 101:
	Total payoff:
		22.6964 ± 4.9614
	Mean payoff:
		0.2247 ± 0.0491
	Total number of recruited nodes:
		101 ± 0

Results at turn 102:
	Total payoff:
		22.7321 ± 4.9818
	Mean payoff:
		0.2229 ± 0.0488
	Total number of recruited nodes:
		102 ± 0

Results at turn 103:
	Total payoff:
		22.7679 ± 5.0235
	Mean payoff:
		0.221 ± 0.0488
	Total number of recruited nodes:
		103 ± 0

Results at turn 104:
	Total payoff:
		22.8393 ± 4.9755
	Mean payoff:
		0.2196 ± 0.0478
	Total number of recruited nodes:
		104 ± 0

Results at turn 105:
	Total payoff:
		22.875 ± 5.0238
	Mean payoff:
		0.2179 ± 0.0478
	Total number of recruited nodes:
		105 ± 0

Results at turn 106:
	Total payoff:
		22.9107 ± 5.0426
	Mean payoff:
		0.2161 ± 0.0476
	Total number of recruited nodes:
		106 ± 0

Results at turn 107:
	Total payoff:
		22.9821 ± 5.0829
	Mean payoff:
		0.2148 ± 0.0475
	Total number of recruited nodes:
		107 ± 0

Results at turn 108:
	Total payoff:
		23.0714 ± 5.1411
	Mean payoff:
		0.2136 ± 0.0476
	Total number of recruited nodes:
		108 ± 0

Results at turn 109:
	Total payoff:
		23.1607 ± 5.1125
	Mean payoff:
		0.2125 ± 0.0469
	Total number of recruited nodes:
		109 ± 0

Results at turn 110:
	Total payoff:
		23.2679 ± 5.0362
	Mean payoff:
		0.2115 ± 0.0458
	Total number of recruited nodes:
		110 ± 0

Results at turn 111:
	Total payoff:
		23.3214 ± 5.06
	Mean payoff:
		0.2101 ± 0.0456
	Total number of recruited nodes:
		111 ± 0

Results at turn 112:
	Total payoff:
		23.3214 ± 5.06
	Mean payoff:
		0.2082 ± 0.0452
	Total number of recruited nodes:
		112 ± 0

Results at turn 113:
	Total payoff:
		23.3571 ± 5.0863
	Mean payoff:
		0.2067 ± 0.045
	Total number of recruited nodes:
		113 ± 0

Results at turn 114:
	Total payoff:
		23.3929 ± 5.1228
	Mean payoff:
		0.2052 ± 0.0449
	Total number of recruited nodes:
		114 ± 0

Results at turn 115:
	Total payoff:
		23.4464 ± 5.159
	Mean payoff:
		0.2039 ± 0.0449
	Total number of recruited nodes:
		115 ± 0

Results at turn 116:
	Total payoff:
		23.4821 ± 5.2014
	Mean payoff:
		0.2024 ± 0.0448
	Total number of recruited nodes:
		116 ± 0

Results at turn 117:
	Total payoff:
		23.5357 ± 5.2257
	Mean payoff:
		0.2012 ± 0.0447
	Total number of recruited nodes:
		117 ± 0

Results at turn 118:
	Total payoff:
		23.5357 ± 5.2257
	Mean payoff:
		0.1995 ± 0.0443
	Total number of recruited nodes:
		118 ± 0

Results at turn 119:
	Total payoff:
		23.6429 ± 5.3204
	Mean payoff:
		0.1987 ± 0.0447
	Total number of recruited nodes:
		119 ± 0

Results at turn 120:
	Total payoff:
		23.6786 ± 5.3329
	Mean payoff:
		0.1973 ± 0.0444
	Total number of recruited nodes:
		120 ± 0

Results at turn 121:
	Total payoff:
		23.75 ± 5.4213
	Mean payoff:
		0.1963 ± 0.0448
	Total number of recruited nodes:
		121 ± 0

Results at turn 122:
	Total payoff:
		23.7857 ± 5.369
	Mean payoff:
		0.195 ± 0.044
	Total number of recruited nodes:
		122 ± 0

Results at turn 123:
	Total payoff:
		23.8393 ± 5.4398
	Mean payoff:
		0.1938 ± 0.0442
	Total number of recruited nodes:
		123 ± 0

Results at turn 124:
	Total payoff:
		23.8929 ± 5.4861
	Mean payoff:
		0.1927 ± 0.0442
	Total number of recruited nodes:
		124 ± 0

Results at turn 125:
	Total payoff:
		23.9643 ± 5.5431
	Mean payoff:
		0.1917 ± 0.0443
	Total number of recruited nodes:
		125 ± 0

Results at turn 126:
	Total payoff:
		24.0536 ± 5.6453
	Mean payoff:
		0.1909 ± 0.0448
	Total number of recruited nodes:
		126 ± 0

Results at turn 127:
	Total payoff:
		24.1071 ± 5.6975
	Mean payoff:
		0.1898 ± 0.0449
	Total number of recruited nodes:
		127 ± 0

Results at turn 128:
	Total payoff:
		24.1607 ± 5.7612
	Mean payoff:
		0.1888 ± 0.045
	Total number of recruited nodes:
		128 ± 0

Results at turn 129:
	Total payoff:
		24.1964 ± 5.8073
	Mean payoff:
		0.1876 ± 0.045
	Total number of recruited nodes:
		129 ± 0

Results at turn 130:
	Total payoff:
		24.25 ± 5.8504
	Mean payoff:
		0.1865 ± 0.045
	Total number of recruited nodes:
		130 ± 0

Results at turn 131:
	Total payoff:
		24.3214 ± 5.9087
	Mean payoff:
		0.1857 ± 0.0451
	Total number of recruited nodes:
		131 ± 0

Results at turn 132:
	Total payoff:
		24.375 ± 6.0017
	Mean payoff:
		0.1847 ± 0.0455
	Total number of recruited nodes:
		132 ± 0

Results at turn 133:
	Total payoff:
		24.375 ± 6.0017
	Mean payoff:
		0.1833 ± 0.0451
	Total number of recruited nodes:
		133 ± 0

Results at turn 134:
	Total payoff:
		24.375 ± 6.0017
	Mean payoff:
		0.1819 ± 0.0448
	Total number of recruited nodes:
		134 ± 0

Results at turn 135:
	Total payoff:
		24.4464 ± 6.075
	Mean payoff:
		0.1811 ± 0.045
	Total number of recruited nodes:
		135 ± 0

Results at turn 136:
	Total payoff:
		24.5 ± 6.1289
	Mean payoff:
		0.1801 ± 0.0451
	Total number of recruited nodes:
		136 ± 0

Results at turn 137:
	Total payoff:
		24.5357 ± 6.1555
	Mean payoff:
		0.1791 ± 0.0449
	Total number of recruited nodes:
		137 ± 0

Results at turn 138:
	Total payoff:
		24.6071 ± 6.2919
	Mean payoff:
		0.1783 ± 0.0456
	Total number of recruited nodes:
		138 ± 0

Results at turn 139:
	Total payoff:
		24.6607 ± 6.3883
	Mean payoff:
		0.1774 ± 0.046
	Total number of recruited nodes:
		139 ± 0

Results at turn 140:
	Total payoff:
		24.6786 ± 6.4247
	Mean payoff:
		0.1763 ± 0.0459
	Total number of recruited nodes:
		140 ± 0

Results at turn 141:
	Total payoff:
		24.7143 ± 6.4743
	Mean payoff:
		0.1753 ± 0.0459
	Total number of recruited nodes:
		141 ± 0

Results at turn 142:
	Total payoff:
		24.7321 ± 6.4314
	Mean payoff:
		0.1742 ± 0.0453
	Total number of recruited nodes:
		142 ± 0

Results at turn 143:
	Total payoff:
		24.7679 ± 6.4723
	Mean payoff:
		0.1732 ± 0.0453
	Total number of recruited nodes:
		143 ± 0

Results at turn 144:
	Total payoff:
		24.8036 ± 6.5127
	Mean payoff:
		0.1722 ± 0.0452
	Total number of recruited nodes:
		144 ± 0

Results at turn 145:
	Total payoff:
		24.8214 ± 6.548
	Mean payoff:
		0.1712 ± 0.0452
	Total number of recruited nodes:
		145 ± 0

Results at turn 146:
	Total payoff:
		24.8393 ± 6.5886
	Mean payoff:
		0.1701 ± 0.0451
	Total number of recruited nodes:
		146 ± 0

Results at turn 147:
	Total payoff:
		24.8571 ± 6.6262
	Mean payoff:
		0.1691 ± 0.0451
	Total number of recruited nodes:
		147 ± 0

Results at turn 148:
	Total payoff:
		24.9107 ± 6.6532
	Mean payoff:
		0.1683 ± 0.045
	Total number of recruited nodes:
		148 ± 0

Results at turn 149:
	Total payoff:
		24.9643 ± 6.7257
	Mean payoff:
		0.1675 ± 0.0451
	Total number of recruited nodes:
		149 ± 0

Results at turn 150:
	Total payoff:
		24.9643 ± 6.7257
	Mean payoff:
		0.1664 ± 0.0448
	Total number of recruited nodes:
		150 ± 0

Results at turn 151:
	Total payoff:
		24.9643 ± 6.7257
	Mean payoff:
		0.1653 ± 0.0445
	Total number of recruited nodes:
		151 ± 0

Results at turn 152:
	Total payoff:
		24.9821 ± 6.7675
	Mean payoff:
		0.1644 ± 0.0445
	Total number of recruited nodes:
		152 ± 0

Results at turn 153:
	Total payoff:
		25.0179 ± 6.8291
	Mean payoff:
		0.1635 ± 0.0446
	Total number of recruited nodes:
		153 ± 0

Results at turn 154:
	Total payoff:
		25.0536 ± 6.8422
	Mean payoff:
		0.1627 ± 0.0444
	Total number of recruited nodes:
		154 ± 0

Results at turn 155:
	Total payoff:
		25.0536 ± 6.8422
	Mean payoff:
		0.1616 ± 0.0441
	Total number of recruited nodes:
		155 ± 0

Results at turn 156:
	Total payoff:
		25.0714 ± 6.8804
	Mean payoff:
		0.1607 ± 0.0441
	Total number of recruited nodes:
		156 ± 0

Results at turn 157:
	Total payoff:
		25.0893 ± 6.9237
	Mean payoff:
		0.1598 ± 0.0441
	Total number of recruited nodes:
		157 ± 0

Results at turn 158:
	Total payoff:
		25.1607 ± 6.9407
	Mean payoff:
		0.1592 ± 0.0439
	Total number of recruited nodes:
		158 ± 0

Results at turn 159:
	Total payoff:
		25.2679 ± 6.9452
	Mean payoff:
		0.1589 ± 0.0437
	Total number of recruited nodes:
		159 ± 0

Results at turn 160:
	Total payoff:
		25.3929 ± 6.9483
	Mean payoff:
		0.1587 ± 0.0434
	Total number of recruited nodes:
		160 ± 0

Results at turn 161:
	Total payoff:
		25.4286 ± 6.9436
	Mean payoff:
		0.1579 ± 0.0431
	Total number of recruited nodes:
		161 ± 0

Results at turn 162:
	Total payoff:
		25.5536 ± 6.9985
	Mean payoff:
		0.1577 ± 0.0432
	Total number of recruited nodes:
		162 ± 0

Results at turn 163:
	Total payoff:
		25.6071 ± 6.9979
	Mean payoff:
		0.1571 ± 0.0429
	Total number of recruited nodes:
		163 ± 0

Results at turn 164:
	Total payoff:
		25.7679 ± 7.0375
	Mean payoff:
		0.1571 ± 0.0429
	Total number of recruited nodes:
		164 ± 0

Results at turn 165:
	Total payoff:
		25.9643 ± 6.9986
	Mean payoff:
		0.1574 ± 0.0424
	Total number of recruited nodes:
		165 ± 0

Results at turn 166:
	Total payoff:
		26.1071 ± 6.9797
	Mean payoff:
		0.1573 ± 0.042
	Total number of recruited nodes:
		166 ± 0

Results at turn 167:
	Total payoff:
		26.2679 ± 6.8927
	Mean payoff:
		0.1573 ± 0.0413
	Total number of recruited nodes:
		167 ± 0

Results at turn 168:
	Total payoff:
		26.375 ± 6.8849
	Mean payoff:
		0.157 ± 0.041
	Total number of recruited nodes:
		168 ± 0

Results at turn 169:
	Total payoff:
		26.5 ± 6.8517
	Mean payoff:
		0.1568 ± 0.0405
	Total number of recruited nodes:
		169 ± 0

Results at turn 170:
	Total payoff:
		26.6607 ± 6.8551
	Mean payoff:
		0.1568 ± 0.0403
	Total number of recruited nodes:
		170 ± 0

Results at turn 171:
	Total payoff:
		26.8214 ± 6.9337
	Mean payoff:
		0.1569 ± 0.0405
	Total number of recruited nodes:
		171 ± 0

Results at turn 172:
	Total payoff:
		27.125 ± 6.9047
	Mean payoff:
		0.1577 ± 0.0401
	Total number of recruited nodes:
		172 ± 0

Results at turn 173:
	Total payoff:
		27.25 ± 6.9472
	Mean payoff:
		0.1575 ± 0.0402
	Total number of recruited nodes:
		173 ± 0

Results at turn 174:
	Total payoff:
		27.4821 ± 6.9518
	Mean payoff:
		0.1579 ± 0.04
	Total number of recruited nodes:
		174 ± 0

Results at turn 175:
	Total payoff:
		27.7679 ± 6.894
	Mean payoff:
		0.1587 ± 0.0394
	Total number of recruited nodes:
		175 ± 0

Results at turn 176:
	Total payoff:
		28.0536 ± 6.8634
	Mean payoff:
		0.1594 ± 0.039
	Total number of recruited nodes:
		176 ± 0

Results at turn 177:
	Total payoff:
		28.2679 ± 6.789
	Mean payoff:
		0.1597 ± 0.0384
	Total number of recruited nodes:
		177 ± 0

Results at turn 178:
	Total payoff:
		28.5357 ± 6.6905
	Mean payoff:
		0.1603 ± 0.0376
	Total number of recruited nodes:
		178 ± 0

Results at turn 179:
	Total payoff:
		28.6607 ± 6.6504
	Mean payoff:
		0.1601 ± 0.0372
	Total number of recruited nodes:
		179 ± 0

Results at turn 180:
	Total payoff:
		28.9286 ± 6.6109
	Mean payoff:
		0.1607 ± 0.0367
	Total number of recruited nodes:
		180 ± 0

Results at turn 181:
	Total payoff:
		29.1786 ± 6.467
	Mean payoff:
		0.1612 ± 0.0357
	Total number of recruited nodes:
		181 ± 0

Results at turn 182:
	Total payoff:
		29.3571 ± 6.4397
	Mean payoff:
		0.1613 ± 0.0354
	Total number of recruited nodes:
		182 ± 0

Results at turn 183:
	Total payoff:
		29.6964 ± 6.3587
	Mean payoff:
		0.1623 ± 0.0347
	Total number of recruited nodes:
		183 ± 0

Results at turn 184:
	Total payoff:
		29.9464 ± 6.2767
	Mean payoff:
		0.1628 ± 0.0341
	Total number of recruited nodes:
		184 ± 0

Results at turn 185:
	Total payoff:
		30.2321 ± 6.2522
	Mean payoff:
		0.1634 ± 0.0338
	Total number of recruited nodes:
		185 ± 0

Results at turn 186:
	Total payoff:
		30.4107 ± 6.146
	Mean payoff:
		0.1635 ± 0.033
	Total number of recruited nodes:
		186 ± 0

Results at turn 187:
	Total payoff:
		30.5714 ± 6.1047
	Mean payoff:
		0.1635 ± 0.0326
	Total number of recruited nodes:
		187 ± 0

Results at turn 188:
	Total payoff:
		30.7679 ± 6.0242
	Mean payoff:
		0.1637 ± 0.032
	Total number of recruited nodes:
		188 ± 0

Results at turn 189:
	Total payoff:
		31.0357 ± 5.939
	Mean payoff:
		0.1642 ± 0.0314
	Total number of recruited nodes:
		189 ± 0

Results at turn 190:
	Total payoff:
		31.4821 ± 5.8465
	Mean payoff:
		0.1657 ± 0.0308
	Total number of recruited nodes:
		190 ± 0

Results at turn 191:
	Total payoff:
		31.8036 ± 5.8104
	Mean payoff:
		0.1665 ± 0.0304
	Total number of recruited nodes:
		191 ± 0

Results at turn 192:
	Total payoff:
		32.1071 ± 5.7672
	Mean payoff:
		0.1672 ± 0.03
	Total number of recruited nodes:
		192 ± 0

Results at turn 193:
	Total payoff:
		32.3929 ± 5.7134
	Mean payoff:
		0.1678 ± 0.0296
	Total number of recruited nodes:
		193 ± 0

Results at turn 194:
	Total payoff:
		32.8214 ± 5.5436
	Mean payoff:
		0.1692 ± 0.0286
	Total number of recruited nodes:
		194 ± 0

Results at turn 195:
	Total payoff:
		33.0714 ± 5.5493
	Mean payoff:
		0.1696 ± 0.0285
	Total number of recruited nodes:
		195 ± 0

Results at turn 196:
	Total payoff:
		33.4643 ± 5.4937
	Mean payoff:
		0.1707 ± 0.028
	Total number of recruited nodes:
		196 ± 0

Results at turn 197:
	Total payoff:
		33.75 ± 5.4548
	Mean payoff:
		0.1713 ± 0.0277
	Total number of recruited nodes:
		197 ± 0

Results at turn 198:
	Total payoff:
		34.1607 ± 5.3045
	Mean payoff:
		0.1725 ± 0.0268
	Total number of recruited nodes:
		198 ± 0

Results at turn 199:
	Total payoff:
		34.4643 ± 5.2083
	Mean payoff:
		0.1732 ± 0.0262
	Total number of recruited nodes:
		199 ± 0

Results at turn 200:
	Total payoff:
		34.7857 ± 5.1371
	Mean payoff:
		0.1739 ± 0.0257
	Total number of recruited nodes:
		200 ± 0

Results at turn 201:
	Total payoff:
		35.0357 ± 5.0053
	Mean payoff:
		0.1743 ± 0.0249
	Total number of recruited nodes:
		201 ± 0

Results at turn 202:
	Total payoff:
		35.375 ± 4.9417
	Mean payoff:
		0.1751 ± 0.0245
	Total number of recruited nodes:
		202 ± 0

Results at turn 203:
	Total payoff:
		35.6607 ± 4.7722
	Mean payoff:
		0.1757 ± 0.0235
	Total number of recruited nodes:
		203 ± 0

Results at turn 204:
	Total payoff:
		35.9286 ± 4.686
	Mean payoff:
		0.1761 ± 0.023
	Total number of recruited nodes:
		204 ± 0

Results at turn 205:
	Total payoff:
		36.1786 ± 4.6675
	Mean payoff:
		0.1765 ± 0.0228
	Total number of recruited nodes:
		205 ± 0

Results at turn 206:
	Total payoff:
		36.4286 ± 4.6743
	Mean payoff:
		0.1768 ± 0.0227
	Total number of recruited nodes:
		206 ± 0

Results at turn 207:
	Total payoff:
		36.7679 ± 4.6593
	Mean payoff:
		0.1776 ± 0.0225
	Total number of recruited nodes:
		207 ± 0

Results at turn 208:
	Total payoff:
		37.0357 ± 4.6825
	Mean payoff:
		0.1781 ± 0.0225
	Total number of recruited nodes:
		208 ± 0

Results at turn 209:
	Total payoff:
		37.2857 ± 4.619
	Mean payoff:
		0.1784 ± 0.0221
	Total number of recruited nodes:
		209 ± 0

Results at turn 210:
	Total payoff:
		37.5 ± 4.5687
	Mean payoff:
		0.1786 ± 0.0218
	Total number of recruited nodes:
		210 ± 0

Results at turn 211:
	Total payoff:
		37.75 ± 4.4405
	Mean payoff:
		0.1789 ± 0.021
	Total number of recruited nodes:
		211 ± 0

Results at turn 212:
	Total payoff:
		37.9286 ± 4.3936
	Mean payoff:
		0.1789 ± 0.0207
	Total number of recruited nodes:
		212 ± 0

Results at turn 213:
	Total payoff:
		38.2679 ± 4.3714
	Mean payoff:
		0.1797 ± 0.0205
	Total number of recruited nodes:
		213 ± 0

Results at turn 214:
	Total payoff:
		38.5357 ± 4.3941
	Mean payoff:
		0.1801 ± 0.0205
	Total number of recruited nodes:
		214 ± 0

Results at turn 215:
	Total payoff:
		38.7321 ± 4.4004
	Mean payoff:
		0.1801 ± 0.0205
	Total number of recruited nodes:
		215 ± 0

Results at turn 216:
	Total payoff:
		39.1607 ± 4.4875
	Mean payoff:
		0.1813 ± 0.0208
	Total number of recruited nodes:
		216 ± 0

Results at turn 217:
	Total payoff:
		39.3393 ± 4.4935
	Mean payoff:
		0.1813 ± 0.0207
	Total number of recruited nodes:
		217 ± 0

Results at turn 218:
	Total payoff:
		39.4821 ± 4.5287
	Mean payoff:
		0.1811 ± 0.0208
	Total number of recruited nodes:
		218 ± 0

Results at turn 219:
	Total payoff:
		39.7143 ± 4.5635
	Mean payoff:
		0.1813 ± 0.0208
	Total number of recruited nodes:
		219 ± 0

Results at turn 220:
	Total payoff:
		39.8929 ± 4.4708
	Mean payoff:
		0.1813 ± 0.0203
	Total number of recruited nodes:
		220 ± 0

Results at turn 221:
	Total payoff:
		40.0714 ± 4.406
	Mean payoff:
		0.1813 ± 0.0199
	Total number of recruited nodes:
		221 ± 0

Results at turn 222:
	Total payoff:
		40.2679 ± 4.3838
	Mean payoff:
		0.1814 ± 0.0197
	Total number of recruited nodes:
		222 ± 0

Results at turn 223:
	Total payoff:
		40.4643 ± 4.2724
	Mean payoff:
		0.1815 ± 0.0192
	Total number of recruited nodes:
		223 ± 0

Results at turn 224:
	Total payoff:
		40.5893 ± 4.2202
	Mean payoff:
		0.1812 ± 0.0188
	Total number of recruited nodes:
		224 ± 0

Results at turn 225:
	Total payoff:
		40.7321 ± 4.2103
	Mean payoff:
		0.181 ± 0.0187
	Total number of recruited nodes:
		225 ± 0

Results at turn 226:
	Total payoff:
		40.9286 ± 4.0714
	Mean payoff:
		0.1811 ± 0.018
	Total number of recruited nodes:
		226 ± 0

Results at turn 227:
	Total payoff:
		41.0893 ± 4.0194
	Mean payoff:
		0.181 ± 0.0177
	Total number of recruited nodes:
		227 ± 0

Results at turn 228:
	Total payoff:
		41.25 ± 3.9185
	Mean payoff:
		0.1809 ± 0.0172
	Total number of recruited nodes:
		228 ± 0

Results at turn 229:
	Total payoff:
		41.375 ± 3.9013
	Mean payoff:
		0.1807 ± 0.017
	Total number of recruited nodes:
		229 ± 0

Results at turn 230:
	Total payoff:
		41.5714 ± 3.8558
	Mean payoff:
		0.1807 ± 0.0168
	Total number of recruited nodes:
		230 ± 0

Results at turn 231:
	Total payoff:
		41.6607 ± 3.8247
	Mean payoff:
		0.1803 ± 0.0166
	Total number of recruited nodes:
		231 ± 0

Results at turn 232:
	Total payoff:
		41.8036 ± 3.734
	Mean payoff:
		0.1802 ± 0.0161
	Total number of recruited nodes:
		232 ± 0

Results at turn 233:
	Total payoff:
		42.0179 ± 3.5853
	Mean payoff:
		0.1803 ± 0.0154
	Total number of recruited nodes:
		233 ± 0

Results at turn 234:
	Total payoff:
		42.0893 ± 3.5073
	Mean payoff:
		0.1799 ± 0.015
	Total number of recruited nodes:
		234 ± 0

Results at turn 235:
	Total payoff:
		42.2321 ± 3.4902
	Mean payoff:
		0.1797 ± 0.0149
	Total number of recruited nodes:
		235 ± 0

Results at turn 236:
	Total payoff:
		42.375 ± 3.446
	Mean payoff:
		0.1796 ± 0.0146
	Total number of recruited nodes:
		236 ± 0

Results at turn 237:
	Total payoff:
		42.5714 ± 3.3783
	Mean payoff:
		0.1796 ± 0.0143
	Total number of recruited nodes:
		237 ± 0

Results at turn 238:
	Total payoff:
		42.6607 ± 3.2879
	Mean payoff:
		0.1792 ± 0.0138
	Total number of recruited nodes:
		238 ± 0

Results at turn 239:
	Total payoff:
		42.75 ± 3.2488
	Mean payoff:
		0.1789 ± 0.0136
	Total number of recruited nodes:
		239 ± 0

Results at turn 240:
	Total payoff:
		42.9286 ± 3.2071
	Mean payoff:
		0.1789 ± 0.0134
	Total number of recruited nodes:
		240 ± 0

Results at turn 241:
	Total payoff:
		43.0357 ± 3.1964
	Mean payoff:
		0.1786 ± 0.0133
	Total number of recruited nodes:
		241 ± 0

Results at turn 242:
	Total payoff:
		43.125 ± 3.128
	Mean payoff:
		0.1782 ± 0.0129
	Total number of recruited nodes:
		242 ± 0

Results at turn 243:
	Total payoff:
		43.2321 ± 3.0748
	Mean payoff:
		0.1779 ± 0.0127
	Total number of recruited nodes:
		243 ± 0

Results at turn 244:
	Total payoff:
		43.25 ± 3.0763
	Mean payoff:
		0.1773 ± 0.0126
	Total number of recruited nodes:
		244 ± 0

Results at turn 245:
	Total payoff:
		43.375 ± 3.0185
	Mean payoff:
		0.177 ± 0.0123
	Total number of recruited nodes:
		245 ± 0

Results at turn 246:
	Total payoff:
		43.5179 ± 2.9664
	Mean payoff:
		0.1769 ± 0.0121
	Total number of recruited nodes:
		246 ± 0

Results at turn 247:
	Total payoff:
		43.6071 ± 2.9706
	Mean payoff:
		0.1765 ± 0.012
	Total number of recruited nodes:
		247 ± 0

Results at turn 248:
	Total payoff:
		43.6786 ± 2.9487
	Mean payoff:
		0.1761 ± 0.0119
	Total number of recruited nodes:
		248 ± 0

Results at turn 249:
	Total payoff:
		43.7857 ± 2.9028
	Mean payoff:
		0.1758 ± 0.0117
	Total number of recruited nodes:
		249 ± 0

Results at turn 250:
	Total payoff:
		43.8214 ± 2.8738
	Mean payoff:
		0.1753 ± 0.0115
	Total number of recruited nodes:
		250 ± 0

Results at turn 251:
	Total payoff:
		43.9464 ± 2.8247
	Mean payoff:
		0.1751 ± 0.0113
	Total number of recruited nodes:
		251 ± 0

Results at turn 252:
	Total payoff:
		44.0893 ± 2.7718
	Mean payoff:
		0.175 ± 0.011
	Total number of recruited nodes:
		252 ± 0

Results at turn 253:
	Total payoff:
		44.1964 ± 2.7331
	Mean payoff:
		0.1747 ± 0.0108
	Total number of recruited nodes:
		253 ± 0

Results at turn 254:
	Total payoff:
		44.2857 ± 2.6813
	Mean payoff:
		0.1744 ± 0.0106
	Total number of recruited nodes:
		254 ± 0

Results at turn 255:
	Total payoff:
		44.3929 ± 2.7215
	Mean payoff:
		0.1741 ± 0.0107
	Total number of recruited nodes:
		255 ± 0

Results at turn 256:
	Total payoff:
		44.4286 ± 2.716
	Mean payoff:
		0.1735 ± 0.0106
	Total number of recruited nodes:
		256 ± 0

Results at turn 257:
	Total payoff:
		44.5714 ± 2.6551
	Mean payoff:
		0.1734 ± 0.0103
	Total number of recruited nodes:
		257 ± 0

Results at turn 258:
	Total payoff:
		44.625 ± 2.6667
	Mean payoff:
		0.173 ± 0.0103
	Total number of recruited nodes:
		258 ± 0

Results at turn 259:
	Total payoff:
		44.7679 ± 2.6423
	Mean payoff:
		0.1728 ± 0.0102
	Total number of recruited nodes:
		259 ± 0

Results at turn 260:
	Total payoff:
		44.8214 ± 2.6293
	Mean payoff:
		0.1724 ± 0.0101
	Total number of recruited nodes:
		260 ± 0

Results at turn 261:
	Total payoff:
		44.9464 ± 2.6036
	Mean payoff:
		0.1722 ± 0.01
	Total number of recruited nodes:
		261 ± 0

Results at turn 262:
	Total payoff:
		45.0714 ± 2.5645
	Mean payoff:
		0.172 ± 0.0098
	Total number of recruited nodes:
		262 ± 0

Results at turn 263:
	Total payoff:
		45.1429 ± 2.5756
	Mean payoff:
		0.1716 ± 0.0098
	Total number of recruited nodes:
		263 ± 0

Results at turn 264:
	Total payoff:
		45.1786 ± 2.5162
	Mean payoff:
		0.1711 ± 0.0095
	Total number of recruited nodes:
		264 ± 0

Results at turn 265:
	Total payoff:
		45.25 ± 2.4365
	Mean payoff:
		0.1708 ± 0.0092
	Total number of recruited nodes:
		265 ± 0

Results at turn 266:
	Total payoff:
		45.4107 ± 2.4104
	Mean payoff:
		0.1707 ± 0.0091
	Total number of recruited nodes:
		266 ± 0

Results at turn 267:
	Total payoff:
		45.4821 ± 2.3432
	Mean payoff:
		0.1703 ± 0.0088
	Total number of recruited nodes:
		267 ± 0

Results at turn 268:
	Total payoff:
		45.5179 ± 2.3198
	Mean payoff:
		0.1698 ± 0.0087
	Total number of recruited nodes:
		268 ± 0

Results at turn 269:
	Total payoff:
		45.5714 ± 2.303
	Mean payoff:
		0.1694 ± 0.0086
	Total number of recruited nodes:
		269 ± 0

Results at turn 270:
	Total payoff:
		45.7321 ± 2.236
	Mean payoff:
		0.1694 ± 0.0083
	Total number of recruited nodes:
		270 ± 0

Results at turn 271:
	Total payoff:
		45.8036 ± 2.2109
	Mean payoff:
		0.169 ± 0.0082
	Total number of recruited nodes:
		271 ± 0

Results at turn 272:
	Total payoff:
		45.9107 ± 2.2505
	Mean payoff:
		0.1688 ± 0.0083
	Total number of recruited nodes:
		272 ± 0

Results at turn 273:
	Total payoff:
		45.9821 ± 2.2522
	Mean payoff:
		0.1684 ± 0.0082
	Total number of recruited nodes:
		273 ± 0

Results at turn 274:
	Total payoff:
		46.125 ± 2.1914
	Mean payoff:
		0.1683 ± 0.008
	Total number of recruited nodes:
		274 ± 0

Results at turn 275:
	Total payoff:
		46.2143 ± 2.1635
	Mean payoff:
		0.1681 ± 0.0079
	Total number of recruited nodes:
		275 ± 0

Results at turn 276:
	Total payoff:
		46.2857 ± 2.1381
	Mean payoff:
		0.1677 ± 0.0077
	Total number of recruited nodes:
		276 ± 0

Results at turn 277:
	Total payoff:
		46.3214 ± 2.0724
	Mean payoff:
		0.1672 ± 0.0075
	Total number of recruited nodes:
		277 ± 0

Results at turn 278:
	Total payoff:
		46.3393 ± 2.0739
	Mean payoff:
		0.1667 ± 0.0075
	Total number of recruited nodes:
		278 ± 0

Results at turn 279:
	Total payoff:
		46.4286 ± 2.1222
	Mean payoff:
		0.1664 ± 0.0076
	Total number of recruited nodes:
		279 ± 0

Results at turn 280:
	Total payoff:
		46.5 ± 2.0091
	Mean payoff:
		0.1661 ± 0.0072
	Total number of recruited nodes:
		280 ± 0

Results at turn 281:
	Total payoff:
		46.6429 ± 2.0129
	Mean payoff:
		0.166 ± 0.0072
	Total number of recruited nodes:
		281 ± 0

Results at turn 282:
	Total payoff:
		46.6786 ± 2.0281
	Mean payoff:
		0.1655 ± 0.0072
	Total number of recruited nodes:
		282 ± 0

Results at turn 283:
	Total payoff:
		46.7321 ± 2.0404
	Mean payoff:
		0.1651 ± 0.0072
	Total number of recruited nodes:
		283 ± 0

Results at turn 284:
	Total payoff:
		46.8393 ± 2.0518
	Mean payoff:
		0.1649 ± 0.0072
	Total number of recruited nodes:
		284 ± 0

Results at turn 285:
	Total payoff:
		46.9643 ± 1.9256
	Mean payoff:
		0.1648 ± 0.0068
	Total number of recruited nodes:
		285 ± 0

Results at turn 286:
	Total payoff:
		47.0179 ± 1.9211
	Mean payoff:
		0.1644 ± 0.0067
	Total number of recruited nodes:
		286 ± 0

Results at turn 287:
	Total payoff:
		47.1071 ± 1.9039
	Mean payoff:
		0.1641 ± 0.0066
	Total number of recruited nodes:
		287 ± 0

Results at turn 288:
	Total payoff:
		47.1607 ± 1.8662
	Mean payoff:
		0.1638 ± 0.0065
	Total number of recruited nodes:
		288 ± 0

Results at turn 289:
	Total payoff:
		47.25 ± 1.8315
	Mean payoff:
		0.1635 ± 0.0063
	Total number of recruited nodes:
		289 ± 0

Results at turn 290:
	Total payoff:
		47.2857 ± 1.8162
	Mean payoff:
		0.1631 ± 0.0063
	Total number of recruited nodes:
		290 ± 0

Results at turn 291:
	Total payoff:
		47.375 ± 1.7844
	Mean payoff:
		0.1628 ± 0.0061
	Total number of recruited nodes:
		291 ± 0

Results at turn 292:
	Total payoff:
		47.4107 ± 1.7032
	Mean payoff:
		0.1624 ± 0.0058
	Total number of recruited nodes:
		292 ± 0

Results at turn 293:
	Total payoff:
		47.4643 ± 1.662
	Mean payoff:
		0.162 ± 0.0057
	Total number of recruited nodes:
		293 ± 0

Results at turn 294:
	Total payoff:
		47.4821 ± 1.6513
	Mean payoff:
		0.1615 ± 0.0056
	Total number of recruited nodes:
		294 ± 0

Results at turn 295:
	Total payoff:
		47.5 ± 1.6514
	Mean payoff:
		0.161 ± 0.0056
	Total number of recruited nodes:
		295 ± 0

Results at turn 296:
	Total payoff:
		47.5357 ± 1.6729
	Mean payoff:
		0.1606 ± 0.0057
	Total number of recruited nodes:
		296 ± 0

Results at turn 297:
	Total payoff:
		47.5714 ± 1.6826
	Mean payoff:
		0.1602 ± 0.0057
	Total number of recruited nodes:
		297 ± 0

Results at turn 298:
	Total payoff:
		47.6071 ± 1.6698
	Mean payoff:
		0.1598 ± 0.0056
	Total number of recruited nodes:
		298 ± 0

Results at turn 299:
	Total payoff:
		47.6964 ± 1.6615
	Mean payoff:
		0.1595 ± 0.0056
	Total number of recruited nodes:
		299 ± 0

Results at turn 300:
	Total payoff:
		47.7321 ± 1.6678
	Mean payoff:
		0.1591 ± 0.0056
	Total number of recruited nodes:
		300 ± 0


