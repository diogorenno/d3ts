Valid tests: 8 out of 8

<Settings>
  Public:
    clone: function
    cold_pulls: 5
    initialize: function
    nattempts: 1400
    nmodels: 1
    nseeds: 1
    ofname: citeseer_t8_a1400_m1_s1_variational.txt
    problem: environment
    rng_seed: 112358
    seeds: NA
    silent_sampler: TRUE
    simulation_id: NA
    validate: function


Results at turn 46:
	Total payoff:
		24.375 ± 10.3777
	Mean payoff:
		0.6476 ± 0.1774
	Total number of recruited nodes:
		38.375 ± 16.344

Results at turn 93:
	Total payoff:
		43.25 ± 22.5182
	Mean payoff:
		0.6184 ± 0.1869
	Total number of recruited nodes:
		73.625 ± 37.8868

Results at turn 140:
	Total payoff:
		63.75 ± 35.2491
	Mean payoff:
		0.6201 ± 0.1804
	Total number of recruited nodes:
		108.875 ± 59.5853

Results at turn 186:
	Total payoff:
		90.25 ± 51.4108
	Mean payoff:
		0.6554 ± 0.167
	Total number of recruited nodes:
		143.375 ± 80.8525

Results at turn 233:
	Total payoff:
		116.25 ± 67.1773
	Mean payoff:
		0.6725 ± 0.1586
	Total number of recruited nodes:
		178.625 ± 102.5934

Results at turn 280:
	Total payoff:
		137.125 ± 80.0061
	Mean payoff:
		0.6657 ± 0.157
	Total number of recruited nodes:
		213.875 ± 124.3399

Results at turn 326:
	Total payoff:
		155.125 ± 91.1191
	Mean payoff:
		0.6535 ± 0.1577
	Total number of recruited nodes:
		248.375 ± 145.6267

Results at turn 373:
	Total payoff:
		172 ± 101.951
	Mean payoff:
		0.64 ± 0.1614
	Total number of recruited nodes:
		283.625 ± 167.378

Results at turn 420:
	Total payoff:
		192.75 ± 115.4788
	Mean payoff:
		0.6388 ± 0.1634
	Total number of recruited nodes:
		318.875 ± 189.1307

Results at turn 466:
	Total payoff:
		213.5 ± 127.8291
	Mean payoff:
		0.6388 ± 0.1606
	Total number of recruited nodes:
		353.375 ± 210.4213

Results at turn 513:
	Total payoff:
		232.125 ± 138.5295
	Mean payoff:
		0.6338 ± 0.1583
	Total number of recruited nodes:
		388.625 ± 232.1754

Results at turn 560:
	Total payoff:
		254.25 ± 152.379
	Mean payoff:
		0.6358 ± 0.1579
	Total number of recruited nodes:
		423.875 ± 253.9299

Results at turn 606:
	Total payoff:
		273.5 ± 164.1654
	Mean payoff:
		0.6335 ± 0.1577
	Total number of recruited nodes:
		458.375 ± 275.222

Results at turn 653:
	Total payoff:
		293.375 ± 176.1135
	Mean payoff:
		0.6319 ± 0.1569
	Total number of recruited nodes:
		493.625 ± 296.9771

Results at turn 700:
	Total payoff:
		315.25 ± 189.3083
	Mean payoff:
		0.6333 ± 0.1555
	Total number of recruited nodes:
		528.875 ± 318.7325

Results at turn 746:
	Total payoff:
		335.375 ± 201.3362
	Mean payoff:
		0.6328 ± 0.1545
	Total number of recruited nodes:
		563.375 ± 340.0252

Results at turn 793:
	Total payoff:
		359 ± 215.4961
	Mean payoff:
		0.6362 ± 0.1525
	Total number of recruited nodes:
		598.625 ± 361.7809

Results at turn 840:
	Total payoff:
		378.125 ± 227.205
	Mean payoff:
		0.6338 ± 0.1528
	Total number of recruited nodes:
		633.875 ± 383.5367

Results at turn 886:
	Total payoff:
		396 ± 238.3119
	Mean payoff:
		0.6308 ± 0.1538
	Total number of recruited nodes:
		668.375 ± 404.8297

Results at turn 933:
	Total payoff:
		415.375 ± 250.2244
	Mean payoff:
		0.6293 ± 0.1541
	Total number of recruited nodes:
		703.625 ± 426.5858

Results at turn 980:
	Total payoff:
		437.25 ± 263.6658
	Mean payoff:
		0.6304 ± 0.1536
	Total number of recruited nodes:
		738.875 ± 448.3418

Results at turn 1026:
	Total payoff:
		455.375 ± 274.8152
	Mean payoff:
		0.6282 ± 0.1542
	Total number of recruited nodes:
		773.375 ± 469.6351

Results at turn 1073:
	Total payoff:
		476.5 ± 287.81
	Mean payoff:
		0.6286 ± 0.154
	Total number of recruited nodes:
		808.625 ± 491.3913

Results at turn 1120:
	Total payoff:
		494 ± 298.7316
	Mean payoff:
		0.6257 ± 0.155
	Total number of recruited nodes:
		843.875 ± 513.1476

Results at turn 1166:
	Total payoff:
		511.125 ± 309.1904
	Mean payoff:
		0.6231 ± 0.1556
	Total number of recruited nodes:
		878.375 ± 534.4409

Results at turn 1213:
	Total payoff:
		523.375 ± 316.7396
	Mean payoff:
		0.6164 ± 0.1577
	Total number of recruited nodes:
		913.625 ± 556.1973

Results at turn 1260:
	Total payoff:
		535.5 ± 324.4754
	Mean payoff:
		0.61 ± 0.1601
	Total number of recruited nodes:
		948.875 ± 577.9536

Results at turn 1306:
	Total payoff:
		553.25 ± 335.4403
	Mean payoff:
		0.6087 ± 0.1605
	Total number of recruited nodes:
		983.375 ± 599.2471

Results at turn 1353:
	Total payoff:
		566.5 ± 343.9643
	Mean payoff:
		0.6039 ± 0.1625
	Total number of recruited nodes:
		1018.625 ± 621.0036

Results at turn 1400:
	Total payoff:
		579.125 ± 351.5579
	Mean payoff:
		0.5992 ± 0.1638
	Total number of recruited nodes:
		1053.125 ± 642.2971


