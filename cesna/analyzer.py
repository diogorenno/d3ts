#-*- coding: utf-8 -*-

##
 # | renno@ubuntu ⬥ ~/Documents/ufmg/mestrado/project/renno/cesna |
 # $ python analyzer.py datasets/001/cesna_c2a1
 ##

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

import gzip
import numpy
import os
import random
from collections import defaultdict
from pprint import pprint


if __name__ == "__main__":

  ntests = int(sys.argv[2])
  dataset = sys.argv[1]
  name = dataset.split("/")[-1]
  folder = dataset.split("/")[-2]
  
  run = 1
  while True:
  
    ungraph_file = "{}.ungraph.run{:02d}.txt.gz".format(dataset, run)
    attribs_file = "{}.attribs.run{:02d}.txt.gz".format(dataset, run)
    partitions_file = "{}.partitions.run{:02d}.txt".format(dataset, run)
    
    if not os.path.isfile(ungraph_file):
      break
    
    print "#" * 130
    print "\n# {} - {}".format(dataset, run)

    # Loads the vertex partitions.  
    vertex2part = dict()
    part2vertex = dict()
    with open(partitions_file) as handler:
      for i, line in enumerate(handler):
        part2vertex[i+1] = map(int, line.split())
        for vertex in part2vertex[i+1]:
          vertex2part[vertex] = i + 1
    
    # Loads the vertex attributes.
    attribs = dict()
    with gzip.open(attribs_file) as handler:
      for i, line in enumerate(handler):
        attribs[i+1] = map(int, line.split())
          
    # Loads the graph.
    nedges = 0
    graph = defaultdict(list)
    with gzip.open(ungraph_file) as handler:
      for i, line in enumerate(handler):
        i, j = map(int, line.split())
        graph[i].append(j)
        graph[j].append(i)
        nedges += 1

    # Prints basic statistics.
    stats_string = "\n\tV: {} nodes\n\tE: {} edges\n\tK: {} attributes"
    print stats_string.format(len(vertex2part.keys()), nedges, len(attribs.keys()))

    # Estimates the connectivity among the vertex partitions.
    matrix = dict()
    for pindex, vertices in part2vertex.items():
      # For each vertex in the partition, counts how many edges it has to nodes in each partition.
      counts = {vertex: {partition: 0 for partition in part2vertex.keys()} for vertex in vertices}
      for vi in vertices:
        for vj in graph[vi]:
          counts[vi][vertex2part[vj]] += 1
      # Calculates the mean number of edges from a vertex in the partition to each other partition.
      # Each mean is divided by the size of the corresponding partition so as to estimate how connected
      # both partitions are (the closer to 1, the more connected they are).
      row = dict()
      for pj in part2vertex.keys():
        values = map(lambda x: x[pj], counts.values())
        row[pj] = round(numpy.mean(values) / float(len(part2vertex[pj])), 3)
      matrix[pindex] = row
    # Prints the matrix.
    print "\n\tP(A[u,v]=1), u E partition i, v E partition j:\n"
    print reduce(
        lambda acc1, (key1, value1): acc1 + "\t\ti={}: {}\n".format(
            key1, reduce(lambda acc2, (key2, value2): acc2 + " j={}: {:.3f} ".format(key2, value2), value1.items(), "")),
        matrix.items(), "")[:-1]
    
    # Calculates for each partition the proportion of nodes in the target set.
    targets = attribs[1]
    target_proportions = {partition: None for partition in part2vertex.keys()}
    for pindex, vertices in part2vertex.items():
      target_proportions[pindex] = len(filter(lambda v: v in targets, vertices)) / float(len(vertices))
    # Prints the proportions.
    print "\n\tProportion of nodes from each partition which are targets:\n"
    print reduce(lambda acc, (key, value): acc + "\t\t{}: {:.3f}\n".format(key, value), target_proportions.items(), "")

    # Selects seeds.
    # For each partition, one seed is sampled from the target nodes belonging to that partition.
    # ntests vectors are listed, each with one seed for each partition.
    seeds = [map(
        lambda (pindex, targets): random.sample(targets, 1)[0] if len(targets) else part2vertex[pindex][0],
        map(lambda (key, vertices): (key, filter(lambda v: v in targets, vertices)), part2vertex.items()))
    for i in xrange(ntests)]
  
    # Prints problem configuration for "problems.R".
    print "-" * 80; print
    print "{}{} = Problem$new(\"{}\",".format(4 * " ", name, name)
    print "{}\"../../cesna/datasets/{}/{}\",".format(8 * " ", folder, ungraph_file.split("/")[-1])
    print "{}\"../../cesna/datasets/{}/{}\",".format(8 * " ", folder, attribs_file.split("/")[-1])
    print "{}accept_trait = {}, target_size = {}, nattribs = {})".format(8 * " ", 0, len(targets), len(attribs.keys()))
    print; print "-" * 80

    # Prints settings configuration for "problems.R".
    npartitions = len(part2vertex.keys())
    seeds_string = reduce(
        lambda full, vector: full + "c({})".format(reduce(lambda acc, x: acc + "\"{}\", ".format(x), vector, "")[:-2]) + ", ",
        seeds,
        "list(")[:-2] + ")"
        
    print
    # Local models settings.
    print "# {}".format(name)
    print "SETTINGS${} <- Settings$new(problem=PROBLEMS${}, rng_seed=112358, use_UCB1=TRUE,".format(name, name),
    print "nmodels={}, nseeds=1, seeds=NA, cold_pulls=5,".format(npartitions, seeds_string),
    print "nattempts={}, nclusters={}, model_weights=NA)".format(len(targets), int(npartitions / 2))
    print "SETTINGS${}$seeds = {}".format(name, seeds_string)
    print
    # No UCB1 settings.
    print "SETTINGS${}_noUCB1 <- SETTINGS${}$clone()".format(name, name)
    print "SETTINGS${}_noUCB1$use_UCB1 = FALSE".format(name)
    print
    # Single model settings.
    print "SETTINGS${}_single <- SETTINGS${}$clone()".format(name, name)
    print "SETTINGS${}_single$use_UCB1 = FALSE".format(name)
    print "SETTINGS${}_single$nclusters = NA".format(name)
    print "SETTINGS${}_single$nmodels = 1".format(name)
    print "SETTINGS${}_single$nseeds = SETTINGS${}$nmodels".format(name, name)
    print "SETTINGS${}_single$model_weights = c(1, 0, 0)".format(name)
    print

    run += 1

